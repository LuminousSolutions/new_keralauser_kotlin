package com.mongokerala.taxi.newuser.ui.registration

import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.*
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class RegistrationViewModel (
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    val launchRegistrationtoLogin: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchEmailExisting: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()

    val mainIn: MutableLiveData<Boolean> = MutableLiveData()
    val name: MutableLiveData<String> = MutableLiveData()
    val emailField: MutableLiveData<String> = MutableLiveData()
    val passwordField: MutableLiveData<String> = MutableLiveData()
    val mobileNumber: MutableLiveData<String> = MutableLiveData()
    private val validationsList: MutableLiveData<List<Validation>> = MutableLiveData()
    override fun onCreate() {

    }
    fun onSignup() {

        launchRegistrationtoLogin.postValue(Event(emptyMap()))
    }

    fun getMobilenumber():String{
        return userRepository.getMobileNUmber().toString()
    }

    fun signUpValidation() {
        val name: String? = name.value
        val email = emailField.value
        val phonenumber = mobileNumber.value
        val password = passwordField.value
        val validations = Validator.signUpValidation(name, email, phonenumber, password)
        validationsList.postValue(validations)
    }

    fun doRegistrationRequest(email: String, name: String, password: String, phonenumber: String) {

        if (email.isNotEmpty() && name.isNotEmpty() && password.isNotEmpty() && phonenumber.isNotEmpty() )
         {
            mainIn.postValue(true)

            if (checkInternetConnectionWithMessage()) {
            compositeDisposable.addAll(
                userRepository.doUserRegistration(email, name, password, phonenumber)
                    .subscribeOn(schedulerProvider.io())
                    .subscribe(
                        {
                            when (it.infoId) {
                                "500" -> {
                                    launchRegistrationtoLogin.postValue(Event(emptyMap()))
                                    mainIn.postValue(false)
                                }
                                "700" -> {
                                    launchEmailExisting.postValue(Event(emptyMap()))
                                    mainIn.postValue(false)
                                }
                                else -> {
                                    mainIn.postValue(false)
                                }
                            }

                        },
                        {
                            handleNetworkError(it)
                            mainIn.postValue(false)
                        }
                    )
            )
          }
       }
}

    }


