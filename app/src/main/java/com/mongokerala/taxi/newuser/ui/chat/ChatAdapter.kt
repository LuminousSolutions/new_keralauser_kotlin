package com.mongokerala.taxi.newuser.ui.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.data.remote.response.ChatTypeDTO
import com.mongokerala.taxi.newuser.ui.triphistory.RecyclerViewItemClickListener

class ChatAdapter(
    private val context: Context,
    chatArrayList: List<ChatTypeDTO>,
    chatActivity: ChatActivity
) :
    RecyclerView.Adapter<ChatAdapter.chatActivityViewHolder?>() {
    private val recyclerViewItemClickListener: RecyclerViewItemClickListener
    private var chat_Array_List: List<ChatTypeDTO> = emptyList()

    /**
     * @param parent   : parent ViewPgroup
     * @param viewType : viewType
     * @return ViewHolder
     *
     *
     * Inflate the Views
     * Create the each views and Hold for Reuse
     */
    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): chatActivityViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.text_chat, parent, false)
        return chatActivityViewHolder(view)
    }

    /**
     * @param chatActivityViewHolder   :view Holder
     * @param position : position of each Row
     * set the values to the views
     */
    override fun onBindViewHolder(
        @NonNull chatActivityViewHolder: chatActivityViewHolder,
        position: Int
    ) {
        chatActivityViewHolder.msg.text = chat_Array_List[position].MSG
        chatActivityViewHolder.time.text = chat_Array_List[position].DATE
        chatActivityViewHolder.name.text = chat_Array_List[position].NAME
    }



    override fun getItemCount(): Int {
        return chat_Array_List.size
    }

    /**
     * Create The view First Time and hold for reuse
     * View Holder for Create and Hold the view for ReUse the views instead of create again
     * Initialize the views
     */
    inner class chatActivityViewHolder internal constructor(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var msg: TextView
        var time: TextView
        var name: TextView
        override fun onClick(view: View) {
            recyclerViewItemClickListener.onItemClickListener(this.getAdapterPosition(), view)
        }

        init {

            msg = itemView.findViewById(R.id.messageTextView)
            time = itemView.findViewById(R.id.date_time_txt)
            name = itemView.findViewById(R.id.txt_name)

            itemView.setOnClickListener(this)
        }
    }

    /**
     * Initialize the values
     * @param context               : context reference
     * @param chatArrayList       : data
     * @param chatActivity
     */
    init {
        chat_Array_List = chatArrayList
        recyclerViewItemClickListener = chatActivity
    }
}


