package com.mongokerala.taxi.newuser.ui.profile_edit

import android.graphics.Bitmap
import android.util.Base64
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.remote.request.ImageInfo
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import java.io.ByteArrayOutputStream

class ProfileEditViewModel (
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    val emailField: MutableLiveData<String> = MutableLiveData()
    val nameField: MutableLiveData<String> = MutableLiveData()
    val mob_numberField: MutableLiveData<String> = MutableLiveData()
    val profileUpdated: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val updateinfo: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val notupdated: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val profileIn: MutableLiveData<Boolean> = MutableLiveData()
    val updateMobileNumber: MutableLiveData<String> = MutableLiveData()
    val proifileImageUpload: MutableLiveData<String?> = MutableLiveData()
    val isprofileImage: MutableLiveData<String?> = MutableLiveData()



    override fun onCreate() {
    }

    fun onProfileEdit(name: String,email_id: String,phoneNumber:String,img_profile: ImageView){

        img_profile.buildDrawingCache()
        val bitmap: Bitmap = img_profile.getDrawingCache()
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
        val byteFormat = stream.toByteArray()
        // get the base 64 string
        // get the base 64 string
        val imgString =
            Base64.encodeToString(byteFormat, Base64.NO_WRAP)
        profileIn.postValue(true)
            compositeDisposable.addAll(
                userRepository.doProfileEdit(
                    userRepository.getCurrentUserID()!!, name, email_id, phoneNumber,
                    "KERALACABS", "KERALACABS"
                )
                    .subscribeOn(schedulerProvider.io())
                    .subscribe(
                        {
                            if (it.infoId.equals("850")) {
                                profileUpdated.postValue(Event(emptyMap()))
                                //profileIn.postValue(false)
                                userRepository.setImageUrl(ImageInfo(imgString))
                                onUserDetail()
                            } else {
                                notupdated.postValue(Event(emptyMap()))
                                profileIn.postValue(false)
                            }

                            onUserDetail()

                        },
                        {
                            profileIn.postValue(false)
                            handleNetworkError(it)
                        }
                    )
            )

    }

    fun onUserDetail() {
        profileIn.postValue(true)
        compositeDisposable.addAll(
            userRepository.doUserdetailRequest(userRepository.getCurrentUserID()!!)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {

                        try {
                            if(it.data.phoneNumber != null) {
                                userRepository.saveMobileNumber(it.data.phoneNumber)
                            }

                            if(it.data.domain != null) {
                                userRepository.saveDomain(it.data.domain)
                            }
                            if(it.data.website != null) {
                                userRepository.saveWebsite(it.data.website)}
                            if(it.data.email != null) {
                                userRepository.saveEmail(it.data.email)
                            }
                            if(it.data.firstName != null) {
                                userRepository.saveName(it.data.firstName)
                            }
                            if(it.data.email != null) {
                                emailField.postValue(it.data.email)
                            }
                            if(it.data.firstName != null) {
                                nameField.postValue(it.data.firstName)
                            }
                            if(it.data.phoneNumber != null) {
                                mob_numberField.postValue(it.data.phoneNumber)
                            }
                            if (it.data != null){
                                updateinfo.postValue(Event(emptyMap()))
                            }
                            profileIn.postValue(false)
                        }catch (e: Exception){
                            profileIn.postValue(false)

                        }

                    },
                    {
                        handleNetworkError(it)
                        profileIn.postValue(false)
                    }
                )
        )

    }

    fun onUserDetaiImage(){

        compositeDisposable.addAll(
                userRepository.doCheckUserImage(userRepository.getCurrentUserID()!!)
                        .subscribeOn(schedulerProvider.io())
                        .subscribe(
                                {
                                    if (it.data != null) {
                                        isprofileImage.postValue(it.data.isImage)
                                    }

                                },
                                {
                                    handleNetworkError(it)
                                }
                        )
        )

    }

    fun getFacebookLoginStatus(): String? {
        return userRepository.getFbLoginstatus()
    }
    fun doGetFbImageurl(): String?{
        return userRepository.getFbImageUrl()
    }
    fun getGmailLoginStatus():String?{
        return userRepository.getGmailLoginStatus()
    }
    fun getgMailLoginImageUrl(): String? {
        return userRepository.getgMailLoginImageUrl()
    }

    fun getimageresponse(): ImageInfo? {
        return userRepository.getImageUrl()
    }
    fun getuserid(): String? {
        return userRepository.getCurrentUserID()
    }
    fun getusername(): String? {
        return userRepository.getName()
    }
    fun setImage(image_info: ImageInfo?) {
        userRepository.setImageUrl(image_info)
    }
    fun getfacebookLoginStatus():String?{
        return userRepository.getFbLoginstatus()
    }
    fun getCurrentUserID(): String {
        return userRepository.getCurrentUserID().toString()
    }


    fun onImageprofile() {
        profileIn.postValue(true)
        compositeDisposable.addAll(
                userRepository.doUserdetailRequest(userRepository.getCurrentUserID()!!)
                        .subscribeOn(schedulerProvider.io())
                        .subscribe(
                                {

                                    try {
                                        if(it.data.phoneNumber != null) {
                                            userRepository.saveMobileNumber(it.data.phoneNumber)
                                        }
                                        /*if (it.data.imageInfo != null) {
                                            userRepository.setImageUrl(ImageInfo(it.data.imageInfo.imageUrl))
                                        }*/

                                        if(it.data.domain != null) {
                                            userRepository.saveDomain(it.data.domain)
                                        }
                                        if(it.data.website != null) {
                                            userRepository.saveWebsite(it.data.website)}
                                        if(it.data.email != null) {
                                            userRepository.saveEmail(it.data.email)
                                        }
                                        if(it.data.firstName != null) {
                                            userRepository.saveName(it.data.firstName)
                                        }
                                        if(it.data.email != null) {
                                            emailField.postValue(it.data.email)
                                        }
                                        if(it.data.firstName != null) {
                                            nameField.postValue(it.data.firstName)
                                        }
                                        if(it.data.phoneNumber != null) {
                                            mob_numberField.postValue(it.data.phoneNumber)
                                        }
                                        profileIn.postValue(false)
                                    }catch (e: Exception){
                                        profileIn.postValue(false)

                                    }

                                },
                                {
                                    handleNetworkError(it)
                                    profileIn.postValue(false)
                                }
                        )
        )

    }

}
