package com.mongokerala.taxi.newuser.utils.common

interface LoadMoreListener {

    fun onLoadMore()
}