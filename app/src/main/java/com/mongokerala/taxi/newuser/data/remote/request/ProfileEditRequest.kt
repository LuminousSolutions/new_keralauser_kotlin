package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.utils.network.ERole


class ProfileEditRequest(
    @field:SerializedName(Keys.id) @field:Expose var id: String?,
    @field:SerializedName(Keys.firstName) @field:Expose var firstName: String?,
    @field:SerializedName(Keys.email) @field:Expose var email: String?,
    @field:SerializedName(Keys.phoneNumber) @field:Expose var phoneNumber: String?,
    @field:SerializedName(Keys.domain) @field:Expose var domain: String?,
    @field:SerializedName(Keys.website) @field:Expose var website: String?
) {

    @SerializedName("address")
    @Expose
    private var address: String? = null

    @SerializedName("carType")
    @Expose
    private var carType: String? = null

    @SerializedName("category")
    @Expose
    private var category: String? = null

    @SerializedName("code")
    @Expose
    private var code: String? = null

    @SerializedName("deviceId")
    @Expose
    private var deviceId: String? = null

    @SerializedName("lang")
    @Expose
    private var lang: String? = null

    @SerializedName("lastName")
    @Expose
    private var lastName: String? = null

    @SerializedName("latitude")
    @Expose
    private var latitude: String? = null


    @SerializedName("loginStatus")
    @Expose
    private var loginStatus: String? = null

    @SerializedName("longitude")
    @Expose
    private var longitude: String? = null

    @SerializedName("password")
    @Expose
    private var password: String? = null

    @SerializedName("token")
    @Expose
    private var token: String? = null

    @SerializedName("phoneVerified")
    @Expose
    private var phoneVerified: String? = null

    @SerializedName("price")
    @Expose
    private var price: Int? = null

    @SerializedName("restKey")
    @Expose
    private var restKey: String? = null

    @SerializedName("role")
    @Expose
    private var role: String? = null

    @SerializedName("status")
    @Expose
    private var status: String? = null

    init {
        deviceId = "string"
        password = "string"
        address = ""
        carType="string"
        category="string"
        loginStatus="string"
        lang="string"
        code = ""
        latitude = "0"
        longitude = "0"
        lastName = "driver"
        restKey = ""
        role = ERole.ROLE_USER.toString()
        price = 0
        status = "string"
        phoneVerified = "string"
        token = "string"
    }
}