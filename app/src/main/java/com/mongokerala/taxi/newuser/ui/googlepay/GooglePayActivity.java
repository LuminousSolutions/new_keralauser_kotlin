package com.mongokerala.taxi.newuser.ui.googlepay;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mongokerala.taxi.newuser.R;
import com.mongokerala.taxi.newuser.ui.setting.SettingActivity;

public class GooglePayActivity extends AppCompatActivity {

    EditText amount, transaction_note;
    private static final int TEZ_REQUEST_CODE = 123;
    private static final String GOOGLE_TEZ_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user";
    private boolean isAppInstalled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_pay);
        amount = (EditText) findViewById(R.id.amount);
        transaction_note = (EditText) findViewById(R.id.transaction_note);
        isAppInstalled = checkGooglePayInstalled("com.google.android.apps.nbu.paisa.user");

    }

    public void onGooglePayClick(View view) {
        if(!amount.getText().toString().isEmpty() && !transaction_note.getText().toString().isEmpty()) {
            try {
                if (isAppInstalled) {
                    Uri uri =
                            new Uri.Builder()
                                    .scheme("upi")
                                    .authority("pay")
                                    .appendQueryParameter("pa", "2008splender@oksbi")
                                    .appendQueryParameter("pn", "MohammedLatief")
//                                    .appendQueryParameter("mc", "1234")
//                                    .appendQueryParameter("tr", "123456789")
                                    .appendQueryParameter("tn", transaction_note.getText().toString())
                                    .appendQueryParameter("am", amount.getText().toString())
                                    .appendQueryParameter("cu", "INR")
//                                    .appendQueryParameter("url", "https://merchant.website")
                                    .build();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(uri);
                    intent.setPackage(GOOGLE_TEZ_PACKAGE_NAME);
                    startActivityForResult(intent, TEZ_REQUEST_CODE);
            }
            }
            catch (Exception e){
                Log.d("onGooglePayClick", "onGooglePayClick: "+e.toString());
                Toast.makeText(this,"google pay plz installed ",Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, "Enter the price and transaction note", Toast.LENGTH_SHORT).show();
        }

    }

    private boolean checkGooglePayInstalled(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    public void onBackButtonClick(View view) {
        Intent i = new Intent(getApplicationContext(), SettingActivity.class);
        startActivity(i);
        finish();
    }

}