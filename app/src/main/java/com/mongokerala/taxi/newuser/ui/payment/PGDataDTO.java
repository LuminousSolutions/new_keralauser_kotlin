package com.mongokerala.taxi.newuser.ui.payment;

import java.math.BigDecimal;
import java.util.TreeMap;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public class PGDataDTO {
    private String txnId;
    private PaymentStatus payStatus;
    private PaymentGateway paymentGateway;
    private String currency;
    private BigDecimal  amount;
    private String paytmChecksum;
    private TreeMap<String, String> paytmParams;

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public PaymentStatus getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(PaymentStatus payStatus) {
        this.payStatus = payStatus;
    }

    public PaymentGateway getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(PaymentGateway paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaytmChecksum() {
        return paytmChecksum;
    }

    public void setPaytmChecksum(String paytmChecksum) {
        this.paytmChecksum = paytmChecksum;
    }

    public TreeMap<String, String> getPaytmParams() {
        return paytmParams;
    }

    public void setPaytmParams(TreeMap<String, String> paytmParams) {
        this.paytmParams = paytmParams;
    }
}
