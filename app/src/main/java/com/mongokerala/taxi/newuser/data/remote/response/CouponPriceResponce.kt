package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class CouponPriceResponce(

        @SerializedName("statusCode") val statusCode : Int,
        @SerializedName("status") val status : Boolean,
        @SerializedName("message") val message : String?,
        @SerializedName("data") val data : String?,
        @SerializedName("jwt") val jwt : String?,
        @SerializedName("infoId") val infoId : Int?
)
