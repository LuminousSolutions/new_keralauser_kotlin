package com.mongokerala.taxi.newuser.ui.feedback

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.display.Toaster
import kotlinx.android.synthetic.main.activity_feedback.*

class FeedbackActivity : BaseActivity<FeedbackViewModel>() {

    companion object {
        const val TAG = "FeedbackActivity"
    }

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun provideLayoutId(): Int = R.layout.activity_feedback

    override fun setupView(savedInstanceState: Bundle?) {
    }

    override fun setupObservers() {
        super.setupObservers()

        viewModel.feedIn.observe(this, Observer {
            pb_loading_feedback.visibility = if (it) View.VISIBLE else View.GONE
        })

        viewModel.feedbacksuccess.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                finish()
                Toaster.show(applicationContext,getString(R.string.Thank_you_for_sharing_your_feedback))
            }
        })

        btn_submit.setOnClickListener { viewModel.doFeedback(editText.text.toString()) }

        btn_later.setOnClickListener { finish() }

    }

    fun onFeedbackBackClick(view: View) {
        finish()
    }
}
