package com.mongokerala.taxi.newuser.ui.splash

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.Constants.LOCAL_START_RIDE
import com.mongokerala.taxi.newuser.utils.common.Constants.RIDE_FINISH
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import java.util.*


class SplashViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {


    val launchMain: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchTour: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val setUpLanguage: MutableLiveData<String> = MutableLiveData()
    val rideStatus: MutableLiveData<String> = MutableLiveData()
    val destination: MutableLiveData<String> = MutableLiveData()
    val userTotalPrice: MutableLiveData<String> = MutableLiveData()
    val rideDistance: MutableLiveData<String> = MutableLiveData()
    val fireStore: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val rideFinishFireStore: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()

    override fun onCreate() {

    }


    fun decideNextActivity() {
        Handler().postDelayed({
            if (userRepository.getLan() != null) {
                setUpLanguage.postValue(userRepository.getLan())
            }
            if (userRepository.getCurrentStatus()!!.isEmpty()) {
                launchTour.postValue(Event(emptyMap()))
            } else {

                if (userRepository.getCurrentStatus()
                        .toString() == "LOGIN_IN" && userRepository.getLocalStatus()
                        .toString() == "LOGIN"
                ) {
                    launchMain.postValue(Event(emptyMap()))
                } else {
                    getUpdatedStatus()
                }
            }
        }, 2500)

    }

    private fun getUpdatedStatus() {

        val db = FirebaseFirestore.getInstance()
        FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(false)
            .build()
        if (userRepository.getRIDE_KEY_LOCAL().toString().isNotEmpty()) {
            try {
                val docRef = db.collection("RIDE_KEY")
                    .document(userRepository.getRIDE_KEY_LOCAL().toString())
                docRef.get()
                    .addOnSuccessListener { documentSnapshot ->
                        try {
                            if (Objects.requireNonNull(documentSnapshot["RIDE_Status"]).toString()
                                    .isNotEmpty()
                            ) {

                                when(documentSnapshot["RIDE_Status"].toString()) {
                                    LOCAL_START_RIDE ->{
                                        userRepository.saveTaxiId(documentSnapshot["TAXI_ID"].toString())
                                        userRepository.setRide_ID(documentSnapshot["RIDE_ID"].toString())
                                        rideStatus.postValue(documentSnapshot["RIDE_Status"].toString())
                                        destination.postValue(documentSnapshot["DESTINATION"].toString())
                                        fireStore.postValue(Event(emptyMap()))
                                    }
                                    RIDE_FINISH ->{
                                        userRepository.saveTaxiId(documentSnapshot["TAXI_ID"].toString())
                                        userRepository.setRide_ID(documentSnapshot["RIDE_ID"].toString())
                                        userTotalPrice.postValue(documentSnapshot["USER_TOTAL_PRICE"].toString())
                                        rideDistance.postValue(documentSnapshot["RIDE_DISTANCE"].toString())
                                        destination.postValue(documentSnapshot["DESTINATION"].toString())
                                        rideFinishFireStore.postValue(Event(emptyMap()))
                                    }
                                    else -> {
                                        rideStatus.postValue(documentSnapshot["RIDE_Status"].toString())
                                        destination.postValue(documentSnapshot["DESTINATION"].toString())
                                        fireStore.postValue(Event(emptyMap()))
                                    }

                                }
                             }
                        } catch (e: Exception) {

                        }
                    }
                launchMain.postValue(Event(emptyMap()))
            } catch (e: java.lang.Exception) {

            }

        } else {
            launchMain.postValue(Event(emptyMap()))
        }

    }
}
