package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImageResponse {
    @SerializedName("statusCode")
    @Expose
    var statusCode: Int? = null

    @SerializedName("status")
    @Expose
    var status: Boolean? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("data")
    @Expose
    var data: String? = null

    @SerializedName("jwt")
    @Expose
    var jwt: String? = null

    @SerializedName("infoId")
    @Expose
    var infoId: Int? = null
}