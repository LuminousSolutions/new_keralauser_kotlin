package com.mongokerala.taxi.newuser.ui.razorpay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.mongokerala.taxi.newuser.R;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import static android.widget.Toast.*;

public class RazPayActivity extends AppCompatActivity implements PaymentResultListener {

    Button testPaymentButton;

    private Button mGooglePayButton;

    final int UPI_PAYMENT = 0;
    private static final int TEZ_REQUEST_CODE = 123;

    private static final String GOOGLE_TEZ_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_razpay);
        Checkout.preload(getApplicationContext());

        testPaymentButton = findViewById(R.id.testPaymentButton);
        mGooglePayButton = findViewById(R.id.googlePayButton);

        testPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startPaymentRazorPay();
                startPayment();
            }
        });

        boolean isAppInstalled = checkGooglePayInstalled("com.google.android.apps.nbu.paisa.user");

        if(isAppInstalled)
        {
            mGooglePayButton.setVisibility(View.VISIBLE);
        }
        else
        {
            mGooglePayButton.setVisibility(View.GONE);
        }

       mGooglePayButton.setOnClickListener(new View.OnClickListener()

       {







           @Override
           public void onClick(View v) {
               googlepayAction();
               //Toast.makeText(RazPayActivity.this, "cooming Soon....", Toast.LENGTH_LONG).show();


           }
       });

//        mGooglePayButton.setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Uri uri =
//                                new Uri.Builder()
//                                        .scheme("upi")
//                                        .authority("pay")
//                                        .appendQueryParameter("pa", "2008splender@oksbi")
//                                        .appendQueryParameter("pn", "Test Merchant")
//                                        .appendQueryParameter("mc", "1234")
//                                        .appendQueryParameter("tr", "123456789")
//                                        .appendQueryParameter("tn", "Taxi Deals transaction")
//                                        .appendQueryParameter("am", "01.01")
//                                        .appendQueryParameter("cu", "INR")
//                                        .appendQueryParameter("url", "https://test.merchant.website")
//                                        .build();
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setData(uri);
//                        intent.setPackage(GOOGLE_TEZ_PACKAGE_NAME);
//                        startActivityForResult(intent, TEZ_REQUEST_CODE);
//                    }
//                });
// .appendQueryParameter("pa", "mohansurya59@okicici")
//                .appendQueryParameter("pn", "mohansurya59")
//                .appendQueryParameter("tn", "Taxi Deals transaction")
    }


    private void googlepayAction() {
        String GOOGLE_TEZ_PACKAGE_NAME="com.google.android.apps.nbu.paisa.user";
        Uri uri =
                new Uri.Builder()
                        .scheme("upi")
                        .authority("pay")
                        .appendQueryParameter("pa", "keralacabs01@okicici")
                        .appendQueryParameter("pn", "kerala Cabs")
                        .appendQueryParameter("mc", "BCR2DN6TU7HMPMTK")
                        .appendQueryParameter("tr", "123456789")
                        .appendQueryParameter("tn", "test transaction note")
                        .appendQueryParameter("am", "1.00")
                        .appendQueryParameter("cu", "INR")
                        .appendQueryParameter("url", "https://merchant.website")
//                        .appendQueryParameter("url", "https://merchant.website")
                        .build();
        Intent upiPayIntent = new Intent(Intent.ACTION_VIEW);
        upiPayIntent.setData(uri);
        Intent chooser = Intent.createChooser(upiPayIntent, "Pay with");
        // check if intent resolves
        if(null != chooser.resolveActivity(getPackageManager())) {
            startActivityForResult(chooser, UPI_PAYMENT);
        } else {
            Toast.makeText(this,"No UPI app found, please install one to continue",Toast.LENGTH_SHORT).show();
        }


//        intent.setPackage(GOOGLE_TEZ_PACKAGE_NAME);
//        startActivityForResult(intent, TEZ_REQUEST_CODE);



    }

    private void getStatus(String data){
        boolean ispaymentcancel=false;
        boolean ispaymentsuccess=false;
        boolean ispaymentfailure=false;

        String value[]=data.split("&");
        for (int i=0;i<value.length;i++){
            String checkString[]=value[i].split("=");
            if (checkString.length >= 2){
                if (checkString[0].toLowerCase().equals("status")){
                    if (checkString[1].equals("success")){
                        ispaymentsuccess=true;
                    }
                }
            }else {
                ispaymentcancel=true;
            }
        }

        if (ispaymentsuccess){
            Log.d("TAG", "getStatus:  is payment successfull");
        }else if (ispaymentcancel){
            Log.d("TAG", "getStatus:  is payment canceled");
        }else{
            Log.d("TAG", "getStatus:  is payment failured");
        }
    }

    public void startPaymentRazorPay()
    {
//        String amount = String.valueOf(decimalformat(round_total));
//        amount = amount.replace(".","");
//        Log.i("Amount : ",amount);

        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Demo Razor Pay");
            options.put("description", "Service Charges");
//            //You can omit the image option to fetch the image from dashboard
//            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", "100");
//            options.put("amount", amount);

            JSONObject preFill = new JSONObject();
            preFill.put("email", "taxideals.ch@gmail.com");
            preFill.put("contact", "8344254882");

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            makeText(activity, "Error in payment: " + e.getMessage(), LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    /**
     * The name of the function has to be
     * onPaymentSuccess
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            makeText(this, "Payment Successful: " + razorpayPaymentID, LENGTH_SHORT).show();
            show_toast("Payment Successful: " + razorpayPaymentID);
        } catch (Exception e) {
            show_toast("Exception in onPaymentSuccess"+e.getMessage());
            Log.e("CheckoutAct", "Exception in onPaymentSuccess", e);
        }
    }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try
        {
            show_toast("Payment failed" + code + " " + response);
        }
        catch (Exception e)
        {
            show_toast("Exception in onPaymentSuccess"+e.getMessage());
            Log.e("CheckoutAct", "Exception in onPaymentError", e);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void show_toast(String msg) {
//		Toast.makeText(con, msg, Toast.LENGTH_SHORT).show();

        Toast toast = makeText(this, msg, LENGTH_SHORT);
        LinearLayout toastLayout = (LinearLayout) toast.getView();
        TextView toastTV = (TextView) toastLayout.getChildAt(0);
        toast.show();

    }

    public String decimalformat(double d)
    {
        NumberFormat formatter = new DecimalFormat("#0.00");

        System.out.println(formatter.format(d));

        return formatter.format(d);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==101){
            if (resultCode== RESULT_OK){
                if (data != null){
                    String value=data.getStringExtra("response");
                    ArrayList<String> list=new ArrayList<>();
                    list.add(value);
                    getStatus(list.get(0));
                    Log.d("TAG", "onActivityResult:  payment successfully......");
                }
            }else{
                Log.d("TAG", "onActivityResult:  payment failed.....");
            }
        }
        switch (requestCode) {
            case UPI_PAYMENT:
                if ((RESULT_OK == resultCode) || (resultCode == 11)) {
                    if (data != null) {
                        String trxt = data.getStringExtra("response");
                        Log.e("UPI", "onActivityResult: " + trxt);
                        ArrayList<String> dataList = new ArrayList<>();
                        dataList.add(trxt);
                        upiPaymentDataOperation(dataList);
                    } else {
                        Log.e("UPI", "onActivityResult: " + "Return data is null");
                        ArrayList<String> dataList = new ArrayList<>();
                        dataList.add("nothing");
                        upiPaymentDataOperation(dataList);
                    }
                } else {
                    //when user simply back without payment
                    Log.e("UPI", "onActivityResult: " + "Return data is null");
                    ArrayList<String> dataList = new ArrayList<>();
                    dataList.add("nothing");
                    upiPaymentDataOperation(dataList);
                }
                break;
        }
//        switch (requestCode) {
//            case TEZ_REQUEST_CODE:
//                Log.i("result", data.getStringExtra("Status"));
//                break;
//        }
//
    }

    private void upiPaymentDataOperation(ArrayList<String> data) {
        if (isConnectionAvailable(RazPayActivity.this)) {
            String str = data.get(0);
            Log.e("UPIPAY", "upiPaymentDataOperation: "+str);
            String paymentCancel = "";
            if(str == null) str = "discard";
            String status = "";
            String approvalRefNo = "";
            String response[] = str.split("&");
            for (int i = 0; i < response.length; i++) {
                String equalStr[] = response[i].split("=");
                if(equalStr.length >= 2) {
                    if (equalStr[0].toLowerCase().equals("Status".toLowerCase())) {
                        status = equalStr[1].toLowerCase();
                    }
                    else if (equalStr[0].toLowerCase().equals("ApprovalRefNo".toLowerCase()) || equalStr[0].toLowerCase().equals("txnRef".toLowerCase())) {
                        approvalRefNo = equalStr[1];
                    }
                }
                else {
                    paymentCancel = "Payment cancelled by user.";
                }
            }
            if (status.equals("success")) {
                //Code to handle successful transaction here.
                Toast.makeText(RazPayActivity.this, "Transaction successful.", Toast.LENGTH_SHORT).show();
                Log.e("UPI", "payment successfull: "+approvalRefNo);
            }
            else if("Payment cancelled by user.".equals(paymentCancel)) {
                Toast.makeText(RazPayActivity.this, "Payment cancelled by user.", Toast.LENGTH_SHORT).show();
                Log.e("UPI", "Cancelled by user: "+approvalRefNo);
            }
            else {
                Toast.makeText(RazPayActivity.this, "Transaction failed.Please try again", Toast.LENGTH_SHORT).show();
                Log.e("UPI", "failed payment: "+approvalRefNo);
            }
        } else {
            Log.e("UPI", "Internet issue: ");
            Toast.makeText(RazPayActivity.this, "Internet connection is not available. Please check and try again", Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isConnectionAvailable(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()
                    && netInfo.isConnectedOrConnecting()
                    && netInfo.isAvailable()) {
                return true;
            }
        }
        return false;
    }

    public void startPayment() {

        Checkout checkout = new Checkout();
        final Activity activity = this;
        try {
            JSONObject options = new JSONObject();

            options.put("name", "Taxi deals");
            options.put("description", "Reference No. #123456");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");

//            options.put("order_id", "order_DBJOWzybf0sJbb");//from response of step 3.

            options.put("theme.color", "#3399cc");
            options.put("currency", "INR");
            options.put("amount", "100");//pass amount in currency subunits

            options.put("prefill.email", "taxideals.ch@gmail.com");
            options.put("prefill.contact","8344254882");

            /*JSONObject retryObj = new JSONObject();
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);*/

            checkout.open(activity, options);

        } catch(Exception e) {
            Log.e("TAG", "Error in starting Razorpay Checkout", e);
        }
    }


    private boolean checkGooglePayInstalled(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

}
