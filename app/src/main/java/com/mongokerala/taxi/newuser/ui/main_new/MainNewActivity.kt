package com.mongokerala.taxi.newuser.ui.main_new

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.*
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.media.MediaPlayer
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.Html
import android.text.format.DateFormat
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.afollestad.materialdialogs.BuildConfig
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.JsonParser
import com.mongokerala.taxi.newuser.BuildConfig.BASE_URL
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.arcLayout.ArcLayoutController
import com.mongokerala.taxi.newuser.data.model.DriverListType
import com.mongokerala.taxi.newuser.data.remote.Endpoints
import com.mongokerala.taxi.newuser.data.remote.LocationData
import com.mongokerala.taxi.newuser.data.remote.fcm.FcmCustomizeNotificationData
import com.mongokerala.taxi.newuser.data.remote.request.AdvanceSeachApiRequest
import com.mongokerala.taxi.newuser.data.remote.request.BookingRequest
import com.mongokerala.taxi.newuser.data.remote.request.CouponBookingRequest
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.about.AboutActivity
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.common_dialog.MYOnClickListener
import com.mongokerala.taxi.newuser.ui.common_dialog.RideFinishCustomAlertDialog
import com.mongokerala.taxi.newuser.ui.coupon.CouponApply
import com.mongokerala.taxi.newuser.ui.coupon.MyCoupon
import com.mongokerala.taxi.newuser.ui.custom.RoundedImageView
import com.mongokerala.taxi.newuser.ui.feedback.FeedbackActivity
import com.mongokerala.taxi.newuser.ui.login.LoginActivity
import com.mongokerala.taxi.newuser.ui.map.MapsView
import com.mongokerala.taxi.newuser.ui.profile_edit.ProfileEditActivity
import com.mongokerala.taxi.newuser.ui.rate_us.RateUsActivity
import com.mongokerala.taxi.newuser.ui.razorpay.RazorPayInfoActivity
import com.mongokerala.taxi.newuser.ui.setting.SettingActivity
import com.mongokerala.taxi.newuser.ui.triphistory.RecyclerViewItemClickListener
import com.mongokerala.taxi.newuser.ui.triphistory.TripHistoryActivity
import com.mongokerala.taxi.newuser.ui.tryagain_dialog.DialogTryAgain
import com.mongokerala.taxi.newuser.ui.tryagain_dialog.MyDialog
import com.mongokerala.taxi.newuser.ui.tryagain_dialog.MyDialogTryagain
import com.mongokerala.taxi.newuser.ui.wallet.MyWallet
import com.mongokerala.taxi.newuser.utils.common.*
import com.mongokerala.taxi.newuser.utils.common.Constants.ACCEPT_ORDER
import com.mongokerala.taxi.newuser.utils.common.Constants.CANCEL_BY_USER
import com.mongokerala.taxi.newuser.utils.common.Constants.CANCEl_BOOKING
import com.mongokerala.taxi.newuser.utils.common.Constants.CUSTOM_LOCATION_DATA
import com.mongokerala.taxi.newuser.utils.common.Constants.CUSTOM_NOTIFICATION_DATA
import com.mongokerala.taxi.newuser.utils.common.Constants.DEFAULT_ZOOM
import com.mongokerala.taxi.newuser.utils.common.Constants.DRIVER_CANCEL_RIDE
import com.mongokerala.taxi.newuser.utils.common.Constants.LOCAL_ACCEPT_RIDE
import com.mongokerala.taxi.newuser.utils.common.Constants.LOCAL_CONTINUE_RIDE
import com.mongokerala.taxi.newuser.utils.common.Constants.LOCAL_DONE_PICKUP
import com.mongokerala.taxi.newuser.utils.common.Constants.LOCAL_START_PICKUP
import com.mongokerala.taxi.newuser.utils.common.Constants.LOCAL_START_RIDE
import com.mongokerala.taxi.newuser.utils.common.Constants.PICKUP_DONE
import com.mongokerala.taxi.newuser.utils.common.Constants.PRECANCEL_BY_USER
import com.mongokerala.taxi.newuser.utils.common.Constants.REQUEST_CHANGE_DESTINATION
import com.mongokerala.taxi.newuser.utils.common.Constants.RIDE_CANCEL_BOOKING
import com.mongokerala.taxi.newuser.utils.common.Constants.RIDE_FINISH
import com.mongokerala.taxi.newuser.utils.common.Constants.RIDE_REQUEST_CHANGEDESTINATION
import com.mongokerala.taxi.newuser.utils.common.Constants.is_address
import com.mongokerala.taxi.newuser.utils.display.Toaster
import com.mongokerala.taxi.newuser.utils.network.JsonHelper
import com.mongokerala.taxi.newuser.utils.network.LocationTrackingService
import com.mongokerala.taxi.newuser.utils.network.ServerFasade
import com.pixplicity.easyprefs.library.Prefs
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main_new.*
import kotlinx.android.synthetic.main.activity_main_new.spin_kit
import kotlinx.android.synthetic.main.booking_bootom_sheet.*
import kotlinx.android.synthetic.main.dialog_ride_finish.*
import kotlinx.android.synthetic.main.dialog_wallet_balance_details.*
import org.json.JSONObject
import timber.log.Timber
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.math.round
import kotlin.math.roundToInt
import kotlinx.android.synthetic.main.app_bar_main.toolbar as toolbar1


class MainNewActivity : BaseActivity<MainNewViewModel>(), OnMapReadyCallback, MYOnClickListener,
    ArcLayoutController.OnOptionSClickListener, DatePickerDialog.OnDateSetListener,
    MyDialogTryagain, CouponApply,
    TimePickerDialog.OnTimeSetListener, RecyclerViewItemClickListener, LocationListener {
    private var desPlace: Place? = null
    private val MINDISTANCECHANGEFORUPDATES = 10.toFloat()
    private val MINTIMEBWUPDATES = 1000.toLong()
    protected var locationManager: LocationManager? = null
    protected var context: Context? = null
    private var checkGpsStatus = false
    protected var checkNetworkStatus = false
    private var mtot_amount: TextView? = null
    private var currentLocationUser: LocationResult? = null
    private var userRepository: UserRepository? = null
    val db = FirebaseFirestore.getInstance()
    private var sourcePlace: Place? = null
    private var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>? = null
    private var ridekeyLocal: String? = null
    private val Tracking = Handler()
    var userLocationMarker: Marker? = null
    var userLocationAccuracyCircle: Circle? = null
    private val listenerRegistration: ListenerRegistration? = null
    private var arcLayoutController: ArcLayoutController? = null
    var peek_hight: Int = 0
    private var driverListType: DriverListType? = null
    private val nearbyCabMarkerList = arrayListOf<Marker>()
    var oFFLINE_DriverList: ArrayList<AdvanceSearhData> = ArrayList()
    private var countDownTimer: CountDownTimer? = null
    lateinit var bitmap: Bitmap
    private lateinit var defaultLocation: LatLng
    private var grayPolyline: Polyline? = null
    private var blackPolyline: Polyline? = null
    private var movingCabMarker: Marker? = null
    private var previousLatLng: LatLng? = null
    private var currentLatLng: LatLng? = null
    private var originMarker: Marker? = null
    private var destinationMarker: Marker? = null
    var locationList = ArrayList<LatLng>()
    var number = 0
    var list: List<AdvanceSearhData?>? = null


    lateinit var button: Button
    lateinit var textView: TextView
    var day = 0
    var month: Int = 0
    var year: Int = 0
    var hour: Int = 0
    var minute: Int = 0
    var myDay = 0
    var myMonth: Int = 0
    var myYear: Int = 0
    var myHour: Int = 0
    var myMinute: Int = 0
    var sourceLatitude: String = ""
    var sourceLangtitude: String = ""
    var firebasekm = ""
    var cochinairportplaceid = ""
    var kozhikodeairportplaceid = ""
    var kannurairportplaceid = ""
    var Thiruvanhpuramairportplaceid = ""

    companion object {
        val locationListeners = arrayOf(
            LocationTrackingService.Companion.LTRLocationListener(LocationManager.GPS_PROVIDER),
            LocationTrackingService.Companion.LTRLocationListener(LocationManager.NETWORK_PROVIDER)
        )
        const val TAG = "MainNewActivity"
        var myCoupon = ""
        var checkcoupon = false
        var couponCarPrice = ""
        var mainNewActivity: MainNewActivity? = null
        var fcmCustomizeNotificationData_background = FcmCustomizeNotificationData()
        var wallet_amount = ""
        var carPositionCoupon = mutableListOf("")
        var carPosition: Int? = null
        var car_price: String? = ""
        var couponCarType: String = ""
        var onlineDriverListApplyCoupon: ArrayList<AdvanceSearhData> = ArrayList()
        var oNLINE_DriverList: ArrayList<AdvanceSearhData> = ArrayList()
        var oFFLINE_DriverList_taxi: ArrayList<AdvanceSearhData> = ArrayList()
        val BROADCAST_TAG = MainNewActivity::class.java.canonicalName
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
        private var isShowMainActivityAlive = false
        private var isStopMethodAlive = false
        private var isDoneStartRideAlive = false
        private var isFirstDonePickuplatlong = true
        private var isFirstDonePickuplatlongnew = true
        private var isFirstLatLoong = true
        private var isRideFinishAlive = false
        private var stopRideMethodTwo = false
        private var isSupplierId = ""
        private var isUserId = ""
        private var isLocalStatus = ""
        private var isSourceId = ""
        private var isDestinationId = ""
        private var isTotalKm = ""
        private var isLocABTotalKm = ""
        private var isTotalGoogleKmDriver = ""
        private var isLat = 0.0
        private var isLong = 0.0
        private var isLatfinal = 0.0
        private var isLongfinal = 0.0

        private var lonNewfinal = 0.0
        private var latNewfinal = 0.0
        private var lonoldfinal = 0.0
        private var latoldfinal = 0.0
        private var distancemeter = 0.0
        private var vcontext: Context? = null
        private var dR_mobile_num = ""
        private var selectbox = ""
        var apx_total_km = ""
        var mySourceId = ""
        var driverTrackingId = ""
        var driverTrackingkm = ""
        var check_driverTrackingId_status = false
        var apx_time = ""
        var check_Wallet = ""
        var points: List<LatLng> = ArrayList()
        var indexmovingcab = 0

        var driverTrackingDistance = ""
        var driverTrackinglatitude = 0.0
        var driverTrackinglangtitude = 0.0


        fun setIslonNewfinal(islonNewfinal: Double) {
            lonNewfinal = islonNewfinal
        }


        fun setIslatNewfinal(isslatNewfinal: Double) {
            latNewfinal = isslatNewfinal
        }

        fun getIsContext(): Context {
            return vcontext!!
        }

        fun setIsContext(mcontext: Context) {
            vcontext = mcontext
        }


        fun setIslonoldfinal(isslonoldfinal: Double) {
            lonoldfinal = isslonoldfinal
        }

        fun getIsDistanceMeter(): Double {
            return 0.0
        }

        fun setIslatoldfinal(isslatoldfinal: Double) {
            latoldfinal = isslatoldfinal
        }

        fun getIsMainActivityAlive(): Boolean {
            return isShowMainActivityAlive
        }

        fun setIsMainActivityAlive(isHomeAlive: Boolean) {
            isShowMainActivityAlive = isHomeAlive
        }

        fun getIsMainNewActivity(): MainNewActivity? {
            return mainNewActivity
        }

        fun setIsMainNewActivty(mainNewActivity: MainNewActivity) {
            this.mainNewActivity = mainNewActivity
        }

        fun getIsDoneStartRide(): Boolean {
            return isDoneStartRideAlive
        }


        fun getFirstDoneLatLong(): Boolean {
            return isFirstDonePickuplatlong
        }

        fun setFirstDoneLatLong(isFirstDonelatlong: Boolean) {
            isFirstDonePickuplatlong = isFirstDonelatlong
        }

        fun getFirstDoneLatLongNew(): Boolean {
            return isFirstDonePickuplatlongnew
        }

        fun setFirstDoneLatLongNew(isFirstDonelatlongnew: Boolean) {
            isFirstDonePickuplatlongnew = isFirstDonelatlongnew
        }

        fun getFirstLatLong(): Boolean {
            return isFirstLatLoong
        }

        fun setFirstLatLong(isFirstLatLong: Boolean) {
            isFirstLatLoong = isFirstLatLong
        }

        fun setIsTotalKm(isstotalkm: String) {
            isTotalKm = isstotalkm
        }


        fun setIsLocABTotalkm(issloctotalkm: String) {
            isLocABTotalKm = issloctotalkm
        }

        fun setIsLat(issLat: Double) {
            isLat = issLat
        }

        fun setIsLong(issLong: Double) {
            isLong = issLong
        }

        fun setIsLatfinal(issLatfinal: Double) {
            isLat = issLatfinal
        }

        fun setIsLongfinal(issLongfinal: Double) {
            isLong = issLongfinal
        }
    }

    private var runnable: Runnable? = null
    private var handler = Handler()

    private var mNameTextView: TextView? = null
    private var mEmailTextView: TextView? = null
    private var wallet_balance: TextView? = null
    private var credit_amount: TextView? = null
    private val SYSTEM_ALERT_WINDOW_PERMISSION = 2084
    private var mMap: GoogleMap? = null
    private var driverTrackingMap: GoogleMap? = null
    private var carMap: GoogleMap? = null
    private var showAvilableVehicle: GoogleMap? = null
    var mapView: View? = null
    private var totalDistance = "0.0"
    private var totalDistanceSearch = "0.0"
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationRequestgps: LocationRequest
    private lateinit var locationCallback: LocationCallback
    var sourceaddress: String = ""
    var destinationaddress: String = ""
    var sourcePlaceId = ""
    var destinationPlaceId: String = ""
    var check_driver_accept: Boolean = true

    override fun provideLayoutId(): Int = R.layout.activity_main_new

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    @SuppressLint("LogNotTimber")
    override fun setupObservers() {
        super.setupObservers()
        if (runnable == null) {
            runnable = Runnable {}
        }
        getTokenUpdate()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        checkLocationUpdate()
        textView = findViewById(R.id.textView)
        textView = findViewById(R.id.btnPick)
        textView.setOnClickListener {
            val calendar: Calendar = Calendar.getInstance()
            day = calendar.get(Calendar.DAY_OF_MONTH)
            month = calendar.get(Calendar.MONTH)
            year = calendar.get(Calendar.YEAR)
            val datePickerDialog =
                DatePickerDialog(this@MainNewActivity, this@MainNewActivity, year, month, day)
            datePickerDialog.show()
        }

        setIsContext(applicationContext)
        setSupportActionBar(toolbar1)
        arcLayoutController = ArcLayoutController(this)
        arcLayoutController?.setCallBackListener(this)
        Places.initialize(
            Objects.requireNonNull(
                applicationContext
            ), "AIzaSyDkCytxQTkidI9IkQmXRrnB9SPX1HeVbm4"

        )


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getLocationUpdates()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            askPermission()
        }

        viewModel.getusername.observe(this) {
            if (mNameTextView!!.text.toString() != it) mNameTextView!!.setText(it)

        }

        viewModel.loadGoogleMapsRoadPointsTask.observe(this) {
            it.getIfNotHandled()?.run {
                LoadGoogleMapsRoadPointsTask().execute(
                    "", ""
                )
            }
        }
        viewModel.sourceId.observe(this) {
            mySourceId = it
            viewModel.setSource_Place_ID(it)
            getTotalKm()
        }

        viewModel.driverTrackingId.observe(this) {
            driverTrackingId = it
            check_driverTrackingId_status = true
            getTotalKmDriverTracking()
        }


        viewModel.couponPrice.observe(this) {
            finalCouponPrice(it)
        }
        viewModel.checkCoupon.observe(this) {
            checkCouponResult(it)
        }
        viewModel.myCoupon.observe(this) {
            myCoupon = it
            findCouponPrice(myCoupon)
        }
        viewModel.getuseremail.observe(this) {
            if (mEmailTextView!!.text.toString() != it) mEmailTextView!!.setText(it)
        }
        bottomSheet.visibility = View.GONE

        viewModel.liveDataNotaxi.observe(this) {
            noTaxiAvailable()
        }



        viewModel.liveDataSearchTaxiResponse.observe(this@MainNewActivity) {
            if (it != null && !it.data.isNullOrEmpty()) {
                if (!viewModel.getRIDE_KEY_LOCAL().equals(null) || viewModel.getRIDE_KEY_LOCAL()
                        .toString() != ""
                ) {
                    rideKeyBookingStatus()
                    viewModel.setRIDE_KEY_LOCAL("")
                }
                openBottomSheet(it.data)
                showMapAddress()
                onLocationChange()


            } else {
                bottomSheet.visibility = View.GONE
            }

        }
        viewModel.liveDataShowTaxiResponse.observe(this@MainNewActivity) {
            if (it.data != null) {
                for (showtaxi in it.data!!.indices) {
                    val latLng = it.data!![showtaxi]?.latitude?.let { it1 ->
                        it.data!![showtaxi]?.longitude?.let { it2 ->
                            LatLng(
                                it1,
                                it2
                            )
                        }
                    }
                    val markerOptions = MarkerOptions()
                    if (latLng != null) {
                        markerOptions.position(latLng)
                    }
                    if (it.data!![showtaxi]?.carType.equals("Taxi4")) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car))
                        if (latLng != null) {
                            val marker = showAvilableVehicle!!.addMarker(
                                MarkerOptions().position(latLng).title("HatchbackTaxi")
                            )
                            marker?.showInfoWindow()
                            showAvilableVehicle?.addMarker(markerOptions)
                        }
                    } else if (it.data!![showtaxi]?.carType.equals("Taxi")) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car))
                        if (latLng != null) {
                            val marker = showAvilableVehicle!!.addMarker(
                                MarkerOptions().position(latLng).title("sedanTaxi")
                            )
                            marker?.showInfoWindow()
                            showAvilableVehicle?.addMarker(markerOptions)
                        }
                    } else if (it.data!![showtaxi]?.carType.equals("Taxi6")) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car))
                        if (latLng != null) {
                            val marker = showAvilableVehicle!!.addMarker(
                                MarkerOptions().position(latLng).title("suvTaxi")
                            )
                            marker?.showInfoWindow()
                            showAvilableVehicle?.addMarker(markerOptions)
                        }
                    } else if (it.data!![showtaxi]?.carType.equals("Auto")) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.auto))
                        if (latLng != null) {
                            val marker = showAvilableVehicle!!.addMarker(
                                MarkerOptions().position(latLng).title("AutoTaxi")
                            )
                            marker?.showInfoWindow()
                            showAvilableVehicle?.addMarker(markerOptions)
                        }
                    } else if (it.data!![showtaxi]?.carType.equals("Transport")) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.volvo))
                        if (latLng != null) {
                            val marker = showAvilableVehicle!!.addMarker(
                                MarkerOptions().position(latLng).title("Traveller")
                            )
                            marker?.showInfoWindow()
                            showAvilableVehicle?.addMarker(markerOptions)
                        }
                    } else if (it.data!![showtaxi]?.carType.equals("Bike")) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car))
                        if (latLng != null) {
                            val marker = showAvilableVehicle!!.addMarker(
                                MarkerOptions().position(latLng).title("Bike")
                            )
                            marker?.showInfoWindow()
                            showAvilableVehicle?.addMarker(markerOptions)
                        }
                    } else if (it.data!![showtaxi]?.carType.equals("Ambulance")) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car))
                        if (latLng != null) {
                            val marker = showAvilableVehicle!!.addMarker(
                                MarkerOptions().position(latLng).title("Ambulance")
                            )
                            marker?.showInfoWindow()
                            showAvilableVehicle?.addMarker(markerOptions)
                        }
                    } else {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car))

                    }

                    markerOptions.anchor(0.5.toFloat(), 0.5.toFloat())
                    userLocationMarker = showAvilableVehicle?.addMarker(markerOptions)
                }
            }
        }

        viewModel.updateVechile.observe(this) {
            updateDriverList(it, "", true)

        }
        viewModel.launchLogin.observe(this) {
            it.getIfNotHandled()?.run {
                val intent = Intent(applicationContext, LoginActivity::class.java)
                intent.putExtra("LOGOUT", "LOGOUT")
                startActivity(intent)
                finish()
            }
        }
        viewModel.rideFinishReview.observe(this) {
            sucessfullFinish()
        }
        viewModel.liveBookingResponse.observe(this) {

        }
        viewModel.RIDE_Status.observe(this) {
            updateStatusOnScreen()
        }
        viewModel.wallet_check.observe(this) {
            check_Wallet = it.toString()
            if (!it) {
                Toast.makeText(
                    this,
                    Html.fromHtml("<big><b>You Have Eligible to Get 500 Rupees on Your Wallet Apply Promocode</b></big>"),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        viewModel.wallet_balance.observe(this) {
            wallet_amount = it.toString()
            if (it != null) {
                wallet_balance!!.text = it.toString()
            }

        }
        viewModel.wallet_credit.observe(this) {
            if (it != null) {
                credit_amount!!.text = it.toString()
            }
        }
        viewModel.profileimagestatus.observe(this) {
            if (it) {
                userRepository?.saveFbprofileImage("")
            } else {
                userRepository?.saveFbprofileImage("")
            }

        }


        viewModel.isprofileImage.observe(this) {
            isProfileImage(it)
        }

        viewModel.add_walletTokenBalance.observe(this) {
            addWalletAmount(it)
        }

        viewModel.userWalletBalance.observe(this) {
            if (it) {
                Toast.makeText(
                    this,
                    Html.fromHtml("<big><b>Congratulation 500 Rupess Credited In Your Wallet Amount</b></big>"),
                    Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(this, "Please Enter Valid Token", Toast.LENGTH_SHORT).show()
            }

        }
        viewModel.checkToken.observe(this) {
            if (it) {
                Toast.makeText(this, "PromoCode Applied Successfully", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "You Are Applied Wrong Promo Code", Toast.LENGTH_SHORT).show()
            }

        }

        viewModel.walletTokenBalance.observe(this) {
            wallet_balance!!.text = it.toString()
        }


        viewModel.totalamout.observe(this) {
            if (mtot_amount!!.text.toString() == "CASH" || mtot_amount!!.text.toString()
                    .isNotEmpty()
            ) {
                mtot_amount!!.text = it
            }
        }

    }

    private fun getAvailableVehicleAround(locationResult: LocationResult) {

        if (showAvilableVehicle != null) {

            if (card_view.visibility == View.VISIBLE) {

                showAvilableVehicle?.clear()
                if (locationResult.lastLocation != null) {
                    setUserLocationMarker(locationResult.lastLocation!!)
                }
                val showTaxiRequest = AdvanceSeachApiRequest(
                    0,
                    0,
                    locationResult.lastLocation?.latitude,
                    "string",
                    "string",
                    "string",
                    "string",
                    0,
                    "string",
                    "string",
                    "string",
                    "string",
                    locationResult.lastLocation?.longitude,
                    "string"
                )
                viewModel.showTaxies(showTaxiRequest)
            }
        }
    }

    private fun showAvailableVehicleViaDest() {

        if (showAvilableVehicle != null) {

            showAvilableVehicle?.clear()
            currentLocationUser?.let {
                setUserLocationMarker(it.lastLocation!!)
                val showTaxiRequest = AdvanceSeachApiRequest(
                    0,
                    0,
                    it.lastLocation?.latitude,
                    "string",
                    "string",
                    "string",
                    "string",
                    0,
                    "string",
                    "string",
                    "string",
                    "string",
                    it.lastLocation?.longitude,
                    "string"
                )
                viewModel.showTaxies(showTaxiRequest)
            }
        }
    }

    private fun getTokenUpdate() {
        val oldToken = viewModel.getUser_FCM_Token()
        var newToken = ""

        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            newToken = it.toString()

            if (!oldToken.isNullOrEmpty()) {
                if (oldToken == newToken) {
                    viewModel.setUser_FCM_Token(newToken)
                } else {
                    viewModel.setUser_FCM_Token(newToken)
                }
            }
        }

    }
    private fun getCustomerCareNumber() {
        val domain = viewModel.getDomain().toString()
         db.collection("phone_number").document(domain).get()
            .addOnSuccessListener { documentSnapshot ->
                val phoneNumber = documentSnapshot.getString("phoneNumber")
                if (!phoneNumber.isNullOrEmpty()) {
                    viewModel.saveCustomerCareNumber(phoneNumber)
                }
            }.addOnFailureListener {
            }
    }

    private fun addWalletAmount(token: String) {
        viewModel.addWalletAmount(token)
    }


    private fun isProfileImage(profileImage: String?) {
        val headerLayout: View = navigation_view.getHeaderView(0)
        val profHeader: RoundedImageView = headerLayout.findViewById(R.id.prof_header)

        if (profileImage.equals(null) || profileImage.equals("false")) {
            if (viewModel.getFacebookLoginStatus().equals("LOGIN")) {
                if (viewModel.doGetFbImageurl().toString().isNotEmpty()) {
                    Picasso.get().load(viewModel.doGetFbImageurl()).into(profHeader)
                }
                if (viewModel.doGetFbImageurl().toString().isNotEmpty()) {
                    Picasso.get().load(viewModel.doGetFbImageurl()).into(profHeader)
                }
            } else {
                setProfileImage(profHeader)
            }
            if (viewModel.getGmailLoginStatus().equals("LOGIN")) {
                if (!viewModel.getgMailLoginImageUrl().isNullOrEmpty()) {
                    Picasso.get().load(viewModel.getgMailLoginImageUrl()).into(profHeader)
                }
            } else {
                setProfileImage(profHeader)
            }
        } else {
            setProfileImage(profHeader)
        }
    }
    private fun checkLocationUpdate() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        checkGpsStatus = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        checkNetworkStatus = locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        if (checkGpsStatus) {
            if (!(ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            )) {
                locationManager!!.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    MINTIMEBWUPDATES,
                    MINDISTANCECHANGEFORUPDATES,
                    locationListeners[0])
            }
        } else {
            getLocationGpsStatus()
        }
        if (checkNetworkStatus && !(ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
                    )) {
                locationManager!!.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    MINTIMEBWUPDATES,
                    MINDISTANCECHANGEFORUPDATES,
                    locationListeners[1]
                )
        }
        checkNetwork(this)
    }

    @SuppressLint("ServiceCast")
    private fun checkNetwork(context: Context): Boolean {
        val cm: ConnectivityManager =
            context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        if (activeNetwork != null) {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                checkNetworkStatus = true
            } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                checkNetworkStatus = true
            }
        } else {
            checkNetworkStatus = false
        }
        return checkNetworkStatus
    }
    private fun finalCouponPrice(amount: String?) {
        when (carPosition) {
            0 ->
                couponCarType = "Taxi4"
            1 ->
                couponCarType = "Taxi"
            2 ->
                couponCarType = "Taxi6"
            3 ->
                couponCarType = "Auto"
            4 ->
                couponCarType = "Transport"
            5 ->
                couponCarType = "Bike"
            6 ->
                couponCarType = "Ambulance"
        }
        val onlineDriverListWithCoupon=onlineDriverListApplyCoupon
        val onlineDriverListFilterCarType = onlineDriverListWithCoupon.filter { it.carType == couponCarType }
        if(onlineDriverListFilterCarType.isNotEmpty()){
            checkcoupon = true
            couponCarPrice = onlineDriverListFilterCarType[onlineDriverListFilterCarType.size - 1].price.toString()
            for (couponPrice in onlineDriverListFilterCarType.indices) {
                onlineDriverListFilterCarType[couponPrice].price = amount.toString()
            }
        }
        openBottomSheet(onlineDriverListWithCoupon)
    }

    fun driverAcceptStatus(isAccept: Boolean) {
        viewModel.driverAcceptStatus(isAccept)
    }

    private fun findCouponPrice(coupon: String) {
        var checkCarPosition = false
        when (carPosition) {
            0 -> {
                if (carPositionCoupon.indexOf("Taxi4") == -1) {
                    checkCarPosition = true
                    carPositionCoupon.add("Taxi4")
                } else {
                    checkCarPosition = false
                }
            }
            1 -> {
                if (carPositionCoupon.indexOf("Taxi") == -1) {
                    checkCarPosition = true
                    carPositionCoupon.add("Taxi")
                } else {
                    checkCarPosition = false
                }
            }
            2 -> {
                if (carPositionCoupon.indexOf("Taxi6") == -1) {
                    checkCarPosition = true
                    carPositionCoupon.add("Taxi6")
                } else {
                    checkCarPosition = false
                }
            }
            3 -> {
                if (carPositionCoupon.indexOf("Auto") == -1) {
                    checkCarPosition = true
                    carPositionCoupon.add("Auto")
                } else {
                    checkCarPosition = false
                }
            }
            4 -> {
                if (carPositionCoupon.indexOf("Transport") == -1) {
                    checkCarPosition = true
                    carPositionCoupon.add("Transport")
                } else {
                    checkCarPosition = false
                }
            }
            5 -> {
                if (carPositionCoupon.indexOf("Bike") == -1) {
                    checkCarPosition = true
                    carPositionCoupon.add("Bike")
                } else {
                    checkCarPosition = false
                }
            }
            6 -> {
                if (carPositionCoupon.indexOf("Ambulance") == -1) {
                    checkCarPosition = true
                    carPositionCoupon.add("Ambulance")
                } else {
                    checkCarPosition = false
                }
            }
        }
        val finalAmount: String = driverListType?.getDriverLists(carPosition.toString().toInt())
            ?.get(0)?.price.toString().toDouble().toInt().toString()

        if (checkCarPosition) {
            viewModel.doCheckCouponPrice(finalAmount, coupon)
        } else {
            Toast.makeText(
                this,
                "you have already enter this coupon this vehicle ",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun checkCouponResult(coupon: Boolean) {
        if (coupon) {
            Toast.makeText(this, "Coupon Applied SuccessFully.........", Toast.LENGTH_SHORT).show()
        } else {
            myCoupon = ""
            Toast.makeText(this, "Please Enter Valid Coupon.......", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkCoupen(coupon: String) {
        viewModel.checkingCoupon(coupon)
    }

    private fun setTotalAmount() {
        viewModel.dogetTotalAmount(viewModel.getUserID())
    }

    private fun updateStatusOnScreen() {
        val fireStoreStatus = intent.getStringExtra("FIRE_STORE_STATUS")

        if (!fireStoreStatus.isNullOrEmpty()) {
            if (!viewModel.getLocalStataus().isNullOrEmpty() && !viewModel.getLocalStataus().equals("LOGIN")) {
                afterAccept()
                assert(etxt_source_address != null)
                etxt_source_address.text = viewModel.getSource()
                etxt_des_address.text = viewModel.getDestination()
                assert(etxt_des_address != null)
                etxt_des_address.text = viewModel.getDestination()
                if (fireStoreStatus.equals(LOCAL_DONE_PICKUP, ignoreCase = true)) {
                    val data = FcmCustomizeNotificationData()
                    data.action = LOCAL_START_PICKUP
                    data.cancelReason = getString(R.string.Hi_I_am_Started_here_be_ready)
                    onReceiveNotification(data.action, data)
                    driver_call.visibility = View.VISIBLE
                } else if ( fireStoreStatus.equals(
                        LOCAL_START_PICKUP,
                        ignoreCase = true
                    )
                ) {
                    val data = FcmCustomizeNotificationData()
                    data.action = ACCEPT_ORDER
                    data.rideId = viewModel.getRideId().toString()
                    data.taxiDetailId = userRepository?.getTaxiId().toString()
                    driver_call.visibility = View.VISIBLE
                    driverInformationAfterBooking()
                    routeMapSourceDestination()
                } else if (fireStoreStatus.equals(
                        LOCAL_START_RIDE,
                        ignoreCase = true
                    )
                ) {val data = FcmCustomizeNotificationData()
                    data.action = PICKUP_DONE
                    data.cancelReason = getString(R.string.Are_You_in_Shall_We_Start)
                    data.rideId = viewModel.getRideId().toString()
                    data.taxiDetailId = userRepository?.getTaxiId().toString()
                    onReceiveNotification(data.action, data)
                } else if (fireStoreStatus.equals(
                        LOCAL_CONTINUE_RIDE,
                        ignoreCase = true
                    )
                ) {
                    val data = FcmCustomizeNotificationData()
                    data.action = LOCAL_START_RIDE
                    data.cancelReason = getString(R.string.be_safe_and_enjoy_your_ride)
                    onReceiveNotification(data.action, data)
                } else if (fireStoreStatus.equals(RIDE_FINISH, ignoreCase = true)) {
                    if (intent.extras != null && intent.getSerializableExtra(
                            CUSTOM_NOTIFICATION_DATA
                        ) != null
                    ) {
                        val notificationData =
                            intent.getSerializableExtra(CUSTOM_NOTIFICATION_DATA) as FcmCustomizeNotificationData
                        onReceiveNotification(notificationData.action, notificationData)
                        intent.removeExtra(CUSTOM_NOTIFICATION_DATA)
                    }
                }
            } else {
                assert(etxt_source_address != null)
                etxt_source_address.text = viewModel.getSource()
            }
        }
    }

    private fun noTaxiAvailable() {
        spin_kit.visibility = View.GONE
        val myDialog = MyDialog(
            this,
            getString(R.string.No_Taxi_Avilable_In_this_Location),
            getString(R.string.Sorry_No_Taxi),
            "\uf1ae",
            this@MainNewActivity
        )
        myDialog.show()
        myDialog.setCancelable(false)
    }

    private fun setTimerDismiss() {
        countDownTimer = object : CountDownTimer(40000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                try {
                    if (showAvilableVehicle != null) {
                        showAvilableVehicle!!.clear()
                    }
                    search_booking.visibility = View.GONE
                    checkcoupon = false
                    couponCarPrice = ""
                    tryAgainPopUp()
                    viewModel.onPauseSaveBooking(true)

                } catch (exce: java.lang.Exception) {
                    exce.printStackTrace()
                }
            }
        }.start()
    }

    private fun tryAgainPopUp() {
        assert(etxt_source_address != null)
        if (etxt_source_address.isEnabled) {
            val myDialog = MyDialog(
                this,
                getString(R.string.No_worriess_goOffline),
                getString(R.string.Please_Try_Again),
                "\uE828",
                this@MainNewActivity
            )
            myDialog.show()
            myDialog.setCancelable(false)
            online_driver_count.visibility = View.GONE

        }
    }

    private fun bookingonClickFunction() {
        txt_confirm_booking.performClick()
    }

    private fun sucessfullFinish() {
        unlockDrawer()
        linear_source.visibility = View.GONE
        assert(lay_taxi != null)
        lay_taxi.visibility = View.VISIBLE
        assert(lay_driver_profile != null)
        lay_driver_profile.visibility = View.GONE
        assert(live_car_details != null)
        live_car_details.visibility = View.GONE
        assert(etxt_source_address != null)
        etxt_source_address.isEnabled = true
        assert(etxt_des_address != null)
        etxt_des_address.isEnabled = true
        assert(txt_scheduler != null)
        txt_scheduler.isEnabled = true
        assert(txt_for_oth != null)
        txt_for_oth.isEnabled = true
        assert(txt_sor_close != null)
        txt_sor_close.isEnabled = true
        assert(txt_des_close != null)
        assert(txt_dess_close != null)
        txt_des_close.isEnabled = true
        txt_dess_close.isEnabled = true
        on_line_dr.visibility = View.VISIBLE
        assert(lay_sor_des != null)
        lay_sor_des.visibility = View.VISIBLE
        etxt_source_address.text = ""
        etxt_des_address.text = ""
        userRepository?.saveLocalStatus("")
        Tracking.removeMessages(0)
        if (runnable != null) {
            Tracking.removeCallbacks(runnable!!)
        }
        if (listenerRegistration != null) {
            listenerRegistration.remove()
        }
        finish()
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    override fun setupView(savedInstanceState: Bundle?) {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        if (viewModel.userRepository.getMobileNUmber()
                .isNullOrEmpty() || viewModel.userRepository.getMobileNUmber().equals("null")
            || viewModel.userRepository.getEmail()
                .isNullOrEmpty() || viewModel.userRepository.getEmail().equals("null")
        ) {
            val intent = Intent(this, ProfileEditActivity::class.java)
            intent.putExtra("MobileNumber", viewModel.userRepository.getMobileNUmber())
            startActivity(intent)
            finish()
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getLocationUpdates()
        getBaseDistance()
        val sourceaddresnew = ""
        turnGPSOn()

        val mDrawerToggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
            this,
            drawer_view,
            toolbar1,
            R.string.open_drawer,
            R.string.close_drawer
        ) {
            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                setTotalAmount()
            }
        }

        drawer_view.addDrawerListener(mDrawerToggle)
        mDrawerToggle.syncState()
        setupNavMenu()


        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.fragmentDriver_map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)


        viewModel.onUserDetail()
        getCustomerCareNumber()
        if (userRepository?.getFbLoginstatus().equals("LOGIN") || viewModel.getGmailLoginStatus()
                .equals("LOGIN")
        ) {
            viewModel.onUserDetaiImage()
        }
        viewModel.onUserDetaiImage()
        val destinationAutoComplete =
            supportFragmentManager.findFragmentById(R.id.etxt_des_addressnew) as AutocompleteSupportFragment?
        val sourceAutoComplete =
            supportFragmentManager.findFragmentById(R.id.etxt_source_addressnew) as AutocompleteSupportFragment?

        sourceAutoComplete!!.setPlaceFields(
            Arrays.asList(
                Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG
            )
        )
        sourceAutoComplete.setHint(resources.getString(R.string.Please_Enter_Your_Source))
        sourceAutoComplete.setText(sourceaddresnew)

        sourceAutoComplete.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                place.latLng
                pickUpTextView.text = place.address
                sourceAutoComplete.setText(place.address)
                mySourceId = place.id!!
                viewModel.setSource_Place_ID(mySourceId)
                sourcePlace = place
                sourceaddress = place.address?.toString().toString()
            }

            override fun onError(status: Status) {}
        })
        destinationAutoComplete!!.setPlaceFields(
            listOf(
                Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG
            )
        )
        destinationAutoComplete
        destinationAutoComplete.setHint(resources.getString(R.string.Please_Enter_Your_Destination))
        destinationAutoComplete.setOnPlaceSelectedListener(object : PlaceSelectionListener {

            override fun onPlaceSelected(place: Place) {
                etxt_des_address.text = place.address
                destinationaddress = place.address!!.toString()
                viewModel.saveDestination(destinationaddress)
                destinationPlaceId = place.id!!
                desPlace = place

                if (desPlace != null) {
                    viewModel.saveSearchDestinationLatLang(
                        desPlace!!.latLng?.latitude!!,
                        desPlace!!.latLng?.longitude!!
                    )
                }
                if (mySourceId.isNotEmpty() && sourcePlace != null) {
                    getTotalKm()
                    GoogleMapsPath(
                        this@MainNewActivity,
                        mMap,
                        sourcePlace?.latLng,
                        desPlace!!.latLng
                    )
                    sourceaddress = sourcePlace!!.address!!.toString()
                } else {
                    val address = getAddress(this@MainNewActivity)
                    GoogleMapsPath(
                        this@MainNewActivity,
                        mMap,
                        currentLocationUser?.lastLocation?.latitude?.let { it1 ->
                            currentLocationUser?.lastLocation?.longitude?.let { it2 ->
                                LatLng(
                                    it1,
                                    it2
                                )
                            }
                        },
                        place.latLng
                    )
                    findDistance(place)
                    getPlaceIdRequest(
                        currentLocationUser?.lastLocation?.latitude.toString(),
                        currentLocationUser?.lastLocation?.longitude.toString()
                    )
                }
                spin_kit.visibility = View.VISIBLE
                txt_dess_close.visibility = View.GONE
                val toast: Toast = Toast.makeText(
                    applicationContext,
                    "SEARCHING VEHICLES",
                    Toast.LENGTH_LONG
                )
                val v = toast.view?.findViewById(android.R.id.message) as? TextView
                if (v != null) {
                    v.setTextColor(Color.BLACK)
                    v.setBackgroundColor(Color.YELLOW)
                    toast.show()
                }
                carPositionCoupon.clear()

            }

            override fun onError(status: Status) {}
        })
        assert(etxt_source_address != null)
        etxt_source_address.setOnClickListener { view: View? ->

        }
        assert(etxt_des_address != null)
        viewModel.onViewInitialized()
    }
    private fun getLocationGpsStatus() {
        locationRequestgps = LocationRequest()
        locationRequestgps.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequestgps.interval = 5000
        locationRequestgps.fastestInterval = 2000

        val locationResultSetting: LocationSettingsRequest.Builder =
            LocationSettingsRequest.Builder().addLocationRequest(locationRequestgps)
        locationResultSetting.setAlwaysShow(true)

        val result: com.google.android.gms.tasks.Task<LocationSettingsResponse> =
            LocationServices.getSettingsClient(applicationContext)
                .checkLocationSettings(locationResultSetting.build())

        result.addOnCompleteListener {
            try {
                it.getResult(ApiException::class.java)
                Toast.makeText(this, " GPS IS ON ", Toast.LENGTH_SHORT).show()
            } catch (e: ApiException) {
                when (e.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        try {
                            val resolvableApiException: ResolvableApiException =
                                e as ResolvableApiException
                            resolvableApiException.startResolutionForResult(this, 1001)
                        } catch (intentSender: SendIntentException) {

                        }
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {

                    }
                }
            }
        }
    }

    private fun checkGps(): Boolean {
        return locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun getTotalKm() {
        LoadGoogleMapsRoadPointsTask().execute(
            mySourceId,
            destinationPlaceId

        )
    }

    private fun getTotalKmDriverTracking() {
        if (!viewModel.getSourcePlaceid().isNullOrEmpty()) {
            LoadGoogleMapsRoadPointsTask().execute(
                viewModel.getSourcePlaceid(),
                driverTrackingId
            )
        }
    }

    fun bookingApi(searchDistance: String) {
        val distance=  if (searchDistance.isEmpty()) 0 else searchDistance.toDouble().toInt()
        var category: String
        category = if (viewModel.getCategory().isNullOrEmpty()) {
            "string"
        } else {
            if (mySourceId == "ChIJb0qPdIIICDsRWISWXeTrsjA" || destinationPlaceId == "ChIJb0qPdIIICDsRWISWXeTrsjA") {
                "AIRPORT"
            } else if (mySourceId == "ChIJ3yaItv5OpjsReigCbYGd8qw" || destinationPlaceId == "ChIJ3yaItv5OpjsReigCbYGd8qw") {
                "AIRPORT"
            } else if (mySourceId == "ChIJB7Gj3MsxpDsRexuP4_63cqc" || destinationPlaceId == "ChIJB7Gj3MsxpDsRexuP4_63cqc") {
                "AIRPORT"
            } else if (mySourceId == "ChIJlbKEE4C7BTsR92Udc6T1WR0" || destinationPlaceId == "ChIJlbKEE4C7BTsR92Udc6T1WR0") {
                "AIRPORT"
            } else {
                viewModel.getCategory().toString()
            }
        }
        if (viewModel.getCategory().toString() == "TAXI") {
            val baseDistance = viewModel.getBaseDistance()
            if (baseDistance.isNotEmpty() && searchDistance.toDouble().toInt() >= baseDistance.toInt()) {
                category = "OUTSTATION"
            }
        }
        if (viewModel.getCategory().toString() == "OUTSTATION") {
            val baseDistance = viewModel.getBaseDistance()
            if (baseDistance.isNotEmpty() && searchDistance.toDouble().toInt() < baseDistance.toInt()) {
                    category = "TAXI"
            }
        }

        val latitude: Double
        val longitude: Double
        if (sourcePlace != null) {
            latitude = sourcePlace!!.latLng!!.latitude
            longitude = sourcePlace!!.latLng!!.longitude
        } else {
            latitude = currentLocationUser?.lastLocation?.latitude!!
            longitude = currentLocationUser?.lastLocation?.longitude!!
        }

        val bookingRequest = AdvanceSeachApiRequest(
            distance,
            distance,
            latitude,
            destinationaddress,
            "string",
            "string",
            "string",
            0,
            "string",
            "string",
            category,
            "kerala",
            longitude,
            "KERALACABS"
        )
        txt_dess_close.visibility = View.GONE
        viewModel.saveCategeory(category)
        viewModel.searchTaxies(bookingRequest)
    }

    private fun getBaseDistance() {
        var baseDistance: String
        FirebaseFirestore.getInstance().collection("base_km").document("base_km").get()
            .addOnSuccessListener { documentSnapshot ->
                baseDistance = documentSnapshot.getString("base_km").toString()
                viewModel.saveBasekm(baseDistance)
            }.addOnFailureListener {
            }

    }

    private fun getAddress(context: Context): String {

        var place = ""
        if (sourceLatitude.isNotEmpty() && sourceLangtitude.isNotEmpty()) {
            place = AppUtils.latLongToAddress(
                sourceLatitude.toDouble(),
                sourceLangtitude.toDouble(),
                context,
                is_address
            ).toString()
            sourceaddress = place
            pickUpTextView.text = sourceaddress
            if (place.isNotEmpty()) {
                viewModel.getSourceid(sourceLatitude, sourceLangtitude, place)
            }
        }

        return place
    }

    @SuppressLint("LogNotTimber")
    private fun findDistance(place: Place) {
        var startLocation: Location? = null
        var endLocation: Location? = null
        if (currentLocationUser?.lastLocation != null) {
            startLocation = Location("startLocation")
            startLocation.longitude = currentLocationUser?.lastLocation?.longitude!!
            startLocation.latitude = currentLocationUser?.lastLocation?.latitude!!
        }
        if (place.latLng != null) {
            endLocation = Location("endLocation")
            endLocation.longitude = place.latLng?.longitude!!
            endLocation.latitude = place.latLng?.latitude!!
        }
        if (startLocation != null && endLocation != null) {
            totalDistance = (round(startLocation.distanceTo(endLocation) / 1000)).toString()
        }

    }
    
    @SuppressLint("ClickableViewAccessibility")
    private fun openBottomSheet(advanceSearchdata: List<AdvanceSearhData?>?) {
        spin_kit.visibility = View.GONE
        lockDrawer()
        txt_dess_close.visibility = View.VISIBLE
        pickUpDropLayout.visibility = View.GONE
        if (!advanceSearchdata.isNullOrEmpty()) {
            myCoupon = ""
            bottomSheetBehavior = null
            rvCabs.adapter = null
            lay_sor_des.visibility = View.GONE
            bottomSheet.visibility = View.VISIBLE
            tvEndDest.text = etxt_des_address.text.toString()
            bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior?.isDraggable = true
            bottomSheetBehavior?.isHideable = false
            if (peek_hight == 0) {
                peek_hight = BottomSheetBehavior.from(bottomSheet).peekHeight
            }
            if (bottomSheetBehavior?.peekHeight == 0) {
                bottomSheetBehavior?.peekHeight = peek_hight
            }
            showAvailableVehicleViaDest()
            txt_coupon.setOnClickListener {
                val myCoupon = MyCoupon(this, this@MainNewActivity)
                myCoupon.show()
                myCoupon.setCancelable(false)
                setTotalAmount()
            }
            bottomSheetBehavior?.setBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    when (newState) {
                        BottomSheetBehavior.STATE_COLLAPSED -> {
                            cvBookingTime.visibility = View.GONE
                            lay_sor_des.visibility = View.VISIBLE
                            rvBooking.visibility = View.GONE
                        }
                        BottomSheetBehavior.PEEK_HEIGHT_AUTO -> {
                            cvBookingTime.visibility = View.VISIBLE
                        }
                        BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                            cvBookingTime.visibility = View.VISIBLE
                        }

                    }
                }

                override fun onSlide(bottomSheet: View, slideOffset: Float) {

                    if (slideOffset == 0.2f) {
                        lay_sor_des.visibility = View.VISIBLE
                        rvBooking.visibility = View.GONE

                    }
                    if (slideOffset > 0.5f) {
                        cvBookingTime.visibility = View.VISIBLE
                    }
                }

            })
            cvBookingTime.setOnClickListener {
                lay_sor_des.visibility = View.VISIBLE
                rvBooking.visibility = View.GONE
                mMap?.clear()
                val latitude = currentLocationUser?.lastLocation?.latitude
                val longitude = currentLocationUser?.lastLocation?.longitude
                if (latitude != null && longitude != null) {
                    mMap?.addMarker(
                        MarkerOptions().position(LatLng(latitude, longitude)).snippet("Mani").icon(
                            BitmapDescriptorFactory.defaultMarker(
                                BitmapDescriptorFactory.HUE_BLUE
                            )
                        )
                    )
                }


                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            rvCabs.setOnTouchListener { v, event ->
                v.parent.requestDisallowInterceptTouchEvent(true)
                v.onTouchEvent(event);
                true
            }
            rvCabs.isNestedScrollingEnabled = true
            var carPrice = ""
            var carTypeCoupon = ""
            if (checkcoupon && couponCarType.isNotEmpty()) {
                carPrice = couponCarPrice
                carTypeCoupon = couponCarType
            } else {
                carPrice = ""
                carTypeCoupon = ""
            }
            rvCabs.adapter = BookingListAdapter(
                advanceSearchdata,
                currentLocationUser,
                desPlace,
                totalDistanceSearch,
                carTypeCoupon,
                carPrice,
                object : ListSelectionCallback {
                    override fun onSelection(
                        advanceSearhData: List<AdvanceSearhData>,
                        position: Int
                    ) {
                        var carType: String = ""
                        car_price = driverListType?.getDriverLists(position)?.get(0)?.price
                        carPosition = position
                        lay_sor_des.visibility = View.VISIBLE
                        if (advanceSearhData[0].isSelcted)
                            rvBooking.visibility = View.VISIBLE
                        else rvBooking.visibility = View.GONE
                        txt_confirm_booking.setOnClickListener {
                            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
                            when (position) {
                                0 ->
                                    carType = "HatchBack"
                                1 ->
                                    carType = "Sedan"
                                2 ->
                                    carType = "Suv"
                                3 ->
                                    carType = "Auto"
                                4 ->
                                    carType = "Transport"
                                5 ->
                                    carType = "Bike"
                                6 ->
                                    carType = "Ambulance"
                            }
                            var sourceAddress = ""
                            if (sourcePlace != null) {
                                sourceAddress = sourceaddress
                            } else {
                                sourceAddress = getCompleteAddressString(
                                    currentLocationUser?.lastLocation?.latitude!!,
                                    currentLocationUser?.lastLocation?.longitude!!
                                ).toString()
                            }
                            val rideFinishCustomAlertDialog = RideFinishCustomAlertDialog(
                                this@MainNewActivity,
                                sourceAddress,
                                destinationaddress,
//                                totalDistance,
                                totalDistanceSearch,
                                "200",
                                "request_order",
                                carType,
                                txt_payment_type.text.toString(),
                                this@MainNewActivity
                            )
                            rideFinishCustomAlertDialog.show()
                            rideFinishCustomAlertDialog.setCancelable(false)
                            rideFinishCustomAlertDialog.btn_yes.setOnClickListener {
                                bottomSheetBehavior?.peekHeight = 0
                                rideFinishCustomAlertDialog.dismiss()
                                txt_dess_close.visibility = View.GONE
                                search_booking.visibility = View.VISIBLE
                                val toast: Toast = Toast.makeText(
                                    applicationContext,
                                    "SEARCHING DRIVERS",
                                    Toast.LENGTH_LONG
                                )
                                val v = toast.view?.findViewById(android.R.id.message) as? TextView
                                if (v != null) {
                                    v.setTextColor(Color.BLACK)
                                    v.setBackgroundColor(Color.YELLOW)
                                    toast.show()
                                }
                                viewModel.setRIDE_KEY_LOCAL("")
                                ridekeyLocal = viewModel.getRidekey()
                                confirm_btn_Function(advanceSearhData, position)
                            }
                            rideFinishCustomAlertDialog.btn_later.setOnClickListener {
                                rideFinishCustomAlertDialog.dismiss()
                                lay_sor_des.visibility = View.VISIBLE
                                openBottomSheet(advanceSearchdata)
                            }

                        }

                    }
                })

            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HALF_EXPANDED


        } else {
            lay_sor_des.visibility = View.VISIBLE
            Toast.makeText(this, "No cabs available", Toast.LENGTH_LONG).show()
        }


    }

    private fun confirm_btn_Function(advanceSearhData: List<AdvanceSearhData>, position: Int) {
        viewModel.saveSource(sourceaddress)
        viewModel.saveDestination(destinationaddress)
        oNLINE_DriverList.clear()
        viewModel.driverAcceptStatus(false)
        oFFLINE_DriverList_taxi.clear()
        onsuccess_driver(0, driverListType?.getDriverLists(position), "")
        if (viewModel.getRIDE_KEY_LOCAL().equals(null) || viewModel.getRIDE_KEY_LOCAL().toString()
                .equals(
                    ""
                )
        ) {
            viewModel.setRIDE_KEY_LOCAL(ridekeyLocal.toString())
        }
        FireStoreCloud.cloud_ride_key(ridekeyLocal.toString(), db)
    }


    fun onsuccess_driver(i: Int, driverList_taxiX1: List<AdvanceSearhData>?, coupon: String) {

        online_driver_count.visibility = View.VISIBLE
        val count = oNLINE_DriverList.size
        online_driver_count_txt.text = "Currently Available   $count  Drivers"

        if (driverList_taxiX1 != null && driverList_taxiX1.size > i) {
            val docRef = db.collection("KERALACABS_DRIVER_STATUS").document(
                java.lang.String.valueOf(
                    driverList_taxiX1[i].driverId
                )
            )
            docRef.get().addOnSuccessListener { documentSnapshot: DocumentSnapshot? ->
                try {
                    if (documentSnapshot == null) {
                        onsuccess_driver(1 + i, driverList_taxiX1, coupon)
                    } else if (documentSnapshot.get((driverList_taxiX1[i].driverId).toString())!! == true) {
                        oNLINE_DriverList.add(driverList_taxiX1[i])
                        onsuccess_driver(1 + i, driverList_taxiX1, coupon)
                    } else if (documentSnapshot.get((driverList_taxiX1[i].driverId).toString())!! != true) {
                        oFFLINE_DriverList_taxi.add(driverList_taxiX1[i])
                        onsuccess_driver(1 + i, driverList_taxiX1, coupon)
                    } else {
                        onsuccess_driver(1 + i, driverList_taxiX1, coupon)
                    }
                } catch (e: java.lang.Exception) {
                    onsuccess_driver(1 + i, driverList_taxiX1, coupon)
                }
            }
        } else {
            if (oNLINE_DriverList.size != 0) {
                if (!myCoupon.isEmpty()) {
                    oNLINE_DriverList.forEach {
                        val coupon_request = CouponBookingRequest(
                            action = "request_order_new",
                            category = viewModel.getCategory(),
//                                category = it.category,
                            coupons = myCoupon,
                            des = destinationaddress,
                            driverId = it.driverId,
                            isAndroid = "true",
                            km = totalDistanceSearch,
                            latitude = currentLocationUser?.lastLocation?.latitude.toString(),
                            longitude = currentLocationUser?.lastLocation?.longitude.toString(),
                            message = "please accept or reject the booking",
                            paymentType = txt_payment_type.text.toString(),
                            phoneNumber = viewModel.userRepository.getMobileNUmber(),
                            price = it.price,
                            rideKeyLocal = ridekeyLocal,
                            source = getCompleteAddressString(
                                currentLocationUser?.lastLocation?.latitude!!,
                                currentLocationUser?.lastLocation?.longitude!!
                            ),
                            title = "BOOKING",
                            token = it.token,
                            topic = "string",
                            tripId = "string",
                            userId = viewModel.userRepository.getCurrentUserID(),
                            userToken = viewModel.userRepository.getUser_FCM_Token()
                        )
                        viewModel.couponBookingApi(coupon_request)
                    }
                    setTimerDismiss()
                } else {
                    var sourceAddress = ""
                    if (sourcePlace != null) {
                        sourceAddress = sourceaddress
                    } else {
                        sourceAddress = getCompleteAddressString(
                            currentLocationUser?.lastLocation?.latitude!!,
                            currentLocationUser?.lastLocation?.longitude!!
                        ).toString()
                    }
                    oNLINE_DriverList.forEach {
                        val request = BookingRequest(
                            action = "request_order_new",
                            isAndroid = "true",
                            message = "please accept or reject the booking",
                            title = "BOOKING",
                            topic = "string",
                            tripId = "string",
                            latitude = currentLocationUser?.lastLocation?.latitude.toString(),
                            longitude = currentLocationUser?.lastLocation?.longitude.toString(),
                            phoneNumber = viewModel.userRepository.getMobileNUmber(),
                            km = totalDistanceSearch,
                            source = sourceAddress,
                            des = destinationaddress,
                            paymentType = txt_payment_type.text.toString(),
                            token = it.token,
                            userId = viewModel.userRepository.getCurrentUserID(),
                            rideKeyLocal = ridekeyLocal, // ride key channge
                            userToken = viewModel.userRepository.getUser_FCM_Token(),
                            coupen = "",
                            driverId = it.driverId,
                            price = it.price,
                            category = viewModel.getCategory()
                        )
                        viewModel.hitBookingApi(request)
                    }
                    setTimerDismiss()
                }
            } else {
                assert(etxt_source_address != null)
                if (etxt_source_address.isEnabled) {

                    checkcoupon = false
                    couponCarPrice = ""
                    search_booking.visibility = View.GONE

                    val myDialog = MyDialog(
                        this,
                        getString(R.string.No_worriess_goOffline),
                        getString(R.string.Please_Try_Again),
                        "\uE828",
                        this@MainNewActivity
                    )
                    myDialog.show()
                    myDialog.setCancelable(false)

                }
            }
        }
    }

    fun show_Payment_Type(view: View?) {

        val items = arrayOf<CharSequence>(
            getString(R.string.Cash),
            getString(R.string.wallet),
            getString(R.string.CARD),
            getString(R.string.upi)
        )

        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.Make_your_selection)
        builder.setItems(items) { dialog: DialogInterface?, item: Int ->
            if (txt_payment_type != null) {
                txt_payment_type.text = items[item]
            }
            if (item == 1) {
                if (wallet_amount.isNotEmpty()) {
                    txt_payment_type.text = items[item]
                    val myWallet = MyWallet(this, car_price.toString(), wallet_amount)
                    myWallet.show()
                    myWallet.setCancelable(false)

                    myWallet.wd_close_cancel.setOnClickListener {
                        myWallet.dismiss()
                    }
                    myWallet.btn_okk.setOnClickListener {
                        myWallet.dismiss()
                        txt_confirm_booking.performClick()
                    }
                } else {
                    Toast.makeText(this, "Please Enable The Wallet", Toast.LENGTH_LONG).show()
                    txt_payment_type.text = items[0]
                }


            }
        }
        val alert = builder.create()
        alert.show()
    }


    fun setupNavMenu() {


        val headerLayout: View = navigation_view.getHeaderView(0)
        mNameTextView = headerLayout.findViewById<TextView>(R.id.text_name)
        mEmailTextView = headerLayout.findViewById<TextView>(R.id.text_email)
        wallet_balance = headerLayout.findViewById<TextView>(R.id.ext_tot_amount)
        credit_amount = headerLayout.findViewById<TextView>(R.id.ext_last_recharge)
        val prof_header: RoundedImageView = headerLayout.findViewById(R.id.prof_header)

//        setProfileImage(prof_header);


        val profileEditTextView =
            headerLayout.findViewById<TextView>(R.id.profile_edit_icon)

        navigation_view.setNavigationItemSelectedListener { item ->
            drawer_view.closeDrawer(GravityCompat.START)
            when (item.getItemId()) {
                R.id.nav_item_my_trips -> {
                    if (AppUtils.isNetworkConnected(this))
                        startActivity(Intent(applicationContext, TripHistoryActivity::class.java))
                    true
                }
                R.id.nav_item_favorite -> {
                    Toaster.show(this, "Coming Soon...")
                    true
                }
                R.id.nav_item_setting -> {
                    startActivity(Intent(applicationContext, SettingActivity::class.java))
                    true
                }
                R.id.nav_item_help -> {
                    showFilterDialog()
                    true
                }
                R.id.nav_item_wallet -> {
                    showPromocodeDialogs()
                    true
                }
                R.id.nav_item_rate_us -> {
                    if (AppUtils.isNetworkConnected(this))
                        startActivity(Intent(applicationContext, RateUsActivity::class.java))
                    true
                }
                R.id.nav_item_feed -> {
                    if (AppUtils.isNetworkConnected(this))
                        startActivity(Intent(applicationContext, FeedbackActivity::class.java))
                    true
                }
                R.id.nav_item_share -> {
                    if (AppUtils.isNetworkConnected(this))
                        share()
                    true
                }
                R.id.nav_item_about -> {
                    if (AppUtils.isNetworkConnected(this))
                        startActivity(Intent(applicationContext, AboutActivity::class.java))
                    true
                }
                R.id.nav_item_logout -> {
                    if (AppUtils.isNetworkConnected(this))
                        alertDialogueLogout()
                    true
                }
                else -> false
            }
        }

        profileEditTextView.setOnClickListener {
            if (AppUtils.isNetworkConnected(this))
                startActivity(Intent(applicationContext, ProfileEditActivity::class.java))
        }
    }

    private fun alertDialogueLogout() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setTitle(getString(R.string.want_logout_dialogue_title))
        builder.setPositiveButton(getString(R.string.yes)) { dialog, which ->
            if (AppUtils.isNetworkConnected(this))
                viewModel.doLoginStatus()
        }
        builder.setNegativeButton(getString(R.string.no)) { dialog, which -> dialog.dismiss() }
        builder.show()
    }

    fun getPlaceIdRequest(lat: String?, lon: String?) {

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        /*https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=cruise&key=AIzaSyCCYN_PMPGFD67vZS1YRwWujFs-SlN4L_E*/
/*
        val requestFormat = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&rankby=50&radius=500&key=%s"
        val uriBuilder = Uri.parse(requestFormat).buildUpon()
        val googlePlaciIDApiUrl = URL(uriBuilder.build().toString())
        val googleDirectionsApiResponse: String = ServerFasade.httpPost(googlePlaciIDApiUrl, null, null)
        val directionJson: JSONObject = ServerFasade.makeJsonFromResponse(googleDirectionsApiResponse, JSONObject::class.java)
        val place_id = directionJson.getJSONArray("results").getJSONObject(0)["place_id"].toString()
*/
//        GeocodeSync(lat, lon!!)
    }

    @Throws(IOException::class, InterruptedException::class)
    fun GeocodeSync(lat: String?, lng: String): String {
        val url =
            URL("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&key=AIzaSyCbgtbU4_Ylij6aQaAv9RmZJO_9vKNZUmE")

        with(url.openConnection() as HttpURLConnection) {
            requestMethod = "POST"  // optional default is GET

            println("\nSent 'POST' request to URL : $url; Response Code : $responseCode")
            val jsonParser = JsonParser()
            val jsonObject = jsonParser.parse(
                InputStreamReader(inputStream, "UTF-8")
            )
            sourcePlaceId =
                jsonObject.asJsonObject["results"].asJsonArray[0].asJsonObject["place_id"].toString()
            Log.e(
                "Json",
                "${jsonObject.asJsonObject["results"].asJsonArray[0].asJsonObject["place_id"]}"
            )
//                inputStream.bufferedReader().use {
//                    it.lines().forEach { line ->
//                        println(line)
//                    }
//                }
        }
        return ""
    }

    fun setProfileImage(prof_header: RoundedImageView) {

        val userId = viewModel.getUserID()
        val image_type = "PROFILE"
        val bese_url = Prefs.getString("base_url", BASE_URL)

        val id = Prefs.getString("user_id", "")
        val type = Prefs.getString("type", "")

        GlideApp.with(this) //please change this later with constant
            .load(bese_url + Endpoints.GET_IMAGE_BY_ID + image_type + "/" + userId)
            .diskCacheStrategy(DiskCacheStrategy.NONE) //here you can add a default image drawable
            .error(getDrawable(R.drawable.userr))
            .skipMemoryCache(true)
            .into(prof_header)
    }

    private fun getCompleteAddressString(LATITUDE: Double, LONGITUDE: Double): String? {
        var strAdd = ""
        val geocoder = Geocoder(this, Locale.getDefault())
        try {
            val addresses: List<Address>? = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
            if (addresses != null) {
                val returnedAddress: Address = addresses[0]
                val strReturnedAddress = StringBuilder("")
                for (i in 0..returnedAddress.getMaxAddressLineIndex()) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
                }
                strAdd = strReturnedAddress.toString()
                sourceaddress = strAdd
                viewModel.saveSource(strAdd)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return strAdd
    }

    private fun getLocationUpdates() {
        try {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            locationRequest = LocationRequest()
            locationRequest.interval = 50000
            locationRequest.fastestInterval = 50000
            locationRequest.smallestDisplacement = 170f // 170 m = 0.1 mile
            locationRequest.priority =
                LocationRequest.PRIORITY_HIGH_ACCURACY //set according to your app function
            Log.e("outsideCallback::::", "$fusedLocationClient")

            if (Settings.Secure.getString(getContentResolver(), Settings.Secure.ALLOW_MOCK_LOCATION)
                    .equals("0")
            )

                try {

                    locationCallback = object : LocationCallback() {
                        override fun onLocationResult(locationResult: LocationResult) {
                            currentLocationUser = locationResult
                            sourceLatitude = locationResult.lastLocation?.latitude.toString()
                            sourceLangtitude = locationResult.lastLocation?.longitude.toString()
                            val location = LatLng(
                                locationResult.lastLocation!!.latitude,
                                locationResult.lastLocation!!.longitude
                            )
                            if (mMap != null) {
                                val zoomLevel = 16.0f //This goes up to 21
                                mMap!!.moveCamera(
                                    CameraUpdateFactory.newLatLngZoom(
                                        location,
                                        zoomLevel
                                    )
                                )
                                mMap?.isBuildingsEnabled = true
                                mMap?.addMarker(
                                    MarkerOptions()
                                        .position(location)
                                        .snippet("Mani")
                                        .icon(
                                            BitmapDescriptorFactory.fromResource(R.drawable.location_user)


                                        )
                                )
                            }
                            locationResult ?: return
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                startForegroundService(
                                    Intent(
                                        applicationContext,
                                        LocationTrackingService::class.java
                                    )
                                )
                            } else {
                                startService(
                                    Intent(
                                        applicationContext,
                                        LocationTrackingService::class.java
                                    )
                                )
                            }
                            if (locationResult.locations.isNotEmpty()) {
                                val location =
                                    locationResult.lastLocation
                                if (location != null) {
                                    setIsLatfinal(location.latitude)
                                    setIsLongfinal(location.longitude)
                                    val intent =
                                        Intent(
                                            applicationContext,
                                            LocationTrackingService::class.java
                                        )
                                    startService(intent)
                                }


                            }
                            getAvailableVehicleAround(locationResult)
                        }
                    }
                } catch (e: Exception) {
                }

        } catch (e: Exception) {
            getLocationUpdates()
        }
    }


    private fun setUserLocationMarker(location: Location) {
        val latLng = LatLng(location.latitude, location.longitude)
        if (userLocationMarker == null) {
            val markerOptions = MarkerOptions()
            markerOptions.position(latLng).title(sourceaddress)
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car))
            markerOptions.rotation(location.bearing)
            markerOptions.anchor(0.5.toFloat(), 0.5.toFloat())
            userLocationMarker = mMap!!.addMarker(markerOptions)
            mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f))
            val latitude: Double = location.latitude
            val longitude: Double = location.longitude
            var distance = 0F
            val crntLocation = Location("crntlocation")
            crntLocation.latitude = location.latitude
            crntLocation.longitude = location.longitude

            val newLocation = Location("newlocation")
            newLocation.latitude = latitude
            newLocation.longitude = longitude
            distance = crntLocation.distanceTo(newLocation) / 1000


            val latLng = LatLng(newLocation.latitude, newLocation.longitude)
            markerOptions.position(latLng).title(sourceaddress)
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car))
            markerOptions.rotation(location.bearing)
            markerOptions.anchor(0.5.toFloat(), 0.5.toFloat())
            userLocationMarker = mMap!!.addMarker(markerOptions)


        } else {
            userLocationMarker!!.setPosition(latLng)
            userLocationMarker!!.rotation = location.bearing
        }
        return if (userLocationAccuracyCircle == null) {
            val circleOptions = CircleOptions()
            circleOptions.center(latLng)
            circleOptions.strokeWidth(4f)
            circleOptions.strokeColor(Color.argb(255, 0, 0, 255))
            circleOptions.fillColor(Color.argb(32, 0, 0, 255))
            circleOptions.radius(location.accuracy.toDouble())
            userLocationAccuracyCircle = mMap!!.addCircle(circleOptions)
        } else {
            userLocationAccuracyCircle!!.center = latLng
            userLocationAccuracyCircle!!.radius = location.accuracy.toDouble()
        }


    }


    //start location updates
    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper() /* Looper */
        )
    }

    // stop location updates
    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    override fun onBackPressed() {
        if (runnable != null) {
            handler.removeCallbacks(runnable!!)
        }
        alertDialogue()
    }


    private fun share() {
        val appPackageName: String =
            applicationContext.getPackageName() // getPackageName() from Context or Activity object

        try {
            val i = Intent(Intent.ACTION_SEND)
            i.type = "text/plain"
            i.putExtra(Intent.EXTRA_SUBJECT, "Taxideals")
            var sAux = "\nLet me recommend you this application\n\n"
            sAux = sAux + "https://play.google.com/store/apps/details?id=" + appPackageName
            i.putExtra(Intent.EXTRA_TEXT, sAux)
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val new_intent = Intent.createChooser(i, "Choose one")
            startActivity(new_intent)

        } catch (e: Exception) {
            Log.e(TAG, "share: " + e.toString())
        }
    }

    private fun alertDialogue() {
        val alertDialogue =
            androidx.appcompat.app.AlertDialog.Builder(this)
        alertDialogue.setTitle(getString(R.string.close_this_screen))
        alertDialogue.setCancelable(false)
        alertDialogue.setPositiveButton(getString(R.string.yes)) { dialog, which ->
            if (runnable != null) {
                handler.removeCallbacks(runnable!!)
            }
            finish()
        }
        alertDialogue.setNegativeButton(getString(R.string.no)) { dialog, which ->
            dialog.dismiss()
        }
        alertDialogue.show()
    }

    private fun showFilterDialog() {

        val dialog = MaterialDialog(this)
            .noAutoDismiss()
            .customView(R.layout.layout_filter)


        dialog.findViewById<CardView>(R.id.twitter_link).setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/KeralaCabs2?s=09")
                )
            )
        }
        dialog.findViewById<CardView>(R.id.facebook_link).setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/kerala.cabs.31")
                )
            )

        }
        dialog.findViewById<CardView>(R.id.phone_link).setOnClickListener {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:9539735313")

            if (ContextCompat.checkSelfPermission(
                    applicationContext,
                    Manifest.permission.CALL_PHONE
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.CALL_PHONE),
                    1
                )
            } else {
                startActivity(callIntent)
            }
        }
        dialog.findViewById<CardView>(R.id.mail_link).setOnClickListener {
            try {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("mailto:" + "contact@keralacabs01")
                )
                intent.putExtra(Intent.EXTRA_SUBJECT, R.string.Help_What_you_need_from_Us)
                intent.putExtra(Intent.EXTRA_TEXT, R.string.Hi)
                if (intent.resolveActivity(
                        applicationContext.getPackageManager()
                    ) != null
                ) {
                    startActivity(intent)
                }
            } catch (e: ActivityNotFoundException) {
            }
        }
        dialog.findViewById<CardView>(R.id.website_link).setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://kerala-cabs.ueniweb.com")
                )
            )

        }

        dialog.findViewById<Button>(R.id.button).setOnClickListener {
            dialog.dismiss()
        }


        dialog.show()
    }

    private fun showPromocodeDialogs() {

        if (check_Wallet == "false" && check_Wallet.isNotEmpty()) {
            val dialog = MaterialDialog(this)
                .noAutoDismiss()
                .customView(R.layout.activity_wallets)

            dialog.show()
            dialog.setCancelable(false)

            dialog.findViewById<Button>(R.id.promo_cancel_button).setOnClickListener {
                dialog.dismiss()
            }
            dialog.findViewById<Button>(R.id.promo_apply_button).setOnClickListener {
                dialog.dismiss()

                val promocodetext = dialog.findViewById<TextInputEditText>(R.id.promo)

                dialog.dismiss()
                hideKeyboard(this)

                val promocode: String = promocodetext.text.toString()

                if (promocode.trim().isEmpty() || promocode.equals(null)) {
                    Toast.makeText(this, "Please Enter a Valid PromoCode", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    viewModel.onUserTokencheck(promocode)
                }
            }
        } else {
            Toast.makeText(this, "You Have Already Enter Your Wallet CODE", Toast.LENGTH_SHORT)
                .show()
        }


    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        if (activity.currentFocus != null) {
            val view: View = activity.currentFocus!!
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            /* if (view == null) {
                 view = View(activity)
             }*/
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

    }

    fun backgroundBooking(fcmCustomizeNotificationData: FcmCustomizeNotificationData) {
        viewModel.setRIDE_KEY_LOCAL(fcmCustomizeNotificationData.RIDE_KEY_LOCAL.toString())
        viewModel.saveLocalStatus(ACCEPT_ORDER)
        fcmCustomizeNotificationData_background = fcmCustomizeNotificationData
    }

    override fun onResume() {
        super.onResume()
        getTokenUpdate()
        setIsMainNewActivty(this)
        startLocationUpdates()
        registerLocalReceiver()
//        isShowMainActivityAlive=true
        setIsMainActivityAlive(true)
        if (intent.extras != null && intent.getSerializableExtra(CUSTOM_NOTIFICATION_DATA) != null) {
            val notificationData: FcmCustomizeNotificationData = intent.getSerializableExtra(
                CUSTOM_NOTIFICATION_DATA
            ) as FcmCustomizeNotificationData
            onReceiveNotification(notificationData.action, notificationData)
            intent.removeExtra(CUSTOM_NOTIFICATION_DATA)
        }
        if (viewModel.getOnPauseState() && viewModel.onPausegetBooking()) {
            if (viewModel.getIsDriverAccept()) {
                if (!fcmCustomizeNotificationData_background.action.isNullOrEmpty()) {
                    onReceiveNotification(
                        fcmCustomizeNotificationData_background.action,
                        fcmCustomizeNotificationData_background
                    )
                }
            } else {
                tryAgainPopUp()
            }
        } else {
            if (viewModel.getIsDriverAccept()) {
                if (!fcmCustomizeNotificationData_background.action.isNullOrEmpty()) {
                    onReceiveNotification(
                        fcmCustomizeNotificationData_background.action,
                        fcmCustomizeNotificationData_background
                    )
                }
            }
        }
        viewModel.saveOnPauseState(false)
    }

    @SuppressLint("Assert")
    private fun onReceiveNotification(
        action: String?,
        notificationData: FcmCustomizeNotificationData
    ) {
        val paymentType: String
        val builder = AlertDialog.Builder(this)
        card_view.visibility = View.GONE
        carPositionCoupon.clear()
        online_driver_count.visibility = View.GONE
        search_booking.visibility = View.GONE
        if (showAvilableVehicle != null) {
            showAvilableVehicle!!.clear()
        }
        when (action) {
            ACCEPT_ORDER -> {
                if (viewModel.getLocalStataus()?.equals("")!!) {
//                    return
                }
                lockDrawer()
                viewModel.stopTimer()
                driverphotoAccept(notificationData.userId)
                viewModel.saveLocalStatus(LOCAL_ACCEPT_RIDE)
                if (oNLINE_DriverList.size == 0) {
                    viewModel.setClient_FCM_Token(notificationData.driverFcmToken!!)
                    userRepository?.setIs_Android(false)
                    if (!viewModel.getSource()!!.isEmpty() || viewModel.getSource().toString()
                            .equals(
                                ""
                            )
                    ) {
                        viewModel.saveSource(notificationData.source.toString())
                        viewModel.saveDestination(notificationData.destination.toString())
                    }
                    viewModel.saveCilentId(notificationData.userId)
                    dR_mobile_num = notificationData.phoneNumber.toString()
                    if (notificationData.taxiNumber.toString().equals("")) {
                        notificationData.taxiNumber = "NONUMBER"
                    }
                    if (notificationData.taxiNumber != null) {
                        val value: Int = notificationData.taxiNumber!!.length / 2
                        val taxi_n: Array<String> = splitToNChar(
                            notificationData.taxiNumber!!,
                            value
                        )
                        assert(txt_taxi_code != null)
                        txt_taxi_code.text = taxi_n[0]
                        assert(txt_taxi_number != null)
                        txt_taxi_number.text = taxi_n[1]
                    }
                    assert(txt_OTP != null)
                    txt_OTP.setText(R.string.more)
                    taxi_name.text = notificationData.vehicleBrand
                    txt_driver_name.text = notificationData.driverName
                } else {
                    for (driver in oNLINE_DriverList.indices) {
                        if (oNLINE_DriverList.get(driver).driverId.equals(notificationData.userId)) {
                            acceptDriver(
                                notificationData.source,
                                notificationData.destination,
                                oNLINE_DriverList.get(driver).token,
                                notificationData.userId,
                                notificationData.taxiNumber,
                                notificationData.vehicleBrand,
                                oNLINE_DriverList.get(driver).phoneNumber,
                                notificationData.isAndroid,
                                notificationData.taxi_type
                            )
                            txt_driver_name.setText(oNLINE_DriverList[driver].name)
                            viewModel.saveClientName(oNLINE_DriverList[driver].name.toString())
                            if (notificationData.taxiNumber.toString().equals("")) {
                                notificationData.taxiNumber = "NONUMBER"
                            }
                            if (notificationData.taxiNumber != null) {
                                val value: Int = notificationData.taxiNumber!!.length / 2
                                val taxi_n: Array<String> = splitToNChar(
                                    notificationData.taxiNumber!!,
                                    value
                                )
                                assert(txt_taxi_code != null)
                                txt_taxi_code.text = taxi_n[0]
                                assert(txt_taxi_number != null)
                                txt_taxi_number.text = taxi_n[1]
                            }
                            assert(txt_OTP != null)
                            txt_OTP.setText(R.string.more)
                            dR_mobile_num = oNLINE_DriverList[0].phoneNumber.toString()
                            assert(taxi_name != null)
                            taxi_name.text = notificationData.vehicleBrand
                        }
                    }

                }

                driver_call.visibility = View.VISIBLE
                viewModel.saveLocalStatus(ACCEPT_ORDER)
                afterAccept()

            }
            LOCAL_START_PICKUP -> {
                driverInformationAfterBooking()
                viewModel.saveLocalStatus(LOCAL_START_PICKUP)

                val myDialog = MyDialog(
                    this, getString(R.string.Hi_I_am_Started_here_be_ready), getString(
                        R.string.Driver_Started_To_PickUP
                    ), "\uf1b9", this@MainNewActivity
                )
                myDialog.show()
                myDialog.setCancelable(false)
                getTrackingDriverInformation()

            }
            LOCAL_START_RIDE -> {
                viewModel.saveLocalStatus(LOCAL_START_RIDE)
                Handler().postDelayed({
                    driverInformationAfterBooking()

                    val myDialog = MyDialog(
                        this, getString(R.string.be_safe_and_enjoy_your_ride), getString(
                            R.string.Ride_Started
                        ), "\uf1b9", this@MainNewActivity
                    )
                    myDialog.show()
                    myDialog.setCancelable(false)
                    driver_call.visibility = View.GONE
                    routeMapSourceDestination()

                }, 1000)
            }
            PICKUP_DONE -> {
                viewModel.saveLocalStatus(PICKUP_DONE)
                Handler().postDelayed({
                    driverInformationAfterBooking()

                    val myDialog = MyDialog(
                        this, getString(R.string.Are_You_in_Shall_We_Start), getString(
                            R.string.PickUP_Done
                        ), "\uf1b9", this@MainNewActivity
                    )
                    myDialog.show()
                    myDialog.setCancelable(false)

                }, 1000)
                MapUtils.getDriverId("")
                routeMapSourceDestination()
                driver_call.visibility = View.VISIBLE
            }
            RIDE_FINISH -> {
                assert(txt_payment_type != null)
                viewModel.saveLocalStatus(RIDE_FINISH)
                driverInformationAfterBooking()
                if (txt_payment_type.text.toString() == getString(R.string.wallet)) {
                    paymentType = getString(R.string.wallet)
                } else if (txt_payment_type.text.toString() == getString(R.string.CARD)) {
                    paymentType = getString(R.string.CARD)
                } else if (txt_payment_type.text.toString() == getString(R.string.upi)) {
                    paymentType = getString(R.string.upi)
                } else {
                    paymentType = getString(R.string.Cash)
                }
                if (lay_sor_des != null) {
                    lay_sor_des.isEnabled = true
                }

                if (paymentType == "Wallet") {
                    val rideFinishCustomAlertDialog = RideFinishCustomAlertDialog(
                        this,
                        viewModel.getSource().toString(),
                        notificationData.destination.toString(),
                        notificationData.rideDistance.toString(),
                        notificationData.rideAmount.toString(),
                        notificationData.action.toString(),
                        notificationData.cancelReason,
                        paymentType,
                        this
                    )
                    rideFinishCustomAlertDialog.show()
                    rideFinishCustomAlertDialog.setCancelable(false)
                } else {
                    val name = viewModel.getUserName().toString()
                    val email = viewModel.getUserEmail().toString()
                    val mobileNumber = viewModel.getUserMobileNumber().toString()
                    val price = notificationData.rideAmount.toString().toDouble()

                    var razpay = RazorPayInfoActivity(
                        viewModel.getSource().toString(),
                        notificationData.destination.toString(),
                        notificationData.rideDistance.toString(),
                        price.toInt(),
                        notificationData.action.toString(),
                        notificationData.cancelReason,
                        paymentType,
                        name,
                        email,
                        mobileNumber,
                        this
                    )
                    val i = Intent(this, RazorPayInfoActivity::class.java)
                    startActivity(i)
                    finish()

                }


            }
            DRIVER_CANCEL_RIDE -> {
                var reason = ""
                if (notificationData.cancelReason.isNotEmpty()) {
                    reason = notificationData.cancelReason
                    val myDialog = MyDialog(
                        this, reason, getString(
                            R.string.Driver_canceled_your_ride
                        ), "\uf1b9", this@MainNewActivity
                    )
                    myDialog.show()
                    myDialog.setCancelable(false)
                } else {
                    val myDialog = MyDialog(
                        this, getString(R.string.Sorry_for_the_inconvenience),
                        getString(R.string.Driver_canceled_your_ride), "\uf1b9",
                        this@MainNewActivity
                    )
                    myDialog.show()
                    myDialog.setCancelable(false)
                }
                viewModel.saveLocalStatus(DRIVER_CANCEL_RIDE)
                driverInformationAfterBooking()


            }
            CANCEL_BY_USER -> {
                assert(txt_payment_type != null)

                if (txt_payment_type.text.toString() == getString(R.string.googlepay)) {
                    paymentType = getString(R.string.googlepay)
                } else {
                    paymentType = getString(R.string.Cash)
                }

                driverInformationAfterBooking()
                val rideFinishCustomAlertDialog = RideFinishCustomAlertDialog(
                    this,
                    getCompleteAddressString(
                        currentLocationUser?.lastLocation?.latitude!!,
                        currentLocationUser?.lastLocation?.longitude!!
                    ).toString(),
                    notificationData.destination.toString(),
                    notificationData.rideDistance.toString(),
                    notificationData.rideAmount.toString(),
                    notificationData.action.toString(),
                    notificationData.cancelReason,
                    paymentType,
                    this
                )
                rideFinishCustomAlertDialog.show()
                rideFinishCustomAlertDialog.setCancelable(false)
            }
        }
    }

    private fun routeMapSourceDestination() {
        val userCurrentLatitude = viewModel.getUserSearchLatitude()
        val userCurrentLangtitude = viewModel.getUserSearchLangitude()
        val userSearchDestinationLatitude = viewModel.getSearchDestinationLatitude()
        val userSearchDestinationLangitude = viewModel.getSearchDestinationLangitude()

        driverTrackingMap?.clear()
        GoogleMapsPath(
            this@MainNewActivity,
            driverTrackingMap,
            LatLng(userCurrentLatitude, userCurrentLangtitude),
            LatLng(userSearchDestinationLatitude, userSearchDestinationLangitude)
        )
        defaultLocation = LatLng(userCurrentLatitude, userCurrentLangtitude)
        showDefaultLocationOnMap(defaultLocation)
    }

    private fun getTrackingDriverInformation() {
        if (driverTrackingMap != null) {
            if (!viewModel.getCilentId().isNullOrEmpty()) {
                MapUtils.getDriverId(viewModel.getCilentId()!!)

                val db = FirebaseFirestore.getInstance()

                val docRef = db.collection("KERALACABS_DRIVER_STATUS")
                    .document(viewModel.getCilentId().toString())
                docRef.get()
                    .addOnSuccessListener { document ->
                        if (document != null) {

                            if (document.data != null) {
                                val latitude = document.data!!["LATITUDE"].toString().toDouble()
                                val longitude = document.data!!["LONGITUDE"].toString().toDouble()
                                onMapDrivertracking(driverTrackingMap, latitude, longitude)
                            }

                        }
                    }
                    .addOnFailureListener { exception ->
                    }

            }

        }
    }

    private fun driverInformationAfterBooking() {
        driverphotoAccept(viewModel.getCilentId().toString())
        txt_driver_name.text = viewModel.getClientName()
        taxi_name.text = viewModel.getVehicleBrand()
        val taxiNumber = "NO NUMBER"
        lockDrawer()
        if (viewModel.getVehiclenumber().toString().equals("") || viewModel.getVehiclenumber()
                .equals(
                    null
                )
        ) {
            val value: Int = taxiNumber.length / 2
            val taxi_n: Array<String> = splitToNChar(
                taxiNumber,
                value
            )
            assert(txt_taxi_code != null)
            txt_taxi_code.text = taxi_n[0]
            assert(txt_taxi_number != null)
            txt_taxi_number.text = taxi_n[1]
        } else {
            val value: Int = viewModel.getVehiclenumber().toString().length / 2
            val taxi_n: Array<String> = splitToNChar(
                viewModel.getVehiclenumber().toString(),
                value
            )
            assert(txt_taxi_code != null)
            txt_taxi_code.text = taxi_n[0]
            assert(txt_taxi_number != null)
            txt_taxi_number.text = taxi_n[1]
        }
    }

    fun removeDriverInformation() {
        viewModel.saveCilentId("")
        viewModel.saveClientName("")
        viewModel.saveVehicle_number("")
        viewModel.saveVehicleBrand("")
        viewModel.saveDriverMobileNumber("")
    }

    private fun acceptDriver(
        source: String?,
        destination: String?,
        token: String?,
        userId: String,
        taxiNumber: String?,
        vehicleBrand: String?,
        phoneNumber: String?,
        android: Boolean?,
        taxiType: String?
    ) {
        if (!source?.isEmpty()!! || source.toString().equals("")) {
            viewModel.saveSource(source.toString())
            viewModel.saveDestination(destination.toString())
        }
        viewModel.setClient_FCM_Token(token.toString())
        userRepository?.setIs_Android(false)
        viewModel.saveCilentId(userId)
        viewModel.saveVehicleBrand(vehicleBrand.toString())
        viewModel.saveVehicle_number(taxiNumber.toString())
        dR_mobile_num = phoneNumber.toString()
        viewModel.saveDriverMobileNumber(phoneNumber.toString())

        taxi_name.text = vehicleBrand

    }

    private fun splitToNChar(taxiNumber: String, value: Int): Array<String> {
        val parts: MutableList<String> = ArrayList()
        val length: Int = taxiNumber.length
        run {
            var i = 0
            while (i < length) {
                parts.add(taxiNumber.substring(i, Math.min(length, i + value)))
                i += value
            }
        }
        return parts.toTypedArray()
    }

    @SuppressLint("Assert")
    private fun afterAccept() {
        assert(lay_taxi != null)
        lay_taxi.visibility = View.GONE
        txt_dess_close.visibility = View.GONE
        assert(lay_driver_profile != null)
        lay_driver_profile.visibility = View.VISIBLE
        spin_kit.visibility = View.GONE
        val toast: Toast = Toast.makeText(
            applicationContext,
            "DRIVER ACCEPETED YOUR RIDE",
            Toast.LENGTH_LONG
        )
        val v = toast.view?.findViewById(android.R.id.message) as? TextView
        if (v != null) {
            v.setTextColor(Color.BLACK)
            v.setBackgroundColor(Color.YELLOW)
            toast.show()
        }
        card_view.visibility = View.GONE
        val destinationAutoComplete =
            supportFragmentManager.findFragmentById(R.id.etxt_des_addressnew) as AutocompleteSupportFragment?
        val sourceAutoComplete =
            supportFragmentManager.findFragmentById(R.id.etxt_source_addressnew) as AutocompleteSupportFragment?
        sourceAutoComplete!!.requireView().isEnabled = false
        destinationAutoComplete!!.requireView().isEnabled = false
        sourceAutoComplete.requireView().findViewById<View>(R.id.etxt_source_addressnew)
            .clearFocus()
        destinationAutoComplete.requireView().visibility = View.GONE
        sourceAutoComplete.requireView().visibility = View.GONE
        assert(etxt_des_address != null)
        etxt_des_address.visibility = View.VISIBLE
        assert(etxt_source_address != null)
        etxt_source_address.visibility = View.VISIBLE
        assert(etxt_source_address != null)
        etxt_source_address.isEnabled = false
        assert(etxt_des_address != null)
        etxt_des_address.isEnabled = false
        assert(txt_scheduler != null)
        txt_scheduler.isEnabled = false
        assert(txt_for_oth != null)
        txt_for_oth.isEnabled = false
        assert(txt_sor_close != null)
        txt_sor_close.isEnabled = false
        lay_driver_profile.visibility = View.VISIBLE
        assert(txt_des_close != null)
        assert(txt_dess_close != null)
        txt_des_close.isEnabled = false
        txt_dess_close.isEnabled = false
        total_destination_and_details.visibility = View.GONE
    }

    private fun driverphotoAccept(userId: String) {
        val userType = "PROFILE"

        val userid: String = userId
        val bese_url = Prefs.getString("base_url", BASE_URL)
        GlideApp.with(this) //please change this later with constant
//                 .load(BASE_URL + Endpoints.GET_IMAGE_BY_ID + userType + "/" + user_id)
            .load(bese_url + Endpoints.GET_IMAGE_BY_ID + userType + "/" + userid)
            .diskCacheStrategy(DiskCacheStrategy.NONE) //here you can add a default image drawable
            .error(getDrawable(R.drawable.userr))
            .skipMemoryCache(true)
            .into(img_profile)
    }

    fun updateDriverList(
        driverItemsList: List<AdvanceSearhData?>?,
        msg: String?,
        is_try_again: Boolean?
    ) {
        var online_taxi_count = 0
        onlineDriverListApplyCoupon.clear()
        for (coupon_driver in driverItemsList?.indices!!) {
            onlineDriverListApplyCoupon.add(driverItemsList[coupon_driver]!!)

        }
        driverListType = DriverListType()
        val taxidriverLists: ArrayList<AdvanceSearhData> = ArrayList()
        val Taxi4driverLists: ArrayList<AdvanceSearhData> = ArrayList()
        val Taxi6driverLists: ArrayList<AdvanceSearhData> = ArrayList()
        val TransportdriverLists: ArrayList<AdvanceSearhData> = ArrayList()
        val autodriverLists: ArrayList<AdvanceSearhData> = ArrayList()
        val bikedriverLists: ArrayList<AdvanceSearhData> = ArrayList()
        val ambulancedriverList: ArrayList<AdvanceSearhData> = ArrayList()
        if (msg != null) {
            oFFLINE_DriverList.clear()
            oFFLINE_DriverList_taxi.clear()
            for (i in driverItemsList.indices) {
                when (driverItemsList[i]!!.carType) {
                    "Transport" -> {
                        // driverItemsList.get(i).setTotal_taxi(trans_count);
                        if (driverItemsList[i]!!.status.equals("waiting")) {
                            driverItemsList[i]!!.is_Online = true
                        }
                        TransportdriverLists.add(driverItemsList[i]!!)
                    }
                    "Taxi4" -> {
                        // driverItemsList.get(i).setTotal_taxi(taxi4_count);
                        if (driverItemsList[i]!!.status.equals("waiting")) {
                            driverItemsList[i]!!.is_Online = true
                        }
                        Taxi4driverLists.add(driverItemsList[i]!!)
                    }
                    "Taxi6" -> {
                        if (driverItemsList[i]!!.status.equals("waiting")) {
                            driverItemsList[i]!!.is_Online = true
                        }
                        Taxi6driverLists.add(driverItemsList[i]!!)
                    }
                    "Taxi" -> {
                        if (driverItemsList[i]!!.status.equals("waiting")) {
                            driverItemsList[i]!!.is_Online = true
                        }
                        taxidriverLists.add(driverItemsList[i]!!)
                    }
                    "Auto" -> {
                        if (driverItemsList[i]!!.status.equals("waiting")) {
                            driverItemsList[i]?.is_Online = true
                        }
                        autodriverLists.add(driverItemsList[i]!!)
                    }
                    "Bike" -> {
                        //  driverItemsList.get(i).setTotal_taxi(Bike count);
                        if (driverItemsList[i]!!.status.equals("waiting")) {
                            driverItemsList[i]?.is_Online = true
                        }
                        bikedriverLists.add(driverItemsList[i]!!)
                    }
                    "Ambulance" -> {
                        if (driverItemsList[i]!!.status.equals("waiting")) {
                            driverItemsList[i]?.is_Online = true
                        }
                        ambulancedriverList.add(driverItemsList[i]!!)
                    }
                }
                if (driverItemsList[i]?.status == null) {
                    oFFLINE_DriverList.add(driverItemsList[i]!!)
                } else if (!driverItemsList[i]!!.status.equals("WAITING")) {
                    oFFLINE_DriverList.add(driverItemsList[i]!!)
                    oFFLINE_DriverList_taxi.add(driverItemsList[i]!!)
                } else if (driverItemsList[i]!!.status.equals("WAITING")) {
                    online_taxi_count = online_taxi_count + 1
                    oNLINE_DriverList.add(driverItemsList[i]!!)
                }
            }

        }
        driverListType!!.setTransport_driverLists(TransportdriverLists)
        driverListType!!.setTaxi4_driverLists(Taxi4driverLists)
        driverListType!!.setTaxi6_driverLists(Taxi6driverLists)
        driverListType!!.setTaxi_driverLists(taxidriverLists)
        driverListType!!.setAuto_driverLists(autodriverLists)
        driverListType!!.setBike_driverLists(bikedriverLists)
        driverListType!!.setAmbulance_driverList(ambulancedriverList)

        assert(txt_taxi_count != null)
        txt_taxi_count.text = online_taxi_count.toString()

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun askPermission() {
        val intent = Intent(
            Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
            Uri.parse("package:$packageName")
        )
        startActivityForResult(intent, SYSTEM_ALERT_WINDOW_PERMISSION)


    }

    private fun addMarkersToMap(
        driverItemsList: List<AdvanceSearhData?>?

    ) {


        if (driverItemsList != null) {
            for (i in 0 until driverItemsList.size) {
                driverItemsList.get(i)?.latitude?.let {
                    driverItemsList.get(i)!!.longitude?.let { it1 ->
                        LatLng(
                            it,
                            it1
                        );
                    }
                }
                var bitmapMarker: BitmapDescriptor?
                val markerOptions = MarkerOptions()
                userLocationMarker = mMap!!.addMarker(markerOptions)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        setIsMainActivityAlive(false)
        stopLocationUpdates()
        unRegisterLocalReceiver()
        viewModel.saveOnPauseState(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopService(Intent(this, LocationTrackingService::class.java))
    }

    @SuppressLint("LogNotTimber")
    private fun turnGPSOn() {
        val googleApiClient = GoogleApiClient.Builder(this)
            .addApi(LocationServices.API).build()
        googleApiClient.connect()
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 10000 / 2.toLong()
        val builder =
            LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)
        val result =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback { result1: LocationSettingsResult ->
            val status = result1.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> Log.i(
                    TAG,
                    "All location settings are satisfied."
                )
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    Log.i(
                        TAG,
                        "Location settings are not satisfied. Show the user a dialog to upgrade location settings "
                    )
                    try {
                        status.startResolutionForResult(this, 1)
                    } catch (e: SendIntentException) {
                        Log.i(TAG, "PendingIntent unable to execute request.")
                    }
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                    TAG,
                    "Location settings are inadequate, and cannot be fixed here. Dialog not created."
                )
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1001) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    Toast.makeText(this, "gps is turn on ", Toast.LENGTH_SHORT).show()
                }
                Activity.RESULT_CANCELED -> {
                    Toast.makeText(this, "gps is cancelled  ", Toast.LENGTH_SHORT).show()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivity(intent)
                }
            }
        }
        if (resultCode == RESULT_OK && data?.getData() != null) {

        }

    }

    private fun addOriginDestinationMarkerAndGet(latLng: LatLng): Marker {
        val bitmapDescriptor =
            BitmapDescriptorFactory.fromBitmap(MapUtils.getOriginDestinationMarkerBitmap())
        return mMap?.addMarker(MarkerOptions().position(latLng).flat(true).icon(bitmapDescriptor))!!
    }

    private fun updateCarLocation(latLng: LatLng) {
        if (movingCabMarker == null) {
            movingCabMarker = addCarMarkerAndGet(latLng)
        }
        if (previousLatLng == null) {
            currentLatLng = latLng
            previousLatLng = currentLatLng
            movingCabMarker?.position = latLng
            movingCabMarker?.setAnchor(0.5f, 0.5f)
            animateCamera(currentLatLng!!)
        } else {
            previousLatLng = currentLatLng
            currentLatLng = latLng
            val valueAnimator = AnimationUtils.carAnimator()
            valueAnimator.addUpdateListener { va ->
                if (currentLatLng != null && previousLatLng != null) {
                    val multiplier = va.animatedFraction
                    val nextLocation = LatLng(
                        multiplier * currentLatLng!!.latitude + (1 - multiplier) * previousLatLng!!.latitude,
                        multiplier * currentLatLng!!.longitude + (1 - multiplier) * previousLatLng!!.longitude
                    )
                    movingCabMarker?.position = nextLocation
                    val rotation = MapUtils.getRotation(previousLatLng!!, nextLocation)
                    if (!rotation.isNaN()) {
                        movingCabMarker?.rotation = rotation
                    }
                    movingCabMarker?.setAnchor(0.5f, 0.5f)
                    animateCamera(nextLocation)
                }
            }
            valueAnimator.start()
        }
    }

    private fun showPath(latLngList: ArrayList<LatLng>) {

        if (!latLngList.isEmpty()) {
            val builder = LatLngBounds.Builder()
            for (latLng in latLngList) {
                builder.include(latLng)
            }
            // this is used to set the bound of the Map
            val bounds = builder.build()
            mMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 2))

            val polylineOptions = PolylineOptions()
            polylineOptions.color(Color.GRAY)
            polylineOptions.width(5f)
            polylineOptions.addAll(latLngList)
            grayPolyline = mMap?.addPolyline(polylineOptions)

            val blackPolylineOptions = PolylineOptions()
            blackPolylineOptions.color(Color.BLACK)
            blackPolylineOptions.width(5f)
            blackPolyline = mMap?.addPolyline(blackPolylineOptions)


            originMarker = addOriginDestinationMarkerAndGet(latLngList[0])
            originMarker?.setAnchor(0.5f, 0.5f)
            destinationMarker = addOriginDestinationMarkerAndGet(latLngList[latLngList.size - 1])
            destinationMarker?.setAnchor(0.5f, 0.5f)

            val polylineAnimator = AnimationUtils.polylineAnimator()
            polylineAnimator.addUpdateListener { valueAnimator ->
                val percentValue = (valueAnimator.animatedValue as Int)
                val index = (grayPolyline?.points!!.size) * (percentValue / 100.0f).toInt()
                blackPolyline?.points = grayPolyline?.points!!.subList(0, index)
            }
            polylineAnimator.start()

        }
    }

    private fun showMovingCab(cabLatLngList: ArrayList<LatLng>) {
        handler = Handler()
        var index = 0
        runnable = Runnable {
            run {
                if (index < cabLatLngList.size) {
                    updateCarLocation(cabLatLngList[index])
                    this.runnable?.let { handler.postDelayed(it, 3000) }
                    ++index
                } else {
                    this.runnable?.let { handler.removeCallbacks(it) }

                }
            }
        }
        if (runnable != null) {
            handler.postDelayed(runnable!!, 5000)
        }
    }

    private fun showMovingCabTaxi(cabLatLngList: ArrayList<LatLng>) {


        if (indexmovingcab < cabLatLngList.size) {
            updateCarLocation(cabLatLngList[indexmovingcab])
            indexmovingcab += 1
        }


    }

    private fun moveCamera(latLng: LatLng) {
        driverTrackingMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    private fun animateCamera(latLng: LatLng) {
        val cameraPosition = CameraPosition.Builder().target(latLng).zoom(15.5f).build()
        driverTrackingMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun showDefaultLocationOnMap(latLng: LatLng) {
        moveCamera(latLng)
        animateCamera(latLng)

    }

    fun getListOfLocations(): ArrayList<LatLng> {


        if (number == 0) {
            locationList.add(LatLng(13.049571674007842, 80.20974652551297))
            number++
        } else if (number == 1) {
            locationList.add(LatLng(13.050988929298338, 80.20992462392962))
            number++
        } else if (number == 2) {
            locationList.add(LatLng(13.050792437298577, 80.2118365026464))
            number++
        } else if (number == 3) {
            locationList.add(LatLng(13.052009013640413, 80.21195451984306))
            number++
        } else if (number == 4) {
            locationList.add(LatLng(13.053250667793131, 80.21167771534886))
            number++
        } else if (number == 5) {
            locationList.add(LatLng(13.054542483365267, 80.21147815860525))
            number++
        } else if (number == 6) {
            locationList.add(LatLng(13.054429606359896, 80.21366469526322))
            number++
        } else if (number == 7) {
            locationList.add(LatLng(13.054550844481113, 80.21488992847219))
            number++
        } else if (number == 8) {
            locationList.add(LatLng(13.054513218819665, 80.21626107358964))
            number++
        } else if (number == 9) {
            locationList.add(LatLng(13.056866898080989, 80.21603576803238))
            number++
        } else if (number == 10) {
            locationList.add(LatLng(13.05874815286387, 80.21800558141615))
            number++
        } else if (number == 11) {
            locationList.add(LatLng(13.058986443926976, 80.21932522759664))
            number++
        } else if (number == 12) {
            locationList.add(LatLng(13.059429581483684, 80.21970502734541))
            number++
        } else if (number == 13) {
            locationList.add(LatLng(13.056866898080989, 80.21603576803238))
            number++
        } else if (number == 14) {
            locationList.add(LatLng(13.059542455805937, 80.22009770248326))
            number++
        } else if (number == 15) {
            locationList.add(LatLng(13.060106827600327, 80.22023288555567))
            number++
        } else {
            locationList.add(LatLng(13.060094285809814, 80.22090021863471))
        }

        return locationList

    }

    override fun onMapReady(googleMap: GoogleMap) {
        viewModel.showProgress()

        mMap = googleMap
        driverTrackingMap = googleMap
        carMap = googleMap
        showAvilableVehicle = googleMap

        mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
        mMap?.uiSettings?.setAllGesturesEnabled(true)
        mMap?.uiSettings?.isMapToolbarEnabled = true
        mMap?.uiSettings?.isZoomControlsEnabled = true
        mMap?.setPadding(0, 0, 0, 180)

        try {
            if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
        } catch (e: java.lang.Exception) {
        }

        mMap?.isMyLocationEnabled = true

        val handler = Handler()
        handler.postDelayed({ this.intiMyLocation() }, 2000)
        googleMap.setOnMyLocationButtonClickListener(OnMyLocationButtonClickListener {
            turnGPSOn()
            //refresh()
            false
        })

        if (mapView != null && mapView!!.findViewById<View?>("1".toInt()) != null) {
            // Get the button view

            // Get the button view
            val locationButton =
                (mapView!!.findViewById<View>("1".toInt())
                    .parent as View).findViewById<View>("2".toInt())
            // and next place it, on bottom right (as Google Maps app)
            val layoutParams =
                locationButton.layoutParams as RelativeLayout.LayoutParams
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE)
            layoutParams.setMargins(
                0,
                0,
                resources.getDimension(R.dimen._30ssp).roundToInt(),
                resources.getDimension(R.dimen._30ssp).roundToInt()
            )
        }

    }

    private fun onMapDrivertracking(googleMap: GoogleMap?, latitude: Double, longitude: Double) {
        this.driverTrackingMap = googleMap
        driverTrackingMap?.clear()
        defaultLocation = LatLng(latitude, longitude)
        showDefaultLocationOnMap(defaultLocation)
        showDriverMap(latitude, longitude)
        getDriverLatitudeLangitude()

        handler.postDelayed(object : Runnable {
            override fun run() {
                if (driverTrackinglatitude == 0.0 && driverTrackinglangtitude == 0.0) {
                    getDriverLatitudeLangitude()
                    driverTracking(latitude, longitude)
                } else {
                    getDriverLatitudeLangitude()
                    driverTracking(driverTrackinglatitude, driverTrackinglangtitude)
                }
                handler.postDelayed(this, 10000)
                if (viewModel.getLocalStataus()
                        .equals(LOCAL_START_RIDE) || viewModel.getLocalStataus()
                        .equals("LOGIN") || viewModel.getLocalStataus().equals(DRIVER_CANCEL_RIDE)
                ) {
                    handler.removeCallbacks(this)
                }

            }
        }, 10000)
    }

    private fun getDriverLatitudeLangitude() {
        val db = FirebaseFirestore.getInstance()
        if (!viewModel.getCilentId().isNullOrEmpty()) {
            val docRef = db.collection("KERALACABS_DRIVER_STATUS")
                .document(viewModel.getCilentId().toString())
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {

                        if (document.data != null) {
                            val latitude = document.data!!["LATITUDE"].toString().toDouble()
                            val longitude = document.data!!["LONGITUDE"].toString().toDouble()
                            driverTrackinglatitude = latitude
                            driverTrackinglangtitude = longitude
                        }

                    }
                }
                .addOnFailureListener { exception ->
                }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun driverTracking(latitude: Double, longitude: Double) {
        val get_driver_location = MapUtils.getListOfLocationsTracking()
        if (driverTrackingkm.isEmpty()) {
            getDriverTrackingKm(latitude, longitude)
        } else {
            getDriverTrackingKm(latitude, longitude)

            txt_apxtime.text = "$driverTrackingDistance  mins"
            txt_taxi_arrivalkm.text = "$driverTrackingkm  km"

            Toast.makeText(
                this,
                "driver started pick up  km  is $driverTrackingkm  km \n driver arrival time is  $driverTrackingDistance mins",
                Toast.LENGTH_SHORT
            ).show()
        }
        if (get_driver_location.size > 1) {
            showMovingCabTaxi(get_driver_location)
            get_driver_location.size
        }
    }

    fun callToDriver(view: View) {
        val driver_number = viewModel.getDriverMobileNumber()
        if (!driver_number.isNullOrEmpty()) {
            callDriver(driver_number)
        } else {
            Toast.makeText(this, "Driver Mobile Number was not Register", Toast.LENGTH_SHORT).show()
        }


    }

    private fun getDriverTrackingKm(latitude: Double, longitude: Double) {
        val place = AppUtils.latLongToAddress(
            latitude,
            longitude,
            applicationContext,
            is_address
        ).toString()
        if (place.isNotEmpty()) {
            viewModel.getDriverTrackingLocationId(latitude.toString(), longitude.toString(), place)
        }
    }

    private fun showDriverMap(driver_currentlat: Double, driver_currentlang: Double) {
        val userCurrentLatitude = viewModel.getUserSearchLatitude()
        val userCurrentLangtitude = viewModel.getUserSearchLangitude()
        val driverCurrentlat = driver_currentlat
        val driverCurrentlang = driver_currentlang
        GoogleMapsPath(
            this@MainNewActivity,
            driverTrackingMap,
            LatLng(driverCurrentlat, driverCurrentlang),
            LatLng(userCurrentLatitude, userCurrentLangtitude)
        )
    }

    private fun intiMyLocation() {
        /*    val currentPosition =
                LatLng(viewModel.getLatitude(), viewModel.getLongitude())
            if (currentLocationMarker == null && viewModel.getLatitude() != 0.0 && viewModel.getLongitude() != 0.0) {
                mMap!!.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(currentPosition, DEFAULT_ZOOM.toFloat()), 3000,
                        null
                )
                //currentLocationMarker = mMap!!.addMarker(get_car_Type(currentPosition))
                viewModel.closeShowProgress()
            } else {
                viewModel.closeShowProgress()
            }*/
    }

    public override fun onStart() {
        super.onStart()
        if (!checkPermissions()) {
            requestPermissions()
        } else {
            getLocationUpdates()
        }
    }

    override fun onStop() {
        super.onStop()
    }


    private fun showmessage(text: String) {
        val container = findViewById<View>(R.id.drawer_view)
        if (container != null) {
            Toast.makeText(this, text, Toast.LENGTH_LONG).show()
        }
    }

    private fun showSnackbar(
        mainTextStringId: Int, actionStringId: Int,
        listener: View.OnClickListener
    ) {

        Toast.makeText(this, getString(mainTextStringId), Toast.LENGTH_LONG).show()
    }

    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )


        if (shouldProvideRationale) {

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                View.OnClickListener {
                    // Request permission
                    startLocationPermissionRequest()
                })

        } else {
            startLocationPermissionRequest()
        }
    }


    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLocationUpdates()
            } else {
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                    View.OnClickListener {
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts(
                            "package",
                            BuildConfig.APPLICATION_ID, null
                        )
                        intent.data = uri
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    })
            }
        }

    }

    /*Fcm receiver message*/
    private fun registerLocalReceiver() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(mHandler, IntentFilter(BROADCAST_TAG))
    }

    private fun unRegisterLocalReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandler)
    }


    private val mHandler: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            try {
                val locationData: LocationData =
                    intent.getSerializableExtra(CUSTOM_LOCATION_DATA) as LocationData
                onReceiveLocationData(locationData)
            } catch (e: java.lang.Exception) {
            }
            try {
                val notificationData: FcmCustomizeNotificationData =
                    intent.getSerializableExtra(CUSTOM_NOTIFICATION_DATA) as FcmCustomizeNotificationData
                onReceiveNotification(notificationData.action!!, notificationData)
            } catch (e: java.lang.Exception) {
            }
        }
    }

    fun getBroadcastTag(): String? {
        return BROADCAST_TAG
    }


    private fun onReceiveLocationData(
        locationData: LocationData
    ) {
        try {
            val lat: Double = locationData.lat!!.toDouble()
            val long: Double = locationData.long!!.toDouble()
            if ((viewModel.getlocationlat().toString() == "0.0") && (viewModel.getlocationlang()
                    .toString() == "0.0")
            ) {
                viewModel.setDriverCurrentLatLng(lat, long)
            }
            mMap!!.uiSettings.isZoomControlsEnabled = false
            viewModel.setDriverCurrentLatLng(lat, long)
        } catch (e: Exception) {
        }
    }

    fun unlockDrawer() {
        if (drawer_view != null) drawer_view.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    fun lockDrawer() {
        if (drawer_view != null) drawer_view.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    override fun onButtonClick(action: String?) {

    }

    override fun onBunAcceptClick(action: String?, coupon: String?) {

    }

    override fun onBunRejectClick(action: String?) {

    }

    override fun onUpdateReview(action: String?, cmd: String?, review: Double, ride_amt: String?) {
        if (AppUtils.isNetworkConnected(this@MainNewActivity)) {
            if (RIDE_FINISH.equals(action, ignoreCase = true)) {

                rideFinish(true)

                viewModel.saveCategeory("")
                viewModel.saveCurrentStataus("LOGIN_IN")
                viewModel.saveLocalStatus("LOGIN")
                removeDriverTrackingInformation()
                removeDriverInformation()
                viewModel.setRIDE_KEY_LOCAL("")
                fcmCustomizeNotificationData_background = FcmCustomizeNotificationData()
                viewModel.onPauseSaveBooking(false)
                viewModel.driverAcceptStatus(false)
                val telicoun = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
                val config = resources.configuration
                assert(txt_payment_type != null)
                if (txt_payment_type.text.toString() == getString(R.string.googlepay)) {
                    /*if (teli_coun.getNetworkCountryIso().equalsIgnoreCase("in")) {
                        PaymentGatewayDialog paymentGatewayDialog = PaymentGatewayDialog.newInstance(PaymentGateway.PAYTM);
                        paymentGatewayDialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_DialogTransparentLayout);
                        Bundle bundle = new Bundle();
                        bundle.putString("card_amt_paytm", ride_amt);
                        paymentGatewayDialog.setArguments(bundle);
                        paymentGatewayDialog.show(getFragmentManager(), "paymentGatewayDialog");
                    } else {
                        Intent newActivity1 = new Intent(this, Stripe_MainActivity.class);
                        newActivity1.putExtra("card_amount", ride_amt);
                        startActivity(newActivity1);
                    }*/
                }
            }
        }
    }

    private fun removeDriverTrackingInformation() {
        viewModel.saveUserSearchLatLng(0.0, 0.0)
        viewModel.saveSearchDestinationLatLang(0.0, 0.0)
        driverTrackingMap?.clear()
        MapUtils.getDriverId("")
    }

    fun rideFinish(rideFinish: Boolean) {
        unlockDrawer()
        linear_source.visibility = View.GONE
        assert(lay_taxi != null)
        lay_taxi.visibility = View.VISIBLE
        assert(lay_driver_profile != null)
        lay_driver_profile.visibility = View.GONE
        assert(live_car_details != null)
        live_car_details.visibility = View.GONE
        assert(etxt_source_address != null)
        etxt_source_address.isEnabled = true
        assert(etxt_des_address != null)
        etxt_des_address.isEnabled = true
        assert(txt_scheduler != null)
        txt_scheduler.isEnabled = true
        assert(txt_for_oth != null)
        txt_for_oth.isEnabled = true
        assert(txt_sor_close != null)
        txt_sor_close.isEnabled = true
        assert(txt_des_close != null)
        assert(txt_dess_close != null)
        txt_des_close.isEnabled = true
        txt_dess_close.isEnabled = true
        on_line_dr.visibility = View.VISIBLE
        assert(lay_sor_des != null)
        lay_sor_des.visibility = View.VISIBLE
        etxt_source_address.text = ""
        etxt_des_address.text = ""
        viewModel.saveCurrentStataus("")
        Tracking.removeMessages(0)
        if (runnable != null) {
            Tracking.removeCallbacks(runnable!!)
        }

//        mPresenter.ride_FINISH_CLEAR()
        if (listenerRegistration != null) {
            listenerRegistration.remove()
        }
        finish()
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    override fun validateCoupon(coupon: String?): Boolean {
        return true
    }

    fun imagebtn(view: View) {
        val items = arrayOf<CharSequence>(
            getString(R.string.Call),
            getString(R.string.Change_Destination),
            getString(R.string.Alert),
            getString(
                R.string.Help
            ),
            getString(R.string.Share_Loc),
            getString(R.string.Cancel)
        )
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.select_one)
        builder.setItems(items) { dialog: DialogInterface?, item: Int ->
            // Do something with the selection
            selectbox = items[item].toString()
            if (item == 0) {
                selectbox = items[item].toString()
            }
            if (item == 1) {
                selectbox = items[item].toString()
            }
            if (item == 2) {
                selectbox = items[item].toString()
            }
            if (item == 3) {
                selectbox = items[item].toString()
            }
            if (item == 4) {
                selectbox = items[item].toString()
            }
            if (item == 5) {
                selectbox = items[item].toString()
            }
            checkFunction(item)

        }
        val alert = builder.create()
        alert.show()
    }

    private fun checkFunction(position: Int) {
        if (position == 0) {
            if (viewModel.getDriverMobileNumber().toString()
                    .isEmpty() || viewModel.getDriverMobileNumber().equals(
                    null
                )
            ) {
                callDriver(dR_mobile_num)
            } else {
                callDriver(viewModel.getDriverMobileNumber().toString())
            }
        } else if (position == 1) {

            val myDialog = MyDialog(
                this,
                getString(R.string.Under_Development_Coming_Soon),
                getString(R.string.Change_Destination),
                "\uf1ae",
                this@MainNewActivity
            )
            myDialog.show()
            myDialog.setCancelable(false)


        } else if (position == 2) {

            val myDialog = MyDialog(
                this,
                getString(R.string.Under_Development_Coming_Soon),
                getString(R.string.Emergence_Alert),
                "\uf1ae",
                this@MainNewActivity
            )
            myDialog.show()
            myDialog.setCancelable(false)


        } else if (position == 3) {
            showFilterDialog()
        } else if (position == 4) {

            if (viewModel.getLocalStataus() == LOCAL_START_RIDE) {
                Toast.makeText(
                    this,
                    "Your Ride was Started So Could not be Canceled",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val myDialog = MyDialog(
                    this,
                    getString(R.string.Under_Development_Coming_Soon),
                    getString(R.string.Location_Share),
                    "\uf1ae",
                    this@MainNewActivity
                )
                myDialog.show()
                myDialog.setCancelable(false)
            }


        } else if (position == 5) {

            if (viewModel.getLocalStataus() == LOCAL_START_RIDE) {
                Toast.makeText(
                    this,
                    "Your Ride was Started So Could not be Cancelled",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val myDialog = MyDialog(
                    this, getString(R.string.Are_You_Sure_Want_To_Cancel_The_Ride), getString(
                        R.string.Cancel_Ride
                    ), "\uf1ae", this@MainNewActivity
                )
                myDialog.show()
                myDialog.setCancelable(false)
            }


        }


    }

    private fun checkCancelDialog(action: String, reasonMsg: String) {

        if (action.equals(this.getString(R.string.Location_Change), ignoreCase = true)) {
            if (viewModel.getLocalStataus().equals(LOCAL_DONE_PICKUP, ignoreCase = false) ||
                viewModel.getLocalStataus().equals(LOCAL_START_RIDE, ignoreCase = false)
            ) {
                viewModel.change_Destination_ByFCM(
                    reasonMsg,
                    RIDE_REQUEST_CHANGEDESTINATION,
                    this.getString(R.string.I_need_to_Change_my_Destination)
                )
            } else {
                viewModel.change_Destination_ByFCM(
                    reasonMsg,
                    REQUEST_CHANGE_DESTINATION,
                    this.getString(R.string.I_need_to_Change_my_Destination)
                )
            }
        } else if (action.equals(this.getString(R.string.Cancel_Ride), ignoreCase = true)) {
            Toaster.show(this, getString(R.string.Cancel_Ride))
            if (viewModel.getLocalStataus().equals(LOCAL_START_RIDE)
                || viewModel.getLocalStataus().equals(LOCAL_DONE_PICKUP)
                || viewModel.getLocalStataus().equals(LOCAL_CONTINUE_RIDE)
            ) {
                viewModel.cancel_Ride_ByFCM(RIDE_CANCEL_BOOKING, reasonMsg)
                viewModel.saveCategeory("")
            } else {
                viewModel.cancel_Ride_ByFCM(CANCEl_BOOKING, reasonMsg)
                assert(txt_payment_type != null)
                viewModel.createRideStatus(
                    PRECANCEL_BY_USER,
                    reasonMsg,
                    txt_payment_type.text.toString()
                )
                FireStoreCloud.cloud_USER_CANCEL_BOOKING(
                    viewModel.getRIDE_KEY_LOCAL().toString(),
                    reasonMsg,
                    "CANCEL_BY_USER"
                )
                viewModel.saveCategeory("")
            }
            rideFinish(false)
            viewModel.saveCurrentStataus("LOGIN_IN")
            removeDriverTrackingInformation()
            viewModel.setRIDE_KEY_LOCAL("")
            viewModel.onPauseSaveBooking(false)
            viewModel.driverAcceptStatus(false)
        } else if (action.equals(
                this.getString(R.string.Driver_canceled_your_ride),
                ignoreCase = true
            )
        ) {
            rideFinish(false)
            viewModel.saveCategeory("")
            viewModel.saveLocalStatus("LOGIN")
            viewModel.saveCurrentStataus("LOGIN_IN")
            removeDriverInformation()
            removeDriverTrackingInformation()
            viewModel.setRIDE_KEY_LOCAL("")
            viewModel.onPauseSaveBooking(false)
            viewModel.driverAcceptStatus(false)
        }
    }

    override fun onIconClick(position: Int) {
        val handler = Handler()
        handler.postDelayed({
            //Do something after 100ms
            when (position) {
                1 -> {

                }
                4 -> {
                    /*   DialogTryAgain.newInstance(
                            this@MainActivity,
                            false,
                            this.getString(R.string.Under_Development_Coming_Soon),
                            this.getString(R.string.Location_Share),
                            "\uf1ae"
                        ).show(supportFragmentManager)*/
                }
                0 -> {
                    //                callDriver(dR_mobile_num)
                }
                5 -> {
                    /* DialogTryAgain.newInstance(
                            this@MainActivity,
                            false,
                            this.getString(R.string.Are_You_Sure_Want_To_Cancel_The_Ride),
                            this.getString(R.string.Cancel_Ride),
                            "\uf1ae"
                        ).show(supportFragmentManager)*/
                }
                3 -> {
                    //                HelpDialog.newInstance().show(supportFragmentManager)
                }
                2 -> {
                    /* DialogTryAgain.newInstance(
                            this@MainActivity,
                            false,
                            this.getString(R.string.Under_Development_Coming_Soon),
                            this.getString(R.string.Emergence_Alert),
                            "\uf1ae"
                        ).show(supportFragmentManager)*/
                }
            }
        }, 200)
    }

    private fun callDriverFireBase() {

        val domain = viewModel.getDomain().toString()

        db.collection("phone_number").document(domain).get()
            .addOnSuccessListener { documentSnapshot ->
                val phonenumber = documentSnapshot.getString("phoneNumber").toString()

                callDriver(phonenumber)

            }.addOnFailureListener {
            }

    }

    private fun callDriver(num: String) {
        val intent =
            Intent(Intent.ACTION_CALL)
        intent.data = Uri.parse("tel:$num")
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CALL_PHONE),
                0
            )
            return
        }
        startActivity(intent)
    }

    fun oneway(view: View) {
        if (checkGps() && checkNetwork(this)) {
            sourceAddressConvert()
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            card_view.visibility = View.GONE
            lay_sor_des.visibility = View.GONE
            lay_app_bar_main.visibility = View.GONE
            txt_dess_close.visibility = View.VISIBLE
            pickUpDropLayout.visibility = View.VISIBLE
            nextRideButton.visibility = View.GONE
            viewModel.saveCategeory("TAXI")
            val toast: Toast = Toast.makeText(
                applicationContext,
                "ONEWAY-TAXI SELECTED",
                Toast.LENGTH_LONG
            )
            val v = toast.view?.findViewById(android.R.id.message) as? TextView
            if (v != null) {
                v.setTextColor(Color.BLACK)
                v.setBackgroundColor(Color.YELLOW)
                toast.show()
            }
        } else {
            if (!checkGps()) {
                Toast.makeText(this, "Please Enable Your Location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            if (checkGps()) {
                if (!checkNetwork(this)) {
                    Toast.makeText(this, "Please Enable Your Internet or WIFI", Toast.LENGTH_LONG)
                        .show()
                    val intent = Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS)
                    startActivity(intent)
                }
            }
        }


    }

    fun sourceAddressConvert() {
        var place = ""
        if (sourceLatitude.isNotEmpty() && sourceLangtitude.isNotEmpty()) {
            place = AppUtils.latLongToAddress(
                sourceLatitude.toDouble(),
                sourceLangtitude.toDouble(),
                this,
                is_address
            ).toString()
            sourceaddress = place
            pickUpTextView.setText(sourceaddress)
        }

    }

    fun showMapAddress() {
        val currentPosition: LatLng
        if (sourceLatitude.isNotEmpty() && sourceLangtitude.isNotEmpty()) {
            currentPosition = LatLng(
                sourceLatitude.toDouble(),
                sourceLangtitude.toDouble()
            )

            var latLng = currentPosition

            val marker = mMap!!.addMarker(MarkerOptions().position(latLng).title(sourceaddress))
            marker?.showInfoWindow()

        }
    }

    private fun onLocationChange() {

        val currentPosition: LatLng
        if (sourceLatitude.isNotEmpty() && sourceLangtitude.isNotEmpty()) {
            currentPosition = LatLng(
                sourceLatitude.toDouble(),
                sourceLangtitude.toDouble()
            )

            var latLng = currentPosition
            val zoomLevel = 18.0.toFloat()
//                 mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel))
            /* mMap?.animateCamera(mMap?.getCameraPosition()?.zoom?.minus(5f)?.let { CameraUpdateFactory.zoomTo(it) });*/
            mMap!!.animateCamera(
                CameraUpdateFactory.newLatLngZoom(currentPosition, DEFAULT_ZOOM.toFloat()), 300,
                null
            )
        }
    }

    fun outstaion(view: View) {
        if (checkGps() && checkNetwork(this)) {
            sourceAddressConvert()
            card_view.visibility = View.GONE
            lay_sor_des.visibility = View.GONE
            lay_app_bar_main.visibility = View.GONE
            txt_dess_close.visibility = View.VISIBLE
            pickUpDropLayout.visibility = View.VISIBLE
            nextRideButton.visibility = View.GONE
            viewModel.saveCategeory("OUTSTATION")
            val toast: Toast = Toast.makeText(
                applicationContext,
                "OUTSTATION-TAXI SELECTED",
                Toast.LENGTH_LONG
            )
            val v = toast.view?.findViewById(android.R.id.message) as? TextView
            if (v != null) {
                v.setTextColor(Color.BLACK)
                v.setBackgroundColor(Color.YELLOW)
                toast.show()
            }
        } else {
            if (!checkGps()) {
                Toast.makeText(this, "Please Enable Your Location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            if (checkGps()) {
                if (!checkNetwork(this)) {
                    Toast.makeText(this, "Please Enable Your Internet or WIFI", Toast.LENGTH_LONG)
                        .show()
                    val intent = Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS)
                    startActivity(intent)
                }
            }
        }

    }

    fun rental(view: View) {
//        card_view.visibility=View.GONE
//        lay_sor_des.visibility=View.GONE
//        lay_app_bar_main.visibility=View.GONE
//        txt_dess_close.visibility=View.VISIBLE
//        pickUpDropLayout.visibility=View.VISIBLE
//        nextRideButton.visibility=View.GONE
//        viewModel.saveCategeory("PACKAGE")
        val toast: Toast = Toast.makeText(
            applicationContext,
            "COMING SOON",
            Toast.LENGTH_LONG
        )
        val v = toast.view?.findViewById(android.R.id.message) as? TextView
        if (v != null) {
            v.setTextColor(Color.BLACK)
            v.setBackgroundColor(Color.YELLOW)
            toast.show()
        }
    }

    fun wherto(view: View) {
        if (checkGps() && checkNetwork(this)) {
            card_view.visibility = View.GONE
            lay_sor_des.visibility = View.VISIBLE
            lay_app_bar_main.visibility = View.GONE
            txt_dess_close.visibility = View.VISIBLE
            nextRideButton.visibility = View.GONE
            pickUpDropLayout.visibility = View.VISIBLE
            val toast: Toast = Toast.makeText(
                applicationContext,
                "PLEASE ENTER YOUR DESTINATION",
                Toast.LENGTH_LONG
            )
            val v = toast.view?.findViewById(android.R.id.message) as? TextView
            if (v != null) {
                v.setTextColor(Color.BLACK)
                v.setBackgroundColor(Color.WHITE)
                toast.show()
            }
        } else {
            if (!checkGps()) {
                Toast.makeText(this, "Please Enable Your Location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            if (checkGps()) {
                if (!checkNetwork(this)) {
                    Toast.makeText(this, "Please Enable Your Internet or WIFI", Toast.LENGTH_LONG)
                        .show()
                    val intent = Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS)
                    startActivity(intent)
                }
            }
        }

    }


    fun backclick(view: View) {

        checkcoupon = false
        couponCarPrice = ""
        startActivity(Intent(applicationContext, MainNewActivity::class.java))

    }

    fun hidedest(view: View) {
        bottomSheet.visibility = View.GONE
        lay_sor_des.visibility = View.VISIBLE
        pickUpDropLayout.visibility = View.VISIBLE
        rvBooking.visibility = View.GONE
        val toast: Toast = Toast.makeText(
            applicationContext,
            "PLEASE ENTER YOUR DESTINATION",
            Toast.LENGTH_LONG
        )
        val v = toast.view?.findViewById(android.R.id.message) as? TextView
        if (v != null) {
            v.setTextColor(Color.BLACK)
            v.setBackgroundColor(Color.WHITE)
            toast.show()
        }
        checkcoupon = false
        couponCarPrice = ""


    }


    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        myDay = day
        myYear = year
        myMonth = month
        val calendar: Calendar = Calendar.getInstance()
        hour = calendar.get(Calendar.HOUR)
        minute = calendar.get(Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(
            this@MainNewActivity, this@MainNewActivity, hour, minute,
            DateFormat.is24HourFormat(this)
        )
        timePickerDialog.show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        myHour = hourOfDay
        myMinute = minute
        textView.text =
            "Year: " + myYear + "\n" + "Month: " + myMonth + "\n" + "Day: " + myDay + "\n" + "Hour: " + myHour + "\n" + "Minute: " + myMinute
    }

    fun closedestination(view: View) {
        txt_des_close.visibility = View.GONE
        card_view.visibility = View.VISIBLE


    }

    fun showNearbyCabs(latLngList: List<LatLng>) {
        nearbyCabMarkerList.clear()
        for (latLng in latLngList) {
            val nearbyCabMarker = addCarMarkerAndGet(latLng)
            nearbyCabMarkerList.add(nearbyCabMarker)
        }
    }

    private fun addCarMarkerAndGet(latLng: LatLng): Marker {
        val bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(MapUtils.getCarBitmap(this))
        return carMap?.addMarker(
            MarkerOptions().position(latLng).flat(true).icon(bitmapDescriptor)
        )!!
    }

    inner class LoadGoogleMapsRoadPointsTask :
        AsyncTask<String?, Void?, Boolean>() {
        private var totoalkm = ""
        override fun onPreExecute() {

        }

        override fun onPostExecute(success: Boolean) {
        }

        override fun doInBackground(vararg params: String?): Boolean {
            try {
                val googleDirectionApiRequest: String? =
                    AppUtils.createPlaceIdDirectionApiRequest(params[0], params[1])
                val uriBuilder =
                    Uri.parse(googleDirectionApiRequest).buildUpon()
                val googleDirectionsApiUrl =
                    URL(uriBuilder.build().toString())
                val googleDirectionsApiResponse: String =
                    ServerFasade.httpPost(googleDirectionsApiUrl, null, null)
                val directionJson: JSONObject = ServerFasade.makeJsonFromResponse(
                    googleDirectionsApiResponse,
                    JSONObject::class.java
                )
                points = JsonHelper.calculateRoutePoints(directionJson) as List<LatLng>
                totoalkm = java.lang.String.valueOf(
                    JsonHelper.calculateFinalDirectionDistance(directionJson)
                )
                apx_time = java.lang.String.valueOf(
                    JsonHelper.calculateFinalDirectionDuration(directionJson)
                )
                if (check_driverTrackingId_status) {
                    driverTrackingkm = totoalkm
                    driverTrackingDistance = apx_time
                    check_driverTrackingId_status = false
                } else {
                    if (totoalkm.isNotEmpty()) {
                        apx_total_km = totoalkm
                        bookingApi(apx_total_km)

                    }
                }

                return true
            } catch (e: java.lang.Exception) {
                e.printStackTrace()

            }
            return false

        }
    }

    override fun noTaxiavilable() {
        startActivity(Intent(applicationContext, MainNewActivity::class.java))
    }

    override fun tryAgain_button() {
        check_driver_accept = true
        spin_kit.visibility = View.GONE
        rideKeyBookingStatus()
        bookingonClickFunction()
        viewModel.onPauseSaveBooking(false)
    }

    private fun rideKeyBookingStatus() {
        val rideKey = viewModel.getRIDE_KEY_LOCAL()
        if (!rideKey.isNullOrEmpty()) {
            val db = FirebaseFirestore.getInstance()
            val docref = db.collection("RIDE_KEY").document(rideKey)
                .get()
                .addOnSuccessListener {
                    if (it.data != null) {
                        if (it.data!!["IS_ACCEPTED"] != null) {
                            val isaccept = it.data!!["IS_ACCEPTED"].toString()
                            if (isaccept == "false") {
                                FireStoreCloud.delRideKeyLocalValues(rideKey, db)
                            }
                        }
                    }
                }.addOnFailureListener {
                }
        }
    }

    override fun offlineButton() {
        check_driver_accept = true
        spin_kit.visibility = View.GONE
        rideKeyBookingStatus()
        Toast.makeText(this, "Offline Taxi is Coming Soon", Toast.LENGTH_SHORT).show()
        pickUpDropLayout.visibility = View.VISIBLE
        txt_dess_close.visibility = View.VISIBLE
        /* if (!oFFLINE_DriverList_taxi.isNullOrEmpty() && oFFLINE_DriverList_taxi.size>0){
             var offlineTaxiAdapter=Offline_Taxi_Adapter(
                 applicationContext,
                 oFFLINE_DriverList_taxi,
                 this
             )
             val recycler_view: RecyclerView = findViewById(R.id.driver_list)
             val mLayoutManager = LinearLayoutManager(this)
             recycler_view.setLayoutManager(mLayoutManager)
             offlineTaxiAdapter.notifyDataSetChanged()
             recycler_view.setAdapter(offlineTaxiAdapter)
             lay_off_list.visibility=View.VISIBLE
         }else{
             Toast.makeText(this, "Offline TAXI Coming Soon...", Toast.LENGTH_SHORT).show()
             pickUpDropLayout.visibility=View.VISIBLE
         }*/
        viewModel.onPauseSaveBooking(false)
    }

    override fun closeIconBtn() {
        check_driver_accept = true
        spin_kit.visibility = View.GONE
        pickUpDropLayout.visibility = View.VISIBLE
    }

    override fun closeIconTryAgain() {
        check_driver_accept = true
        spin_kit.visibility = View.GONE
        pickUpDropLayout.visibility = View.GONE
        card_view.visibility = View.VISIBLE
        lay_app_bar_main.visibility = View.VISIBLE
        viewModel.onPauseSaveBooking(false)
        rideKeyBookingStatus()
    }

    override fun driverCanceledRide(title: String, reasonMsg: String) {
        checkCancelDialog(title, reasonMsg)
        viewModel.saveLocalStatus("LOGIN")
        removeDriverInformation()
    }

    override fun changeDestination(title: String, reasonMsg: String) {
        checkCancelDialog(title, " ")
    }

    override fun callCustomerCare() {
        val customerCareNumber = viewModel.getCustomerCareNumber()
        if (!customerCareNumber.isNullOrEmpty()) {
            callDriver(customerCareNumber)
        }

    }


    fun can(view: View) {

        //lay_twosheet.visibility=View.GONE
    }

    fun COUPON(view: View) {
        setTotalAmount()
        //close_cancell.setOnClickListener { dismiss() }


    }


    override fun sendCoupon(coupon: String) {
        checkCoupen(coupon)
    }

    override fun onItemClickListener(pos: Int, v: View?) {
        callDriver(oFFLINE_DriverList_taxi[pos].phoneNumber.toString())
    }

    fun txtView_Close_icon(view: View) {
        lay_off_list.visibility = View.GONE
//        pickUpDropLayout.visibility=View.VISIBLE
        spin_kit.visibility = View.GONE
        pickUpDropLayout.visibility = View.GONE
        card_view.visibility = View.VISIBLE
        lay_app_bar_main.visibility = View.VISIBLE
    }

    override fun onLocationChanged(location: Location) {


    }

}












