package com.mongokerala.taxi.newuser.ui.profile_edit

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.NonNull
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mongokerala.taxi.newuser.BuildConfig
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.data.remote.Endpoints
import com.mongokerala.taxi.newuser.data.remote.ImageUploadService
import com.mongokerala.taxi.newuser.data.remote.response.ImageResponse
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity
import com.mongokerala.taxi.newuser.ui.reqotp.ReqotpActivity
import com.mongokerala.taxi.newuser.utils.common.*
import com.mongokerala.taxi.newuser.utils.display.Toaster
import com.pixplicity.easyprefs.library.Prefs
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.yalantis.ucrop.UCrop
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_profile_edit.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProfileEditActivity : BaseActivity<ProfileEditViewModel>() {

    companion object {
        const val TAG = "ProfileEditActivity"
        var isProfileImage :String? =""
    }

    private val SAMPLE_CROPPED_IMG_NAME = "SampleCropImg"


    private lateinit var selecImage: Uri


    override fun provideLayoutId(): Int = R.layout.activity_profile_edit

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun setupView(savedInstanceState: Bundle?) {

        if(viewModel.getCurrentUserID().isNotEmpty() && viewModel.getCurrentUserID() != "null"){
            viewModel.onUserDetail()
            viewModel.onUserDetaiImage()

            if (viewModel.getfacebookLoginStatus().equals("LOGIN") || viewModel.getGmailLoginStatus().equals("LOGIN")){
                viewModel.onUserDetaiImage()
            }
        }

        img_profile.setOnClickListener {
            getImageFromFile()
        }

        cameraClick.setOnClickListener {
            getImageFromFile()
        }

        galleryClick.setOnClickListener {
            getImageFromFile()
        }

    }

    private fun getImageFromFile(){
        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .start(this)
    }

    private fun uploadNumber() {
        if (!intent.getStringExtra("UpdateMobileNumber").isNullOrEmpty()){
            img_back_update.visibility = View.GONE
            val number=intent.getStringExtra("UpdateMobileNumber").toString()
            if (etxt_mobile_number.text.toString() != number){
                etxt_mobile_number.text = number
            }
            intent.removeExtra("UpdateMobileNumber")
        }else{
            img_back_update.visibility = View.VISIBLE
        }
    }

    fun profileBack(view: View){
        val refresh = Intent(this@ProfileEditActivity, MainNewActivity::class.java)
        startActivity(refresh)
        finish()
    }
    private fun setProfileImage() {
        val userId=viewModel.getuserid()
        val image_type = "PROFILE"
        val bese_url=Prefs.getString("base_url", BuildConfig.BASE_URL)

        GlideApp.with(this)
            .load(bese_url + Endpoints.GET_IMAGE_BY_ID +image_type+"/"+userId)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .error(getDrawable(R.drawable.userr))
            .skipMemoryCache(true)
            .into(img_profile)
    }

    private fun uploadProfileImage() {

        if (this::selecImage.isInitialized) {

            val file = File(selecImage.path!!)

            val compressedFile = Compressor(this)
                .setMaxHeight(200)
                .setMaxWidth(200)
                .compressToFile(file)

            val fileBody: RequestBody =
                compressedFile.asRequestBody("image/*".toMediaTypeOrNull())

            val filePart: MultipartBody.Part =
                MultipartBody.Part.createFormData("image", compressedFile.name, fileBody)
            val type = "PROFILE"
            val userId=viewModel.getuserid()
            val userName=viewModel.getusername()

            val imageUploadService = ImageUploadService()

            imageUploadService.uploadFile(
                filePart,
                userName,
                type,
                userId

            ).enqueue(object : Callback<ImageResponse?> {
                override fun onResponse(
                    call: Call<ImageResponse?>,
                    response: Response<ImageResponse?>
                ) {
                    if (response.isSuccessful) {
                        val imageResponse = response.body()
                        Prefs.putString("user_id", userId)
                        Prefs.putString("type", type)
                    }
                }

                override fun onFailure(call: Call<ImageResponse?>, t: Throwable) {
                }
            })
        }
        val firstName=edtText_first_name.text.toString()
        val email=edtText_email.text.toString()
        val mobileNumber=etxt_mobile_number.text.toString()
        if (firstName.isNotEmpty() && email.isNotEmpty() && mobileNumber.isNotEmpty() && mobileNumber != "null" && email != "null") {
            if (Validator.EMAIL_ADDRESS.matcher(email).matches() && Validator.MOBILENUMBER.matcher(mobileNumber).matches()){
                viewModel.onProfileEdit(
                        firstName,
                        email,
                        mobileNumber,
                        img_profile)
            }else{
                if (!Validator.EMAIL_ADDRESS.matcher(email).matches()){
                    Toast.makeText(this, "Please Enter Valid EmailAddress", Toast.LENGTH_SHORT).show()
                }else if (!Validator.MOBILENUMBER.matcher(mobileNumber).matches()){
                    Toast.makeText(this, "Please Enter Valid mobileNumber", Toast.LENGTH_SHORT).show()
                }
            }

        }else{
            if (mobileNumber == "null" || mobileNumber.isEmpty()){
                Toast.makeText(this, "Please Update Your Mobile Number", Toast.LENGTH_SHORT).show()
            }else if (email == "null" || email.isEmpty()){
                Toast.makeText(this, "Please Update Your Email", Toast.LENGTH_SHORT).show()
            }
        }


    }



    override fun setupObservers() {
        super.setupObservers()
        if ((intent != null && intent.getStringExtra("MobileNumber").isNullOrEmpty()) || intent.getStringExtra("MobileNumber").equals("null")) {
            img_back_update.visibility = View.GONE
            intent.removeExtra("MobileNumber")
        } else {
            img_back_update.visibility = View.VISIBLE
        }

        viewModel.profileUpdated.observe(this) {
            it.getIfNotHandled()?.run {
                startActivity(Intent(applicationContext, MainNewActivity::class.java))
                finish()
                Toaster.show(applicationContext, "updated")
            }
        }
        viewModel.notupdated.observe(this) {
            it.getIfNotHandled()?.run {
                Toaster.show(applicationContext, "not updated")
            }
        }

        viewModel.emailField.observe(this) {
            if (edtText_email.text.toString() != it) edtText_email.setText(it)
        }

        viewModel.nameField.observe(this) {
            if (edtText_first_name.text.toString() != it) edtText_first_name.setText(it)
        }

        viewModel.updateinfo.observe(this) {
            uploadNumber()
        }

        viewModel.updateMobileNumber.observe(this) {
            if (etxt_mobile_number.text.toString() != it) {
                etxt_mobile_number.text = it
            }

        }
        viewModel.isprofileImage.observe(this) {
            if (it.equals(null) || it.equals("false")) {
                isProfileImage(it)
            } else {
                setProfileImage()
            }

        }

        viewModel.proifileImageUpload.observe(this) {
            isProfileImage = it
        }
        viewModel.mob_numberField.observe(this) {
            if (etxt_mobile_number.text.toString() != it) etxt_mobile_number.text = it
        }

        btn_update_profile_update.setOnClickListener {
            if (AppUtils.isNetworkConnected(this))
                uploadProfileImage()
        }
    }

    private fun isProfileImage(profileImage: String?) {
        if (profileImage.equals(null) || profileImage.equals("false")){
            if (viewModel.getFacebookLoginStatus().equals("LOGIN")){
                if (viewModel.doGetFbImageurl().toString().isNotEmpty()){
                    Picasso.get().load(viewModel.doGetFbImageurl()).into(img_profile)
                }
            }
            else{
                setProfileImage()
            }
            if (viewModel.getGmailLoginStatus().equals("LOGIN")){
                if (viewModel.getgMailLoginImageUrl().toString().isNotEmpty()){
                    Picasso.get().load(viewModel.getgMailLoginImageUrl()).into(img_profile)
                }
            }
            else{
                setProfileImage()
            }

        }else{
            setProfileImage()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                selecImage = result.uri
                Glide.with(this)
                    .load(selecImage)
                    .into(img_profile)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        }
    }

    private fun startCrop(@NonNull uri: Uri) {
        var destinationFileName: String =
            SAMPLE_CROPPED_IMG_NAME + System.currentTimeMillis()
        destinationFileName += ".jpg"
        val ucrop = UCrop.of(
            uri,
            Uri.fromFile(File(cacheDir, destinationFileName))
        )
        ucrop.withAspectRatio(1f, 1f)
        ucrop.withMaxResultSize(450, 450)
        getCropOptions().let { ucrop.withOptions(it) }
        ucrop.start(this)
    }

    private fun getCropOptions(): UCrop.Options {
        val options = UCrop.Options()
        options.setCompressionQuality(70)

        //UI
        options.setHideBottomControls(false)
        options.setFreeStyleCropEnabled(false)

        //Colors
        options.setStatusBarColor(resources.getColor(R.color.blue_900))
        options.setToolbarColor(resources.getColor(R.color.blue_100))
        options.setToolbarTitle("Recortar image")

        return options
    }

   fun validateMobileNumberProfile(view:View){
       val mobileProfileActivity = Intent(this, ReqotpActivity::class.java)
       mobileProfileActivity.putExtra(Constants.is_Settings, true)
       startActivity(mobileProfileActivity)
       finish()
   }
}
