package com.mongokerala.taxi.newuser.paymentgateway.util;

import com.mongokerala.taxi.newuser.paymentgateway.StripePaymentRequestOrder;
import com.mongokerala.taxi.newuser.paymentgateway.model.StripeResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PostRequestSender {

    //String BASE_URL = "http://63.142.252.78:8080";
    //String BASE_URL = "http://10.0.2.2:8080";

    //http://63.142.252.78:8080/paymentgateway-api/api/orders/v1/order/strip-payment/create
    String BASE_URL = "http://1-dot-taxi2dealin.appspot.com";

    @POST("/api/orders/v1/order/strip-payment/create")
    Call<StripeResponse> createStripeOrderPayment(@Body StripePaymentRequestOrder orderDTO);

}
