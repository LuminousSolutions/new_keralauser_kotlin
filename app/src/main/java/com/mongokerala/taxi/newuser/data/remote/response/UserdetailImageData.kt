package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class UserdetailImageData (
        @SerializedName("isImage") val isImage : String?,
        @SerializedName("id") val id : String,
        @SerializedName("email") val email : String,
        @SerializedName("firstName") val firstName : String,
        @SerializedName("lastName") val lastName : String,
        @SerializedName("lang") val lang : String,
        @SerializedName("latitude") val latitude : Int,
        @SerializedName("longitude") val longitude : Int,
        @SerializedName("phoneNumber") val phoneNumber : String,
        @SerializedName("role") val role : String,
        @SerializedName("phoneVerified") val phoneVerified : String?
)