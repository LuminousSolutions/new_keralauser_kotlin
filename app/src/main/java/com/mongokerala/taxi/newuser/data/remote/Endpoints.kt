package com.mongokerala.taxi.newuser.data.remote

object Endpoints {

    const val DUMMY = "dummy/list"
    const val LOGIN = "login/mindorks"
    const val HOME_POSTS_LIST = "driver/post/list"
    const val POST_LIKE = "driver/post/like"
    const val POST_UNLIKE = "driver/post/unlike"


    const val GET_IMAGE_BY_ID = "/driver-photo/id/"
    const val ADD_PHOTO = "/driver-photo"
    //const val INDIA_FORGOT_SMS = "/app/phoneNumber/v1/twillo/forgotPassword/sms"
    const val INDIA_FORGOT_SMS = "/mobileVerification/app/phoneNumber/v1/twillo/forgotPassword/sms"

    //const val FORGET_PASSWORD_REQ_OTP = "/app/phoneNumber/v1/twillo/sms"
    const val FORGET_PASSWORD_REQ_OTP = "/mobileVerification/app/phoneNumber/v1/twillo/sms"

    //serach api
    const val SEARCH_API = "/search/v1/image/advanceSearch"
    const val VEHICLES_LATLNG_API = "/search/v2/image/advanceSearch"
    const val BOOKING_API = "/notification/tripId/all/user/back/todevice"
    const val COUPON_BOOKING_API = "/notification/v1/tripId/all/user/back/todevice"

    // wallet api
    const val ENDPOINT_CHECK_WALLET_BALANCE = "/wallet/app/v1/find/{userId}"
    const val ENDPOINT_CHECK_WALLET_ADD = "/wallet/app/v1/add"
    const val ENDPOINT_CHECK_WALLET_TOKENCHECK ="/wallet/app/v1/find/token/{token}"

    //Taxi deals end point
    val ENDPOINT_SERVER_ForgetPassword: String = "/app/user/v1/forgotpassword/retrieve/{email}"
    const val ENDPOINT_SERVER_TaxiDetails = "/taxi/app/taxi/v1/details/{taxiId}"
    const val ENDPOINT_SERVER_UserDetails = "/logout/app/userDetail/v1/{userId}"
    const val ENDPOINT_SERVER_CheckUserImage = "/logout/app/userDetail/v2/{userId}"
    const val ENDPOINT_SERVER_VehicleInfo = "/vehicleType/app/taxi/v1/type/updateInfo"
    const val ENDPOINT_SERVER_ProfileUpdate = "/logout/app/user/v2/update"
    const val ENDPOINT_SERVER_ProfileImage = "/driver-photo"
    const val ENDPOINT_SERVER_LOGIN = "/userLogin/app/user/v1/mobile/user/login"
    const val ENDPOINT_RIDE_CREATE = "/ride/v1/dummy/add"
    const val ENDPOINT_RIDE_STATUS_WITHREASON = "/ride/v2/dummy/add"
    const val ENDPOINT_SEND_MESSAGES = "/app/message/v1/create"
    const val ENDPOINT_STATUS_UPDATE =
        "/search/app/taxi/v1/status/{taxiId}/{userId}/{latitude}/{longitude}/{driverStatus}"
    const val ENDPOINT_DRIVERBILL_TOTAL_AMOUNT = "/app/driverBilling/v1/{supplierId}/{driverId}"
//    const val ENDPOINT_FEEDBACK = "/app/contactus/mail/create"
    const val ENDPOINT_FEEDBACK = "/contact/app/mail/create"
    const val ENDPOINT_RATE_US = "/contact/app/mail/create"
    const val ENDPOINT_REVIEW = "/review/v1/add"
    const val ENDPOINT_LOGOUT = "/logout/app/admin/user/v1/loginStatus/{userId}/{loginStatus}"
    const val ENDPOINT_STATUS_UPDATE_LANGUAGE = "/app/user/v1/language/update/{userId}/{language}"
//    const val ENDPOINT_OTP_REQ = "/app/phoneNumber/v1/twillo/new/sms"
    const val ENDPOINT_OTP_REQ = "/mobileVerification/app/phoneNumber/v1/twillo/new/sms"
    const val ENDPOINT_RIDE_OTP_SMS_TO_USER = "/mobileVerification/app/phoneNumber/v1/twillo/ride/verification/sms"
    const val ENDPOINT_VALIDATE_OTP_REQ = "/mobileVerification/app/user/twillo/v1/token"
    const val ENDPOINT_VALIDATE_RIDE_OTP_REQ = "/mobileVerification/app/user/twillo/v1/ride/token"
    const val ENDPOINT_CONFIG = "/app/config/getAll"
    const val ENDPOINT_UPDATE_REVIEW = "/app/review/v1/create"
    //trip history api user---surya
    const val ENDPOINT_TRIP_HISTORY = "/ride/app/v1/user/get/{userId}"
//    const val ENDPOINT_TRIP_HISTORY = "/ride/app/ride/v1/driver/get/{driverId}"
   // const val ENDPOINT_TRIP_HISTORY = "/ride/app/v1/get/check/id/{driverId}"
    const val ENDPOINT_FAV_PLACE = "/app/taxi/v1/search/{code}"
 //   const val ENDPOINT_TRIP_INVOICE = "/app/ride/v1/invoice/{rideId}"
    const val ENDPOINT_TRIP_INVOICE = "/ride/app/ride/v1/invoice/{rideId}"
    //const val ENDPOINT_SIGNUP_REQ = "/userLogin/app/user/v1/mobile/full/registration"
    const val ENDPOINT_SIGNUP_REQ = "/userLogin/app/user/v1/mobile/full/registration"
//    const val ENDPOINT_FCM_TOKEN = "/logout/app/user/v1/fcm/update/{userId}/{oneSignalValue}"
    const val ENDPOINT_FCM_TOKEN = "/user/v1/find/id/token"
    const val ENDPOINT_UPDATE_PRICE_KM = "/ride/app/v1/rideUpdateFinished/gps/"
    const val ENDPOINT_FCM_REQUEST = ("https://fcm.googleapis.com" + "/fcm/send")
    const val UPDATE_MOBILE_REQ_OTP = "/app/phoneNumber/v1/twillo/update/sms"
    const val VALIDATE_MOBILE_REQ_OTP = "/app/user/twillo/v1/password/token"
    const val VALIDATE_UPDATED_MOBILE_REQ_OTP = "/app/user/twillo/v1/updateNumber/token"
    const val ENDPOINT_CHANGES_DES = "/app/ride/v1/rideUpdates/changeDest/{rideId}/{dest}"
    const val OFFLINE_BOOKING_SMS = "/phoneBooking/app/phoneBooking/v1/userOffline/sms/create"
    const val OFFLINE_BOOKING_SMS_FINAL = "/app/phoneBooking/v1/userOffline/final/create"
    const val USER_OFFLINE = "/phoneBooking/app/phoneBooking/v1/userOffline/create"
    const val USER_OFFLINE_PRICE_CALCULATION = "/ride/app/v1/PriceCalculation/gps/"
    const val CHECK_COUPON = "/coupan/v1/find/supplierId/{coupon}"
    const val ENDPOINTS_TOGET_AMOUNT = "/billing/v1/get/{id}"
    const val COUPON_PRICE="/coupan/v1/find/copon/{coupon}/{amount}"
    const val FBLOGIN="/social/app/v1/add/user"


}