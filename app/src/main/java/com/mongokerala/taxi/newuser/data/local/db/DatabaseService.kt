package com.mongokerala.taxi.newuser.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mongokerala.taxi.newuser.data.local.db.dao.DummyDao
import com.mongokerala.taxi.newuser.data.local.db.entity.DriverDetails
import javax.inject.Singleton

@Singleton
@Database(
    entities = [
        DriverDetails::class
    ],
    exportSchema = false,
    version = 1
)
abstract class DatabaseService : RoomDatabase() {

    abstract fun dummyDao(): DummyDao
}