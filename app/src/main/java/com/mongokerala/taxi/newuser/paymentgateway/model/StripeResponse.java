package com.mongokerala.taxi.newuser.paymentgateway.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.stripe.android.model.Customer;

public class StripeResponse {
    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("orderReqType")
    @Expose
    private String orderReqType;
    @SerializedName("stripePaymentRequest")
    @Expose
    private StripePaymentRequest stripePaymentRequest;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getOrderReqType() {
        return orderReqType;
    }

    public void setOrderReqType(String orderReqType) {
        this.orderReqType = orderReqType;
    }

    public StripePaymentRequest getStripePaymentRequest() {
        return stripePaymentRequest;
    }

    public void setStripePaymentRequest(StripePaymentRequest stripePaymentRequest) {
        this.stripePaymentRequest = stripePaymentRequest;
    }
}
