package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class SupplierDTO {

    @SerializedName("id")
    @Expose
    private val id: Int? = null

    @SerializedName("licenseNumber")
    @Expose
    private val licenseNumber: String? = null

    @SerializedName("name")
    @Expose
    private val name: String? = null

    @SerializedName("userId")
    @Expose
    private val userId: Int? = null
}