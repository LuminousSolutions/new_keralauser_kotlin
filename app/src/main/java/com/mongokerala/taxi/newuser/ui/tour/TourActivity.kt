package com.mongokerala.taxi.newuser.ui.tour


import android.Manifest.permission
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.login.LoginActivity
import com.mongokerala.taxi.newuser.ui.registration.RegistrationActivity
import com.mongokerala.taxi.newuser.ui.reqotp.ReqotpActivity
import com.mongokerala.taxi.newuser.utils.common.Constants
import com.mongokerala.taxi.newuser.utils.common.Event
import kotlinx.android.synthetic.main.activity_tour.*
import java.util.*


class TourActivity : BaseActivity<TourViewModel>() {

    companion object {
        const val TAG = "TourActivity"
    }

    private val REQUEST_MULTIPLE_PERMISSIONS = 1


    override fun provideLayoutId(): Int = R.layout.activity_tour

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun setupView(savedInstanceState: Bundle?) {
        checkAndRequestPermission()

        lay_login.setOnClickListener { viewModel.ToLogin() }
        lay_Register.setOnClickListener {

            val mobileNumberValidation = Intent(this, ReqotpActivity::class.java)
            mobileNumberValidation.putExtra(
                Constants.mobileNumberValidation,
                Constants.mobileNumberValidation
            )
            startActivity(mobileNumberValidation)
            finish()
        }
        btnplaystore.setOnClickListener {
            try {
                val playstoreuri1: Uri = Uri.parse("https://play.google.com/store/apps/details?id=com.mongokerala.taxi.newuser&hl=en_IN" + packageName)
                //or you can add
                //var playstoreuri:Uri=Uri.parse("market://details?id=manigautam.app.myplaystoreratingapp")
                val playstoreIntent1: Intent = Intent(Intent.ACTION_VIEW, playstoreuri1)
                startActivity(playstoreIntent1)
                //it genrate exception when devices do not have playstore
            }catch (exp:Exception){
                val playstoreuri2: Uri = Uri.parse("https://play.google.com/store/apps/details?id=com.mongokerala.taxi.newuser&hl=en_IN" + packageName)
                //var playstoreuri:Uri=Uri.parse("https://play.google.com/store/apps/details?id=manigautam.app.myplaystoreratingapp")
                val playstoreIntent2: Intent = Intent(Intent.ACTION_VIEW, playstoreuri2)
                startActivity(playstoreIntent2)
            }

        }


        val MyPagerAdapter = MyPagerAdapter(supportFragmentManager)
        viewPager.adapter = MyPagerAdapter

    }


    override fun setupObservers() {
        super.setupObservers()
        // Event is used by the view model to tell the activity to launch another activity
        // view model also provided the Bundle in the event that is needed for the Activity
        viewModel.launchMain.observe(this) {
            it.getIfNotHandled()?.run {
                startActivity(Intent(applicationContext, LoginActivity::class.java))
                finish()
            }
        }

        viewModel.launchReqotp.observe(this) {
            it.getIfNotHandled()?.run {
                startActivity(Intent(applicationContext, RegistrationActivity::class.java))
                finish()
            }
        }


    }

    private fun checkAndRequestPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            val listOfPermissionsNedded: MutableList<String> =
                ArrayList()
            val camera = ContextCompat.checkSelfPermission(
                Objects.requireNonNull(this), permission.CAMERA
            )
            val permissionPhoneState = ContextCompat.checkSelfPermission(
                this,
                permission.READ_PHONE_STATE
            )
            val permissionCoarseLocation =
                ContextCompat.checkSelfPermission(
                    this,
                    permission.ACCESS_COARSE_LOCATION
                )
            val recive = ContextCompat.checkSelfPermission(
                this,
                permission.RECEIVE_SMS
            )
            val read =
                ContextCompat.checkSelfPermission(this, permission.READ_SMS)
            val READ_EXTERNAL = ContextCompat.checkSelfPermission(
                this,
                permission.READ_EXTERNAL_STORAGE
            )
            val ACCESS_FINE = ContextCompat.checkSelfPermission(
                this,
                permission.ACCESS_FINE_LOCATION
            )
            if (camera != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.CAMERA)
            }
            if (permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.ACCESS_COARSE_LOCATION)
            }
            if (permissionPhoneState != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.READ_PHONE_STATE)
            }
            if (recive != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.RECEIVE_SMS)
            }
            if (read != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.READ_SMS)
            }
            if (READ_EXTERNAL != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.READ_EXTERNAL_STORAGE)
            }
            if (ACCESS_FINE != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.ACCESS_FINE_LOCATION)
            }
            if (!listOfPermissionsNedded.isEmpty()) {
                requestPermissions(
                    listOfPermissionsNedded.toTypedArray(),
                    REQUEST_MULTIPLE_PERMISSIONS
                )
            }
        }
    }


class MyPagerAdapter(supportFragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(supportFragmentManager) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> FirstTourFragment.newInstance("FirstTourFragment, Instance 1")
            1 -> SecondTourFragment.newInstance("SecondTourFragment, Instance 1")
            2 -> ThirdTourFragment.newInstance("ThirdTourFragment, Instance 1")
            else -> FourthTourFragment.newInstance("fourthFragment, Default")
        }
    }

    override fun getCount(): Int {
        return 4
    }

}



}


