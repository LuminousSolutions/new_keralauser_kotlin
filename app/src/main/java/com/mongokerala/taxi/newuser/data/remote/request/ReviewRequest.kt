package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ReviewRequest (
    @field:SerializedName(Keys.userId) @field:Expose var userId: String?,
    @field:SerializedName(Keys.taxiDetailID) @field:Expose var taxiDetailId: String?,
    @field:SerializedName(Keys.rating) @field:Expose var rating: String?,
    @field:SerializedName(Keys.driverId) @field:Expose var driverId: String?,
    @field:SerializedName(Keys.rideId) @field:Expose var rideId: String?,
    @field:SerializedName(Keys.postedBy) @field:Expose var postedBy: String?
){

    @SerializedName("comment")
    @Expose
    public var comment: String? = null

    @SerializedName("formattedDate")
    @Expose
    public var formattedDate: String? = null

    @SerializedName("id")
    @Expose
    public var id: String? = null

    @SerializedName("updatedOn")
    @Expose
    public var updatedOn: String? = null

    @SerializedName("userFullName")
    @Expose
    public var userFullName: String? = null

}