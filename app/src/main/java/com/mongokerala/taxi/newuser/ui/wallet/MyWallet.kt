package com.mongokerala.taxi.newuser.ui.wallet

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.mongokerala.taxi.newuser.R

class MyWallet(context: Context):Dialog(context) {

    var walletbalnce: String? = ""
    private var forthisride: String? = ""
    private var Currentwalletbalance: String? = ""
    private var rideamount: String? = ""
    var ReducedAmount: String? = ""
    private var totalamount: String? = ""
    private var carPrice: String? = ""
    private var walletAmount: String? =""



    constructor(context: Context, car_price:String, wallet_amount:String):this(context){

        carPrice = car_price
        walletAmount =wallet_amount
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_wallet_balance_details)

        val walletbalnce =findViewById<TextView>(R.id.walletbalnce) as TextView
        val wallet_balance =findViewById<TextView>(R.id.wallet_balance) as TextView
        val forthisride =findViewById<TextView>(R.id.forthisride) as TextView
        val forthisride_value =findViewById<TextView>(R.id.forthisride_value) as TextView
        val Currentwalletbalance = findViewById<TextView>(R.id.Currentwalletbalance) as TextView
        val Currentwalletbalance_value = findViewById<TextView>(R.id.Currentwalletbalance_value) as TextView
        val rideamount = findViewById<TextView>(R.id.rideamount) as TextView
        val rideamountvalue = findViewById<TextView>(R.id.rideamountvalue) as TextView
        val ReducedAmount = findViewById<TextView>(R.id.ReducedAmount) as TextView
        val ReducedAmount_values = findViewById<TextView>(R.id.ReducedAmount_values) as TextView
        val totalamount = findViewById<TextView>(R.id.totalamount) as TextView
        val totalamount_value = findViewById<TextView>(R.id.totalamount_value) as TextView
//        val btn_ok = findViewById<TextView>(R.id.btn_ok) as TextView
        val wd_close_cancel =findViewById<TextView>(R.id.wd_close_cancel) as TextView
        val btn = findViewById<Button>(R.id.btn_okk) as Button

        if (!walletAmount.isNullOrEmpty()){
            wallet_balance.text = walletAmount.toString()

        }
        if (!carPrice.isNullOrEmpty()){
            rideamountvalue.text =carPrice.toString()
            var forthisride = carPrice?.toDouble()?.times(0.2)
            forthisride_value.text = forthisride?.toInt().toString()
             var walletamount_i : Int? = walletAmount?.toInt()
             var walletamount_d : Double? = walletamount_i?.toDouble()


            if (walletamount_d != null) {
                if (walletamount_d > forthisride!!){
                    var currentwallet_balance:Double? = walletamount_d.minus(forthisride)
                    Currentwalletbalance_value.text =currentwallet_balance.toString()
                    ReducedAmount_values.text = forthisride.toString()
                    var car_price_i = carPrice?.toDouble()
                    var car_price_d = car_price_i
                    var totalamount = car_price_d?.minus(forthisride)
                    totalamount_value.text = totalamount.toString()
                }else{
                    Currentwalletbalance_value.text = "0"
                    ReducedAmount_values.text = walletamount_d.toString()
                    var car_price_i = carPrice?.toDouble()
                    var car_price_d = car_price_i
                     var totalrideamount = car_price_d?.minus(walletamount_d)
                    totalamount_value.text = totalrideamount.toString()

                }
            }
        }


      /*  wd_close_cancel.setOnClickListener { dismiss() }

        btn.setOnClickListener {
            dismiss()
        }*/


    }






}

