package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class VehicleInfoUpdateRequest(

    @field:SerializedName(Keys.carType) @field:Expose var carType: String?,
    @field:SerializedName(Keys.seats) @field:Expose var seats: String?,
    @field:SerializedName(Keys.taxiId) @field:Expose var taxiId: String?,
    @field:SerializedName(Keys.taxiNumber) @field:Expose var taxiNumber: String?,
    @field:SerializedName(Keys.userId) @field:Expose var userId: String?,
    @field:SerializedName(Keys.supplierId) @field:Expose var supplierId: String?,
    @field:SerializedName(Keys.vehicleBrand) @field:Expose var vehicleBrand: String?,
    @field:SerializedName(Keys.vehicleYear) @field:Expose var vehicleYear: String?
) {


    @SerializedName("email")
    @Expose
    private val email: String? = null

    @SerializedName("currency")
    @Expose
    private val currency: String? = null

    @SerializedName("description")
    @Expose
    private val description: String? = null

    @SerializedName("destination")
    @Expose
    private val destination: String? = null

    @SerializedName("hour")
    @Expose
    private val hour: String? = null

    @SerializedName("id")
    @Expose
    private val id: String? = null

    @SerializedName("km")
    @Expose
    private val km: Float? = null

    @SerializedName("name")
    @Expose
    private val name: String? = null

    @SerializedName("phoneNumber")
    @Expose
    private val phoneNumber: String? = null

}