package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginRequest(

    @field:SerializedName(Keys.email) @field:Expose var email: String?,
    @field:SerializedName(Keys.password) @field:Expose var password: String?,
    @field:SerializedName(Keys.phoneNumber) @field:Expose var phoneNumber: String
) { // This class is not publicly instantiable




    @Expose
    @SerializedName(Keys.domain)
    var domain: String

    @Expose
    @SerializedName(Keys.token)
    var token: String

  /*  @Expose
    @SerializedName(Keys.role)
    var role: String*/


    init {
        domain = "KERALACABS"
        token = "string"
//        role =  ERole.ROLE_USER.toString()
    }


}