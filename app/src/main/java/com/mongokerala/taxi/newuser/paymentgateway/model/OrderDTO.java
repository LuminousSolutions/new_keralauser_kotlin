package com.mongokerala.taxi.newuser.paymentgateway.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mongokerala.taxi.newuser.paymentgateway.enums.OrderRequestType;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public class OrderDTO {
    @SerializedName("orderReqType")
    @Expose
    private OrderRequestType orderReqType;
    private String orderId;
    @SerializedName("customer")
    @Expose
    private CustomerDTO customer; // customer info doing payment
    private PGDataDTO pgData; // payment gateway checksu
    private StripePaymentRequest stripePaymentRequest;


    public OrderRequestType getOrderReqType() {
        return orderReqType;
    }

    public void setOrderReqType(OrderRequestType orderReqType) {
        this.orderReqType = orderReqType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public PGDataDTO getPgData() {
        return pgData;
    }

    public void setPgData(PGDataDTO pgData) {
        this.pgData = pgData;
    }

    public StripePaymentRequest getStripePaymentRequest() {
        return stripePaymentRequest;
    }

    public void setPgData(StripePaymentRequest stripePaymentRequest) {
        this.stripePaymentRequest = stripePaymentRequest;
    }


}
