package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class RideDummyCreateRequest(
    @field:SerializedName(Keys.driverId) @field:Expose var driverId: String?,
    @field:SerializedName("status") @field:Expose var status: String?,
    @field:SerializedName(Keys.source) @field:Expose var source: String?,
    @field:SerializedName(Keys.destination) @field:Expose var destination: String?,
    @field:SerializedName(Keys.userId) @field:Expose var userId: String?,
    @field:SerializedName(Keys.payment) @field:Expose var payment: String?
//    @field:SerializedName(Keys.category) @field:Expose var category: String?
    ) {
    @SerializedName("category")
    @Expose
    private var category: String? = null

    @SerializedName("latitude")
    @Expose
    private var latitude: Int? = null

    @SerializedName("longitude")
    @Expose
    private var longitude: Int? = null

    @SerializedName("type")
    @Expose
    private var type: String? = null

    @SerializedName("tripId")
    @Expose
    private var tripId: String? = null

    init {
        category="TAXI"
        latitude = 0
        longitude = 0
        type = "string"
        tripId = "string"
    }

}