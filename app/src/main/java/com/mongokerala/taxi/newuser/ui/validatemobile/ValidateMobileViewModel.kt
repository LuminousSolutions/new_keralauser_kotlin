package com.mongokerala.taxi.newuser.ui.validatemobile

import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class ValidateMobileViewModel (
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    val launchCorrectOtp: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchWrongOtp: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()

    val launchValidate: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchValidateUsedNumber: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val mainIn: MutableLiveData<Boolean> = MutableLiveData()


    override fun onCreate() {
    }


    fun onValidateOtpRegistration(token: String){

        mainIn.postValue(true)
        compositeDisposable.addAll(
            userRepository.doValidateOtpRequest(token,
                userRepository.getMobileNUmber().toString()
            )
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if(it.infoId.equals("500")) {
                            launchCorrectOtp.postValue(Event(emptyMap()))
                            mainIn.postValue(false)

                        }
                        else if(it.infoId.equals("701")){
                            launchWrongOtp.postValue(Event(emptyMap()))
                            mainIn.postValue(false)
                        }else{

                        }

                    },
                    {
                        handleNetworkError(it)
                        mainIn.postValue(false)
                    }
                )
        )
    }

    //Resend Otp
    fun onSubmitClick(){

        mainIn.postValue(true)
        compositeDisposable.addAll(
            userRepository.doUserOtpRequest("+91",
                userRepository.getClientMobileNUmber().toString()
            )
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if(it.infoId.equals("500")) {
                            launchValidate.postValue(Event(emptyMap()))
                            mainIn.postValue(false)
                        }
                        else if(it.infoId.equals("102")){
                            launchValidateUsedNumber.postValue(Event(emptyMap()))
                            mainIn.postValue(false)
                        }else{
                            mainIn.postValue(false)
                        }

                    },
                    {
                        handleNetworkError(it)
                        mainIn.postValue(false)
                    }
                )
        )
    }


}