package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

class OfflineBookingData (
    @SerializedName("userId") val userId : String
)