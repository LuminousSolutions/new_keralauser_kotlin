package com.mongokerala.taxi.newuser.utils.network

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.mongokerala.taxi.newuser.data.remote.LocationData
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.getFirstDoneLatLong
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.getFirstDoneLatLongNew
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.getFirstLatLong
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.getIsContext
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.getIsDistanceMeter
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.getIsDoneStartRide
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.setFirstLatLong
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.setIsTotalKm
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.setIslatNewfinal
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.setIslatoldfinal
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.setIslonNewfinal
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.setIslonoldfinal
import com.mongokerala.taxi.newuser.utils.common.Constants
import java.text.DecimalFormat

class LocationTrackingService: Service() {

    var locationManager: LocationManager? = null


    override fun onBind(intent: Intent?) = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onCreate() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        if (locationManager == null)
            locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager



        try {
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, INTERVAL, DISTANCE, locationListeners[1])
        } catch (e: SecurityException) {
            Log.e(TAG, "Fail to request location update", e)
        } catch (e: IllegalArgumentException) {
            Log.e(TAG, "Network provider does not exist", e)
        }

        try {
            locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, INTERVAL, DISTANCE, locationListeners[0])
        } catch (e: SecurityException) {
            Log.e(TAG, "Fail to request location update", e)
        } catch (e: IllegalArgumentException) {
            Log.e(TAG, "GPS provider does not exist", e)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (locationManager != null)
            for (i in 0..locationListeners.size) {
                try {
                    locationManager?.removeUpdates(locationListeners[i])
                } catch (e: Exception) {
                    Log.w(TAG, "Failed to remove location listeners")
                }
            }
    }


    companion object {
        val TAG = "LocationTrackingService"

        val INTERVAL = 1000.toLong() // In milliseconds
        val DISTANCE = 1.toFloat() // In meters

        val locationListeners = arrayOf(
            LTRLocationListener(LocationManager.GPS_PROVIDER),
            LTRLocationListener(LocationManager.NETWORK_PROVIDER)
        )

        class LTRLocationListener(provider: String) : android.location.LocationListener {

            val lastLocation = Location(provider)
            private var lat1 = 0.0
            private var lon1 = 0.0
            private var latFirst = 0.0
            private var lonFirst = 0.0
            private var distanceInMeter = getIsDistanceMeter()
            private val results = FloatArray(1)
            private var total_km = "0.0"
            private var total_kmnew = 0.0

//             modify Location class safe string due to sdk version 29 to 30
            override fun onLocationChanged(location: Location) {
               lastLocation.set(location)
                val location_data = LocationData()
                if(getIsDoneStartRide() == true) {
                    if (getFirstLatLong() == true) {
                        lat1 = 0.0
                        lon1 = 0.0
                        total_km = "0.0"
                        distanceInMeter = getIsDistanceMeter()
                        setFirstLatLong(false)
                    }
                }
                Log.e("Action is : ", location!!.latitude.toString() + location.longitude.toString())
                Log.d("TAG", location!!.latitude.toString() + location.longitude.toString())
               /* FireStoreCloud.updateLatLong(location.latitude.toString(),
                    location.longitude.toString(),getIsSupplierID(),getIsUserID())*/
                MainNewActivity.setIsLat(location.latitude)
                MainNewActivity.setIsLong(location.longitude)
                var aBoolean_lon1 = lon1 != 0.0
                var aBoolean_lat1 = lat1 != 0.0
                var latitude = location.latitude
                var longitude = location.longitude
                var aBoolean_latitude = latitude != 0.0
                var aBoolean_longitude = longitude != 0.0
                var lonNew : Float = lon1.toFloat()
                var latNew : Float = lat1.toFloat()
                location_data.lat = location.latitude.toString()
                location_data.long = location.longitude.toString()
                location_data.action = "LOCATION_DATA"
                val intent = Intent(MainNewActivity.BROADCAST_TAG)
                intent.putExtra(Constants.CUSTOM_LOCATION_DATA, location_data)
                val localBroadcastManager =
                        LocalBroadcastManager.getInstance(getIsContext())
                localBroadcastManager.sendBroadcast(intent)
                localBroadcastManager.sendBroadcast(intent)
                var lonold : Float = longitude.toFloat()
                var latold : Float = latitude.toFloat()
                var dflonNew = DecimalFormat("#.###")
                //dflonNew.roundingMode = RoundingMode.CEILING
                var lonNewfinal = dflonNew.format(lonNew)
                var latNewfinal = dflonNew.format(latNew)
                var lonoldfinal = dflonNew.format(lonold)

                var latoldfinal = dflonNew.format(latold)
                setIslonNewfinal(lonNewfinal.toDouble())
                setIslatNewfinal(latNewfinal.toDouble())
                setIslonoldfinal(lonoldfinal.toDouble())
                setIslatoldfinal(latoldfinal.toDouble())
                if (aBoolean_lon1 && aBoolean_latitude && aBoolean_longitude && aBoolean_lat1) {
                    if(getIsDoneStartRide() == true) {
                        Location.distanceBetween(lat1, lon1, latitude, longitude, results)
                        println("Distance is: " + results[0])
                        distanceInMeter += results[0]
                        val df = DecimalFormat("#.##") // adjust this as appropriate
                        total_km = df.format(distanceInMeter * 0.001)
                        setIsTotalKm(total_km)
                        MainNewActivity.setIsLocABTotalkm(total_km)
                        Log.e("total_km is : ", total_km)
                        Log.e("RealRideAction is : ", location!!.latitude.toString() + location.longitude.toString())
                    }
                }else{
                    if(getIsDoneStartRide() == true) {
                        if (getFirstDoneLatLongNew() == true) {
                            lat1 = latitude
                            lon1 = longitude
                            MainNewActivity.setFirstDoneLatLongNew(false)
                        }
                    }
                }
                if(getIsDoneStartRide() == true) {
                    if(getFirstDoneLatLong() == true){
                        latFirst = latitude
                        lonFirst = longitude
                        MainNewActivity.setFirstDoneLatLong(false)
                    }
                    val locationA = Location("LocationA")
                    locationA.latitude = latFirst
                    locationA.longitude = lonFirst

                    val locationB = Location("LocationB")
                    locationB.latitude = latitude
                    locationB.longitude = longitude
                    //total_kmnew = (locationA.distanceTo(locationB) / 1000).toDouble()
                }
            }


//          modify Location class safe string due to sdk version 29 to 30
            override fun onProviderDisabled(provider: String) {
            }

//           modify Location class safe string due to sdk version 29 to 30
            override fun onProviderEnabled(provider: String) {
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            }

        }
    }

}