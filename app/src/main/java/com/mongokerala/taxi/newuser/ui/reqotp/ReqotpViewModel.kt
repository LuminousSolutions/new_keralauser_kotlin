package com.mongokerala.taxi.newuser.ui.reqotp

import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.Constants
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class ReqotpViewModel (
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {


    val launchValidate: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchProfile: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchValidateUsedNumber: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val mobileNumber: MutableLiveData<String> = MutableLiveData()
    val mainIn: MutableLiveData<Boolean> = MutableLiveData()
    val numberExisting:MutableLiveData<Event<Map<String, String>>> = MutableLiveData()


    override fun onCreate() {
    }

    fun getCurrentUserID(): String? {
        return userRepository.getCurrentUserID()
    }

    fun onSubmitClick(country_code: String,number: String,intentKey:String){
        mainIn.postValue(true)

        compositeDisposable.addAll(
            userRepository.doUserOtpRequest(country_code,number)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {

                        if (intentKey == Constants.mobileNumberValidation){
                            if (it.status == "true"){
                                launchValidate.postValue(Event(emptyMap()))
                                userRepository.saveMobileNumber(number)
                            }else{
                                numberExisting.postValue(Event(emptyMap()))
                            }

                        }else{
                            if (it.status == "true"){
                                mobileNumber.postValue(number)
                                launchProfile.postValue(Event(emptyMap()))
                            }else{
                                numberExisting.postValue(Event(emptyMap()))
                            }
                        }
                    },
                    {
                        handleNetworkError(it)
                        mainIn.postValue(false)
                    }
                )
        )
    }

}