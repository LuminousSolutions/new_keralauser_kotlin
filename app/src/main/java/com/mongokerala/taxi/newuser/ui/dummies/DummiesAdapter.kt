package com.mongokerala.taxi.newuser.ui.dummies

import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import com.mongokerala.taxi.newuser.data.model.Dummy
import com.mongokerala.taxi.newuser.ui.base.BaseAdapter

class DummiesAdapter(
    parentLifecycle: Lifecycle,
    private val dummies: ArrayList<Dummy>
) : BaseAdapter<Dummy, DummyItemViewHolder>(parentLifecycle, dummies) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = DummyItemViewHolder(parent)
}