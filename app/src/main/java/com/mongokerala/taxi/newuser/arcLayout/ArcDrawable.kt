package com.mongokerala.taxi.newuser.arcLayout

import android.annotation.TargetApi
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Build

internal class ArcDrawable(private val arc: Arc, val radius: Int, color: Int) : Drawable() {
    private val arcPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var arcPath: Path? = null

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
        super.setBounds(left, top, right, bottom)
        ensurePath(left, top, right, bottom)
    }

    override fun draw(canvas: Canvas) {
        canvas.drawPath(arcPath!!, arcPaint)
    }

    override fun setAlpha(alpha: Int) {
        arcPaint.alpha = alpha
    }

    override fun setColorFilter(cf: ColorFilter?) {
        arcPaint.colorFilter = cf
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

    override fun getIntrinsicWidth(): Int {
        return arc.computeWidth(radius)
    }

    override fun getIntrinsicHeight(): Int {
        return arc.computeHeight(radius)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getOutline(outline: Outline) {
        if (arcPath == null || !arcPath!!.isConvex) {
            super.getOutline(outline)
        } else {
            outline.setConvexPath(arcPath!!)
        }
    }

    private fun ensurePath() {
        val r = bounds
        ensurePath(r.left, r.top, r.right, r.bottom)
    }

    private fun ensurePath(left: Int, top: Int, right: Int, bottom: Int) {
        arcPath = arc.computePath(radius, left, top, right, bottom)
    }

    init {
        arcPaint.color = color
    }
}
