package com.mongokerala.taxi.newuser.paymentgateway.util;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Akhi007 on 23-01-2019.
 */
public class StripePaymentConstants {
    private static final String TAG = StripePaymentConstants.class.getSimpleName();

    //By Default PROD ENV is false
    static boolean isProduction = false;
    public enum KEY {
        PUBLISHABLE_KEY
    }

    //TODO : Change API KEYS HERE
    @NonNull
    public static Map<KEY, String> getStripPaymentCredentialMap(){
        if(isProduction){
            //Production Environment
            Map<KEY, String> productionMap = new HashMap<>();
            productionMap.put(KEY.PUBLISHABLE_KEY,"pk_test_UhH84hSlVwcHRsHvNOMbCOMT00tIeCosrQ");
            return productionMap;
        }else{
            //Staging Environment
            Map<KEY, String> stagingMap = new HashMap<>();
            stagingMap.put(KEY.PUBLISHABLE_KEY,"pk_test_UhH84hSlVwcHRsHvNOMbCOMT00tIeCosrQ");
            return stagingMap;
        }
    }

}
