package com.mongokerala.taxi.newuser.ui.main_new

import com.google.gson.annotations.SerializedName

data class AdvanceSearchResponse(

	@field:SerializedName("infoId")
	val infoId: Int? = null,

	@field:SerializedName("data")
	var data: List<AdvanceSearhData?>? = null,

	@field:SerializedName("jwt")
	val jwt: Any? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)

data class AdvanceSearhData(

	@field:SerializedName("image")
	val image: Any? = null,

	@field:SerializedName("latitude")
	val latitude: Double? = null,

	@field:SerializedName("imageBype")
	val imageBype: Any? = null,

	@field:SerializedName("phonenumber")
	val phonenumber: Any? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("phoneNumber")
	val phoneNumber: String? = null,

	@field:SerializedName("carType")
	val carType: String? = null,

	@field:SerializedName("driverId")
	val driverId: String? = null,

	@field:SerializedName("price")
    var price: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("category")
	val category: String? = null,

	@field:SerializedName("region")
	val region: Any? = null,

	@field:SerializedName("longitude")
	val longitude: Double? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("notificationToken")
	val notificationToken: String? = null,
	var isSelcted: Boolean =false,

	@field:SerializedName("is_Online")
	var is_Online:Boolean? =null
) {
	fun getState() {

	}
}
