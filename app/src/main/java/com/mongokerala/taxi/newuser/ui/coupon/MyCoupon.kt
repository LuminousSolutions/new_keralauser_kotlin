package com.mongokerala.taxi.newuser.ui.coupon

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.*
import android.widget.TextView
import com.mongokerala.taxi.newuser.R

class MyCoupon(context: Context,private var coupon_apply: CouponApply):Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_walletcoupon)
        val close_cancel =findViewById<TextView>(R.id.close_cancel) as TextView
        val txt_title =findViewById<TextView>(R.id.txt_title) as TextView
        val etxt_coupon =findViewById<EditText>(R.id.etxt_coupon) as EditText
        val btn_yes =findViewById<Button>(R.id.btn_yes) as Button
        val btn_laterr =findViewById<Button>(R.id.btn_laterr) as Button

        btn_yes.setOnClickListener {
            val coupon=etxt_coupon.text.toString()
            if (coupon.isEmpty()){
                Toast.makeText(this.context, "Please Enter Valid Coupon", Toast.LENGTH_SHORT).show()
            }else{
                coupon_apply.sendCoupon(coupon)
                dismiss()
            }
        }
        btn_laterr.setOnClickListener {
            dismiss()
        }
        close_cancel.setOnClickListener { dismiss() }
    }



}