package com.mongokerala.taxi.newuser.ui.setting

import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class SettingViewModel (
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    override fun onCreate() {

    }

    fun getEmail():String {
        return userRepository.getEmail().toString()
    }
    fun getMobileNumber():String{
       return userRepository.getMobileNUmber().toString()
    }
}