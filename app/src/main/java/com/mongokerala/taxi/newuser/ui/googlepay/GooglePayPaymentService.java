package com.mongokerala.taxi.newuser.ui.googlepay;


/**
 * Created by Akhi007 on 05-08-2019.
 */
public interface GooglePayPaymentService {

    void createGooglePayOrderPayment(CreateGoogleOrderPaymentListener orderPaymentListener, GoogleOrderDTO orderDTO);

   // void createGooglePayOrderPaymentDirect(CreateGoogleOrderPaymentListener orderPaymentListener, GoogleOrderDTO orderDTO);
}
