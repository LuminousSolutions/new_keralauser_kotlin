package com.mongokerala.taxi.newuser.paymentgateway.adyan;


/**
 * Created by Akhi007 on 25-05-2019.
 */
public interface AdyenPaymentService {

    void createPaymentSession(CreateAdyenPaymentSessionListener paymentSessionListener, OrderDTO orderDTO);

    void verifyPayment(VerifyAdyenPaymentListener verifyAdyenPaymentListener, OrderDTO orderDTO);
}
