package com.mongokerala.taxi.newuser.ui.base

import androidx.annotation.StringRes

public interface MvpView {

    fun showLoading()

    fun showSearching()

    fun hideLoading()

    fun openActivityOnTokenExpire()

    fun onError(@StringRes resId: Int)

    fun onError(message: String?)

    fun showMessage(message: String?)

    fun showMessage(@StringRes resId: Int)

    fun isNetworkConnected(): Boolean

    fun hideKeyboard()
}