package com.mongokerala.taxi.newuser.ui.tripdetail

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class TripDetailViewModel (
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    val invoice_success: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()


    override fun onCreate() {
    }


    fun doInvoice(rideId: String){


        compositeDisposable.addAll(
                userRepository.doInvoiceRequest(rideId)
                        .subscribeOn(schedulerProvider.io())
                        .subscribe(
                                {
                                    // invoice_success.postValue(Event(emptyMap()))

                                    invoice_success.postValue(Event(emptyMap()))

                                },
                                {
                                    handleNetworkError(it)

                                }
                        )
        )

    }
}