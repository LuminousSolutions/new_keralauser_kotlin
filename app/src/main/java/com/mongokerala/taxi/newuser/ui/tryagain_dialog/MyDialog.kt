package com.mongokerala.taxi.newuser.ui.tryagain_dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatSpinner
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.ui.custom.RoundedImageView
import kotlinx.android.synthetic.main.dialog_try_again.*

class MyDialog(context: Context, private var myDialogTryagain: MyDialogTryagain): Dialog(context) {
    private val TAG = "MyDialog"
    private var isTryAgain: Boolean? = null
    private var cancelReason: String? = null
    private var reason_msg: String? = null
    private var alert_icon: String? = null
    private var alert_title: String? = null

    constructor(context: Context, text: String, title: String, icon: String,myDialogTryagain: MyDialogTryagain):this(context,myDialogTryagain){
        reason_msg=text
        alert_title=title
        alert_icon=icon

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_try_again)
        val close =findViewById<TextView>(R.id.close) as TextView
        val call =findViewById<TextView>(R.id.more_information_to_call) as TextView
        val txt_alert_icon =findViewById<TextView>(R.id.txt_alert_icon) as TextView
        val txt_No_worriess =findViewById<TextView>(R.id.txt_No_worriess) as TextView
        val txt_errormsg =findViewById<TextView>(R.id.txt_errormsg) as TextView
        val lay_cancel_reason =findViewById<RelativeLayout>(R.id.lay_cancel_reason) as RelativeLayout
        val lay_two_btn =findViewById<RelativeLayout>(R.id.lay_two_btn) as RelativeLayout
        val lay_1 =findViewById<LinearLayout>(R.id.lay_1) as LinearLayout
        val spinner_cancel_reasons =findViewById<AppCompatSpinner>(R.id.spinner_cancel_reasons) as AppCompatSpinner
        val lay_2 =findViewById<RelativeLayout>(R.id.lay_2) as RelativeLayout
        val edt_cancel_reason =findViewById<EditText>(R.id.edt_cancel_reason) as EditText
        val editText =findViewById<EditText>(R.id.editText) as EditText
        val btn_offline =findViewById<Button>(R.id.btn_offline) as Button
        val btn_tryagain =findViewById<Button>(R.id.btn_tryagain) as Button
        val btn_ok =findViewById<Button>(R.id.btn_ok) as Button
        val img_profile1 =findViewById<RoundedImageView>(R.id.img_profile1) as RoundedImageView

        btn_ok.setOnClickListener {

            if (alert_title.toString().equals("Taxi Not Avilable In This Location")){
                dismiss()
                myDialogTryagain.noTaxiavilable()
            }else if (alert_title.toString().equals("Driver Started To Pickup")){
                dismiss()
            }else if (alert_title.toString().equals("Ride Started")){
                dismiss()
            }else if (alert_title.toString().equals("Pickup Done")){
            dismiss()
            }else if (alert_title.toString().equals("Driver canceled your ride")){
                myDialogTryagain.driverCanceledRide(alert_title.toString(),reason_msg.toString())
                dismiss()
            }else if (alert_title.toString().equals("Change Destination")){
                myDialogTryagain.changeDestination(alert_title.toString()," ")
                dismiss()
            }else if (alert_title.toString().equals("Emergence Alert")){
                dismiss()
            }else if (alert_title.toString().equals("Location Share")){
                dismiss()
            }
            else if (alert_title.toString().equals("Cancel Ride")){
                Log.d(TAG, "onCreate: cancelReason    :"+cancelReason)
                if (cancelReason.toString().equals("Other")){
                    cancelReason = edt_cancel_reason.text.toString()
                }
                else if (cancelReason.toString().equals("")){
                    cancelReason=" "
                }
                myDialogTryagain.driverCanceledRide(alert_title.toString(),cancelReason.toString())
                dismiss()
            }
        }
        btn_tryagain.setOnClickListener {
            myDialogTryagain.tryAgain_button()
            dismiss()
        }
        btn_offline.setOnClickListener {
            dismiss()
            myDialogTryagain.offlineButton()

        }
        more_information_to_call.setOnClickListener {
            myDialogTryagain.callCustomerCare()
        }
        close.setOnClickListener {
            if (!alert_title.toString().equals("Cancel Ride")){
                if (alert_title.toString() != "Please Try Again"){
                    myDialogTryagain.closeIconBtn()
                    dismiss()
                }else{
                    myDialogTryagain.closeIconTryAgain()
                    dismiss()
                }
            }
            else{
                dismiss()
            }

        }

        if (alert_title.toString().equals("Taxi Not Avilable In This Location")){
            txt_No_worriess.text= reason_msg
            txt_alert_icon.text=alert_icon
            txt_errormsg.text=alert_title
        }

        if (alert_title.toString().equals("Location Share")){
            txt_No_worriess.text= reason_msg
            txt_alert_icon.text=alert_icon
            txt_errormsg.text=alert_title
            close.visibility = View.GONE
        }

        if (alert_title.toString().equals("Emergence Alert")){
            txt_No_worriess.text= reason_msg
            txt_alert_icon.text=alert_icon
            txt_errormsg.text=alert_title
            close.visibility = View.GONE
        }

        if (alert_title.toString().equals("Driver Started To Pickup")){
            txt_No_worriess.text= reason_msg
            txt_alert_icon.text=alert_icon
            txt_errormsg.text=alert_title
            close.visibility = View.GONE
        }
        if (alert_title.toString().equals("Change Destination")){
            txt_No_worriess.text= reason_msg
            txt_alert_icon.text=alert_icon
            txt_errormsg.text=alert_title
            close.visibility = View.GONE
        }

        if (alert_title.toString().equals("Driver canceled your ride")){
            val reason= "$reason_msg \n  \uf119 Sorry for the inconvenience.   "
            txt_No_worriess.text= reason
            txt_alert_icon.text=alert_icon
            txt_errormsg.text=alert_title
            close.visibility = View.GONE
            more_information_to_call.visibility=View.VISIBLE
        }

        if (alert_title.toString().equals("Ride Started")){
            txt_No_worriess.text= reason_msg
            txt_alert_icon.text=alert_icon
            txt_errormsg.text=alert_title
            close.visibility = View.GONE
        }
        if (alert_title.toString().equals("Pickup Done")){
            txt_No_worriess.text= reason_msg
            txt_alert_icon.text=alert_icon
            txt_errormsg.text=alert_title
            close.visibility = View.GONE
        }

        if (alert_title.toString().equals("Please Try Again")){
            txt_No_worriess.text= reason_msg
            txt_alert_icon.text=alert_icon
            txt_errormsg.text=alert_title
            lay_two_btn.visibility= View.VISIBLE
            btn_ok.visibility=View.GONE
        }

        if (alert_title.toString().equals("Cancel Ride")){
            txt_No_worriess.text= reason_msg
            txt_alert_icon.text=alert_icon
            txt_errormsg.text=alert_title
            lay_1.visibility= View.VISIBLE
            var reason_msg=""
            var adapter =
                    ArrayAdapter.createFromResource(
                            this.context,
                            R.array.Cancel_reasons_list,
                            R.layout.spinner_car_type_text_background
                    )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_cancel_reasons.adapter = adapter
            spinner_cancel_reasons.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View,
                        position: Int,
                        id: Long
                ) {
                    val value = spinner_cancel_reasons.selectedItem.toString()
                    Log.d(TAG, "onItemSelected: value is    :"+value)
                    when (value) {
                        "Other" -> {
                            edt_cancel_reason.visibility = View.VISIBLE
                            reason_msg = "Other"
                        }
                        "Plan Changed" -> reason_msg = "Plan Changed"
                        "Booked another cab" -> reason_msg = "Booked another cab"
                        "Driver delayed" -> reason_msg = "Driver delayed"
                        "Price to high" -> reason_msg = "Price to high"
                    }
                    cancelReason = reason_msg
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
            Log.d(TAG, "onCreate: testing    :cancel reason    :"+reason_msg)

        }
    }

}