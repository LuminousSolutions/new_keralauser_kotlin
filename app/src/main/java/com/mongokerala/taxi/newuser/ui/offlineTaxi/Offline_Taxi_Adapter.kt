package com.mongokerala.taxi.newuser.ui.offlineTaxi

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.chootdev.typefaced.TypeFacedTextView
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.ui.custom.RoundedImageView
import com.mongokerala.taxi.newuser.ui.main_new.AdvanceSearhData
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity
import com.mongokerala.taxi.newuser.ui.triphistory.RecyclerViewItemClickListener
import com.mongokerala.taxi.newuser.utils.common.AppUtils
import com.mongokerala.taxi.newuser.utils.common.Constants.is_city
import java.util.*
import kotlin.collections.ArrayList

class Offline_Taxi_Adapter(
        var context: Context,
        my_offLine_List: ArrayList<AdvanceSearhData>,
        mainNewActivity: MainNewActivity
)  :RecyclerView.Adapter<Offline_Taxi_Adapter.OfflineViewHolder>() {
    private val recyclerViewItemClickListener: RecyclerViewItemClickListener
    private var offLine_List : List<AdvanceSearhData> = emptyList()

//    constructor(context: Context,offlineTaxi:List<AdvanceSearhData>,mainNewActivity: MainNewActivity){
//        offLine_List=offlineTaxi
//    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfflineViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.driver_list_item, parent, false)
        return OfflineViewHolder(view)
    }

    override fun onBindViewHolder(offlineViewHolder: OfflineViewHolder, position: Int) {
        val colors = context.resources.getIntArray(R.array.androidcolors)
        val color = colors[Random().nextInt(colors.size)]

//        offLine_List.apply { my_offLine_List }


//        Picasso.with(mContext).load(deviceList.get(position).getIconUrl()).placeholder(R.mipmap.kneepain_img).into(holder.document);
//        Picasso.get()
//                .load(BuildConfig.BASE_URL+offLine_List.get(position).getImageInfo().getImageUrl())
////                .load("https://www.gstatic.com/webp/gallery3/1.png")
//                .placeholder(R.drawable.userr)
//                .into(tripHistoryViewHolder.customer_pic);
      /*  if (offLine_List[position].imageInfo != null && !offLine_List[position].imageInfo?.imageUrl?.isEmpty()!!) {
            val image: String = offLine_List[position].imageInfo?.imageUrl.toString()
            offlineViewHolder.customer_pic.setImageBitmap(AppUtils.convertStringToImage(image))
        }*/
        // driverWatchDogViewHolder.profile_pic.setImageURI(Uri.parse(driverWatchDogs.get(position).getDriverProfilePic()));
        // driverWatchDogViewHolder.profile_pic.setImageURI(Uri.parse(driverWatchDogs.get(position).getDriverProfilePic()));
        offlineViewHolder.total_price.setText(java.lang.String.valueOf(offLine_List[position].price).toString() + "/km")
//        offlineViewHolder.cmt_txt.setText("\ue82a  " + offLine_List[position].)
        offlineViewHolder.cmt_txt.setTextColor(ContextCompat.getColor(context, R.color.black))
        offlineViewHolder.txt_driver_name.setText(offLine_List[position].name)
        offlineViewHolder.txt_cell.setText(offLine_List[position].phoneNumber)
        offlineViewHolder.txt_car_type.setText(offLine_List[position].carType)
        Log.d("TAG", "onBindViewHolder:  checking latitude  :"+(offLine_List[position].latitude)+"    langtitude   :"+(offLine_List[position].latitude))
        offlineViewHolder.txt_location.setText(AppUtils.latLongToAddress(java.lang.Double.valueOf(offLine_List[position].latitude.toString()), java.lang.Double.valueOf(offLine_List[position].longitude.toString()), context, is_city))
//        tripHistoryViewHolder.ratingBar.setRating(Float.parseFloat(offLine_List.get(position).getStar()));
        /////////////////////////////////
        //        tripHistoryViewHolder.ratingBar.setRating(Float.parseFloat(offLine_List.get(position).getStar()));
        /////////////////////////////////
        offlineViewHolder.txt_ic_person.setText("\ue808")
        //tripHistoryViewHolder.txt_ic_person.setTextColor();
        //tripHistoryViewHolder.txt_ic_person.setTextColor();
        offlineViewHolder.txt_ic_cell.setText("\uf098")
        //tripHistoryViewHolder.txt_ic_cell.setTextColor();
        //tripHistoryViewHolder.txt_ic_cell.setTextColor();
        offlineViewHolder.txt_ic_car_type.setText("\uf1b9")
        offlineViewHolder.txt_ic_car_type.setTextSize(15f)
        // tripHistoryViewHolder.txt_ic_car_type.setTextColor();
        // tripHistoryViewHolder.txt_ic_car_type.setTextColor();
        offlineViewHolder.txt_ic_location.setText("\ue82b")
        offlineViewHolder.txt_ic_location.setTextSize(24f)
        // tripHistoryViewHolder.txt_ic_location.setTextColor();
        // tripHistoryViewHolder.txt_ic_location.setTextColor();
        offlineViewHolder.txt_ic_img_call.setText("\ue82e")
        offlineViewHolder.txt_ic_img_call.setTextColor(ContextCompat.getColor(context, R.color.google_green))
        offlineViewHolder.txt_ic_img_call.setTextSize(30f)
        offlineViewHolder.txt_ic_pri_km.setText("\uf155")
        offlineViewHolder.txt_ic_pri_km.setTextSize(18f)
        offlineViewHolder.relative_layout_bg.setBackgroundColor(color)

    }

    override fun getItemCount(): Int {
        Log.d("TAG", "getItemCount:     :"+offLine_List.size)
        return offLine_List.size
    }

    inner class OfflineViewHolder internal constructor(@NonNull itemView: View) :
            RecyclerView.ViewHolder(itemView), View.OnClickListener{
        var customer_pic: RoundedImageView
        var ratingBar: RatingBar
        var txt_driver_name: TextView
        var txt_location: TextView
        var txt_car_type: TextView
        var txt_cell: TextView
        var total_price: TextView
        var cmt_txt: TypeFacedTextView
        var txt_ic_person: TypeFacedTextView
        var txt_ic_cell: TypeFacedTextView
        var txt_ic_car_type: TypeFacedTextView
        var txt_ic_location: TypeFacedTextView
        var txt_ic_img_call: TypeFacedTextView
        var txt_ic_pri_km: TypeFacedTextView
        var relative_layout_bg: RelativeLayout

        override fun onClick(view: View?) {
            recyclerViewItemClickListener.onItemClickListener(this.getAdapterPosition(), view)
        }

        init {
            customer_pic = itemView.findViewById(R.id.customer_pic)
            ratingBar = itemView.findViewById(R.id.lv_starRating)
            total_price = itemView.findViewById(R.id.price_txt)
            cmt_txt = itemView.findViewById(R.id.cmt_txt)
            txt_driver_name = itemView.findViewById(R.id.txt_driver_name)
            txt_cell = itemView.findViewById(R.id.txt_cell)
            txt_location = itemView.findViewById(R.id.txt_location)
            txt_car_type = itemView.findViewById(R.id.txt_car_type)
            relative_layout_bg = itemView.findViewById<RelativeLayout>(R.id.relative_layout_bg)
            txt_ic_person = itemView.findViewById(R.id.txt_ic_person)
            txt_ic_cell = itemView.findViewById(R.id.txt_ic_cell)
            txt_ic_car_type = itemView.findViewById(R.id.txt_ic_car_type)
            txt_ic_location = itemView.findViewById(R.id.txt_ic_location)
            txt_ic_img_call = itemView.findViewById(R.id.txt_ic_img_call)
            txt_ic_pri_km = itemView.findViewById(R.id.txt_ic_pri_km)
            itemView.setOnClickListener(this)
        }
    }




init {
    recyclerViewItemClickListener= mainNewActivity
    offLine_List=my_offLine_List
}



}