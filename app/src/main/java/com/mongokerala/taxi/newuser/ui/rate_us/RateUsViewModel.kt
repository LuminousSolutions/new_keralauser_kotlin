package com.mongokerala.taxi.newuser.ui.rate_us

import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class RateUsViewModel (
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    val feedbacksuccess: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val showPlayStoreRatingView: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val feedback_validation: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val showRatingMessageView: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val rating_not_provided_error: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    private var isRatingSecondaryActionShown = false
    val rateIn: MutableLiveData<Boolean> = MutableLiveData()


    override fun onCreate() {
    }

    fun doRateUs(rating: Float,message: String){

        if (rating == 0f) {
            rating_not_provided_error.postValue(Event(emptyMap()))
            return
        }

        if (!isRatingSecondaryActionShown) {
            if (rating >= 3) {
                showPlayStoreRatingView.postValue(Event(emptyMap()))
            } else {
                showRatingMessageView.postValue(Event(emptyMap()))
            }
            isRatingSecondaryActionShown = true
            return
        }
        if (message == null || message.isEmpty()) {
            feedback_validation.postValue(Event(emptyMap()))
            return
        }else{
            rateIn.postValue(true)

        }
//        Log.d("doRateUs", "doRateUs: "+userRepository.getTaxiId()!!)
        compositeDisposable.addAll(
            userRepository.doRateusback(message, userRepository.getName()!!,
                userRepository.getMobileNUmber()!!, userRepository.getEmail()!!,
                userRepository.getTaxiId()!!)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if(it.infoId.equals("500")) {
                            feedbacksuccess.postValue(Event(emptyMap()))
                        }
                        rateIn.postValue(false)

                    },
                    {
                        handleNetworkError(it)
                        rateIn.postValue(false)

                    }
                )
        )
    }


}