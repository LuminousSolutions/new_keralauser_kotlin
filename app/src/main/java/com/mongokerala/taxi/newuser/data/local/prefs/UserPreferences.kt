package com.mongokerala.taxi.newuser.data.local.prefs

import android.content.Context
import android.content.SharedPreferences
import com.mongokerala.taxi.newuser.data.remote.request.ImageInfo
import com.mongokerala.taxi.newuser.data.remote.request.Keys
import com.mongokerala.taxi.newuser.di.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserPreferences @Inject constructor(private var prefs: SharedPreferences) {

    companion object {
        const val KEY_USER_ID = "PREF_KEY_USER_ID"
        const val KEY_CAR_TYPE = "KEY_CAR_TYPE"
        const val KEY_CAR_TAX_TYPE_4 = "KEY_CAR_TAX_TYPE_4"
        const val KEY_CAR_TAX_TYPE_6 = "KEY_CAR_TAX_TYPE_6"
        const val KEY_CAR_TAX_TYPE = "KEY_CAR_TAX_TYPE"
        const val KEY_CAR_TRANSPORT_TYPE = "KEY_CAR_TRANSPORT_TYPE"
        const val KEY_CAR_AMBULANCE_TYPE = "KEY_CAR_AMBULANCE_TYPE"
        const val KEY_CAR_BIKE_TYPE = "KEY_CAR_BIKE_TYPE"
        const val KEY_SOURCE_ADDRESS = "KEY_SOURCE_ADDRESS"
        const val KEY_DESTINATION_ADDRESS = "KEY_DESTINATION_ADDRESS"
        const val KEY_PAYMENT_TYPE = "KEY_PAYMENT_TYPE"
        const val PREF_KEY_RIDE_ID = "PREF_KEY_RIDE_ID"
        const val PREF_KEY_DESTINATION_PLACE_ID = "PREF_KEY_DESTINATION_PLACE_ID"
        const val PREF_KEY_SOURCE_PLACE_ID = "PREF_KEY_SOURCE_PLACE_ID"
        const val PREF_KEY_CLIENT_FCM_TOKEN = "PREF_KEY_CLIENT_FCM_TOKEN"
        const val PREF_KEY_START_TIME = "PREF_KEY_START_TIME"
        const val PREF_KEY_RIDE_START_DRIVER_LATITUDE = "RIDE_START_DRIVER_LATITUDE"
        const val PREF_KEY_RIDE_START_DRIVER_LONGITUDE = "RIDE_START_DRIVER_LONGITUDE"
        const val PREF_KEY_COUPON = "PREF_KEY_COUPON"
        const val PREF_KEY_END_TIME = "PREF_KEY_END_TIME"
        const val PREF_KEY_APX_GOOGLE_KM = "PREF_KEY_APX_GOOGLE_KM"
        const val PREF_KEY_APX_GOOGLE_KM_DRIVER = "PREF_KEY_APX_GOOGLE_KM_DRIVER"
        const val KEY_CLIENT_MOBILE_NUMBER = "KEY_CLIENT_MOBILE_NUMBER"
        const val KEY_DRIVER_MOBILE_NUMBER = "KEY_DRIVER_MOBILE_NUMBER"
        const val KEY_BASEKM = "KEY_BASEKM"
        const val KEY_VEHICLE_SEATS = "KEY_VEHICLE_SEATS"
        const val KEY_VEHICLE_NUMBER = "KEY_VEHICLE_NUMBER"
        const val KEY_VEHICLE_BRAND = "KEY_VEHICLE_BRAND"
        const val PREF_KEY_TOTAL_KM = "PREF_KEY_TOTAL_KM"
        const val PREF_KEY_LOCABTOTAL_KM = "PREF_KEY_LOCABTOTAL_KM"
        const val PREF_KEY_CATEGORY = "PREF_KEY_CATEGORY"
        const val KEY_VEHICLE_YEAR = "KEY_VEHICLE_YEAR"
        const val KEY_MOBILE_NUMBER = "KEY_MOBILE_NUMBER"
        const val KEY_DRIVER_NAME = "KEY_DRIVER_NUMBER"
        const val KEY_CLIENT_NAME = "KEY_CLIENT_NAME"
        const val KEY_DRIVER_EMAIL = "KEY_DRIVER_EMAIL"
        const val KEY_DRIVER_DOMAIN = "KEY_DRIVER_DOMAIN"
        const val KEY_DRIVER_WEBSITE = "KEY_DRIVER_WEBSITE"
        const val KEY_BACKEND_BOOKING_TOKEN = "KEY_BACKEND_BOOKING_TOKEN"
        const val KEY_TAXI_ID = "KEY_TAXI_ID"
        const val PREF_KEY_STATUS = "PREF_KEY_STATUS"
        const val PREF_KEY_LOCAL_STATUS = "PREF_KEY_LOCAL_STATUS"
        const val PREF_KEY_ANOTHER_DRIVER_ACCPTED = "PREF_KEY_ANOTHER_DRIVER_ACCPTED"
        const val PREF_KEY_DRIVER_STATUS = "PREF_KEY_DRIVER_STATUS"
        const val PREF_KEY_TOKEN = "PREF_KEY_TOKEN"
        const val PREF_KEY_DRIVER_CURRENT_LATITUDE = "DRIVER_CURRENT_LATITUDE"
        const val PREF_KEY_DRIVER_CURRENT_LONGITUDE = "DRIVER_CURRENT_LONGITUDE"
        const val PREF_KEY_USER_CURRENT_LATITUDE= "USER_CURRENT_LATITUDE"
        const val PREF_KEY_USER_CURRENT_LONGITUDE = "USER_CURRENT_LONGITUDE"
        const val KEY_TYPE = "KEY_TYPE"
        const val KEY_IMAGE_URL = "KEY_IMAGE_URL"
        const val KEY_APPROVED = "KEY_APPROVED"
        const val KEY_SUPPLIER_ID = "KEY_SUPPLIER_ID"
        const val PREF_KEY_CLIENT_ID = "PREF_KEY_CLIENT_ID"
        const val PREF_KEY_IS_ANDROID = "PREF_KEY_IS_ANDROID"
        const val PREF_KEY_IS_START_RIDE = "PREF_KEY_IS_START_RIDE"
        const val PREF_KEY_FIRST_LOGIN = "PREF_KEY_FIRST_LOGIN"
        const val PREF_KEY_RIDE_KEY_LOCAL = "RIDE_KEY_LOCAL"
        const val KEY_IS_ONLINE_BOOKING = "IS_ON_BOOKING"
        const val KEY_DRIVER_LANGUAGE = "KEY_DRIVER_LANGUAGE"
        const val KEY_USER_TOTAL_PRICE = "KEY_USER_TOTAL_PRICE"
        const val KEY_ON_SIGNAL_VALUE = "KEY_ON_SIGNAL_VALUE"
        const val KEY_USER_NAME = "PREF_KEY_USER_NAME"
        const val KEY_USER_EMAIL = "PREF_KEY_USER_EMAIL"
        const val KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
        const val PREF_FILE_NAME = "updated-driver-project-prefs"
        const val PREF_KEY_IS_OFFLINE_BOOKING = "PREF_KEY_IS_OFFLINE_BOOKING"
        const val PREF_KEY_STOP_BUTTON_CLICKED = "PREF_KEY_STOP_BUTTON_CLICKED"
        const val VALUES = "string"
        const val PREF_KEY_USER_FCM_TOKEN = "PREF_KEY_USER_FCM_TOKEN"
        const val PREF_KEY_LOGIN_STATUS = "PREF_KEY_LOGIN_STATUS"
        const val PREF_KEY_FB_LOGIN_STATUS = "PREF_KEY_LOGIN_STATUS"
        const val PREF_KEY_GMAIL_LOGIN_STATUS = "PREF_KEY_GMAIL_LOGIN_STATUS"
        const val PREF_KEY_GMAIL_IMAGE_URL = "PREF_KEY_GMAIL_IMAGE_URL"
        const val PREF_KEY_FB_IMAGE_URL = "PREF_KEY_FB_IMAGE_URL"
        const val PREF_KEY_FB_PROFILE_IMAGE_STATUS = "PREF_KEY_FB_PROFILE_IMAGE_STATUS"
        const val PREF_KEY_USER_SEARCH_LATITUDE = "PREF_KEY_USER_SEARCH_LATITUDE"
        const val PREF_KEY_USER_SEARCH_LANGITUDE = "PREF_KEY_USER_SEARCH_LANGITUDE"
        const val PREF_KEY_USER_SEARCH_DESTINATION_LATITUDE = "PREF_KEY_USER_SEARCH_DESTINATION_LATITUDE"
        const val PREF_KEY_USER_SEARCH_DESTINATION_LANGITUDE = "PREF_KEY_USER_SEARCH_DESTINATION_LANGITUDE"
        const val PREF_KEY_USER_ONPAUSE_STATE = "PREF_KEY_USER_ONPAUSE_STATE"
        const val PREF_KEY_USER_ONPAUSE_BOOKING_STATE = "PREF_KEY_USER_ONPAUSE_BOOKING_STATE"
        const val PREF_KEY_USER_IS_DRIVER_ACCEPT = "PREF_KEY_USER_IS_DRIVER_ACCEPT"
        const val PREF_KEY_CUSTOMERCARE_NUMBER = "PREF_KEY_CUSTOMERCARE_NUMBER"

    }

    @Inject
    fun AppPreferencesHelper(@ApplicationContext context: Context) {
        prefs = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
    }

    fun getUserId(): String? =
        prefs.getString(KEY_USER_ID, null)

    fun getValues(): String? =
            prefs.getString(VALUES, "string")

    fun setValues(values: String) =
            prefs.edit().putString(VALUES, values).apply()


    fun setUserId(userId: String) =
        prefs.edit().putString(KEY_USER_ID, userId).apply()

    fun setBaseKm(userId: String) =
        prefs.edit().putString(KEY_BASEKM, userId).apply()

    fun getClientMobileNumber(): String? =
        prefs.getString(KEY_CLIENT_MOBILE_NUMBER, null)

    fun setClientMobileNumber(client_mobile_number: String) =
        prefs.edit().putString(KEY_CLIENT_MOBILE_NUMBER, client_mobile_number).apply()

    fun getDriverMobileNumber(): String? =
            prefs.getString(KEY_DRIVER_MOBILE_NUMBER, null)

    fun getbaseKm(): String? =
        prefs.getString(KEY_BASEKM, "")

    fun setDriverMobileNumber(client_mobile_number: String) =
            prefs.edit().putString(KEY_DRIVER_MOBILE_NUMBER, client_mobile_number).apply()


    fun getCarType(): String? =
        prefs.getString(KEY_CAR_TYPE, null)

    fun setCarType(car_type: String) =
        prefs.edit().putString(KEY_CAR_TYPE, car_type).apply()

    fun getSource(): String? =
        prefs.getString(KEY_SOURCE_ADDRESS, null)

    fun setSource(source_address: String) =
        prefs.edit().putString(KEY_SOURCE_ADDRESS, source_address).apply()

    fun getDestination(): String? =
        prefs.getString(KEY_DESTINATION_ADDRESS, null)

    fun setDestination(destination_address: String) =
        prefs.edit().putString(KEY_DESTINATION_ADDRESS, destination_address).apply()

    fun getPaymentType(): String? =
        prefs.getString(KEY_PAYMENT_TYPE, "CASH")

    fun setPaymentType(paytype: String) =
        prefs.edit().putString(KEY_PAYMENT_TYPE, paytype).apply()

    fun setEND_TIME(END_TIME: String?) {
        prefs.edit().putString(PREF_KEY_END_TIME, END_TIME).apply()
    }

    fun getEND_TIME(): String? {
        return prefs.getString(PREF_KEY_END_TIME, null)
    }

    fun setApxGoogleKm(apxGoogleKm: String?) {
        prefs.edit().putString(PREF_KEY_APX_GOOGLE_KM, apxGoogleKm).apply()
    }

    fun getApxGoogleKm(): String? {
        return prefs.getString(PREF_KEY_APX_GOOGLE_KM, "0")
    }

    fun setApxGoogleKmDriver(apxGoogleKmdriver: String?) {
        prefs.edit().putString(PREF_KEY_APX_GOOGLE_KM_DRIVER, apxGoogleKmdriver).apply()
    }

    fun getApxGoogleKmDriver(): String? {
        return prefs.getString(PREF_KEY_APX_GOOGLE_KM_DRIVER, "0")
    }

    fun setRide_ID(ride_id: String?) {
        prefs.edit().putString(PREF_KEY_RIDE_ID, ride_id).apply()
    }

    fun getRide_Id(): String? {
        return prefs.getString(PREF_KEY_RIDE_ID, null)
    }

    fun set_Destination_Place_id(destination_place_id: String?) {
        prefs.edit().putString(PREF_KEY_DESTINATION_PLACE_ID, destination_place_id).apply()
    }

    fun get_Destination_Place_id(): String? {
        return prefs.getString(PREF_KEY_DESTINATION_PLACE_ID, "0")
    }

    fun set_Source_Place_id(source_place_id: String?) {
        prefs.edit().putString(PREF_KEY_SOURCE_PLACE_ID, source_place_id).apply()
    }

    fun get_Source_Place_id(): String? {
        return prefs.getString(PREF_KEY_SOURCE_PLACE_ID, "")
    }

    fun getClient_FCM_Token(): String? {
        return prefs.getString(PREF_KEY_CLIENT_FCM_TOKEN, null)
    }

    fun setClient_FCM_Token(client_fcm_token: String?) {
        prefs.edit().putString(PREF_KEY_CLIENT_FCM_TOKEN, client_fcm_token).apply()
    }

    fun set_START_TIME(START_TIME: String?) {
        prefs.edit().putString(PREF_KEY_START_TIME, START_TIME).apply()
    }

    fun getSTART_TIME(): String? {
        return prefs.getString(PREF_KEY_START_TIME, null)
    }

    fun setDriverRideStartLatLng(latitude: Double, longitude: Double) {
        prefs.edit().putFloat(PREF_KEY_RIDE_START_DRIVER_LATITUDE, latitude.toFloat()).apply()
        prefs.edit().putFloat(PREF_KEY_RIDE_START_DRIVER_LONGITUDE, longitude.toFloat()).apply()
    }

    fun getDriverRideStartlatitude(): Double? {
        return prefs.getFloat(PREF_KEY_RIDE_START_DRIVER_LATITUDE, 0.0f).toDouble()
    }

    fun getDriverRideStartLongitude(): Double? {
        return prefs.getFloat(PREF_KEY_RIDE_START_DRIVER_LONGITUDE, 0.0f).toDouble()
    }

    fun setCoupon(Coupon: String?) {
        prefs.edit().putString(PREF_KEY_COUPON, Coupon).apply()
    }

    fun getCoupon(): String? {
        return prefs.getString(PREF_KEY_COUPON, null)
    }

    fun getVehicleSeats(): String? =
        prefs.getString(KEY_VEHICLE_SEATS, null)

    fun setVehicleSeats(vehicle_seats: String) =
        prefs.edit().putString(KEY_VEHICLE_SEATS, vehicle_seats).apply()

    fun getTaxiNumber(): String? =
        prefs.getString(KEY_VEHICLE_NUMBER, "TN0N12345")

    fun setTaxiNumber(taxi_number: String) =
        prefs.edit().putString(KEY_VEHICLE_NUMBER, taxi_number).apply()

    fun getVehicleBrand(): String? =
        prefs.getString(KEY_VEHICLE_BRAND, "TATA")

    fun setVehicleBrand(vehicle_brand: String) =
        prefs.edit().putString(KEY_VEHICLE_BRAND, vehicle_brand).apply()

    fun getCategory(): String? {
        return prefs.getString(PREF_KEY_CATEGORY, null)
    }

    fun setCategory(category: String?) {
        prefs.edit().putString(PREF_KEY_CATEGORY, category).apply()
    }

    fun set_Sou_PlaceID(des_place_id: String?) {
        prefs.edit().putString(PREF_KEY_SOURCE_PLACE_ID, des_place_id).apply()
    }
    fun get_Sou_PlaceID(): String? {
        return prefs.getString(PREF_KEY_SOURCE_PLACE_ID, null)
    }
    fun set_Total_Km(total_km: String?) {
        prefs.edit().putString(PREF_KEY_TOTAL_KM, total_km).apply()
    }

    fun get_Total_Km(): String? {
        return prefs.getString(PREF_KEY_TOTAL_KM, "0")
    }

    fun set_LocABTotal_Km(locabtotal_km: String?) {
        prefs.edit().putString(PREF_KEY_LOCABTOTAL_KM, locabtotal_km).apply()
    }

    fun get_LocABTotal_Km(): String? {
        return prefs.getString(PREF_KEY_LOCABTOTAL_KM, "0")
    }


    fun getVehicleYear(): String? =
        prefs.getString(KEY_VEHICLE_YEAR, null)

    fun setVehicleYear(vehicle_year: String) =
        prefs.edit().putString(KEY_VEHICLE_YEAR, vehicle_year).apply()

    fun getMobileNumber(): String? =
        prefs.getString(KEY_MOBILE_NUMBER, null)

    fun setMobileNumber(mobile_number: String) =
        prefs.edit().putString(KEY_MOBILE_NUMBER, mobile_number).apply()

    fun getName(): String? =
        prefs.getString(KEY_DRIVER_NAME, null)

    fun setName(name: String) =
        prefs.edit().putString(KEY_DRIVER_NAME, name).apply()

    fun getClientName(): String? =
        prefs.getString(KEY_CLIENT_NAME, null)

    fun setClientName(client_name: String) =
        prefs.edit().putString(KEY_CLIENT_NAME, client_name).apply()

    fun getEmail(): String? =
        prefs.getString(KEY_DRIVER_EMAIL, null)

    fun setEmail(name: String) =
        prefs.edit().putString(KEY_DRIVER_EMAIL, name).apply()

    fun getDomain(): String? =
        prefs.getString(KEY_DRIVER_DOMAIN, null)

    fun setDomain(domain: String) =
        prefs.edit().putString(KEY_DRIVER_DOMAIN, domain).apply()

    fun getWebsitae(): String? =
        prefs.getString(KEY_DRIVER_WEBSITE, null)

    fun setWebsite(website: String) =
        prefs.edit().putString(KEY_DRIVER_WEBSITE, website).apply()

    fun getBackendBookingToken(): String? =
        prefs.getString(KEY_BACKEND_BOOKING_TOKEN, null)

    fun setBackendBookingToken(token: String) =
        prefs.edit().putString(KEY_BACKEND_BOOKING_TOKEN, token).apply()

    fun getTaxiId(): String? =
        prefs.getString(KEY_TAXI_ID, null)
    fun setTaxiId(taxi_id: String) =
        prefs.edit().putString(KEY_TAXI_ID, taxi_id).apply()

    fun setCurrentStatus(current_status: String) =
        prefs.edit().putString(PREF_KEY_STATUS, current_status).apply()

    fun getCurrentStatus(): String? =
        prefs.getString(PREF_KEY_STATUS, "")

    fun setLocalStatus(current_status: String) =
            prefs.edit().putString(PREF_KEY_LOCAL_STATUS, current_status).apply()

    fun getLocalStatus(): String? =
            prefs.getString(PREF_KEY_LOCAL_STATUS, "")

    fun saveAnotherDriverAccepted(current_status: String) =
        prefs.edit().putString(PREF_KEY_ANOTHER_DRIVER_ACCPTED, current_status).apply()

    fun getAnotherDriverAccepted(): String? =
        prefs.getString(PREF_KEY_ANOTHER_DRIVER_ACCPTED, "false")

    fun setDriverCurrentStatus(dr_current_status: String) =
        prefs.edit().putString(PREF_KEY_DRIVER_STATUS, dr_current_status).apply()

    fun getDriverCurrentStatus(): String? =
        prefs.getString(PREF_KEY_DRIVER_STATUS, "")

    fun setToken(token: String) =
        prefs.edit().putString(PREF_KEY_TOKEN, token).apply()

    fun getToken(): String? =
        prefs.getString(PREF_KEY_TOKEN, "")


    fun setDriverCurrentLatLng(latitude: Double, longitude: Double) {
        prefs.edit().putFloat(PREF_KEY_DRIVER_CURRENT_LATITUDE, latitude.toFloat()).apply()
        prefs.edit().putFloat(PREF_KEY_DRIVER_CURRENT_LONGITUDE, longitude.toFloat()).apply()
    }
    fun setUserCurrentLatLng(latitude: Double, longitude: Double) {
        prefs.edit().putFloat(PREF_KEY_USER_CURRENT_LATITUDE, latitude.toFloat()).apply()
        prefs.edit().putFloat(PREF_KEY_USER_CURRENT_LONGITUDE, longitude.toFloat()).apply()
    }

    fun getDriverCurrentlatitude(): Double {
        return prefs.getFloat(PREF_KEY_DRIVER_CURRENT_LATITUDE, 0.0f).toDouble()
    }

    fun getDriverCurrentLongitude(): Double {
        return prefs.getFloat(PREF_KEY_DRIVER_CURRENT_LONGITUDE, 0.0f).toDouble()
    }
    fun getUserCurrentlatitude(): Double {
        return prefs.getFloat(PREF_KEY_USER_CURRENT_LATITUDE, 0.0f).toDouble()
    }

    fun getUserCurrentLongitude(): Double {
        return prefs.getFloat(PREF_KEY_USER_CURRENT_LONGITUDE, 0.0f).toDouble()
    }

    fun getType(): String? =
        prefs.getString(KEY_TYPE, null)

    fun setType(type: String) =
        prefs.edit().putString(KEY_TYPE, type).apply()

    fun getIsApproved(): String? =
        prefs.getString(KEY_APPROVED, null)

    fun setIsApproved(isApproved: String) =
        prefs.edit().putString(KEY_APPROVED, isApproved).apply()

    fun getSupplierId(): String? =
        prefs.getString(KEY_SUPPLIER_ID, null)

    fun setSupplierId(supplierId: String) =
        prefs.edit().putString(KEY_SUPPLIER_ID, supplierId).apply()

    fun setClient_ID(client_id: String?) {
        prefs.edit().putString(PREF_KEY_CLIENT_ID, client_id).apply()
    }

    fun getClient_Id(): String? {
        return prefs.getString(PREF_KEY_CLIENT_ID, null)
    }

    fun setIs_Android(is_android: Boolean) {
        prefs.edit().putBoolean(PREF_KEY_IS_ANDROID, is_android).apply()
    }

    fun getIsAndroid(): Boolean {
//        return prefs.getBoolean(PREF_KEY_IS_ANDROID, true)
        return prefs.getBoolean(PREF_KEY_IS_ANDROID, false)
    }

    fun setStartRide(is_android: Boolean) {
        prefs.edit().putBoolean(PREF_KEY_IS_START_RIDE, is_android).apply()
    }

    fun getStartride(): Boolean {
        return prefs.getBoolean(PREF_KEY_IS_START_RIDE, false)
    }

    fun setFirstLogin(start_ride: Boolean) {
        prefs.edit().putBoolean(PREF_KEY_FIRST_LOGIN, start_ride).apply()
    }

    fun getFirstLogin(): Boolean {
        return prefs.getBoolean(PREF_KEY_FIRST_LOGIN, true)
    }


    fun setRIDE_KEY_LOCAL(RIDE_KEY_LOCAL: String?) {
        prefs.edit().putString(PREF_KEY_RIDE_KEY_LOCAL, RIDE_KEY_LOCAL).apply()
    }

    fun getRIDE_KEY_LOCAL(): String? {
        return prefs.getString(PREF_KEY_RIDE_KEY_LOCAL, null)
    }


    fun get_is_On_Booking(): Boolean =
        prefs.getBoolean(KEY_IS_ONLINE_BOOKING, true)

    fun set_is_On_Booking(is_onLineBooking: Boolean) =
        prefs.edit().putBoolean(KEY_IS_ONLINE_BOOKING, true).apply()

    fun getLoginStatus(): String? =
        prefs.getString(PREF_KEY_LOGIN_STATUS,"")

    fun setLoginStatus(login_status: String) =
            prefs.edit().putString(PREF_KEY_LOGIN_STATUS,login_status).apply()

    fun getFbLoginstatus(): String? =
            prefs.getString(PREF_KEY_FB_LOGIN_STATUS,"")

    fun saveFbLoginStatus(login_status: String) =
            prefs.edit().putString(PREF_KEY_FB_LOGIN_STATUS,login_status).apply()

    fun getFbprofileImage():String? =
            prefs.getString(PREF_KEY_FB_PROFILE_IMAGE_STATUS,"")

    fun saveFbprofileImage(profile_image: String) =
            prefs.edit().putString(PREF_KEY_FB_PROFILE_IMAGE_STATUS,profile_image).apply()


    fun getFbImageUrl(): String? =
            prefs.getString(PREF_KEY_FB_IMAGE_URL,"")

    fun saveFbImageUrl(imageUrl: String) =
            prefs.edit().putString(PREF_KEY_FB_IMAGE_URL,imageUrl).apply()


    fun setImageUrl(imageUrl: ImageInfo) {
        if (imageUrl.imageUrl != null) {
            prefs.edit().putString(Keys.blobKey, "").apply()
            prefs.edit().putString(Keys.fileName, "").apply()
            prefs.edit().putString(Keys.imageUrl, imageUrl.imageUrl).apply()
        } else {
            prefs.edit().putString(Keys.imageUrl, null).apply()
        }
    }

    fun getImageUrl(): ImageInfo? {
        val response = ImageInfo(prefs.getString(Keys.imageUrl, ""))
        //response.blobkey = mPrefs.getString(Keys.blobKey, "");
        //response.fileName = prefs.getString(Keys.fileName, "")
        //response.imageUrl = prefs.getString(Keys.imageUrl, "")
        return response
    }

    fun getLan(): String? =
        prefs.getString(KEY_DRIVER_LANGUAGE, null)

    fun setLan(lan: String) =
        prefs.edit().putString(KEY_DRIVER_LANGUAGE, lan).apply()

    fun getUserTotalPrice(): String? =
        prefs.getString(KEY_USER_TOTAL_PRICE, null)

    fun setUserTotalOrice(user_total_price: String) =
        prefs.edit().putString(KEY_USER_TOTAL_PRICE, user_total_price).apply()

    fun getOneSignalValue(): String? =
        prefs.getString(KEY_ON_SIGNAL_VALUE, null)

    fun setOneSignalValue(oneSignalValue: String) =
        prefs.edit().putString(KEY_ON_SIGNAL_VALUE, oneSignalValue).apply()

    fun removeUserId() =
        prefs.edit().remove(KEY_USER_ID).apply()

    fun getUserName(): String? =
        prefs.getString(KEY_USER_NAME, null)

    fun setUserName(userName: String) =
        prefs.edit().putString(KEY_USER_NAME, userName).apply()

    fun removeUserName() =
        prefs.edit().remove(KEY_USER_NAME).apply()

    fun getUserEmail(): String? =
        prefs.getString(KEY_USER_EMAIL, null)

    fun setUserEmail(email: String) =
        prefs.edit().putString(KEY_USER_EMAIL, email).apply()

    fun removeUserEmail() =
        prefs.edit().remove(KEY_USER_EMAIL).apply()

    fun getAccessToken(): String? =
        prefs.getString(KEY_ACCESS_TOKEN, null)

    fun setAccessToken(token: String) =
        prefs.edit().putString(KEY_ACCESS_TOKEN, token).apply()

    fun removeAccessToken() =
        prefs.edit().remove(KEY_ACCESS_TOKEN).apply()

    fun set_is_Offline_Booking(is_Offline_Booking: Boolean) {
        prefs.edit().putBoolean(PREF_KEY_IS_OFFLINE_BOOKING, is_Offline_Booking).apply()
    }

    fun get_is_Offline_Booking(): Boolean? {
        return prefs.getBoolean(PREF_KEY_IS_OFFLINE_BOOKING, false)
    }

    fun setStopButtonClicked(stop_buton: Boolean) {
        prefs.edit().putBoolean(PREF_KEY_STOP_BUTTON_CLICKED, stop_buton).apply()
    }

    fun getStopButtonClicked(): Boolean? {
        return prefs.getBoolean(PREF_KEY_STOP_BUTTON_CLICKED, false)
    }


    fun get_type_Taxi4(): String? =
        prefs.getString(KEY_CAR_TAX_TYPE_4, null)

    fun set_type_Taxi4(taxi4: String) =
        prefs.edit().putString(KEY_CAR_TAX_TYPE_4, taxi4).apply()

    fun get_type_Taxi(): String? =
        prefs.getString(KEY_CAR_TAX_TYPE, null)

    fun set_type_Taxi(taxi: String) =
        prefs.edit().putString(KEY_CAR_TAX_TYPE, taxi).apply()

    fun set_type_Taxi6(taxi6: String) =
        prefs.edit().putString(KEY_CAR_TAX_TYPE_6, taxi6).apply()

    fun get_type_Taxi6(): String? =
        prefs.getString(KEY_CAR_TAX_TYPE_6, null)

    fun set_type_Transport(transport: String) =
        prefs.edit().putString(KEY_CAR_TRANSPORT_TYPE, transport).apply()

    fun get_type_Transport(): String? =
        prefs.getString(KEY_CAR_TRANSPORT_TYPE, null)

    fun set_type_Ambulance(Ambulance: String) =
        prefs.edit().putString(KEY_CAR_AMBULANCE_TYPE, Ambulance).apply()

    fun get_type_Ambulance(): String? =
        prefs.getString(KEY_CAR_AMBULANCE_TYPE, null)



    fun get_type_Bike(): String? =
        prefs.getString(KEY_CAR_BIKE_TYPE, null)

    fun set_type_Bike(Bike: String) =
        prefs.edit().putString(KEY_CAR_BIKE_TYPE, Bike).apply()


    fun getUser_FCM_Token(): String? {
        return prefs.getString(PREF_KEY_USER_FCM_TOKEN, null)
    }

    fun setUser_FCM_Token(FCM_Token: String?) {
        prefs.edit().putString(PREF_KEY_USER_FCM_TOKEN, FCM_Token).apply()
    }
    fun getUserSearchLatitude(): Double {
        return prefs.getFloat(PREF_KEY_USER_SEARCH_LATITUDE, 0.0f).toDouble()
    }
    fun getUserSearchLangitude(): Double{
        return prefs.getFloat(PREF_KEY_USER_SEARCH_LANGITUDE, 0.0f).toDouble()
    }
    fun saveUserSearchLatLng(latitude: Double, longitude: Double) {
        prefs.edit().putFloat(PREF_KEY_USER_SEARCH_LATITUDE, latitude.toFloat()).apply()
        prefs.edit().putFloat(PREF_KEY_USER_SEARCH_LANGITUDE, longitude.toFloat()).apply()
    }
    fun saveSearchDestinationLatLang(latitude: Double, longitude: Double){
        prefs.edit().putFloat(PREF_KEY_USER_SEARCH_DESTINATION_LATITUDE, latitude.toFloat()).apply()
        prefs.edit().putFloat(PREF_KEY_USER_SEARCH_DESTINATION_LANGITUDE, longitude.toFloat()).apply()
    }
    fun getSearchDestinationLatitude(): Double {
        return prefs.getFloat(PREF_KEY_USER_SEARCH_DESTINATION_LATITUDE, 0.0f).toDouble()
    }
    fun getSearchDestinationLangitude(): Double {
        return prefs.getFloat(PREF_KEY_USER_SEARCH_DESTINATION_LANGITUDE, 0.0f).toDouble()
    }

    fun getOnPauseState(): Boolean {
        return prefs.getBoolean(PREF_KEY_USER_ONPAUSE_STATE, false)
    }

    fun saveOnPauseState(onpause: Boolean) {
        prefs.edit().putBoolean(PREF_KEY_USER_ONPAUSE_STATE, onpause).apply()
    }

    fun onPausegetBooking(): Boolean {
        return prefs.getBoolean(PREF_KEY_USER_ONPAUSE_BOOKING_STATE, false)
    }

    fun onPauseSaveBooking(onpauseBooking: Boolean) {
        prefs.edit().putBoolean(PREF_KEY_USER_ONPAUSE_BOOKING_STATE, onpauseBooking).apply()
    }

    fun getIsDriverAccept(): Boolean {
        return prefs.getBoolean(PREF_KEY_USER_IS_DRIVER_ACCEPT, false)
    }

    fun saveisDriverAccept(isaccept: Boolean) {
        prefs.edit().putBoolean(PREF_KEY_USER_IS_DRIVER_ACCEPT, isaccept).apply()
    }

    fun savegmailLoginStatus(loginStatus: String) {
        prefs.edit().putString(PREF_KEY_GMAIL_LOGIN_STATUS,loginStatus).apply()
    }

    fun getGmailLoginStatus(): String? {
       return prefs.getString(PREF_KEY_FB_LOGIN_STATUS,"")
    }

    fun getgMailLoginImageUrl(): String? {
        return prefs.getString(PREF_KEY_GMAIL_IMAGE_URL,"")
    }

    fun savegMailLoginImageUrl(imageurl: String) {
        prefs.edit().putString(PREF_KEY_GMAIL_IMAGE_URL,imageurl).apply()
    }

    fun getCustomerCareNumber(): String? {
        return prefs.getString(PREF_KEY_CUSTOMERCARE_NUMBER,"")
    }

    fun saveCustomerCareNumber(customercareNumber: String) {
        prefs.edit().putString(PREF_KEY_CUSTOMERCARE_NUMBER,customercareNumber).apply()
    }

}