package com.mongokerala.taxi.newuser.ui.triphistory

import android.view.View

interface RecyclerViewItemClickListener {
    fun onItemClickListener(pos: Int, v: View?)

}