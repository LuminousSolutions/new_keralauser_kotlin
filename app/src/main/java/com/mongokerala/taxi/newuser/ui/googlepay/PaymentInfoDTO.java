package com.mongokerala.taxi.newuser.ui.googlepay;

import com.mongokerala.taxi.newuser.paymentgateway.enums.PaymentGateway;
import com.mongokerala.taxi.newuser.paymentgateway.enums.PaymentStatus;

import java.math.BigDecimal;

public class PaymentInfoDTO {

    private String gatewayOrderID; // CCAvenue Tracking Id
    private PaymentGateway paymentGateway; // CCAvenue
    private String paymentMode; // Credit Card, Net Banking
    private String paymentModeInstrument; // ICICI, HDFC, SBI
    private PaymentStatus paymentStatus;
    private String bankStatusCode;
    private String bankStatusMessage;
    private String bankReferenceNumber;
    private String bankFailureMessage;
    private String paymentCurrency;
    private BigDecimal paymentAmount;
    private String transactionId;
    private String email;
    private String name;
    private String additionalNotes;
    private boolean paymentResponseVerified;

    public String getGatewayOrderID() {
        return gatewayOrderID;
    }

    public void setGatewayOrderID(String gatewayOrderID) {
        this.gatewayOrderID = gatewayOrderID;
    }

    public PaymentGateway getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(PaymentGateway paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getBankStatusCode() {
        return bankStatusCode;
    }

    public void setBankStatusCode(String bankStatusCode) {
        this.bankStatusCode = bankStatusCode;
    }

    public String getBankStatusMessage() {
        return bankStatusMessage;
    }

    public void setBankStatusMessage(String bankStatusMessage) {
        this.bankStatusMessage = bankStatusMessage;
    }

    public String getBankReferenceNumber() {
        return bankReferenceNumber;
    }

    public void setBankReferenceNumber(String bankReferenceNumber) {
        this.bankReferenceNumber = bankReferenceNumber;
    }

    public String getBankFailureMessage() {
        return bankFailureMessage;
    }

    public void setBankFailureMessage(String bankFailureMessage) {
        this.bankFailureMessage = bankFailureMessage;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentModeInstrument() {
        return paymentModeInstrument;
    }

    public void setPaymentModeInstrument(String paymentModeInstrument) {
        this.paymentModeInstrument = paymentModeInstrument;
    }

    public String getAdditionalNotes() {
        return additionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getPaymentResponseVerified() {
        return paymentResponseVerified;
    }

    public void setPaymentResponseVerified(boolean paymentResponseVerified) {
        this.paymentResponseVerified = paymentResponseVerified;
    }
}
