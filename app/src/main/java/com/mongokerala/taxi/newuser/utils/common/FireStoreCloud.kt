package com.mongokerala.taxi.newuser.utils.common

import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

object FireStoreCloud {



    fun cloud_ride_key(Ride_Key: String, db: FirebaseFirestore){
        val user: MutableMap<String, Any> =
                HashMap()

        user["Ride_Key"] = Ride_Key
        val currentTime = Calendar.getInstance().time
        user["Booking_Time"] = currentTime
        user["USER_IS_ANDROID"] = true
        user["IS_ACCEPTED"] = false
        user["IS_CANCEL"] = false

        db.collection("RIDE_KEY").document(Ride_Key)
                .set(user)
                .addOnSuccessListener {
                    aVoid: Void? ->
                }
                .addOnFailureListener {

                    e: Exception? ->
                }
    }



    fun cloud_USER_CANCEL_BOOKING(
        Ride_Key: String,
        reasonMsg: String,
        status: String
    ) {

        val db = FirebaseFirestore.getInstance()
        val user: MutableMap<String, Any> =
            HashMap()
        user["RIDE_Status"] = status
        user["CANCEL_REASON"] = reasonMsg

        db.collection("RIDE_KEY").document(Ride_Key)
            .update(user)
            .addOnSuccessListener { aVoid: Void? ->
            }
            .addOnFailureListener { e: java.lang.Exception? ->
            }
    }
    fun delRideKeyLocalValues(Ride_Key: String, db: FirebaseFirestore){

        db.collection("RIDE_KEY").document(Ride_Key)
            .delete()
            .addOnSuccessListener {
                    aVoid: Void? ->
            }
            .addOnFailureListener {

                    e: Exception? ->
            }
    }
}