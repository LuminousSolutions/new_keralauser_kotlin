package com.mongokerala.taxi.newuser.data.remote.request

object Keys {
    const val blobKey = "blobkey"
    const val fileName = "fileName"
    const val imageUrl = "imageUrl"
    const val values="value"
    //region Keys for SignupRequest
    const val priceperkm = "price"
    const val address = "address"
    const val code = "code"
    const val email = "email"
    const val firstName = "firstName"
    const val id = "id"
    const val imageInfo = "imageInfo"
    const val paymentType = "paymentType"

    const val isSocialUser = "isSocialUser"
    const val socialId = "socialId"
    const val lang = "lang"
    const val language = "language"
    const val jwt = "jwt"
    const val isAndroid = "isAndroid"
    const val lastName = "lastName"
    const val latitude = "latitude"
    const val licenseNumber = "licenseNumber"
    const val longitude = "longitude"
    const val password = "password"
    const val phoneNumber = "phoneNumber"
    const val phonenumber = "phonenumber"
    const val rideDistnce = "rideDistnce"
    const val restKey = "restKey"
    const val role = "role"
    const val socialUser = "socialUser"
    const val lan = "lan"
    const val token = "token"
    const val notificationToken = "notificationToken"
    //userDto
    const val countryCode = "countryCode"
    const val country = "country"

    const val region = "region"
    const val zipCode = "zipCode"
    const val domain = "domain"
    const val taxiDetailId = "taxiDetailId"

    const val phoneVerified = "phoneVerified"

    //endregion

    //endregion
//region Keys for Login Response
    const val statusCode = "statusCode"
    const val infoId = "infoId"
    const val message = "message"
    const val data = "data"
    const val taxiId = "taxiId"
    const val Job ="Job"
    //endregion


    //endregion
//region Keys for RideDTO
    const val type = "type"
    const val destination = "destination"
    const val distance = "distance"
    const val googleKm = "googleKm"
    const val driverId = "driverId"
    const val price = "price"
    const val push = "push"
    const val peakPrice = "peakPrice"
    const val radius = "radius"
    const val perDay = "perDay"
    const val source = "source"
    const val status = "status"
    const val des = "des"
    const val userId = "userId"
    const val companyName = "companyName"
    const val payment = "payment"
    const val discount = "discount"
    const val oneSignalconst  = "oneSignalconst value"
    const val oneSignalId = "oneSignalId"
    const val userToAddress = "userToAddress"
    const val carType = "carType"
    const val tag = "tag"
    const val driverStatus = "driverStatus"
    const val carTypeForRegistration = "carType"
    const val oneSignalValue = "oneSignalValue"
    const val COUPON = "COUPON"
    const val is_Online = "is_Online"


    //endregion


    //endregion
//region Keys for RideResponseMessage
    const val base = "base"
    const val cartype = "cartype"
    const val deviceId = "deviceId"
    const val imageInfos = "imageInfos"
    const val name = "name"
    const val supplierId = "supplierId"
    const val transporttype = "transporttype"
    const val waitingTime = "waitingTime"
    const val airPortprice = "airPortprice"
    const val basePrice = "basePrice"

    //endregion

    //endregion
//region Keys for Google Geocoding API
    const val address_components = "address_components"
    const val results = "results"
    const val long_name = "long_name"
    const val short_name = "short_name"
    const val types = "types"
    const val formatted_address = "formatted_address"
    const val geometry = "geometry"
    const val bounds = "bounds"
    const val northeast = "northeast"
    const val lat = "lat"
    const val lng = "lng"
    const val totalTime = "totalTime"
    const val endTIme = "endTIme"
    const val southwest = "southwest"
    const val location = "location"
    const val location_type = "location_type"
    const val viewport = "viewport"
    const val place_id = "place_id"
    const val partial_match = "partial_match"

    const val latlng = "latlng"

    const val key = "key"
    const val rideStatus = "rideStatus"
    //endregion
//tripHistory
    const val km = "km"
    const val userTotalPrice = "userTotalPrice"
    const val apx_google_km = "apx_google_km"
    const val dealId = "dealId"
    const val userName = "userName"
    const val driverName = "driverName"
    const val IS_APPROVED = "IS_APPROVED"
    const val IS_RIDE_STARTED = "IS_RIDE_STARTED"

    const val totalPrice = "totalPrice"
    /*Google map direction Api strings*/
    const val directionRoute = "routes"
    const val directionLegs = "legs"
    const val updatedOn = "updatedOn"
    const val travelTime = "travelTime"

    const val taxiDetailID = "taxiDetailId"
    const val comment = "comment"
    const val rating = "rating"
    const val star = "star"
    const val postedBy = "postedBy"
    const val totalComments = "totalComments"
    const val driverLoginstatus = "loginStatus"
    const val driverNotificationSetting = "notification"
    const val subject = "subject"
    const val senderMail = "senderMail"
    const val hourly = "hourly"
    const val seats = "seats"
    const val vehicleBrand = "vehicleBrand"
    const val vehicleYear = "vehicleYear"
    const val taxiNumber = "taxiNumber"
    const val WeekEndOffer = "weekEndOffer"
    const val BasePrice = "basePrice"
    const val NightPrice = "peakPrice"


    //FCM Notification json keys
    const val notification = "notification"
    const val android = "android"
    const val action = "action"
    const val registration_ids = "registration_ids"
    const val userFcmToken = "userFcmToken"
    const val driverFcmToken = "driverFcmToken"
    const val newDestination = "newDestination"
    const val cancelReason = "cancelReason"
    const val carSpeed = "carSpeed"
    const val rideDistance = "rideDistance"
    const val rideAmount = "rideAmount"

    const val rideId = "rideId"
    const val sourceLatitude = "sourceLatitude"
    const val sourceLongitude = "sourceLongitude"
    const val chatId = "chatId"
    const val year = "year"

    const val deliveryQty = "deliveryQty"
    const val deliveryPickupTime = "deliveryPickupTime"
    const val deliveryCategory = "deliveryCategory"
    const val desc = "desc"
    const val item = "item"
    const val category = "category"
    const val text = "text"


    const val notificationMessage = "notificationMessage"
    const val isApproved = "isApproved"

    const val website = "website"
    const val nightprice = "peakPrice"

    //fcmCustomNotification
    const val otp = "otp"

    const val RIDE_KEY_LOCAL = "RIDE_KEY_LOCAL"
    const val apxPrice = "apxPrice"
    const val coupon = "COUPON"
    const val Category = "Category"
    const val taxi_type = "taxi_type"

    //to get total amount
    const val balance = "balance"
    const val credit = "credit"
    const val debit = "debit"
    const val currentDate = "currentDate"
    const val percentage = "percentage"


}