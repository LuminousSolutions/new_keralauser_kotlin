package com.mongokerala.taxi.newuser.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.mongokerala.taxi.newuser.BuildConfig
import com.mongokerala.taxi.newuser.TaxiDealsFrameWorkApplication
import com.mongokerala.taxi.newuser.data.local.db.DatabaseService
import com.mongokerala.taxi.newuser.data.remote.NetworkService
import com.mongokerala.taxi.newuser.data.remote.Networking
import com.mongokerala.taxi.newuser.di.ApplicationContext
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.RxSchedulerProvider
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import com.pixplicity.easyprefs.library.Prefs
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: TaxiDealsFrameWorkApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application = application

    @Provides
    @Singleton
    @ApplicationContext
    fun provideContext(): Context = application

    /**
     * Since this function do not have @Singleton then each time CompositeDisposable is injected
     * then a new instance of CompositeDisposable will be provided
     */
    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    fun provideSchedulerProvider(): SchedulerProvider = RxSchedulerProvider()

    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences =
        application.getSharedPreferences("updated-driver-project-prefs", Context.MODE_PRIVATE)

    /**
     * We need to write @Singleton on the provide method if we are create the instance inside this method
     * to make it singleton. Even if we have written @Singleton on the instance's class
     */
    @Provides
    @Singleton
    fun provideDatabaseService(): DatabaseService =
        Room.databaseBuilder(
            application, DatabaseService::class.java,
            "updated-driver-project-db"
        ).build()

    @Provides
    @Singleton
    fun provideNetworkService(): NetworkService =
        Networking.create(
            BuildConfig.API_KEY,
            Prefs.getString("base_url", BuildConfig.BASE_URL),
            application.cacheDir,
            10 * 1024 * 1024 // 10MB
        )



    @Singleton
    @Provides
    fun provideNetworkHelper(): NetworkHelper = NetworkHelper(application)
}