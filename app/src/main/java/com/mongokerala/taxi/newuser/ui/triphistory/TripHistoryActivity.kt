package com.mongokerala.taxi.newuser.ui.triphistory

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.data.remote.response.TripHistoryData
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.tripdetail.TripDetailActivity
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*

class TripHistoryActivity : BaseActivity<TripHistoryViewModel>(), RecyclerViewItemClickListener {

    companion object {
        const val TAG = "TripHistoryActivity"
    }

    var name = ""
    var payment = ""
    var rating = 0.0f

    // String carType = "";
    var rideDistancePrice = 0.0f
    var sourceAddress = ""
    var destinationAddress = ""
    var category = ""

    @NonNull
    var imgUrl = ""
    var type: Any = ""

    @NonNull
    var rideKm = " "
    var totalPrice = 0.0f
    var usertotalPrice = 0.0f
    var basePrice = 0.0f
    var durationtime = 0.0
    var taxvalue = 0.0
    var percentagevalue = 0.0
    var travelTime = ""

    @NonNull
    var endtime = ""

    var startTIme= ""

    private var tripHistoryList: List<TripHistoryData>? = null

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun provideLayoutId(): Int = R.layout.activity_trip_history

    override fun setupView(savedInstanceState: Bundle?) {
        viewModel.onLoadTripHistory()
    }

    override fun setupObservers() {
        super.setupObservers()

        viewModel.tripIn.observe(this, Observer {
            spin_kit.visibility = if (it) View.VISIBLE else View.GONE
        })

        viewModel.tripHistoryLiveData.observe(this, Observer {
            tripHistoryList = ArrayList()
            tripHistoryList = it
            val tripHistoryAdapter = TripHistoryAdapter(
                baseContext,
                it, this
            )
            val recycler_view: RecyclerView = findViewById(R.id.list)
            val mLayoutManager = LinearLayoutManager(this)
            recycler_view.setLayoutManager(mLayoutManager)
            tripHistoryAdapter.notifyDataSetChanged()
            recycler_view.setAdapter(tripHistoryAdapter)

        })
    }

    override fun onItemClickListener(pos: Int, v: View?) {

        tripHistoryList!![pos].comments
        name = tripHistoryList!![pos].userName
        rating = tripHistoryList!![pos].star.toFloat()
        rideDistancePrice = tripHistoryList!![pos].price
        sourceAddress = tripHistoryList!![pos].source
        type = tripHistoryList!![pos].type
        payment = tripHistoryList!![pos].payment
        destinationAddress = tripHistoryList!![pos].destination
//        imgUrl = tripHistoryList!![pos].imageInfo.imageUrl.toString()
        rideKm = tripHistoryList!![pos].km.toString()
        totalPrice = tripHistoryList!![pos].totalPrice
        usertotalPrice = tripHistoryList!![pos].userTotalPrice
        basePrice = tripHistoryList!![pos].base
        travelTime = tripHistoryList!![pos].travelTime.toString()
        if (tripHistoryList!![pos].endTIme!=null){
            endtime = tripHistoryList!![pos].endTIme
        }
        if (tripHistoryList!![pos].startTIme!=null){
            startTIme = tripHistoryList!![pos].startTIme
        }

        durationtime = tripHistoryList!![pos].travelTime
        taxvalue= tripHistoryList!![pos].tax
        percentagevalue= tripHistoryList!![pos].percentage
        category = tripHistoryList!![pos].category


        startActivity(
            Intent(this@TripHistoryActivity, TripDetailActivity::class.java)
                .putExtra("ITEM_POSITION", pos)
                .putExtra("NAME", name)
                .putExtra("SOURCE", sourceAddress)
                .putExtra("TYPE", type.toString())
                .putExtra("PAYMENT", payment)
                .putExtra("DESTINATION", destinationAddress)
                .putExtra(
                    "IMAGE_URL",
                    imgUrl
                ) /*Auth :Thiyagesh Des above items added */ //.putExtra("TOTAL_RIDE_PRICE", totalRidePrice)
                .putExtra("DISTANCE", rideKm)
                .putExtra("RATING_VALUES", rating)
                .putExtra("TRIP_DATE", tripHistoryList!![pos].endTIme)
                .putExtra("TRAVELTIME", travelTime)
                .putExtra("DURATIONTIME", durationtime)
                .putExtra("TOTALPRICE", totalPrice)
                .putExtra("USERTOTALPRICE", usertotalPrice)
                .putExtra("BASEPRICE", java.lang.Float.valueOf(basePrice))
                .putExtra("RIDEPRICE", java.lang.Float.valueOf(rideDistancePrice))
                .putExtra("ENDTIME", endtime)
                .putExtra("startTIme", startTIme)
                .putExtra(
                    "RIDE_ID",
                    tripHistoryList!![pos].id
                ).putExtra("TAX", taxvalue)
                .putExtra("PERCENTAGE", percentagevalue)
                .putExtra("CATEGORY", category)
            //.putExtra("Distance",currentTripList.get(position).getTripTotalPrice())
        )

    }

    fun trip_history_back(view: View) {

        finish()

    }

}
