package com.mongokerala.taxi.newuser.arcLayout

import android.graphics.Path
import android.graphics.Point
import android.graphics.RectF

enum class Arc(val startAngle: Int, private val sweepAngle: Int) {
    CENTER(270, 360) {
        override fun computePath(radius: Int, l: Int, t: Int, r: Int, b: Int): Path {
            val o = computeOrigin(l, t, r, b)
            val path = Path()
            path.addCircle(o.x.toFloat(), o.y.toFloat(), radius.toFloat(), Path.Direction.CW)
            return path
        }
    },
    LEFT(270, 180) {
        override fun computeOrigin(l: Int, t: Int, r: Int, b: Int): Point {
            return Point(l, centerY(t, b))
        }

        override fun computeWidth(radius: Int): Int {
            return radius
        }
    },
    RIGHT(90, 180) {
        override fun computeOrigin(l: Int, t: Int, r: Int, b: Int): Point {
            return Point(r, centerY(t, b))
        }

        override fun computeWidth(radius: Int): Int {
            return radius
        }
    },
    TOP(0, 180) {
        override fun computeOrigin(l: Int, t: Int, r: Int, b: Int): Point {
            return Point(centerX(l, r), t)
        }

        override fun computeHeight(radius: Int): Int {
            return radius
        }
    },
    TOP_LEFT(0, 90) {
        override fun computeOrigin(l: Int, t: Int, r: Int, b: Int): Point {
            return Point(l, t)
        }

        override fun computeWidth(radius: Int): Int {
            return radius
        }

        override fun computeHeight(radius: Int): Int {
            return radius
        }
    },
    TOP_RIGHT(90, 90) {
        override fun computeOrigin(left: Int, t: Int, r: Int, b: Int): Point {
            return Point(r, t)
        }

        override fun computeWidth(radius: Int): Int {
            return radius
        }

        override fun computeHeight(radius: Int): Int {
            return radius
        }
    },
    BOTTOM(180, 180) {
        override fun computeOrigin(l: Int, t: Int, r: Int, b: Int): Point {
            return Point(centerX(l, r), b)
        }

        override fun computeHeight(radius: Int): Int {
            return radius
        }
    },
    BOTTOM_LEFT(270, 90) {
        override fun computeOrigin(l: Int, t: Int, r: Int, b: Int): Point {
            return Point(l, b)
        }

        override fun computeWidth(radius: Int): Int {
            return radius
        }

        override fun computeHeight(radius: Int): Int {
            return radius
        }
    },
    BOTTOM_RIGHT(180, 90) {
        override fun computeOrigin(l: Int, t: Int, r: Int, b: Int): Point {
            return Point(r, b)
        }

        override fun computeWidth(radius: Int): Int {
            return radius
        }

        override fun computeHeight(radius: Int): Int {
            return radius
        }
    };

    fun computeDegrees(index: Int, perDegrees: Float): Float {
        val offsetAngle = if (sweepAngle < 360) startAngle - perDegrees / 2f else startAngle.toFloat()
        return offsetAngle + perDegrees + perDegrees * index
    }

    fun computeReverseDegrees(index: Int, perDegrees: Float): Float {
        val offsetAngle = if (sweepAngle < 360) startAngle + perDegrees / 2f else startAngle.toFloat()
        val shiftDegrees = sweepAngle / 360 * perDegrees
        return offsetAngle + sweepAngle - (perDegrees + perDegrees * index) + shiftDegrees
    }

    fun computePerDegrees(size: Int): Float {
        return sweepAngle.toFloat() / size
    }

    open fun computePath(radius: Int, l: Int, t: Int, r: Int, b: Int): Path {
        val o = computeOrigin(l, t, r, b)
        val ol = o.x - radius
        val ot = o.y - radius
        val or = o.x + radius
        val ob = o.y + radius
        val path = Path()
        path.moveTo(o.x.toFloat(), o.y.toFloat())
        when (startAngle) {
            0 -> path.lineTo(or.toFloat(), o.y.toFloat())
            90 -> path.lineTo(o.x.toFloat(), ob.toFloat())
            180 -> path.lineTo(ol.toFloat(), o.y.toFloat())
            270 -> path.lineTo(o.x.toFloat(), ot.toFloat())
            else -> throw UnsupportedOperationException()
        }
        if (Utils.LOLLIPOP_OR_LATER) {
            path.arcTo(ol.toFloat(), ot.toFloat(), or.toFloat(), ob.toFloat(), startAngle.toFloat(), sweepAngle.toFloat(), true)
        } else {
            path.arcTo(RectF(ol.toFloat(), ot.toFloat(), or.toFloat(), ob.toFloat()), startAngle.toFloat(), sweepAngle.toFloat(), true)
        }
        path.lineTo(o.x.toFloat(), o.y.toFloat())
        return path
    }

    open fun computeOrigin(l: Int, t: Int, r: Int, b: Int): Point {
        return Point(centerX(l, r), centerY(t, b))
    }

    open fun computeWidth(radius: Int): Int {
        return diameter(radius)
    }

    open fun computeHeight(radius: Int): Int {
        return diameter(radius)
    }

    companion object {
        fun x(radius: Int, degrees: Float): Int {
            return Math.round(Utils.computeCircleX(radius.toFloat(), degrees))
        }

        fun y(radius: Int, degrees: Float): Int {
            return Math.round(Utils.computeCircleY(radius.toFloat(), degrees))
        }

        private fun centerX(left: Int, right: Int): Int {
            return (left + right) / 2
        }

        private fun centerY(top: Int, bottom: Int): Int {
            return (top + bottom) / 2
        }

        private fun diameter(radius: Int): Int {
            return radius * 2
        }

        fun of(origin: Int): Arc {
            return when (origin and ArcOrigin.VERTICAL_MASK) {
                ArcOrigin.TOP -> ofTop(origin)
                ArcOrigin.BOTTOM -> ofBottom(origin)
                else -> ofCenter(origin)
            }
        }

        private fun ofTop(origin: Int): Arc {
            return when (origin and ArcOrigin.HORIZONTAL_MASK) {
                ArcOrigin.LEFT -> TOP_LEFT
                ArcOrigin.RIGHT -> TOP_RIGHT
                else -> TOP
            }
        }

        private fun ofCenter(origin: Int): Arc {
            return when (origin and ArcOrigin.HORIZONTAL_MASK) {
                ArcOrigin.LEFT -> LEFT
                ArcOrigin.RIGHT -> RIGHT
                else -> CENTER
            }
        }

        private fun ofBottom(origin: Int): Arc {
            return when (origin and ArcOrigin.HORIZONTAL_MASK) {
                ArcOrigin.LEFT -> BOTTOM_LEFT
                ArcOrigin.RIGHT -> BOTTOM_RIGHT
                else -> BOTTOM
            }
        }
    }

}
