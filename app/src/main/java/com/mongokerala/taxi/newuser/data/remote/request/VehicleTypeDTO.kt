package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class VehicleTypeDTO {

    @SerializedName("description")
    @Expose
    private val description: String? = null

    @SerializedName("id")
    @Expose
    private val id: String? = null

    @SerializedName("lang")
    @Expose
    private val lang: String? = null

    @SerializedName("code")
    @Expose
    private val code: String? = null

    @SerializedName("name")
    @Expose
    private val name: String? = null
}