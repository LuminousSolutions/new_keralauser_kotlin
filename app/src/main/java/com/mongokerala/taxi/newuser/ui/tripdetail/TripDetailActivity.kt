package com.mongokerala.taxi.newuser.ui.tripdetail

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.utils.common.AppUtils
import com.mongokerala.taxi.newuser.utils.common.Constants.TIME_FORMAT
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.display.Toaster
import kotlinx.android.synthetic.main.activity_trip_detail.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class TripDetailActivity : BaseActivity<TripDetailViewModel>(){

    companion object {
        const val TAG = "TripDetailActivity"
    }

    private var position = 0
    private var name = " "
    private var payment = " "
    private val total_time: String? = null
    private var rideid = " "

    @NonNull
    private var sourceAddress: String? = null
    private var destinationAddress: String? = null
    private var categorytrip: String? = null
    private var imgUrl: String? = null
    private val totalRidePrice: String? = null
    private var tripDateTime: String? = null
    private val rideKM = " "
    private val bitmap: Bitmap? = null

    override fun setupObservers() {
        super.setupObservers()

        viewModel.invoice_success.observe(
            this,
            androidx.lifecycle.Observer<Event<Map<String, String>>> {
                it.getIfNotHandled()?.run {
                    Toaster.show(applicationContext, "Invoice Sent to Your Mail")
                }
            })
    }

    private val encodedImageUrl: String? = null
    private val distance: String? = null

    @Nullable
    private val rideKm: String? = null
    private var type: Any = ""
    private var basePrice = 0.0f
    private var rating = 0.0f
    private var totalPrice = 0.0f
    private var taxvalue = 0.0
    private var percentagevalue = 0.0
    private var usertotalPrice = 0.0f
    private var travelTime = "11:03 AM"
    private var durationTime = 0.0
    private var totalkm = 0.0f
    private var rideDistancePrice = 0.0f
    private var endtime:String?=null
    private var starttime:String?=null
//    private var endtime: String? = null
    private val df = DecimalFormat("0.00")
    var dflonNew = DecimalFormat("#.##")

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun provideLayoutId(): Int = R.layout.activity_trip_detail

    override fun setupView(savedInstanceState: Bundle?) {

        if (intent != null) {
            position = intent.getIntExtra("ITEM_POSITION", 0)
            if (intent.getStringExtra("NAME")!=null) {
                name = intent.getStringExtra("NAME")!!
            }else{
                name = "test"
            }
            //  = getIntent().getParcelableArrayListExtra("TRIP_ITEMS");
            totalkm = intent.getStringExtra("DISTANCE")!!.toFloat()
            sourceAddress = intent.getStringExtra("SOURCE")
            type = intent.getStringExtra("TYPE")!!
            payment = intent.getStringExtra("PAYMENT")!!
            destinationAddress = intent.getStringExtra("DESTINATION")
            imgUrl = intent.getStringExtra("IMAGE_URL")
            tripDateTime = intent.getStringExtra("TRIP_DATE")
            rating = intent.getFloatExtra("RATING_VALUES", 0.0f)
            basePrice = intent.getFloatExtra("BASEPRICE", 0.0f)
            rideDistancePrice = intent.getFloatExtra("RIDEPRICE", 0.0f)
            totalPrice = intent.getFloatExtra("TOTALPRICE", 0.0f)
            usertotalPrice = intent.getFloatExtra("USERTOTALPRICE", 0.0f)
            if (intent.getStringExtra("TRAVELTIME")==null){
                travelTime = "0.0 min"
            }
            durationTime = intent.getDoubleExtra("DURATIONTIME", 0.0)
            if (intent.getStringExtra("ENDTIME")==null){
                endtime="null"
            }else{
                endtime = intent.getStringExtra("ENDTIME")
            }

            if (intent.getStringExtra("startTIme")==null){
                starttime="null"
            }else{
                starttime = intent.getStringExtra("startTIme")
            }

            rideid = intent.getStringExtra("RIDE_ID").toString()
            taxvalue = intent.getDoubleExtra("TAX", 0.0)
            percentagevalue = intent.getDoubleExtra("PERCENTAGE", 0.0)
            categorytrip = intent.getStringExtra("CATEGORY")


            //   Toast.makeText(this, "Position is: " + position, Toast.LENGTH_SHORT).show();
        }
        if (name != null) {
            user_name1.text = name
        }
        dflonNew.roundingMode = RoundingMode.CEILING
        user_rating.rating = rating
        total_kms.text = "$totalkm km"
        car_type_name.text = type as CharSequence
        ride_mode.text = payment
        //        trip_start_time.setText(getRideTime(travelTime));
//        trip_end_time.setText(getRideTime(endtime));
        val tripstarttime: String = getRideTime(starttime)
        val rideendTime: String = getRideEndTime(endtime)
        //trip_start_time.text = getRideTime(travelTime)
//        trip_end_time.text = getRideEndTime(endtime)
        trip_start_time.text = tripstarttime
        trip_end_time.text = rideendTime
//        trip_end_time.text = getRideTime(endtime!!)

        duration_ride.text = "$durationTime min"
        source_address.text = sourceAddress
        destination_address.text = destinationAddress
        base_fare_amount.text = basePrice.toString()
        ride_fare_amount.text = rideDistancePrice.toString()
        ride_total_fare.text = usertotalPrice.toString()
        total_ride_fare.text = usertotalPrice.toString()
        trip_date_time.text = tripDateTime
        ride_id.text = rideid
        var taxround = dflonNew.format(taxvalue)
        var percentageround = dflonNew.format(percentagevalue)
        /*
        //command by surya
        gst_fare_amount.text = taxround
        percentage_fare_amount.text = percentageround
        driver_fare_amount.text = totalPrice.toString()*/
        if (categorytrip!=null) {
            if (categorytrip.equals("TAXI")) {
                assert(category_txt != null)
                category_txt.text = "One way"
                category_img.setImageResource(R.drawable.ic_baseline_flight_24)
            } else if (categorytrip == "AIRPORT") {
                assert(category_txt != null)
                category_txt.text = "AIRPORT"
                category_img.setImageResource(R.drawable.ic_baseline_flight_24)
            } else if (categorytrip == "OUTSTATION") {
                assert(category_txt != null)
                category_txt.text = "OUTSTATION"
                category_img.setImageResource(R.drawable.ic_baseline_airport_shuttle_24)
            } else if (categorytrip == "KOVALAM") {
                assert(category_txt != null)
                category_txt.text = "KOVALAM"
                category_img.setImageResource(R.drawable.ic_baseline_beach_access_24)
            } else if (categorytrip == "POOVAR") {
                assert(category_txt != null)
                category_txt.text = "POOVAR"
                category_img.setImageResource(R.drawable.ic_baseline_beach_access_24)
            } else if (categorytrip == "KANNIYAKUMARI") {
                assert(category_txt != null)
                category_txt.text = "KANNIYAKUMARI"
                category_img.setImageResource(R.drawable.ic_baseline_beach_access_24)
            } else if (categorytrip == "PACKAGE") {
                assert(category_txt != null)
                category_txt.text = "PACKAGE"
                category_img.setImageResource(R.drawable.ic_baseline_access_time_filled_24)
            }else {
                category_txt.text = categorytrip
            }
        }


        val encodedString = imgUrl
        if (encodedString != null && encodedString != " ") profile_image.setImageBitmap(
            AppUtils.convertStringToImage(
                encodedString.toString()
            )
        )

    }

    private fun getRideTime(startTIme: String?): String {
        Log.d("get Ride Start Time", "get Ride Start Time: $startTIme")
        //Long timeMillis = Long.valueOf(String.valueOf(endTIme));
        if (startTIme.equals("null")) {
            val dateFormat = SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
            dateFormat.timeZone = TimeZone.getTimeZone("GMT") // setting timezone to GMT
            val tripDateTime = AppUtils.convertTIme(dateFormat.format(Date()), TIME_FORMAT)
            Log.d("TAG", "getRideTime: $tripDateTime")
            return tripDateTime.toString()
        }

        var timeMillis =startTIme
        val dateFormat =
            SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone("GMT") // setting timezone to GMT
        val tripDateTime = AppUtils.convertTIme(dateFormat.format(Date(timeMillis)), TIME_FORMAT)
        return tripDateTime.toString()
    }
    private fun getRideEndTime(endTIme: String?): String {

        Log.d("get RideTime", "getRideTime: $endTIme")
        //Long timeMillis = Long.valueOf(String.valueOf(endTIme));
        if (endTIme.equals("null")) {
            val dateFormat = SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
            dateFormat.timeZone = TimeZone.getTimeZone("GMT") // setting timezone to GMT
            val tripDateTime = AppUtils.convertTIme(dateFormat.format(Date()), TIME_FORMAT)
            Log.d("TAG", "getRideTime: $tripDateTime")
            return tripDateTime.toString()
        }

        var timeMillis =endTIme
        val dateFormat =
            SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone("GMT") // setting timezone to GMT
        val tripDateTime = AppUtils.convertTIme(dateFormat.format(Date(timeMillis)), TIME_FORMAT)
        return tripDateTime.toString()
    }

    fun send_invoice(view: View) {

        viewModel.doInvoice(rideid)

    }

    fun backToTripHistory(view: View) {
        finish()
    }


}
