package com.mongokerala.taxi.newuser.paymentgateway.adyan;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import okhttp3.ResponseBody;

/**
 * Created by Akhi007 on 23-01-2019.
 */
public class PaymentUtils {
    private static final String TAG = PaymentUtils.class.getSimpleName();

    public static ApiResponse convertApiErrorBodyToApiResponse(ResponseBody errorResponseBody){
        ApiResponse apiResponse = null;
        try {
            JSONObject jsonObjError = new JSONObject(errorResponseBody.string());
            apiResponse =  new Gson().fromJson(jsonObjError.toString(), ApiResponse.class);
        }catch (Exception e){
            Log.e(TAG, "Exception while converting ErrorBodyResponse : ",e);
        }
        return apiResponse;
    }
}
