package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class PriceUpdate(
    @field:SerializedName(Keys.distance) @field:Expose var distance: Double?,
    @field:SerializedName(Keys.googleKm) @field:Expose var googleKm: Double?,
    @field:SerializedName(Keys.latitude) @field:Expose var latitude: Double?,
    @field:SerializedName(Keys.longitude) @field:Expose var longitude: Double?,
    @field:SerializedName(Keys.region) @field:Expose var region: String?,
    @field:SerializedName(Keys.rideId) @field:Expose var rideId: String?,
    @field:SerializedName(Keys.sourceLatitude) @field:Expose var sourceLatitude: Double?,
    @field:SerializedName(Keys.sourceLongitude) @field:Expose var sourceLongitude: Double?,
    @field:SerializedName(Keys.status) @field:Expose var status: String?,
    @field:SerializedName(Keys.waitingTime) @field:Expose var waitingTime: String?,
    @field:SerializedName(Keys.type) @field:Expose var type: String?,
    @field:SerializedName(Keys.domain) @field:Expose var domain: String?,
    @field:SerializedName(Keys.travelTime) @field:Expose var travelTime: String?,
    @field:SerializedName(Keys.category) @field:Expose var category: String?
    ) {


    @SerializedName("destination")
    @Expose
    private var destination: String? = null

    @SerializedName("discount")
    @Expose
    private var discount: Int? = null

    @SerializedName("elapsedTime")
    @Expose
    private var elapsedTime: Int? = null


    init {
        destination = "string"
        discount  = 0
        elapsedTime = 0
    }

}