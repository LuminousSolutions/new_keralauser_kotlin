package com.mongokerala.taxi.newuser.ui.setting


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.forgetpassword.ForgetPasswordFragment
import com.mongokerala.taxi.newuser.ui.googlepay.GooglePayActivity
import com.mongokerala.taxi.newuser.ui.language.LanguageFragment
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity
import com.mongokerala.taxi.newuser.ui.payment.PaymentActivity
import com.mongokerala.taxi.newuser.ui.razorpay.RazPayActivity
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import kotlinx.android.synthetic.main.activity_setting.*
import org.json.JSONObject
import java.util.*

class SettingActivity : BaseActivity<SettingViewModel>(), PaymentResultListener {

    val UPI_PAYMENT = 0

    companion object {
        const val TAG = "SettingActivity"
    }

    private var activeFragment: Fragment? = null
    private var myactive: Fragment? = null

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun provideLayoutId(): Int = R.layout.activity_setting

    override fun setupView(savedInstanceState: Bundle?) {
    }

    override fun setupObservers() {
        super.setupObservers()
        Checkout.preload(applicationContext)
        /*
        //commanted by surya
        txt_change_pwd.visibility = View.VISIBLE
        txt_payment.visibility = View.VISIBLE
        txt_Upiqrcode.visibility = View.VISIBLE
        */

    }

    fun onBackClick(view: View) {

        val refresh = Intent(this@SettingActivity, MainNewActivity::class.java)
        startActivity(refresh)
        finish()

    }

    private fun showLanguageFragment() {
        if (activeFragment is LanguageFragment) return

        val fragmentTransaction = supportFragmentManager.beginTransaction()

        var fragment = supportFragmentManager.findFragmentByTag(LanguageFragment.TAG) as LanguageFragment?

        if (fragment == null) {
            fragment = LanguageFragment.newInstance()
           /*
           //commanded by surya
           fragmentTransaction.add(R.id.containerLanguageFragment, fragment, LanguageFragment.TAG)
           */
        } else {
            fragmentTransaction.show(fragment)
        }

        if (activeFragment != null) fragmentTransaction.hide(activeFragment as Fragment)

        fragmentTransaction.commit()
        activeFragment = fragment

    }

    /*
    fun onLanguageClick(view: View) {
        lay_setting.isEnabled = false
        line2.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.black))
        line3.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.black))
        line4.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.black))
        line5.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.black))
        txt_change_pwd.visibility = View.GONE
        txt_payment.visibility = View.GONE
        txt_Upiqrcode.visibility = View.GONE
        showLanguageFragment()

    }
    */


    fun onPaymentClick(view: View) {
        /*val refresh = Intent(this@SettingActivity, RazPayActivity::class.java)
        startActivity(refresh)*/
        Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show()

//        startRazorPayment()
    }
    fun onCreditcardClick(view: View) {
        /*  AppUtils.snack_coming_soon(SettingActivity.this, this.getString(R.string.Coming_soon));
      //Toast.makeText(getApplicationContext(), R.string.on_progress, Toast.LENGTH_LONG).show();
      startActivity(new Intent(SettingActivity.this, PaymentActivity.class));
  */
        startActivity(Intent(this@SettingActivity, GooglePayActivity::class.java))
    }



    fun onUPIQrcodeClick(view: View) {

        val refresh = Intent(this@SettingActivity, PaymentActivity::class.java)
        startActivity(refresh)
    }

//

    fun onChangePassword(view: View) {
        /*
         //commanded by surya
        lay_setting.isEnabled = false
        line2.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.black))
        line3.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.black))
        line4.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.black))
        line5.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.black))
        txt_language.visibility = View.GONE
        txt_payment.visibility = View.GONE
        txt_Upiqrcode.visibility = View.GONE
        */
        lay_setting1.visibility=View.GONE
        showChangePassword()


//        val refresh = Intent(this, ForgetPasswordFragment::class.java)
//        startActivity(refresh)

    }
    private fun showChangePassword() {
        if (myactive is ForgetPasswordFragment) return

        val fragmentTransaction = supportFragmentManager.beginTransaction()

        var fragment = supportFragmentManager.findFragmentByTag(ForgetPasswordFragment.TAG) as ForgetPasswordFragment?

        if (fragment == null) {
            fragment = ForgetPasswordFragment.newInstance()
           //commaned by surya

            fragmentTransaction.add(
                R.id.containerLanguageFragment,
                fragment,
                ForgetPasswordFragment.TAG
            )

        } else {
            fragmentTransaction.show(fragment)
        }

        if (myactive != null) fragmentTransaction.hide(myactive as Fragment)

        fragmentTransaction.commit()
        img_back.visibility=View.GONE
        myactive = fragment

    }

    private fun startRazorPayment() {

        val checkout = Checkout()
        var email= viewModel.getEmail()
        var mobileNumber= viewModel.getMobileNumber()

        /*taxideals key id -- razor pay
        checkout.setKeyID("rzp_live_BHeKCqZ5NIMm64")*/


        val activity: Activity = this
        try {
            val options = JSONObject()
            options.put("name", "Taxi deals")
            options.put("description", "Reference No. #123456")
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")

//            options.put("order_id", "order_DBJOWzybf0sJbb");//from response of step 3.
            options.put("theme.color", "#3399cc")
            options.put("currency", "INR")
            options.put("amount", "100") //pass amount in currency subunits
            options.put("prefill.email", email)
            options.put("prefill.contact", mobileNumber)

            /*JSONObject retryObj = new JSONObject();
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);*/
            checkout.open(activity, options)
        } catch (e: Exception) {
            Log.e("TAG", "Error in starting Razorpay Checkout", e)
        }
    }
    fun isConnectionAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val netInfo = connectivityManager.activeNetworkInfo
            if (netInfo != null && netInfo.isConnected
                    && netInfo.isConnectedOrConnecting
                    && netInfo.isAvailable) {
                return true
            }
        }
        return false
    }
    private fun upiPaymentDataOperation(data: ArrayList<String>) {
        if (isConnectionAvailable(this)) {
            val str = data[0]
            Log.e("UPIPAY", "upiPaymentDataOperation: $str")
            var paymentCancel = ""
            var status = ""
            var approvalRefNo = ""
            val response = str.split("&".toRegex()).toTypedArray()
            for (i in response.indices) {
                val equalStr = response[i].split("=".toRegex()).toTypedArray()
                if (equalStr.size >= 2) {
                    if (equalStr[0].equals("Status", ignoreCase = true)) {
                        status = equalStr[1].toLowerCase(Locale.ROOT)
                    } else if (equalStr[0].equals("ApprovalRefNo", ignoreCase = true) || equalStr[0].equals("txnRef", ignoreCase = true)) {
                        approvalRefNo = equalStr[1]
                    }
                } else {
                    paymentCancel = "Payment cancelled by user."
                }
            }
            if (status == "success") {
                //Code to handle successful transaction here.
                Toast.makeText(this, "Transaction successful.", Toast.LENGTH_SHORT).show()
                Log.e("UPI", "payment successfull: $approvalRefNo")
            } else if ("Payment cancelled by user." == paymentCancel) {
                Toast.makeText(this, "Payment cancelled by user.", Toast.LENGTH_SHORT).show()
                Log.e("UPI", "Cancelled by user: $approvalRefNo")
            } else {
                Toast.makeText(this, "Transaction failed.Please try again", Toast.LENGTH_SHORT).show()
                Log.e("UPI", "failed payment: $approvalRefNo")
            }
        } else {
            Log.e("UPI", "Internet issue: ")
            Toast.makeText(this, "Internet connection is not available. Please check and try again", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            UPI_PAYMENT ->
                if (RESULT_OK == resultCode || resultCode == 11) {
                    if (data != null) {
                        val trxt = data.getStringExtra("response")
                        Log.e("UPI", "onActivityResult: $trxt")
                        val dataList = ArrayList<String>()
                        if (trxt != null) {
                            dataList.add(trxt)
                        }
                        upiPaymentDataOperation(dataList)
                    } else {
                        Log.e("UPI", "onActivityResult: " + "Return data is null")
                        val dataList = ArrayList<String>()
                        dataList.add("nothing")
                        upiPaymentDataOperation(dataList)
                    }
                } else {
                    //when user simply back without payment
                    Log.e("UPI", "onActivityResult: " + "Return data is null")
                    val dataList = ArrayList<String>()
                    dataList.add("nothing")
                    upiPaymentDataOperation(dataList)
                }
        }
    }

    override fun onPaymentSuccess(razorpayPaymentID: String?) {
        try {
            Toast.makeText(this, "Payment Successful: $razorpayPaymentID", Toast.LENGTH_SHORT).show()
        } catch (e: java.lang.Exception) {
            Toast.makeText(this, "Exception in onPaymentSuccess: ${e.message}", Toast.LENGTH_SHORT).show()
            Log.e("CheckoutAct", "Exception in onPaymentSuccess", e)
        }
    }

    override fun onPaymentError(code: Int, response: String?) {
        try {
            Toast.makeText(this, "Payment failed: $code  $response ", Toast.LENGTH_SHORT).show()
        } catch (e: java.lang.Exception) {
            Toast.makeText(this, "Exception in onPaymentSuccess: ${e.message}   ", Toast.LENGTH_SHORT).show()
            Log.e("CheckoutAct", "Exception in onPaymentError", e)
        }
    }


}
