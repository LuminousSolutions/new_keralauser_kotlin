package com.mongokerala.taxi.newuser.utils.common

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.annotation.NonNull
import com.mongokerala.taxi.newuser.R

class DialogUtils{

    private fun DialogUtils() { // This utility class is not publicly instantiable
    }

    @NonNull
    fun showLoadingDialog(
        context: Context?,
        isCancelable: Boolean
    ): ProgressDialog? {
        val progressDialog = ProgressDialog(context)
        progressDialog.show()
        if (progressDialog.window != null) {
            progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        progressDialog.setContentView(R.layout.progress_dialog)
        progressDialog.isIndeterminate = true
        progressDialog.setCancelable(true)
        progressDialog.setCanceledOnTouchOutside(isCancelable)
        return progressDialog
    }


}