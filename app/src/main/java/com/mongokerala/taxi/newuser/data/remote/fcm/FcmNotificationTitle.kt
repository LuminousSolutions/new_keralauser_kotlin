package com.mongokerala.taxi.newuser.data.remote.fcm

import androidx.annotation.NonNull


class FcmNotificationTitle {
    @NonNull
    var title = "Taxi Deals"
    var text = "Action Message"

    @NonNull
    var sound = "default"

    fun setMessageText(message: String) {
        text = message
    }
}
