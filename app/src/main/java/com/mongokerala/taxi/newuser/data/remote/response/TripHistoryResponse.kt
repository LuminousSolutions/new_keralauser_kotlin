package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class TripHistoryResponse(
    @SerializedName("statusCode") val statusCode : Int,
    @SerializedName("status") val status : Boolean,
    @SerializedName("data") val data : List<TripHistoryData>,
    @SerializedName("infoId") val infoId : Int
)