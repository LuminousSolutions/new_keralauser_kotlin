package com.mongokerala.taxi.newuser.utils.common

import android.content.Context
import android.net.ConnectivityManager

object NetworkState {
    const val CONNECT_TO_WIFI = "WIFI"
    const val CONNECT_TO_MOBILE = "MOBILE"
    const val NOT_CONNECT = "NOT_CONNECT"
    const val CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE"
    var TYPE_WIFI = 1
    var TYPE_MOBILE = 2
    var TYPE_NOT_CONNECTED = 0
    fun getConnectivityStatus(context: Context): Int {
        val cm = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        if (null != activeNetwork) {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) return TYPE_WIFI
            if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) return TYPE_MOBILE
        }
        return TYPE_NOT_CONNECTED
    }

    fun getConnectivityStatusString(context: Context): String? {
        val conn = getConnectivityStatus(context)
        var status: String? = null
        if (conn == TYPE_WIFI) {
            //status = "Wifi enabled";
            status = CONNECT_TO_WIFI
        } else if (conn == TYPE_MOBILE) {
            //status = "Mobile data enabled";
            status = getNetworkClass(context)
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = NOT_CONNECT
        }
        return status
    }

    fun getNetworkClass(context: Context): String {
        val cm =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = cm.activeNetworkInfo
        if (info == null || !info.isConnected) return "-" //not connected
        if (info.type == ConnectivityManager.TYPE_WIFI) return "WIFI"
        return if (info.type == ConnectivityManager.TYPE_MOBILE) {
            "Mobile"
        } else "?"
    }
}
