package com.mongokerala.taxi.newuser.paymentgateway.enums;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public enum  OrderRequestType {

    ORDER_CREATE_AND_PAYMENT_UPDATE, ORDER_CREATE, PAYMENT_UPDATE, RE_ATTEMPT_PAYMENT

}
