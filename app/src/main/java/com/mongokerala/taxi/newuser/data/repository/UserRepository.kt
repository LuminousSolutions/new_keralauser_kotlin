package com.mongokerala.taxi.newuser.data.repository

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.mongokerala.taxi.newuser.data.local.db.DatabaseService
import com.mongokerala.taxi.newuser.data.local.prefs.UserPreferences
import com.mongokerala.taxi.newuser.data.model.Base
import com.mongokerala.taxi.newuser.data.model.BaseRideResponse
import com.mongokerala.taxi.newuser.data.model.User
import com.mongokerala.taxi.newuser.data.remote.NetworkService
import com.mongokerala.taxi.newuser.data.remote.fcm.FcmNotificationDto
import com.mongokerala.taxi.newuser.data.remote.fcm.FcmNotificationResponse
import com.mongokerala.taxi.newuser.data.remote.request.*
import com.mongokerala.taxi.newuser.data.remote.response.*
import com.mongokerala.taxi.newuser.ui.main_new.AdvanceSearchResponse
import io.reactivex.Single
import org.json.JSONException
import org.json.JSONObject
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class UserRepository @Inject constructor(
    private val networkService: NetworkService,
    private val databaseService: DatabaseService,
    private val userPreferences: UserPreferences
) {

    fun saveCurrentUser(loginresponse: LoginResponse) {
        userPreferences.setUserId(loginresponse.data.userId)
        userPreferences.setUserName(loginresponse.data.name)
        userPreferences.setUserEmail(loginresponse.data.email)
        loginresponse.jwt?.let { userPreferences.setAccessToken(it) }
        userPreferences.setLan(loginresponse.data.lan)
        userPreferences.setSupplierId(loginresponse.data.supplierId)
        userPreferences.setIsApproved(loginresponse.data.isApproved)
        userPreferences.setType(loginresponse.data.type)
    }

    /**
     * Sudhanshu Mani Tripathi
     * call search taxi api
     */




    fun doSearchTaxiRequest(requestData: AdvanceSeachApiRequest): Single<AdvanceSearchResponse> =
            networkService.doSearchTaxi(requestData)
                    .map {
                        AdvanceSearchResponse(
                                it.infoId,
                                it.data,
                                it.jwt,
                                it.message,
                                it.statusCode

                        )
                    }

    fun doShowTaxiRequest(requestData: AdvanceSeachApiRequest): Single<AdvanceSearchResponse> =
        networkService.doShowTaxi(requestData)
            .map {
                AdvanceSearchResponse(
                    it.infoId,
                    it.data,
                    it.jwt,
                    it.message,
                    it.statusCode

                )
            }

    fun doBookingTaxiRequest(requestData: BookingRequest): Single<BookingResponse> =
            networkService.doBookingTaxi(requestData)
                    .map {
                        BookingResponse(
                                it.infoId,
                                it.data,
                                it.jwt,
                                it.message,
                                it.statusCode
                        )
                    }

    fun doCouponBookingTaxiRequest(couponRequestData: CouponBookingRequest): Single<BookingResponse> =
        networkService.doCouponBookingTaxi(couponRequestData)
            .map {
                BookingResponse(
                    it.infoId,
                    it.data,
                    it.jwt,
                    it.message,
                    it.statusCode
                )
            }

    fun removeCurrentUser() {
        userPreferences.removeUserId()
        userPreferences.removeUserName()
        userPreferences.removeUserEmail()
        userPreferences.removeAccessToken()
    }

    fun saveClientMobileNumber(client_mobile_number: String) {
        userPreferences.setClientMobileNumber(client_mobile_number)
    }

    fun getClientMobileNUmber(): String? {
        return userPreferences.getClientMobileNumber()
    }

    fun getDriverMobileNUmber(): String? {
        return userPreferences.getDriverMobileNumber()
    }
    fun saveDriverMobileNumber(driver_mobile_number: String) {
        userPreferences.setDriverMobileNumber(driver_mobile_number)
    }

    fun saveBaseKm(baseKm: String?) {
        if (baseKm != null) {
            userPreferences.setBaseKm(baseKm)
        }
    }

    fun getbaseKm(): String? {
        return userPreferences.getbaseKm()
    }
    fun saveOfflineBooking(offline_booking: Boolean) {
        userPreferences.set_is_Offline_Booking(offline_booking)
    }

    fun getOfflineBooking(): Boolean? {
        return userPreferences.get_is_Offline_Booking()
    }

    fun saveStopButtonClicked(stop_buton: Boolean) {
        userPreferences.setStopButtonClicked(stop_buton)
    }

    fun getStopButtonClicked(): Boolean? {
        return userPreferences.getStopButtonClicked()
    }

    fun saveCarType(car_type: String) {
        userPreferences.setCarType(car_type)
    }

    fun getCarType(): String? {
        return userPreferences.getCarType()
    }

    fun saveSource(source_address: String) {
        userPreferences.setSource(source_address)
    }

    fun getSource(): String? {
        return userPreferences.getSource()
    }

    fun saveDestination(destination_address: String) {
        userPreferences.setDestination(destination_address)
    }

    fun getDestination(): String? {
        return userPreferences.getDestination()
    }

    fun savePaymentype(paytype: String) {
        userPreferences.setPaymentType(paytype)
    }

    fun getPaymentype(): String? {
        return userPreferences.getPaymentType()
    }

    fun setEND_TIME(END_TIME: String?) {
        userPreferences.setEND_TIME(END_TIME)
    }

    fun getEND_TIME(): String? {
        return userPreferences.getEND_TIME()
    }

    fun setApxGoogleKm(apxGoogleKm: String?) {
        userPreferences.setApxGoogleKm(apxGoogleKm)
    }

    fun getApxGoogleKm(): String? {
        return userPreferences.getApxGoogleKm()
    }

    fun setApxGoogleKmDriver(apxGoogleKmdriver: String?) {
        userPreferences.setApxGoogleKmDriver(apxGoogleKmdriver)
    }

    fun getApxGoogleKmDriver(): String? {
        return userPreferences.getApxGoogleKmDriver()
    }

    fun setRide_ID(ride_id: String?) {
        userPreferences.setRide_ID(ride_id)
    }

    fun getRide_Id(): String? {
        return userPreferences.getRide_Id()
    }

    fun set_Destination_Place_id(destination_place_id: String?) {
        userPreferences.set_Destination_Place_id(destination_place_id)
    }

    fun get_Destination_Place_id(): String? {
        return userPreferences.get_Destination_Place_id()
    }

    fun set_Source_Place_id(source_place_id: String?) {
        userPreferences.set_Source_Place_id(source_place_id)
    }

    fun get_Source_Place_id(): String? {
        return userPreferences.get_Source_Place_id()
    }


    fun setClient_FCM_Token(client_fcm_token: String?) {
        userPreferences.setClient_FCM_Token(client_fcm_token)
    }

    fun getClient_FCM_Token(): String? {
        return userPreferences.getClient_FCM_Token()
    }

    fun set_START_TIME(START_TIME: String?) {
        userPreferences.set_START_TIME(START_TIME)
    }

    fun getSTART_TIME(): String? {
        return userPreferences.getSTART_TIME()
    }

    fun setDriverRideStartLatLng(latitude: Double, longitude: Double) {
        userPreferences.setDriverRideStartLatLng(latitude, longitude)
    }

    fun getDriverRideStartlatitude(): Double? {
        return userPreferences.getDriverRideStartlatitude()
    }

    fun getDriverRideStartLongitude(): Double? {
        return userPreferences.getDriverRideStartLongitude()
    }

    fun saveVehicle_seats(vehicle_seats: String) {
        userPreferences.setVehicleSeats(vehicle_seats)
    }

    fun getVehicle_seats(): String? {
        return userPreferences.getVehicleSeats()
    }

    fun saveVehicle_number(vehicle_number: String) {
        userPreferences.setTaxiNumber(vehicle_number)
    }

    fun getVehiclenumber(): String? {
        return userPreferences.getTaxiNumber()
    }

    fun setCoupon(Coupon: String?) {
        userPreferences.setCoupon(Coupon)
    }

    fun getCoupon(): String? {
        return userPreferences.getCoupon()
    }


    fun saveLan(lan: String) {
        userPreferences.setLan(lan)
    }

    fun getLan(): String? {
        return userPreferences.getLan()
    }

    fun setUserTotalPrice(user_total_price: String) {
        userPreferences.setUserTotalOrice(user_total_price)
    }

    fun getUserTotalPrice(): String? {
        return userPreferences.getUserTotalPrice()
    }

    fun saveApproved(isApproved: String) {
        userPreferences.setIsApproved(isApproved)
    }

    fun getApproved(): String? {
        return userPreferences.getIsApproved()
    }

    fun saveVehicleBrand(vehicle_brand: String) {
        userPreferences.setVehicleBrand(vehicle_brand)
    }

    fun getVehicleBrand(): String? {
        return userPreferences.getVehicleBrand()
    }

    fun saveVehicleYear(vehicle_year: String) {
        userPreferences.setVehicleYear(vehicle_year)
    }

    fun getVehicleYear(): String? {
        return userPreferences.getVehicleYear()
    }

    fun setCategory(category: String?) {
        userPreferences.setCategory(category)
    }

    fun getCategory(): String? {
        return userPreferences.getCategory()
    }

//    fun set_Sou_PlaceID(des_place_id: String?)
//    fun get_Sou_PlaceID(): String?

    fun set_Sou_PlaceID(des_place_id: String?) {
        userPreferences.set_Sou_PlaceID(des_place_id)
    }

    fun get_Sou_PlaceID(): String? {
        return userPreferences.get_Sou_PlaceID()
    }

    fun set_Total_Km(total_km: String?) {
        userPreferences.set_Total_Km(total_km)
    }

    fun get_Total_Km(): String? {
        return userPreferences.get_Total_Km()
    }

    fun set_LocABTotal_Km(locabtotal_km: String?) {
        userPreferences.set_LocABTotal_Km(locabtotal_km)
    }

    fun get_LocABTotal_Km(): String? {
        return userPreferences.get_LocABTotal_Km()
    }


    fun saveMobileNumber(mobile_number: String) {
        userPreferences.setMobileNumber(mobile_number)
    }

    fun getMobileNUmber(): String? {
        return userPreferences.getMobileNumber()
    }

    fun saveCurrentStatus(current_status: String) {
        userPreferences.setCurrentStatus(current_status)
    }

    fun getCurrentStatus(): String? {
        return userPreferences.getCurrentStatus()
    }

    fun saveLocalStatus(current_status: String) {
        userPreferences.setLocalStatus(current_status)
    }

    fun getLocalStatus(): String? {
        return userPreferences.getLocalStatus()
    }


    fun saveAnotherDriverAccepted(anotherdriveraccpted: String) {
        userPreferences.saveAnotherDriverAccepted(anotherdriveraccpted)
    }

    fun getAnotherDriverAccepted(): String? {
        return userPreferences.getAnotherDriverAccepted()
    }


    fun saveDriverStatusCurrentStatus(dr_current_status: String) {
        userPreferences.setDriverCurrentStatus(dr_current_status)
    }

    fun getDriverStatusCurrentStatus(): String? {
        return userPreferences.getDriverCurrentStatus()
    }

    fun saveToken(token: String) {
        userPreferences.setToken(token)
    }

    fun getToken(): String? {
        return userPreferences.getToken()
    }

    fun setDriverCurrentLatLng(latitude: Double, longitude: Double) {
        userPreferences.setDriverCurrentLatLng(latitude, longitude)
    }
    fun setUserCurrentLatLng(latitude: Double, longitude: Double) {
        userPreferences.setUserCurrentLatLng(latitude, longitude)
    }

    fun getDriverCurrentlatitude(): Double? {
        return userPreferences.getDriverCurrentlatitude()
    }

    fun getDriverCurrentLongitude(): Double? {
        return userPreferences.getDriverCurrentLongitude()
    }


    fun getUserCurrentlatitude(): Double? {
        return userPreferences.getUserCurrentlatitude()
    }

    fun getUserCurrentLongitude(): Double? {
        return userPreferences.getUserCurrentLongitude()
    }

    fun saveName(name: String) {
        userPreferences.setName(name)
    }

    fun getName(): String? {
        return userPreferences.getName()
    }

    fun saveClientName(client_name: String) {
        userPreferences.setClientName(client_name)
    }

    fun getClientName(): String? {
        return userPreferences.getClientName()
    }

    fun saveEmail(email: String) {
        userPreferences.setEmail(email)
    }

    fun getEmail(): String? {
        return userPreferences.getEmail()
    }


    fun saveDomain(domain: String) {
        userPreferences.setDomain(domain)
    }

    fun getDomain(): String? {
        return userPreferences.getDomain()
    }

    fun saveWebsite(website: String) {
        userPreferences.setWebsite(website)
    }

    fun getWebsite(): String? {
        return userPreferences.getWebsitae()
    }


    fun saveBackendBookingtoken(token: String) {
        userPreferences.setBackendBookingToken(token)
    }

    fun getBackendBookingToken(): String? {
        return userPreferences.getBackendBookingToken()
    }

    fun saveTaxiId(taxi_id: String) {
        userPreferences.setTaxiId(taxi_id)
    }

    fun getTaxiId(): String? {
        return userPreferences.getTaxiId()
    }

    fun setImageUrl(imageUrl: ImageInfo?) {
        userPreferences.setImageUrl(imageUrl!!)
    }

    fun getImageUrl(): ImageInfo? {
        return userPreferences.getImageUrl()
    }


    fun saveSupplierId(supplierId: String) {
        userPreferences.setSupplierId(supplierId)
    }

    fun getSupplierId(): String? {
        return userPreferences.getSupplierId()
    }

    fun setClient_ID(clientId: String?) {
        userPreferences.setClient_ID(clientId)
    }

    fun getClient_Id(): String? {
        return userPreferences.getClient_Id()
    }

    fun setIs_Android(is_android: Boolean) {
        userPreferences.setIs_Android(is_android)
    }

    fun getIsAndroid(): Boolean {
        return userPreferences.getIsAndroid()
    }

    fun setStartride(start_ride: Boolean) {
        userPreferences.setStartRide(start_ride)
    }

    fun getStartride(): Boolean {
        return userPreferences.getStartride()
    }

    fun setFirstLogin(first_login: Boolean) {
        userPreferences.setFirstLogin(first_login)
    }

    fun getFirstLogin(): Boolean {
        return userPreferences.getFirstLogin()
    }

    fun setRIDE_KEY_LOCAL(RIDE_KEY_LOCAL: String?) {
        userPreferences.setRIDE_KEY_LOCAL(RIDE_KEY_LOCAL)
    }

    fun getRIDE_KEY_LOCAL(): String? {
        return userPreferences.getRIDE_KEY_LOCAL()
    }


    fun set_is_On_Booking(is_online_booking: Boolean) {
        userPreferences.set_is_On_Booking(is_online_booking)
    }

    fun get_is_On_Booking(): Boolean? {
        return userPreferences.get_is_On_Booking()
    }


    fun saveOneSignalValue(oneSignalValue: String) {
        userPreferences.setOneSignalValue(oneSignalValue)
    }

    fun getOneSignalValue(): String? {
        return userPreferences.getOneSignalValue()
    }

    fun getCurrentUserID(): String? {
        return userPreferences.getUserId()
    }

    fun getValues(): String? {
        return userPreferences.getValues()
    }

    fun saveCurrentUserID(userId: String) {
        userPreferences.setUserId(userId)
    }
   
    fun set_type_Taxi4(taxi4: String) {
        userPreferences.set_type_Taxi4(taxi4)
    }

    fun get_type_Taxi4(): String? {
        return userPreferences.get_type_Taxi4()
    }

    fun set_type_Taxi(taxi: String) {
        userPreferences.set_type_Taxi(taxi)
    }

    fun get_type_Taxi(): String? {
        return userPreferences.get_type_Taxi()
    }

    fun set_type_Taxi6(taxi6: String) {
        userPreferences.set_type_Taxi6(taxi6)
    }

    fun get_type_Taxi6(): String? {
        return userPreferences.get_type_Taxi6()
    }

    fun set_type_Transport(transport: String) {
        userPreferences.set_type_Transport(transport)
    }

    fun get_type_Transport(): String? {
        return get_type_Transport()
    }

    fun set_type_Ambulance(Ambulance: String) {
        userPreferences.set_type_Ambulance(Ambulance)
    }

    fun get_type_Ambulance(): String? {
        return get_type_Ambulance()
    }

    fun setAccessToken(access_token: String) {
        userPreferences.setAccessToken(access_token)
    }

    fun getAccessToken(): String? {
        return userPreferences.getAccessToken()
    }

    fun setUser_FCM_Token(FCM_Token: String?) {
        userPreferences.setUser_FCM_Token(FCM_Token)
    }

    fun getUser_FCM_Token(): String? {
        return userPreferences.getUser_FCM_Token()
    }

    fun getLoginstatus(): String? {
        return userPreferences.getLoginStatus()
    }
    fun getFbLoginstatus(): String? {
        return userPreferences.getFbLoginstatus()
    }
    fun saveLoginStatus(login_status: String){
        userPreferences.setLoginStatus(login_status)
    }
    fun saveFbLoginStatus(login_status: String){
        userPreferences.saveFbLoginStatus(login_status)
    }
    fun saveFbprofileImage(profile_image:String){
        userPreferences.saveFbprofileImage(profile_image)
    }
    fun getFbprofileImage() : String?{
        return userPreferences.getFbprofileImage()

    }

    fun getFbImageUrl(): String?{
        return userPreferences.getFbImageUrl()
    }

    fun saveFbImageUrl(imageurl:String){
        userPreferences.saveFbImageUrl(imageurl)
    }

    fun saveSearchDestinationLatLang(latitude: Double, longitude: Double) {
        userPreferences.saveSearchDestinationLatLang(latitude, longitude)
    }
    fun getSearchDestinationLatitude(): Double{
        return userPreferences.getSearchDestinationLatitude()
    }
    fun getSearchDestinationLangitude(): Double{
        return userPreferences.getSearchDestinationLangitude()
    }
    fun saveUserSearchLatLng(latitude: Double, longitude: Double) {
        userPreferences.saveUserSearchLatLng(latitude, longitude)
    }
    fun getUserSearchLatitude(): Double{
        return userPreferences.getUserSearchLatitude()
    }
    fun getUserSearchLangitude(): Double{
        return userPreferences.getUserSearchLangitude()
    }

    fun getCurrentUser(): User? {

        val userId = userPreferences.getUserId()
        val userName = userPreferences.getUserName()
        val userEmail = userPreferences.getUserEmail()
        val accessToken = userPreferences.getAccessToken()
        val taxiId = userPreferences.getTaxiId()
        val oneSignalValue = userPreferences.getOneSignalValue()
        val supplierId = userPreferences.getSupplierId()
        val lan = userPreferences.getLan()
        val isApproved = userPreferences.getIsApproved()
        val type = userPreferences.getType()

        return if (userId !== null && userName != null && userEmail != null && accessToken != null
                && taxiId != null && oneSignalValue != null && lan != null && isApproved != null
                && supplierId != null && type != null)
            User(
                userId, userName, userEmail, taxiId, supplierId, lan,
                isApproved, type, oneSignalValue
            )
        else
            null
    }

        fun doUserLogin(email: String, password: String, phoneNumber: String): Single<LoginResponse> =
            networkService.doLoginCall(LoginRequest(email, password, phoneNumber))
                    .map {
                        LoginResponse(
                            it.statusCode,
                            it.status,
                            it.message,
                            it.data,
                            it.jwt,
                            it.infoId
                        )
                    }

    fun doUpdateReview(
        userId: String, taxiId: String, rating: String, driver_id: String, rideId: String,
        postedBy: String
    ): Single<Base> =
            networkService.doUpdateReview(
                ReviewRequest(
                    userId,
                    taxiId,
                    rating,
                    driver_id,
                    rideId,
                    postedBy
                )
            )
                    .map {
                        Base(
                            it.statusCode,
                            it.status,
                            it.message,
                            it.infoId
                        )
                    }

    fun doVehicleInfoUpdate(
        carType: String, seats: String, taxiId: String, taxiNumber: String,
        userId: String, supplierId: String, vehicleBrand: String, vehicleYear: String
    ): Single<Base> =
            networkService.doVehicleInfoUpdateCall(
                VehicleInfoUpdateRequest(
                    carType, seats, taxiId, taxiNumber, userId,
                    supplierId, vehicleBrand, vehicleYear
                )
            )
                    .map {
                        Base(
                            it.statusCode,
                            it.status,
                            it.message,
                            it.infoId
                        )
                    }

    /*  fun doProfileImage(driver_name: String, image: String, type: String, userId: String): Single<Base> =
          networkService.doProfileImageCall(ProfileImageRequest(driver_name,image,type,userId))
              .map {
                  Base(
                      it.statusCode,
                      it.status,
                      it.message,
                      it.infoId
                  )
              }
  */
    fun doProfileEdit(id: String, firstName: String, email: String, phoneNumber:String, domain: String, website: String): Single<BaseResponse> =
            networkService.doProfileEditCall(ProfileEditRequest(id, firstName, email,phoneNumber, domain, website))
////    fun doProfileEdit(id: String, firstName: String, email: String, domain: String, website: String): Single<Base> =
////            networkService.doProfileEditCall(
////                ProfileEditRequest(
////                    id,
////                    firstName,
////                    email,
////                    domain,
////                    website
////                )
////            )
//                    .map {
//                        Base(
//                            it.statusCode,
//                            it.status,
//                            it.message,
//                            it.infoId
//                        )
//                    }



    fun doFcmRequest(fcmNotificationDto: FcmNotificationDto): Single<FcmNotificationResponse> {
        val gson = Gson()
        var obj = gson.toJson(fcmNotificationDto)
        var jsonObject: JSONObject? = null
        jsonObject = JSONObject(obj)
        val objfinal: JsonObject = JsonParser().parse(obj).asJsonObject
        try {


            /* jsonObject.getJSONObject("data").remove("")
             val obj = JSONObject()
             obj.put("nameValuePairs", nameValuePairs.getText().toString().trim())*/
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return networkService.doFcmRequest(objfinal)
                .map {
                    FcmNotificationResponse(
                            it.multicastId,
                            it.success,
                            it.failure,
                            it.canonicalIds,
                            it.results
                    )
                }
    }


    fun doUserRegistration(email: String, firstname: String, password: String, phonenumber: String
    ): Single<Base> =
            networkService.doUserRegistrationCall(SignupRequest(email, firstname, password, phonenumber))
                    .map {
                        Base(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.infoId
                        )
                    }

    fun doFeedback(message: String, name: String, phone_number: String, sender_email: String, taxiId: String): Single<BaseResponse> =
        networkService.doFeedbackCall(FeedbackRequest(message, name, phone_number, sender_email, taxiId))
            .map {
                BaseResponse(
                        it.statusCode,
                        it.status,
                        it.message,
                        it.data,
                        it.jwt,
                        it.infoId
                )
            }
    fun doRateusback(message: String, name: String, phone_number: String, sender_email: String, taxi_id: String): Single<Base> =
            networkService.doRateUsCall(FeedbackRequest(message, name, phone_number, sender_email, taxi_id))
                    .map {
                        Base(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.infoId
                        )
                    }

    fun doUserOtpRequest(country_code: String, mobile_number: String): Single<Base> =
            networkService.doOtpRequestCall(OtpRequest(country_code, mobile_number))
                    .map {
                        Base(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.infoId
                        )
                    }

    fun doForgetUserOtpRequest(country_code: String, mobile_number: String): Single<Base> =
            networkService.doForgetPasswordOtpRequestCall(ForgetPasswordOtpRequest(country_code, mobile_number))
                    .map {
                        Base(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.infoId
                        )
                    }

    fun doForgetUserOtpValidationRequest(country_code: String, mobile_number: String, otp: String, new_password: String): Single<Base> =
            networkService.doForgetPasswordOtpValidationRequestCall(ForgetPasswordOtpValidationRequest(country_code, mobile_number, otp, new_password))
                    .map {
                        Base(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.infoId
                        )
                    }

    fun doUserdetailRequest(userId: String): Single<UserdetailResponse> =
            networkService.doUserdetailCall(userId)
                    .map {
                        UserdetailResponse(
                                it.statusCode,
                                it.status,
                                it.data,
                                it.infoId
                        )
                    }

    fun doCheckUserImage(userId: String): Single<UserDetailImageResponse> =
            networkService.doCheckUserImage(userId)
                    .map {
                        UserDetailImageResponse(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.data,
                                it.jwt,
                                it.infoId
                        )
                    }


    fun doUserwalletBalanceRequest(userId: String): Single<UserWalletBalanceResponce> =
        networkService.doUserwalletBalance(userId)
            .map {
                UserWalletBalanceResponce(
                    it.statusCode,
                    it.status,
                    it.message,
                    it.data,
                    it.jwt,
                    it.infoId
                )
            }

    fun addWalletAmount(balance:Int,token: String,userId: String): Single<AddWalletAmountResponse> =
            networkService.addWalletAmount(AddWalletAmountRequest(balance,token,userId))
                    .map {
                        AddWalletAmountResponse(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.data,
                                it.jwt,
                                it.infoId
                        )
                    }


    fun doUserwalletTokenRequest(token:String): Single<UserWalletTokenResponce> =
        networkService.doUserwalletTokencheck(token)
            .map {
                UserWalletTokenResponce(
                    it.statusCode,
                    it.status,
                    it.message,
                    it.data,
                    it.jwt,
                    it.infoId
                )
            }
    fun doTripHistoryRequest(userId: String): Single<TripHistoryResponse> =
            networkService.doTripHistoryCall(userId)
                    .map {
                        TripHistoryResponse(
                                it.statusCode,
                                it.status,
                                it.data,
                                it.infoId
                        )
                    }

    fun doLoginStatusRequest(userId: String, loginStatus: String): Single<BaseResponse> =
            networkService.doLoginStatusCall(userId, loginStatus)
                    .map {
                        BaseResponse(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.data,
                                it.jwt,
                                it.infoId
                        )
                    }
    fun doReviewStatus(cmd: String?, review: Double){

    }

    fun doLanguageUpdateRequest(userId: String, language: String): Single<BaseResponse> =
            networkService.doLanguageUpdateCall(userId, language)
                    .map {
                        BaseResponse(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.data,
                                it.jwt,
                                it.infoId
                        )
                    }

    fun doStatusUpdate(taxiId: String, userId: String, latitude: Double, longitude: Double, driverStatus: String): Single<BaseResponse> =
            networkService.doStatusUpdateCall(taxiId, userId, latitude, longitude, driverStatus)
                    .map {
                        BaseResponse(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.data,
                                it.jwt,
                                it.infoId
                        )
                    }

//    fun updatePriceAndDistance(distance: Double, googleKm: Double, latitude: Double, longitude: Double,
//                               region: String, rideId: String, sourceLatitude: Double, sourceLongitude: Double,
//                               status: String, waitingTime: String, type: String, domain: String,
//                               travelTime: String, category: String): Single<UpdatePriceResponse> =
//            networkService.updatePriceAndDistance(PriceUpdate(distance, googleKm, latitude, longitude, region, rideId, sourceLatitude,
//                    sourceLongitude, status, waitingTime, type, domain, travelTime, category))
//                    .map {
//                        UpdatePriceResponse(
//                                it.statusCode,
//                                it.status,
//                                it.message,
//                                it.data,
//                                it.jwt,
//                                it.infoId
//                        )
//                    }

    fun doFcm_update(id: String, latitude: Double, longitude: Double, token: String, values: String, notification_token: String): Single<BaseResponsenew> =
            networkService.doFcmCall(FcmUpdateRequest(id, latitude, longitude, token, values, notification_token))
                    .map {
                        BaseResponsenew(
                                it.statusCode,
                                it.status,
                                it.jwt,
                                it.infoId
                        )
                    }

    fun doTaxidetailRequest(taxiId: String): Single<Taxi_detail> =
            networkService.doTaxidetailCall(taxiId)
                    .map {
                        Taxi_detail(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.data,
                                it.infoId
                        )
                    }

    fun doInvoiceRequest(rideId: String): Single<InvoiceResponse> =
            networkService.doTripVoiceCall(rideId)
                    .map {
                        it.jwt?.let { it1 ->
                            InvoiceResponse(
                                    it.statusCode,
                                    it.status,
                                    it.message,
                                    it.data,
                                    it.infoId,
                                    it1
                            )
                        }
                    }


    fun doValidateOtpRequest(token: String, c_mobile_number: String): Single<Base> =
            networkService.doValidateOtpRequest(ValidateOtpRequest(token, c_mobile_number))
                    .map {
                        Base(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.infoId
                        )
                    }

    fun doRideValidateOtpRequest(ctrycode: String, user_id: String, mob_number: String, token: String): Single<Base> =
            networkService.doRideValidateOtpRequest(RideValidateOtpRequest(ctrycode, user_id, mob_number, token))
                    .map {
                        Base(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.infoId
                        )
                    }

    /*
        fun doRideCreateRequest(driver_id: String,ride_status: String,source: String,destination: String,
                                userId: String,payment: String,discount: String,category: String,km: String): Single<BaseRideResponse> =
            networkService.doRideCreate(RideCreateRequest(driver_id,ride_status,source,destination,
                userId,payment,discount,category,km))
                .map {
                    BaseRideResponse(
                        it.statusCode,
                        it.status,
                        it.message,
                        it.infoId,
                        it.data
                    )
                }*/
    fun doRideCreateRequest(driver_id: String, ride_status: String, source: String, destination: String,
                            userId: String, payment: String): Single<BaseResponse> =
            networkService.doRideCreate(RideDummyCreateRequest(driver_id, ride_status, source, destination, userId, payment))
                    .map {
                        BaseResponse(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.data,
                                 it.jwt,
                                it.infoId

                        )
                    }

    fun doRideStatusCreateRequest(driver_id: String,ride_status: String,source: String,destination: String,
                                  userId: String,payment: String,discount: String,category: String,km: String,reasonmsg:String): Single<BaseRideResponse> =
            networkService.doRideStatusReason(RideStatusCreateRequest(driver_id,reasonmsg,ride_status,source,destination, userId,payment,category))
                    .map {
                        BaseRideResponse(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.infoId,
                                it.data
                        )
                    }


    fun doRideOtpSmsRequestToUser(country_code: String, userId: String): Single<Base> =
            networkService.doRideOtpSmsRequestToUser(RideOtpSmsRequestToUser(country_code, userId))
                    .map {
                        Base(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.infoId
                        )
                    }

    fun doUserOfflineSmsRequestToUser(country_code: String, driveridvaluem: String, drivername: String, sourceaddresss: String, useridvalue: String, destinationaddreess: String, tot_price: String, vehnumbere: String, dr_phnumber: String, contactvalue: String, categoryvalue: String, user_name: String, km_value: String, userPhnumber: String): Single<Base> =
            networkService.doUserOfflineSmsRequestToUser(OfflineRideSmsToUserRequest(country_code, userPhnumber,
                    sourceaddresss, useridvalue, destinationaddreess, driveridvaluem, drivername, vehnumbere, tot_price, dr_phnumber,
                    contactvalue, categoryvalue, user_name, km_value))
                    .map {
                        Base(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.infoId
                        )
                    }


    fun doOfflineBooking(name: String, phone_number: String, source: String, destination: String, driver_id: String, emailid: String): Single<OfflineBookingResponse> =
            networkService.doOffineBooking(OfflineBookingRequest(name, phone_number, source, destination, driver_id, emailid))
                    .map {
                        OfflineBookingResponse(
                                it.statusCode,
                                it.status,
                                it.data,
                                it.infoId
                        )
                    }


    fun doPriceCalculation(category: String, distance: String, source: String, destination: String,
                           domain: String, travelTime: String, type: String, waitingTime: String, region: String): Single<PriceCalculationResponse> =
            networkService.doPriceCalculation(PriceCalculationRequest(category, distance, source, destination, domain, travelTime, type, waitingTime, region))
                    .map {
                        PriceCalculationResponse(
                                it.statusCode,
                                it.status,
                                it.data,
                                it.infoId
                        )
                    }


    fun checkingCoupon(coupon:String): Single<CouponResponse> =
            networkService.checkingCoupon(coupon)
                    .map {
                        CouponResponse(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.data,
                                it.jwt,
                                it.infoId

                        )
                    }

    fun dogetTotalAmount(user_id: String): Single<TotalAmountResponse> =
            networkService.dogetTotalMyAmount(TotalAmountRequest(user_id))
                    .map {
                        TotalAmountResponse(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.data,
                                it.jwt,
                                it.infoId
                        )
                    }

    fun doCheckCouponPrice(amount:String,coupon: String?):Single<CouponPriceResponce> =
        networkService.doCheckCouponPrice(amount,coupon)
                .map {
                    CouponPriceResponce(
                            it.statusCode,
                            it.status,
                            it.message,
                            it.data,
                            it.jwt,
                            it.infoId
                    )
                }

    fun dofaceBookLoginApiRequest(firstName: String, lastName: String, email: String, id: String): Single<FbLoginResponse> =
            networkService.doFaceBookLoginApiRequest(FbLoginRequest(email,firstName,id,lastName,""))
                    .map {
                        FbLoginResponse(
                                it.statusCode,
                                it.status,
                                it.message,
                                it.data,
                                it.jwt,
                                it.infoId
                        )
                    }

    fun doSocialGmailLoginrequest(email: String,firstName:String,lastName: String,phoneNumber:String,socialId:String): Single<SocialLoginResponse> =
        networkService.doSocialGmailLoginrequest(SocialLoginRequest(email,firstName,socialId,lastName,phoneNumber ))
            .map {
                SocialLoginResponse(
                    it.statusCode,
                    it.status,
                    it.message,
                    it.data,
                    it.jwt,
                    it.infoId
                )
            }



    fun getOnPauseState(): Boolean {
        return userPreferences.getOnPauseState()
    }
    fun saveOnPauseState(onpause: Boolean) {
        userPreferences.saveOnPauseState(onpause)
    }

    fun onPauseSaveBooking(onpauseBooking: Boolean) {
        userPreferences.onPauseSaveBooking(onpauseBooking)
    }

    fun onPausegetBooking(): Boolean {
        return userPreferences.onPausegetBooking()
    }

    fun getIsDriverAccept(): Boolean {
        return userPreferences.getIsDriverAccept()
    }

    fun saveisDriverAccept(isaccept: Boolean) {
        userPreferences.saveisDriverAccept(isaccept)
    }

    fun savegmailLoginStatus(login_status: String){
        userPreferences.savegmailLoginStatus(login_status)
    }

    fun getGmailLoginStatus():String?{
        return userPreferences.getGmailLoginStatus()
    }

    fun getgMailLoginImageUrl(): String? {
        return userPreferences.getgMailLoginImageUrl()
    }

    fun savegMailLoginImageUrl(imageurl: String) {
         userPreferences.savegMailLoginImageUrl(imageurl)
    }

    fun getCustomerCareNumber(): String? {
        return userPreferences.getCustomerCareNumber()
    }

    fun saveCustomerCareNumber(customercareNumber: String) {
        userPreferences.saveCustomerCareNumber(customercareNumber)
    }


}