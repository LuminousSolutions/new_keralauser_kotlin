
package com.mongokerala.taxi.newuser.utils.common;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.mongokerala.taxi.newuser.BuildConfig;
import com.mongokerala.taxi.newuser.R;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.regex.Pattern;

/**
 * Created by thiyagesh on 05/08/18.
 */

public final class CommonUtils {

    private static final String TAG = "CommonUtils";

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    @NonNull
    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @NonNull
    public static ProgressDialog showSearchingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        //progressDialog.setContentView(R.layout.progress_booking);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle("Finding Taxi");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

//    public static boolean isEmailValid(String email) {
//        Pattern pattern;
//        Matcher matcher;
//        final String EMAIL_PATTERN =
//                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
//                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
//        pattern = Pattern.compile(EMAIL_PATTERN);
//        matcher = pattern.matcher(email);
//        return matcher.matches();
//    }
public static  boolean isEmailValid(String email) {

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        return Pattern.compile(EMAIL_STRING).matcher(email).matches();

    }

    public static  boolean isPhonenumber(String phonenumber) {

        String PHONE_STRING = "@\"^[0-9]{10}$\"";

        return Pattern.compile(PHONE_STRING).matcher(phonenumber).matches();

    }


    public static String loadJSONFromAsset(Context context, @NonNull String jsonFileName)
            throws IOException {

        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }

//    public static String getTimeStamp() {
//        return new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.US).format(new Date());
//    }

    public static  Double calculate(Double km , int base_price, double price_km, int peck_price,int min_price){
            /*
            * need to add peack price*/
        double tot_apx_price = 0;

            tot_apx_price = (base_price * price_km) ;

       /* if(min_price>(km * price_km)){

            tot_apx_price= min_price+base_price;
        }else {
            tot_apx_price = (km * price_km) + base_price;
        }*/


        DecimalFormat myFormatter = new DecimalFormat("##0.0");
       return Double.valueOf(myFormatter.format(tot_apx_price));
    }

    public static String getCurrentVersion() {

        return String.valueOf(BuildConfig.VERSION_CODE);
    }
}
