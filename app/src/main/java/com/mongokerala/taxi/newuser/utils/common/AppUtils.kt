package com.mongokerala.taxi.newuser.utils.common

import android.app.Activity
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import com.google.firebase.firestore.FirebaseFirestore
import com.novoda.merlin.MerlinsBeard
import com.mongokerala.taxi.newuser.BuildConfig
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.utils.common.Constants.PREF_FILE_NAME
import com.mongokerala.taxi.newuser.utils.common.Constants.is_address
import com.mongokerala.taxi.newuser.utils.common.Constants.is_city
import com.mongokerala.taxi.newuser.utils.common.Constants.is_country
import com.mongokerala.taxi.newuser.utils.common.Constants.is_postalCode
import com.mongokerala.taxi.newuser.utils.common.Constants.is_state
import de.mateware.snacky.Snacky
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


object AppUtils {

    //private var merlinsBeard: MerlinsBeard? = null


    @Nullable
    fun convertTIme(date: String, dateOrTimeFormat: String?): String? {
        var date_Output: String? = null
        if (!date.isEmpty()) {
            try {
                val sdf = SimpleDateFormat(dateOrTimeFormat)
                val sdf2 = SimpleDateFormat(dateOrTimeFormat)
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                val date2 = sdf.parse(date)
                val sdfIndia =
                    SimpleDateFormat(dateOrTimeFormat)
                val tzInIndia = TimeZone.getDefault()
                sdfIndia.timeZone = tzInIndia
                val sDateInIndia = sdfIndia.format(date2) // Convert to String first
                val dateInIndia =
                    sdf2.parse(sDateInIndia) // Create a new Date object
                date_Output = sdf2.format(dateInIndia)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return date_Output
    }

    @Nullable
    fun convertStringToImage(@Nullable encodedString: String?): Bitmap? {
        var bitmap: Bitmap? = null
        if (encodedString != null) {
            bitmap = if (encodedString.contains("base64")) {
                val pureBase64Encoded =
                    encodedString.substring(encodedString.indexOf(",") + 1)
                val decodedBytes =
                    Base64.decode(pureBase64Encoded, Base64.DEFAULT)
                //  userAvatar.setImageBitmap(U.decodeImageString(decodedBytes, 100, 100));
                BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
            } else {
                val decodedBytes =
                    Base64.decode(encodedString, Base64.DEFAULT)
                BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
            }
        }
        return bitmap //   userAvatar.setImageBitmap(U.decodeImageString(decodedBytes, 100, 100));
    }


    fun storeOnlineOffline(
        context: Context,
        userId: String,
        isOnline: Boolean,
        supplier_id: String,
        user_name: String,
        user_mobile_number: String,
    lattt: String?,longg: String?,device_id: String?
    ) {

        val db = FirebaseFirestore.getInstance()
        val user: MutableMap<String, Any> =
            HashMap()
        user["Device_MODEL"] = Build.MODEL
        user["Device_Id"] = Build.DEVICE
        user["SDK"] = Build.BRAND
        user["Manufacture"] = Build.MANUFACTURER
        user["FINGERPRINT"] = Build.FINGERPRINT
        user["Device_Version"] = Build.VERSION.RELEASE
        user["App_Version"] = BuildConfig.VERSION_NAME
        user["Device"] = getDeviceName().toString()
        user[userId] = isOnline
        user["SUPPLIER"] = supplier_id
        user["IS_ANDROID"] = true
        user["IS_LOGIN"] = true
        val shared =
            context.getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE)
        val lat = shared.getFloat("DRIVER_CURRENT_LATITUDE", 0.0f).toDouble()
        val lon = shared.getFloat("DRIVER_CURRENT_LONGITUDE", 0.0f).toDouble()
        user["LOCATION"] = "$lattt,$longg"
        user["LATITUDE"] = lattt!!.toDouble()
        user["LONGITUDE"] = longg!!.toDouble()
        val currentTime = Calendar.getInstance().time
        user["LAST_LOGIN"] = currentTime
        user["USER_NAME"] = user_name
        user["USER_MOBILE_NUMBER"] = user_mobile_number
        db.collection("USER-STATUS")
            .document(userId)
            .set(user)
            .addOnSuccessListener { aVoid: Void? ->
                Log.e(
                    "Firebase",
                    "storeOnlineOffline:Success "
                )
            }
            .addOnFailureListener { Log.e("Firebase", "onFailure: fail ") }
    }




    fun isNetworkConnected(@NonNull context: Activity?): Boolean {
        var merlinsBeard: MerlinsBeard? = null
        merlinsBeard = MerlinsBeard.Builder().build(context)
        return if (merlinsBeard.isConnected) {
            true
        } else {
            context?.let { snackActivity(it, "Please check your network connection.    \uf0eb") }
            false
        }
    }

    fun snackActivity(context: Activity, msg: String?) {
        val type = Typeface.createFromAsset(context.assets, "fonts/fontello.ttf")
        Snacky.builder()
            .setActivity(context)
            .setText(msg)
            .setDuration(Snacky.LENGTH_LONG)
            .setTextTypeface(type)
            .setTextSize(12f)
            .setBackgroundColor(ContextCompat.getColor(context, R.color.google))
            .info()
            .show()
    }

    fun latLongToAddress(
        latitude: Double,
        longitude: Double,
        context: Context,
        value: String
    ): String? {
        var convertedAddress: String? = ""
        var addressLine: String? = null
        var city: String? = null
        var state: String? = null
        var country: String? = null
        var postalCode: String? = null
//        val address: List<Address>
        val geocoder = Geocoder(context, Locale.getDefault())
        try {
            val address = geocoder.getFromLocation(latitude, longitude, 1)
            if (address != null) {
                if (address.size > 0) {
                    addressLine = address[0].getAddressLine(0)
                    city = address[0].locality
                    state = address[0].adminArea
                    country = address[0].countryName
                    postalCode = address[0].postalCode
                    convertedAddress = addressLine
                    // convertedAddress = addressLine + "," + city + "," + state + "," + country + "," + postalCode;
                } else {
                    //Toaster.show(context,context.getString(R.string.Not_Get_Cuurent_Address))

                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        when (value) {
            is_address -> return addressLine
            is_city -> return city
            is_state -> return state
            is_country -> return country
            is_postalCode -> return postalCode
        }
        return addressLine
    }

    fun getPlaceIdRequest(lat: String?, lon: String?, place: String?): String? {
        val requestFormat = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&rankby=50&radius=500&key=%s"
        return String.format(Locale.ENGLISH, requestFormat, lat, lon, "AIzaSyDkCytxQTkidI9IkQmXRrnB9SPX1HeVbm4")
    }

    /*@Nullable
    public static String latLongToAddress(double latitude, double longitude, Context context, @NonNull String value) {
        String convertedAddress = "";
        String addressLine = null, city = null, state = null, country = null, postalCode = null;

        Geocoder geocoder;
        List<Address> address;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            address = geocoder.getFromLocation(latitude, longitude, 1);
            if (address.size() > 0) {
                addressLine = address.get(0).getAddressLine(0);
                city = address.get(0).getLocality();
                state = address.get(0).getAdminArea();
                country = address.get(0).getCountryName();
                postalCode = address.get(0).getPostalCode();
                convertedAddress = addressLine;
                // convertedAddress = addressLine + "," + city + "," + state + "," + country + "," + postalCode;
            } else {
                Toaster.INSTANCE.show(context, "Not Get Cuurent Address");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        switch (value){
            case is_address:
                return addressLine;
            case is_city:
                return city;

            case is_state:
                return state;

            case is_country:
                return country;

            case is_postalCode:
                return postalCode;
        }
        return addressLine;
    }*/
    /*offline booking*/
    fun createPlaceIdDirectionApiRequest(sourcePlaceId: String?, destinationPlaceId: String?): String? {
        val requestFormat = ("https://maps.googleapis.com/maps/api/directions/json"
                + "?origin=place_id:%s&destination=place_id:%s&region=ch&key=%s")
        return String.format(Locale.ENGLISH, requestFormat, sourcePlaceId, destinationPlaceId
            , "AIzaSyDkCytxQTkidI9IkQmXRrnB9SPX1HeVbm4")
    }
/*Device detailes
* */
open fun getDeviceName(): String? {
    val manufacturer = Build.MANUFACTURER
    val model = Build.MODEL
    return if (model.startsWith(manufacturer)) {
        capitalize(model)
    } else capitalize(manufacturer) + " " + model
}

    private fun capitalize(str: String): String {
        if (TextUtils.isEmpty(str)) {
            return str
        }
        val arr = str.toCharArray()
        var capitalizeNext = true
        val phrase = StringBuilder()
        for (c in arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c))
                capitalizeNext = false
                continue
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true
            }
            phrase.append(c)
        }
        return phrase.toString()
    }




}