package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.utils.network.ERole

class SignupRequest(
   // @field:SerializedName(Keys.firstName) @field:Expose var firstName: String?,
    @SerializedName("email") val email : String,
    @SerializedName("firstName") val firstName : String,
    @SerializedName("password") val password : String,
    @SerializedName("phoneNumber") val phoneNumber : String
    ) {

    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("lastName")
    @Expose
    var lastName: String? = null

    @SerializedName("latitude")
    @Expose
    var latitude: Double? = null

    @SerializedName("longitude")
    @Expose
    var longitude: Double? = null

    @SerializedName("role")
    @Expose
    var role: ERole? = null

    @SerializedName("phoneVerified")
    @Expose
    var phoneVerified: String? = null

    @SerializedName("token")
    @Expose
    var token: String? = null

    @SerializedName("carType")
    @Expose
    var carType: String? = null

    @SerializedName("category")
    @Expose
    var category: String? = null

    @SerializedName("domain")
    @Expose
    var domain: String? = null

    @SerializedName("website")
    @Expose
    var website: String? = null



    init {
        domain="KERALACABS"
        website="KERALACABS"
        role = ERole.ROLE_USER
        id = "0"
        carType = "string"
        category = "TAXI"
        lastName = "string"
        latitude = 0.0
        longitude = 0.0
        phoneVerified = "string"
    }

}