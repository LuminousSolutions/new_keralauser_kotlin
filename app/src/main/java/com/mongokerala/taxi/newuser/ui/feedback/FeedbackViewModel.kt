package com.mongokerala.taxi.newuser.ui.feedback

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class FeedbackViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {
    val feedIn: MutableLiveData<Boolean> = MutableLiveData()

    val feedbacksuccess: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()


    override fun onCreate() {

    }

    fun doFeedback(message: String){
        feedIn.postValue(true)
         var taxiId:String="string"

        try {
            if (!userRepository.getTaxiId().isNullOrEmpty()){
                taxiId= userRepository.getTaxiId()!!
            }else{
                taxiId="dummy"
            }
        }catch (e:Exception){
            Log.e("feedback", "doFeedback: "+e.toString() )
        }
        
        compositeDisposable.addAll(
            userRepository.doFeedback(message, userRepository.getName()!!,
                userRepository.getMobileNUmber()!!, userRepository.getEmail()!!,
//                    userRepository.getTaxiId()!!
                    taxiId
            )
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if(it.infoId.equals("500")) {
                            feedIn.postValue(false)

                            feedbacksuccess.postValue(Event(emptyMap()))

                        }
                    },
                    {
                        feedIn.postValue(false)

                        handleNetworkError(it)
                    }
                )
        )
    }
}