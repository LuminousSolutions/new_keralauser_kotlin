package com.mongokerala.taxi.newuser.ui.payment;


import androidx.annotation.NonNull;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface PostRequestSender {

    //String BASE_URL = "http://63.142.252.78:8080";
    String BASE_URL = "http://1-dot-taxi2dealin.appspot.com";
    //String BASE_URL = "http://1-dot-taxi2deal.appspot.com";
    //String BASE_URL = "http://10.0.2.2:8080";
    //String BASE_URL = "http://localhost:8080";

   /* @NonNull
    @POST("/paymentgateway-api/api/orders/v1/order/create")
    Call<OrderDTO> createOrder(@Body OrderDTO orderDTO);

    @NonNull
    @POST("/paymentgateway-api/api/orders/v1/order-payment/update")
    Call<ApiResponse> updateOrderPayment(@Body OrderDTO orderDTO);*/

//Paytm below url is our url
      @NonNull
    @POST("/api/orders/v1/order/create")
    Call<OrderDTO> createOrder(@Body OrderDTO orderDTO);

    @NonNull
    @POST("/api/orders/v1/order-payment/update")
    Call<ApiResponse> updateOrderPayment(@Body OrderDTO orderDTO);
}
