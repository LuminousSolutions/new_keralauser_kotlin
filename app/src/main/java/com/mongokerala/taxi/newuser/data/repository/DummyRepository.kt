package com.mongokerala.taxi.newuser.data.repository

import com.mongokerala.taxi.newuser.data.local.db.DatabaseService
import com.mongokerala.taxi.newuser.data.model.Dummy
import com.mongokerala.taxi.newuser.data.remote.NetworkService
import com.mongokerala.taxi.newuser.data.remote.request.DummyRequest
import io.reactivex.Single
import javax.inject.Inject

class DummyRepository @Inject constructor(
    private val networkService: NetworkService,
    private val databaseService: DatabaseService
) {

    fun fetchDummy(id: String): Single<List<Dummy>> =
        networkService.doDummyCall(DummyRequest(id)).map { it.data }

}