package com.mongokerala.taxi.newuser.paymentgateway;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.mongokerala.taxi.newuser.R;
import com.mongokerala.taxi.newuser.paymentgateway.enums.OrderRequestType;
import com.mongokerala.taxi.newuser.paymentgateway.enums.PaymentGateway;
import com.mongokerala.taxi.newuser.paymentgateway.enums.PaymentStatus;
import com.mongokerala.taxi.newuser.paymentgateway.listener.CreateOrderPaymentListener;
import com.mongokerala.taxi.newuser.paymentgateway.model.ApiResponse;
import com.mongokerala.taxi.newuser.paymentgateway.model.CustomerDTO;
import com.mongokerala.taxi.newuser.paymentgateway.model.StripePaymentRequest;
import com.mongokerala.taxi.newuser.paymentgateway.model.StripeResponse;
import com.mongokerala.taxi.newuser.paymentgateway.service.StripePaymentService;
import com.mongokerala.taxi.newuser.paymentgateway.service.impl.StripePaymentServiceImpl;
import com.mongokerala.taxi.newuser.paymentgateway.util.StripePaymentConstants;
import com.mongokerala.taxi.newuser.utils.display.Toaster;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import java.util.Objects;

/**
 * Created by Akhi007 on 23-01-2019.
 */
public class Stripe_MainActivity extends AppCompatActivity implements
        View.OnClickListener, CreateOrderPaymentListener {

    private static final String TAG = Stripe_MainActivity.class.getSimpleName();
    private ProgressBar progressBar;
    private CardInputWidget mCardInputWidget;
    private Button btPayNow;
    private StripePaymentService stripePaymentService;

    private String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stripe_activity_main);
        value = Objects.requireNonNull(getIntent().getExtras()).getString("card_amount");
        progressBar = findViewById(R.id.progressBar);
        mCardInputWidget = findViewById(R.id.card_input_widget);
        btPayNow = findViewById(R.id.btPayNow);
        btPayNow.setOnClickListener(this);
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()){
            case R.id.btPayNow : {
                captureCardDetails();
                break;
            }
        }
    }

    /*NOTE : Here we are using stripe inbuilt form to collect card details you can create your
     * own form to collect card details and validate that card then process below step
     */
    private void captureCardDetails(){
        Card card = mCardInputWidget.getCard();
        if (card != null) {
            progressBar.setVisibility(View.VISIBLE);
            //Step 1 Stripe validate request from server
            Stripe stripe = new Stripe(getApplicationContext(), StripePaymentConstants.getStripPaymentCredentialMap().get(StripePaymentConstants.KEY.PUBLISHABLE_KEY)); ////NOTE* Pass publisher key
            stripe.createToken(card, new TokenCallback() {
                        public void onSuccess(@Nullable Token token) {
                            Log.i(TAG, "Token : "+new Gson().toJson(token));
                            if(null != token && null != token.getId() && !token.getId().isEmpty()) {
                                validateRequestAndStripePGApiCall(token); // Send token to your server (This is most important for payment)
                            } else {
                                progressBar.setVisibility(View.GONE);
                                //TODO : You can show your own error message here
                                Toaster.INSTANCE.show(getApplicationContext(), "Error while generating Token From Stripe!");
                            }
                        }
                        public void onError(@NonNull Exception error) {
                            progressBar.setVisibility(View.GONE);
                            // TODO : You can Show localized error message coming from Stripe
                            //TODO : OR you can show your own error message here
                            Toaster.INSTANCE.show(getApplicationContext(), "Error From Stripe : "+error.getLocalizedMessage());
                        }
                    }
            );
        } else {
            //TODO : Show your own error message
            Toaster.INSTANCE.show(getApplicationContext(), "Invalid Card Data");
        }
    }

    private void validateRequestAndStripePGApiCall(Token token){
        stripePaymentService = new StripePaymentServiceImpl();
        StripePaymentRequestOrder orderDTO = new StripePaymentRequestOrder();
        CustomerDTO customerDTO = new CustomerDTO();
        StripePaymentRequest pgDataDTO = new StripePaymentRequest();
        //TODO : your customer info
        customerDTO.setMobileNumber("9999999999");
        customerDTO.setEmail("test@gmail.com");
        customerDTO.setName("Akhilesh");
        //TODO : payment details
        //NOTE* It is highly recommended to take amount here only to validate in server
        // AS STRIPE PAYMENT is triggered on server side we are sending only card details and customer details
        // from android app platform only.
        pgDataDTO.setAmount(Integer.valueOf(value));
        pgDataDTO.setCurrency("usd"); //as per you
        pgDataDTO.setPayStatus(String.valueOf(PaymentStatus.INITIATED));
        pgDataDTO.setTxnId("String");
        pgDataDTO.setPaymentGateway(String.valueOf(PaymentGateway.STRIPE));
        pgDataDTO.setStripeToken(token.getId()); //this is mandatory and important
        orderDTO.setOrderReqType(String.valueOf(OrderRequestType.ORDER_CREATE_AND_PAYMENT_UPDATE)); // as per you
        //orderId : created in server side only
        orderDTO.setCustomer(customerDTO);
        orderDTO.setStripePaymentRequest(pgDataDTO);
        stripePaymentService.createStripeOrderPayment(this, orderDTO);
    }

    //Please show your custom message here
    @Override
    public void onRequestSuccess(StripePaymentRequestOrder requestDTO, @NonNull StripeResponse orderResponseDTO) {
        progressBar.setVisibility(View.GONE);
        if(null != orderResponseDTO.getStripePaymentRequest() && null != orderResponseDTO.getStripePaymentRequest().getPayStatus()
                && PaymentStatus.SUCCESS.toString().equals(orderResponseDTO.getStripePaymentRequest().getPayStatus())){
            startActivity(new Intent(this, OrderStatusActivity.class));
            finish();
        } else {
            Toaster.INSTANCE.show(getApplicationContext(), "Unable to capture payment!");
        }
    }

    @Override
    public void onRequestFailed(StripePaymentRequestOrder orderDTO, @NonNull ApiResponse apiResponse) {
        progressBar.setVisibility(View.GONE);
       // Toaster.INSTANCE.show(getApplicationContext(), apiResponse.getMessage());
    }

    @Override
    public void onRequestTimeOut(StripePaymentRequestOrder orderDTO) {
        progressBar.setVisibility(View.GONE);
        Toaster.INSTANCE.show(getApplicationContext(), "Network connection timeout");
    }
}
