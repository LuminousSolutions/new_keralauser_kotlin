package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class PriceCalculationRequest (
    @field:SerializedName(Keys.category) @field:Expose var category: String?,
    @field:SerializedName(Keys.distance) @field:Expose var distance: String?,
    @field:SerializedName(Keys.source) @field:Expose var source: String?,
    @field:SerializedName(Keys.destination) @field:Expose var destination: String?,
    @field:SerializedName(Keys.domain) @field:Expose var domain: String?,
    @field:SerializedName(Keys.travelTime) @field:Expose var travelTime: String?,
    @field:SerializedName(Keys.type) @field:Expose var type: String?,
    @field:SerializedName(Keys.waitingTime) @field:Expose var waitingTime: String?,
    @field:SerializedName(Keys.region) @field:Expose var region: String?
){

    @SerializedName("discount")
    @Expose
    private var discount: Int? = null

    @SerializedName("elapsedTime")
    @Expose
    private var elapsedTime: Int? = null

    @SerializedName("googleKm")
    @Expose
    private var googleKm: Int? = null

    @SerializedName("latitude")
    @Expose
    private var latitude: Int? = null

    @SerializedName("longitude")
    @Expose
    private var longitude: Int? = null

    @SerializedName("rideId")
    @Expose
    private var rideId: Int? = null

    @SerializedName("sourceLatitude")
    @Expose
    private var sourceLatitude: Int? = null

    @SerializedName("sourceLongitude")
    @Expose
    private var sourceLongitude: Int? = null

    @SerializedName("status")
    @Expose
    private var status: String? = null


    init {
        status = "string"
        sourceLatitude = 0
        longitude = 0
        sourceLongitude = 0
        rideId = 0
        latitude = 0
        googleKm = 0
        elapsedTime = 0
        discount = 0
    }


}