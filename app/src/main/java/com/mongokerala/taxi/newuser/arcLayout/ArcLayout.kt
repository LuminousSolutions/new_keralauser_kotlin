package com.mongokerala.taxi.newuser.arcLayout

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Point
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import com.mongokerala.taxi.newuser.R
import java.util.*

class ArcLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : ViewGroup(context, attrs, defStyleAttr) {
    private val childAngleHolder = WeakHashMap<View, Float>()
    private var arc: Arc = Arc.CENTER
    private var arcDrawable: ArcDrawable? = null
    private var axisRadius = 0
    private val size = Point()
    private var isFreeAngle = DEFAULT_FREE_ANGLE
    private var isReverseAngle = DEFAULT_REVERSE_ANGLE

    /*@TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ArcLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }*/
    private fun init(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) {
        setWillNotDraw(false)
        val a = context.theme.obtainStyledAttributes(
                attrs, R.styleable.arc_ArcLayout, defStyleAttr, defStyleRes)
        var arcOrigin = a.getInt(
                R.styleable.arc_ArcLayout_arc_origin, DEFAULT_ORIGIN)
        val arcColor = a.getColor(
                R.styleable.arc_ArcLayout_arc_color, DEFAULT_COLOR)
        val arcRadius = a.getDimensionPixelSize(
                R.styleable.arc_ArcLayout_arc_radius, DEFAULT_RADIUS)
        val arcAxisRadius = a.getDimensionPixelSize(
                R.styleable.arc_ArcLayout_arc_axisRadius, DEFAULT_AXIS_RADIUS)
        val isArcFreeAngle = a.getBoolean(
                R.styleable.arc_ArcLayout_arc_freeAngle, DEFAULT_FREE_ANGLE)
        val isArcReverseAngle = a.getBoolean(
                R.styleable.arc_ArcLayout_arc_reverseAngle, DEFAULT_REVERSE_ANGLE)
        a.recycle()
        if (Utils.JELLY_BEAN_MR1_OR_LATER) {
            arcOrigin = ArcOrigin.getAbsoluteOrigin(arcOrigin, layoutDirection)
        }
        arc = Arc.of(arcOrigin)
        arcDrawable = ArcDrawable(arc, arcRadius, arcColor)
        axisRadius = arcAxisRadius
        isFreeAngle = isArcFreeAngle
        isReverseAngle = isArcReverseAngle
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (Utils.DEBUG) {
            Utils.d(TAG, "onMeasure: w=%s, h=%s",
                    MeasureSpec.toString(widthMeasureSpec),
                    MeasureSpec.toString(heightMeasureSpec))
        }
        size.x = Utils.computeMeasureSize(widthMeasureSpec, arcDrawable!!.intrinsicWidth)
        size.y = Utils.computeMeasureSize(heightMeasureSpec, arcDrawable!!.intrinsicHeight)
        setMeasuredDimension(size.x, size.y)
        if (Utils.DEBUG) {
            Utils.d(TAG, "setMeasuredDimension: w=%d, h=%d", size.x, size.y)
        }
    }

    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (isInEditMode) {
            return
        }
        if (Utils.DEBUG) {
            Utils.d(TAG, "onLayout: l=%d, t=%d, r=%d, b=%d", l, t, r, b)
        }
        arcDrawable!!.setBounds(0, 0, r - l, b - t)
        val o = arc.computeOrigin(0, 0, size.x, size.y)
        val radius = if (axisRadius == DEFAULT_AXIS_RADIUS) arcDrawable!!.radius / 2 else axisRadius
        val perDegrees = arc.computePerDegrees(childCountWithoutGone)
        var arcIndex = 0
        run {
            var i = 0
            val size = childCount
            while (i < size) {
                val child = getChildAt(i)
                if (child.visibility == View.GONE) {
                    i++
                    continue
                }
                val lp = child.layoutParams as LayoutParams
                var childAngle: Float
                childAngle = if (isFreeAngle) {
                    arc.startAngle + lp.angle
                } else if (isReverseAngle) {
                    arc.computeReverseDegrees(arcIndex++, perDegrees)
                } else {
                    arc.computeDegrees(arcIndex++, perDegrees)
                }
                val x: Int = o.x + Arc.x(radius, childAngle)
                val y: Int = o.y + Arc.y(radius, childAngle)
                childMeasureBy(child, x, y)
                childLayoutBy(child, x, y)
                childAngleHolder[child] = childAngle
                i++
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    override fun onDraw(canvas: Canvas) {
        if (isInEditMode) {
            return
        }
        super.onDraw(canvas)
        arcDrawable!!.draw(canvas)
    }

    private val childCountWithoutGone: Int
        private get() {
            var childCount = 0
            var i = 0
            val len = getChildCount()
            while (i < len) {
                if (getChildAt(i).visibility != View.GONE) {
                    childCount++
                }
                i++
            }
            return childCount
        }

    private fun childMeasureBy(child: View, x: Int, y: Int) {
        if (Utils.DEBUG) {
            Utils.d(TAG, "childMeasureBy: x=%d, y=%d", x, y)
        }
        val lp = child.layoutParams as LayoutParams
        var origin = lp.origin
        if (Utils.JELLY_BEAN_MR1_OR_LATER) {
            origin = ArcOrigin.getAbsoluteOrigin(origin, layoutDirection)
        }
        val widthSize: Int
        val widthMode: Int
        when (lp.width) {
            ViewGroup.LayoutParams.MATCH_PARENT -> {
                widthSize = Utils.computeWidth(origin, size.x, x)
                widthMode = MeasureSpec.EXACTLY
            }
            ViewGroup.LayoutParams.WRAP_CONTENT -> {
                widthSize = Utils.computeWidth(origin, size.x, x)
                widthMode = MeasureSpec.AT_MOST
            }
            else -> {
                widthSize = lp.width
                widthMode = MeasureSpec.EXACTLY
            }
        }
        val heightSize: Int
        val heightMode: Int
        when (lp.height) {
            ViewGroup.LayoutParams.MATCH_PARENT -> {
                heightSize = Utils.computeHeight(origin, size.y, y)
                heightMode = MeasureSpec.EXACTLY
            }
            ViewGroup.LayoutParams.WRAP_CONTENT -> {
                heightSize = Utils.computeHeight(origin, size.y, y)
                heightMode = MeasureSpec.AT_MOST
            }
            else -> {
                heightSize = lp.height
                heightMode = MeasureSpec.EXACTLY
            }
        }
        child.measure(
                MeasureSpec.makeMeasureSpec(widthSize, widthMode),
                MeasureSpec.makeMeasureSpec(heightSize, heightMode)
        )
    }

    private fun childLayoutBy(child: View, x: Int, y: Int) {
        if (Utils.DEBUG) {
            Utils.d(TAG, "childLayoutBy: x=%d, y=%d", x, y)
        }
        val lp = child.layoutParams as LayoutParams
        var origin = lp.origin
        if (Utils.JELLY_BEAN_MR1_OR_LATER) {
            origin = ArcOrigin.getAbsoluteOrigin(origin, layoutDirection)
        }
        val width = child.measuredWidth
        val height = child.measuredHeight
        val left: Int
        left = when (origin and ArcOrigin.HORIZONTAL_MASK) {
            ArcOrigin.LEFT -> x
            ArcOrigin.RIGHT -> x - width
            else -> x - width / 2
        }
        val top: Int
        top = when (origin and ArcOrigin.VERTICAL_MASK) {
            ArcOrigin.TOP -> y
            ArcOrigin.BOTTOM -> y - height
            else -> y - height / 2
        }
        child.layout(left, top, left + width, top + height)
        if (Utils.DEBUG) {
            Utils.d(TAG, "l=%d, t=%d, r=%d, b=%d", left, top, left + width, top + height)
        }
    }

    override fun generateLayoutParams(p: ViewGroup.LayoutParams): ViewGroup.LayoutParams {
        return LayoutParams(p)
    }

    override fun generateLayoutParams(attrs: AttributeSet): ViewGroup.LayoutParams {
        return LayoutParams(context, attrs)
    }

    override fun generateDefaultLayoutParams(): ViewGroup.LayoutParams {
        return LayoutParams()
    }

    class LayoutParams : MarginLayoutParams {
        var origin = DEFAULT_CHILD_ORIGIN
        var angle = DEFAULT_CHILD_ANGLE

        internal constructor(c: Context, attrs: AttributeSet?) : super(c, attrs) {
            val a = c.theme
                    .obtainStyledAttributes(attrs, R.styleable.arc_ArcLayout_Layout, 0, 0)
            origin = a.getInt(R.styleable.arc_ArcLayout_Layout_arc_origin, DEFAULT_CHILD_ORIGIN)
            angle = a.getFloat(R.styleable.arc_ArcLayout_Layout_arc_angle, DEFAULT_CHILD_ANGLE)
            a.recycle()
        }

        internal constructor() : super(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT) {}
        internal constructor(source: ViewGroup.LayoutParams?) : super(source) {}
    }

    companion object {
        private const val TAG = "ArcLayout"
        private const val DEFAULT_CHILD_ANGLE = 0f
        private val DEFAULT_CHILD_ORIGIN: Int = ArcOrigin.CENTER
        private val DEFAULT_ORIGIN: Int = ArcOrigin.CENTER
        private const val DEFAULT_COLOR = Color.TRANSPARENT
        private const val DEFAULT_RADIUS = 144
        private const val DEFAULT_AXIS_RADIUS = -1 //default: radius / 2
        private const val DEFAULT_FREE_ANGLE = false
        private const val DEFAULT_REVERSE_ANGLE = false
    }

    init {
        init(context, attrs, defStyleAttr, 0)
    }
}
