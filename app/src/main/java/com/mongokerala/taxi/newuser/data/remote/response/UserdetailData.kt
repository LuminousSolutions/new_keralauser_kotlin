package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName


data class UserdetailData (

    @SerializedName("id") val id : String,
    @SerializedName("email") val email : String,
    @SerializedName("firstName") val firstName : String,
    @SerializedName("lastName") val lastName : String,
    @SerializedName("phoneNumber") val phoneNumber : String,
    @SerializedName("address") val address : String,
    @SerializedName("role") val role : String,
    @SerializedName("lang") val lang : String,
    @SerializedName("latitude") val latitude : Double,
    @SerializedName("longitude") val longitude : Double,
    @SerializedName("deviceId") val deviceId : String,
    @SerializedName("website") val website : String,
    @SerializedName("hourly") val hourly : Int,
    @SerializedName("price") val price : Int,
    @SerializedName("domain") val domain : String
)