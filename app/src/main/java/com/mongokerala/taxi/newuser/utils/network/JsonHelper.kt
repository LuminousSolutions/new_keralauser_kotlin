package com.mongokerala.taxi.newuser.utils.network

import com.google.android.gms.maps.model.LatLng
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object JsonHelper {
    val API_DATE_FORMAT =
        SimpleDateFormat("EEE MMM dd zzz yyyy")
    const val METERS_IN_KILOMETERS_COUNT = 1000

    // ---------------------------------------  Other  ---------------------------------------------
    fun dateToString(date: Date?): String {
        return API_DATE_FORMAT.format(date)
    }

    @Throws(JSONException::class)
    fun createAirportSearchRequestJson(
        airportOptions: String?,
        sourceCity: String?
        ,
        sourceAddress: String?,
        destinationCity: String?,
        destinationAddress: String?
    ): String {
        val `object` = JSONObject()
        `object`.put("airportOptions", airportOptions)
        `object`.put("cartype", "Taxi")
        `object`.put("transporttype", "airport")
        `object`.put("source", sourceCity)
        `object`.put("source_address", sourceAddress)
        `object`.put("destination", destinationCity)
        `object`.put("destination_address", destinationAddress)
        return `object`.toString()
    }

    @Throws(JSONException::class)
    fun createAddNewReviewRequestJson(
        userEmail: String?, taxiId: Long
        , rating: String?, comment: String?
    ): String {
        val `object` = JSONObject()
        `object`.put("userEmail", userEmail)
        `object`.put("taxiId", taxiId)
        `object`.put("rating", rating)
        `object`.put("comment", comment)
        return `object`.toString()
    }

    @Throws(JSONException::class)
    fun createOutstationSearchRequestJson(
        sourceCityName: String?,
        destinationCityName: String?
    ): String {
        val jsonObject = JSONObject()
        jsonObject.put("cartype", "Taxi")
        jsonObject.put("transporttype", "outstation")
        jsonObject.put("source", sourceCityName)
        jsonObject.put("source_address", "")
        jsonObject.put("destination", destinationCityName)
        jsonObject.put("destination_address", "")
        return jsonObject.toString()
    }

    @Throws(JSONException::class)
    fun createPointToPointSearchRequestJson(
        sourcePosition: LatLng,
        destinationPosition: LatLng,
        distance: Int,
        duration: Int,
        distanceKm: Int,
        radius: Int
    ): String {
        val jsonObject = JSONObject()
        jsonObject.put("source_latitude", sourcePosition.latitude)
        jsonObject.put("source_longitude", sourcePosition.longitude)
        jsonObject.put("destination_latitude", destinationPosition.latitude)
        jsonObject.put("destination_longitude", destinationPosition.longitude)
        jsonObject.put("distance", distance)
        jsonObject.put("duration", duration)
        jsonObject.put("distancekm", distanceKm)
        jsonObject.put("radius", radius)
        return jsonObject.toString()
    }

    @Throws(JSONException::class)
    fun calculateFinalDirectionDuration(directionJson: JSONObject): Int {
        var finalDurationMin = 0
        val routes = directionJson.getJSONArray("routes")
        for (i in 0 until routes.length()) {
            val route = routes.getJSONObject(i)
            val legs = route.getJSONArray("legs")
            for (j in 0 until legs.length()) {
                val leg = legs.getJSONObject(j)
                val duration = leg.getJSONObject("duration")
                finalDurationMin += duration.getInt("value")
            }
        }
        finalDurationMin /= routes.length()
        finalDurationMin =
            TimeUnit.SECONDS.toMinutes(finalDurationMin.toLong()).toInt()
        return finalDurationMin
    }

    @Throws(JSONException::class)
    fun calculateRoutePoints(directionJson: JSONObject): List<LatLng?> {
        val resultList: MutableList<LatLng?> =
            ArrayList()
        val routes = directionJson.getJSONArray("routes")
        if (routes.length() > 0) {
            val route = routes.getJSONObject(0)
            val legs = route.getJSONArray("legs")
            for (j in 0 until legs.length()) {
                val leg = legs.getJSONObject(j)
                val steps = leg.getJSONArray("steps")
                for (i in 0 until steps.length()) {
                    val step = steps.getJSONObject(i)
                    val startLocation = step.getJSONObject("start_location")
                    resultList.add(getLegPosition(startLocation))
                    val polyline = step.getJSONObject("polyline")
                    val pointsEncoded = polyline.getString("points")
                    resultList.addAll(decodePoly(pointsEncoded))
                    val endLocation = step.getJSONObject("end_location")
                    resultList.add(getLegPosition(endLocation))
                }
            }
        }
        /*for (int i = 0; i < routes.length(); i++){
            JSONObject route = routes.getJSONObject(i);
        }*/return resultList
    }

    @Throws(JSONException::class)
    private fun getLegPosition(locationObject: JSONObject): LatLng {
        val latitude: Double
        val longitude: Double
        latitude = locationObject.getDouble("lat")
        longitude = locationObject.getDouble("lng")
        return LatLng(latitude, longitude)
    }

    private fun decodePoly(encoded: String): List<LatLng?> {
        val poly: MutableList<LatLng?> = ArrayList<LatLng?>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val position =
                LatLng(
                    lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5
                )
            poly.add(position)
        }
        return poly
    }

    @Throws(JSONException::class)
    fun calculateFinalDirectionDistance(directionJson: JSONObject): Double {
        var finalDistanceKm = 0.0
        val myFormatter = DecimalFormat("##0.0")
        val routes = directionJson.getJSONArray("routes")
        for (i in 0 until routes.length()) {
            val route = routes.getJSONObject(i)
            val legs = route.getJSONArray("legs")
            for (j in 0 until legs.length()) {
                val leg = legs.getJSONObject(j)
                val distance = leg.getJSONObject("distance")
                finalDistanceKm = distance.getDouble("value")
            }
        }
        return java.lang.Double.valueOf(myFormatter.format(finalDistanceKm / METERS_IN_KILOMETERS_COUNT))
    }

    @Throws(JSONException::class)
    fun calculateOnRideDistance(directionJson: JSONObject): Double {
        var finalDistanceKm = 0.0
        val myFormatter = DecimalFormat("##0.0")
        val rows = directionJson.getJSONArray("rows")
        for (i in 0 until rows.length()) {
            val route = rows.getJSONObject(i)
            val legs = route.getJSONArray("elements")
            for (j in 0 until legs.length()) {
                val dis = legs.getJSONObject(j)
                val dis_val = dis.getJSONObject("distance")
                finalDistanceKm =
                    finalDistanceKm + java.lang.Double.valueOf(dis_val.getString("value"))
            }
        }
        return java.lang.Double.valueOf(myFormatter.format(finalDistanceKm / 3280.8))
    }

    @Throws(JSONException::class)
    fun createReviewsRequestJson(taxiId: Long): String {
        val `object` = JSONObject()
        `object`.put("taxiId", taxiId)
        return `object`.toString()
    }
}
