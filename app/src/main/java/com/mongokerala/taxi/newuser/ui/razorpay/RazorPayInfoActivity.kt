package com.mongokerala.taxi.newuser.ui.razorpay

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.ui.common_dialog.MYOnClickListener
import com.mongokerala.taxi.newuser.ui.common_dialog.RideFinishCustomAlertDialog
import com.razorpay.Checkout
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import kotlinx.android.synthetic.main.activity_razorpay.*
import org.json.JSONObject

class RazorPayInfoActivity() : AppCompatActivity(), PaymentResultWithDataListener {

    companion object{
        var username :String = ""
        var userEmail:String = ""
        var userMobileNumber:String = ""
        var rideamount:Int=0
        var payment_Type:String=""
        var usersource:String=""
        var userdestination:String=""
        var userridekm:String=""
        var totalrideamt:Int=0
        var useraction:String=""
        var usercancelReason:String=""
        var userpaytype:String=""
        lateinit var usermyOnClickListener:MYOnClickListener
    }
    
    
    constructor(
        source: String,
        destination:String,
        ridedistance:String,
        rideAmount:Int,
        action:String,
        cancelreason:String,
        paymentType:String,
        name:String,
        email:String,
        mobilenumber:String,
        myOnClickListener:MYOnClickListener,
    ):this(){
        rideamount = rideAmount
        username =name
        userEmail =email
        userMobileNumber =mobilenumber
        payment_Type =paymentType
        usersource =source
        userdestination =destination
        userridekm = ridedistance
        totalrideamt = rideAmount
        useraction =action
        usercancelReason =cancelreason
        usermyOnClickListener =myOnClickListener
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_razorpay)
        choosePaymentmode()


        username_txt.text = username
        useremail_txt.text = userEmail
        userphonenumber_txt.text = userMobileNumber
        useramount_txt.text = rideamount.toString()
        payment_mode_txt.text = payment_Type

        when (payment_Type) {
            "CASH" -> {
                payment_spinner.setSelection(0)
            }
            "CARD" -> {
                payment_spinner.setSelection(1)
            }
            "UPI" -> {
                payment_spinner.setSelection(2)
            }
        }

        testPaymentButton_sucess.setOnClickListener{
            rideFinish()
        }
        testPaymentButton_failure.setOnClickListener{
            sucess_fail.visibility= View.GONE
            Realtive_failure.visibility= View.GONE
            pay_details.visibility= View.VISIBLE
        }




        testPaymentButton_confirm.setOnClickListener{

            val payment  = payment_mode_txt.text.toString()

            if (payment == "CASH"){
                rideFinish()
            }else{
                startPayment()
            }


        }





}


    private fun choosePaymentmode() {
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.choose_payment_mode,
            R.layout.spinner_car_type_text_background
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        payment_spinner.adapter = adapter
        payment_spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {


                val paymentType = payment_spinner.selectedItem.toString()
                payment_mode_txt.text = paymentType

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }

    private fun startPayment() {

        val checkout = Checkout()

        checkout.setImage(R.drawable.amazonpay)
        val activity: Activity = this
        try {
            val options = JSONObject()
            options.put("name", "KERALACABS")
            options.put("description", "KERALACABS")
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("theme.color", "#3399cc")
            options.put("currency", "INR")
            options.put("amount", totalrideamt*100)
            options.put("prefill.email", userEmail)
            options.put("prefill.contact", userMobileNumber)
            val retryObj = JSONObject()
            retryObj.put("enabled", true)
            retryObj.put("max_count", 4)
            options.put("retry", retryObj)
            checkout.open(activity, options)
        } catch (e: Exception) {
        }
    }


    fun rideFinish(){
        val rideFinishCustomAlertDialog = RideFinishCustomAlertDialog(
            this,
            usersource,
            userdestination,
            userridekm,
            totalrideamt.toString(),
            useraction,
            usercancelReason,
            payment_Type,
            usermyOnClickListener
        )
        rideFinishCustomAlertDialog.show()
        rideFinishCustomAlertDialog.setCancelable(false)

    }


    private fun paymentSuccess(razorpayPaymentId: String) {

        paymentid.text = razorpayPaymentId
        sucess_fail.visibility = View.VISIBLE
        Relative_sucess.visibility = View.VISIBLE
        pay_details.visibility = View.GONE

        usernames_sucess.text = username
        useremail_sucess.text = userEmail
        userphonenumber_sucess.text = userMobileNumber
        useramount_sucess.text = rideamount.toString()

    }

    private fun paymentFailure(PaymentData: PaymentData?) {
        val jsonObject:JSONObject = PaymentData!!.data
        payment_Reason_failure.text = jsonObject.getJSONObject("error").get("description").toString()
        Realtive_failure.visibility=View.VISIBLE
        sucess_fail.visibility=View.VISIBLE
        pay_details.visibility=View.GONE
        usernames_failure.text = username
        useremail_failure.text = userEmail
        userphonenumber_failure.text = userMobileNumber
        useramount_failure.text = rideamount.toString()
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?, PaymentData: PaymentData?) {
        if (razorpayPaymentId != null) {
            paymentSuccess(razorpayPaymentId)
        }
    }



    override fun onPaymentError(errorCode: Int, response: String?, PaymentData: PaymentData?) {
        paymentFailure(PaymentData)
    }

}