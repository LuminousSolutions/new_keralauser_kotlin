package com.mongokerala.taxi.newuser.ui.payment;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public interface OrderPaymentUpdateListener {
    void onRequestSuccess(OrderDTO orderDTO, ApiResponse apiResponse);

    void onRequestTimeOut(OrderDTO orderDTO);
}
