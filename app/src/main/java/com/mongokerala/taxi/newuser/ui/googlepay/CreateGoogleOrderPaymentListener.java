package com.mongokerala.taxi.newuser.ui.googlepay;


import com.mongokerala.taxi.newuser.paymentgateway.model.ApiResponse;

/**
 * Created by Akhi007 on 23-01-2019.
 */
public interface CreateGoogleOrderPaymentListener {
    void onRequestSuccess(GoogleOrderDTO orderRequestDTO, GoogleOrderDTO orderResponseDTO);

    void onRequestFailed(GoogleOrderDTO orderDTO, ApiResponse apiResponse);

    void onRequestTimeOut(GoogleOrderDTO orderDTO);
}
