package com.mongokerala.taxi.newuser.ui.googlepay;

public enum GoogleOrderRequestType {

    ORDER_CREATE_AND_PAYMENT_UPDATE, ORDER_CREATE, PAYMENT_UPDATE, RE_ATTEMPT_PAYMENT
}
