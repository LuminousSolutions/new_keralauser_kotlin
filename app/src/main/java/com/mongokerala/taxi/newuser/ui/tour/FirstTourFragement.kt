package com.mongokerala.taxi.newuser.ui.tour

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.mongokerala.taxi.newuser.R
import kotlinx.android.synthetic.main.first_tour_frag.*


class FirstTourFragment : Fragment() {


    companion object {
        fun newInstance(title: String): Fragment {
            val fragment = FirstTourFragment()
            val args = Bundle()
            args.putString(title, title)
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.first_tour_frag, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tvTime.tag = requireArguments().getString("hi")
    }


}

class FirstTourViewPagerAdapter(supportFragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(supportFragmentManager) {
    override fun getItem(position: Int): Fragment {

        return FirstTourFragment.newInstance("\"FirstTourFragment, Instance 1\"" + position)

    }

    override fun getCount(): Int {
        return 4
    }
}









