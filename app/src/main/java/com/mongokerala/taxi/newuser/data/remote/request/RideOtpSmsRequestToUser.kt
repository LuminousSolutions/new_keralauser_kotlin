package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class RideOtpSmsRequestToUser(
    @field:SerializedName(Keys.countryCode) @field:Expose var countryCode: String?,
    @field:SerializedName(Keys.userId) @field:Expose var userId: String?
    ) {
    @SerializedName("password")
    @Expose
    private var password: String? = null

    @SerializedName("phoneNumber")
    @Expose
    private var phoneNumber: String? = null

    @SerializedName("rideId")
    @Expose
    private var rideId: Int? = null

    @SerializedName("role")
    @Expose
    private var role: String? = null

    @SerializedName("token")
    @Expose
    private var token: String? = null

    init {
        password = "string"
        phoneNumber = "string"
        rideId = 0
        role  =  "string"
        token = "string"
    }
}