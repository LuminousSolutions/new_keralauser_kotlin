package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.utils.network.ERole

class SocialLoginRequest(
    @field:SerializedName(Keys.email) @field:Expose var email: String?,
    @field:SerializedName(Keys.firstName) @field:Expose var firstName: String?,
    @field:SerializedName(Keys.socialId) @field:Expose var socialId: String,
    @field:SerializedName(Keys.lastName) @field:Expose var lastName: String,
    @field:SerializedName(Keys.phoneNumber) @field:Expose var phoneNumber: String
) {
    @Expose
    @SerializedName(Keys.domain)
    var domain: String = "KERALACABS"

    @Expose
    @SerializedName(Keys.id)
    var id: String = ""

    @Expose
    @SerializedName(Keys.lang)
    var lang: String = "string"

    @Expose
    @SerializedName(Keys.latitude)
    var latitude: Int = 0

    @Expose
    @SerializedName(Keys.role)
    var role: String = ERole.ROLE_USER.toString()
}