package com.mongokerala.taxi.newuser.data.remote

import com.google.gson.JsonObject
import com.mongokerala.taxi.newuser.data.model.BaseRideResponse
import com.mongokerala.taxi.newuser.data.remote.fcm.FcmNotificationResponse
import com.mongokerala.taxi.newuser.data.remote.request.*
import com.mongokerala.taxi.newuser.data.remote.response.*
import com.mongokerala.taxi.newuser.ui.main_new.AdvanceSearchResponse
import io.reactivex.Single
import retrofit2.http.*
import javax.inject.Singleton

@Singleton
interface NetworkService {

    @POST(Endpoints.DUMMY)
    fun doDummyCall(
        @Body request: DummyRequest,
        @Header(Networking.HEADER_API_KEY) apiKey: String = Networking.API_KEY // default value set when Networking create is called
    ): Single<DummyResponse>

    @POST(Endpoints.ENDPOINT_SERVER_LOGIN)
    fun doLoginCall(
        @Body request: LoginRequest
    ): Single<LoginResponse>


    @POST(Endpoints.ENDPOINT_REVIEW)
    fun doUpdateReview(
        @Body request: ReviewRequest
    ): Single<BaseResponse>

    @POST(Endpoints.ENDPOINT_SERVER_VehicleInfo)
    fun doVehicleInfoUpdateCall(
        @Body vehicle_info_update_request: VehicleInfoUpdateRequest
    ): Single<BaseResponse>

    @POST(Endpoints.ENDPOINT_SERVER_ProfileUpdate)
    fun doProfileEditCall(
        @Body profile_edit_request: ProfileEditRequest
    ): Single<BaseResponse>

    @POST(Endpoints.ENDPOINT_SERVER_ProfileImage)
    fun doProfileImageCall(
        @Body profile_edit_request: ProfileEditRequest
    ): Single<BaseResponse>

    @POST(Endpoints.ENDPOINT_FCM_REQUEST)
    fun doFcmRequest(
        @Body jsonObject: JsonObject,
        @Header("Authorization") fcmKey: String = "key=AAAARWQX_KQ:APA91bG6oHkj9hOShlBr-foU3wcyCfeYprG-UmnXLET_sMvK5op9qR-WYTlUXJsRD2AKspEPXuYSBQP5IDRyLj2tuWZCyXq2FXkIgyU_pwhwA2HDhmZXzHQdCv7ht51yb6MWCVjslJP-",
        @Header("Content-Type") app_json: String = "application/json"
    ): Single<FcmNotificationResponse>

    @POST(Endpoints.ENDPOINT_VALIDATE_OTP_REQ)
    fun doValidateOtpRequest(
        @Body request: ValidateOtpRequest
    ): Single<BaseResponse>

    @POST(Endpoints.ENDPOINT_VALIDATE_RIDE_OTP_REQ)
    fun doRideValidateOtpRequest(
        @Body request: RideValidateOtpRequest
    ): Single<BaseResponse>

    @POST(Endpoints.ENDPOINT_RIDE_CREATE)
    fun doRideCreate(
        @Body request: RideDummyCreateRequest
    ): Single<BaseResponse>

    @POST(Endpoints.ENDPOINT_RIDE_STATUS_WITHREASON)
    fun doRideStatusReason(
            @Body request: RideStatusCreateRequest
    ): Single<BaseRideResponse>

    @GET(Endpoints.ENDPOINT_SERVER_CheckUserImage)
      fun doCheckUserImage(
       @Path("userId") userId: String?
    ): Single<UserDetailImageResponse>


    @POST(Endpoints.ENDPOINT_RIDE_OTP_SMS_TO_USER)
    fun doRideOtpSmsRequestToUser(
        @Body rideOtpSmsRequestToUser: RideOtpSmsRequestToUser
    ): Single<BaseResponse>

    @POST(Endpoints.OFFLINE_BOOKING_SMS)
    fun doUserOfflineSmsRequestToUser(
        @Body offlineRideSmsToUserRequest: OfflineRideSmsToUserRequest
    ): Single<BaseRideResponse>

    @POST(Endpoints.USER_OFFLINE)
    fun doOffineBooking(
        @Body offlineBookingRequest: OfflineBookingRequest
    ): Single<OfflineBookingResponse>

    @POST(Endpoints.USER_OFFLINE_PRICE_CALCULATION)
    fun doPriceCalculation(
        @Body priceCalculationRequest: PriceCalculationRequest
    ): Single<PriceCalculationResponse>

    @POST(Endpoints.ENDPOINT_SIGNUP_REQ)
    fun doUserRegistrationCall(
        @Body request: SignupRequest
    ): Single<BaseResponse>

    @POST(Endpoints.ENDPOINT_FEEDBACK)
    fun doFeedbackCall(
        @Body request: FeedbackRequest
    ): Single<BaseResponse>

    @POST(Endpoints.ENDPOINT_RATE_US)
    fun doRateUsCall(
        @Body request: FeedbackRequest
    ): Single<BaseResponse>

    @POST(Endpoints.ENDPOINT_OTP_REQ)
    fun doOtpRequestCall(
        @Body request: OtpRequest
    ): Single<BaseResponse>

    @POST(Endpoints.FORGET_PASSWORD_REQ_OTP)
    fun doForgetPasswordOtpRequestCall(
        @Body request: ForgetPasswordOtpRequest
    ): Single<BaseResponse>

    @POST(Endpoints.INDIA_FORGOT_SMS)
    fun doForgetPasswordOtpValidationRequestCall(
        @Body request: ForgetPasswordOtpValidationRequest
    ): Single<BaseResponse>

    //MANIIIIII
    @POST(Endpoints.SEARCH_API)
    fun doSearchTaxi(
        @Body request: AdvanceSeachApiRequest
    ): Single<AdvanceSearchResponse>

    @POST(Endpoints.VEHICLES_LATLNG_API)
    fun doShowTaxi(
        @Body request: AdvanceSeachApiRequest
    ): Single<AdvanceSearchResponse>


    //TODO chnage it
    @POST(Endpoints.BOOKING_API)
    fun doBookingTaxi(
            @Body request: BookingRequest
    ): Single<BookingResponse>

    @POST(Endpoints.COUPON_BOOKING_API)
    fun doCouponBookingTaxi(
        @Body request: CouponBookingRequest
    ): Single<BookingResponse>

    @GET(Endpoints.ENDPOINT_SERVER_UserDetails)
    fun doUserdetailCall(
        @Path("userId") userId: String?
    ): Single<UserdetailResponse>



    @GET(Endpoints.ENDPOINT_CHECK_WALLET_BALANCE)
    fun doUserwalletBalance(
        @Path("userId") userId: String?
    ): Single<UserWalletBalanceResponce>



    @POST(Endpoints.ENDPOINT_CHECK_WALLET_ADD)
    fun addWalletAmount(
            @Body request:AddWalletAmountRequest
    ):Single<AddWalletAmountResponse>


    @GET(Endpoints.ENDPOINT_CHECK_WALLET_TOKENCHECK)
    fun doUserwalletTokencheck(
        @Path("token") token: String?
    ): Single<UserWalletTokenResponce>




    @GET(Endpoints.ENDPOINT_TRIP_HISTORY)
    fun doTripHistoryCall(
        @Path("userId") userId: String?
    ): Single<TripHistoryResponse>


    @PUT(Endpoints.ENDPOINT_STATUS_UPDATE)
    fun doStatusUpdateCall(
        @Path("taxiId") taxiId: String,
        @Path("userId") userId: String?,
        @Path("latitude") latitude: Double?,
        @Path("longitude") longitude: Double?,
        @Path("driverStatus") driverStatus: String?
    ): Single<BaseResponse>

    @PUT(Endpoints.ENDPOINT_UPDATE_PRICE_KM)
    fun updatePriceAndDistance(@Body priceUpdate: PriceUpdate): Single<UpdatePriceResponse>

    @GET(Endpoints.ENDPOINT_LOGOUT)
    fun doLoginStatusCall(
        @Path("userId") userId: String?,
        @Path("loginStatus") loginStatus: String?
    ): Single<BaseResponse>

    @GET(Endpoints.ENDPOINT_STATUS_UPDATE_LANGUAGE)
    fun doLanguageUpdateCall(
        @Path("userId") userId: String?,
        @Path("language") language: String?
    ): Single<BaseResponse>

    @GET(Endpoints.ENDPOINT_TRIP_INVOICE)
    fun doTripVoiceCall(
        @Path("rideId") userId: String?
    ): Single<InvoiceResponse>

/*    @GET(Endpoints.ENDPOINT_FCM_TOKEN)
    fun doFcmCall(
        @Path("userId") userId: String?,
        @Path("oneSignalValue") oneSignalValue: String?


    ): Single<BaseResponse>*/

    @PUT(Endpoints.ENDPOINT_FCM_TOKEN)
    fun doFcmCall(
        @Body fcmUpdateRequest: FcmUpdateRequest
    ): Single<BaseResponsenew>

    @GET(Endpoints.ENDPOINT_SERVER_TaxiDetails)
    fun doTaxidetailCall(
        @Path("taxiId") taxiId: String?
    ): Single<Taxi_detail>

    @GET(Endpoints.HOME_POSTS_LIST)
    fun doHomePostListCall(
        @Query("firstPostId") firstPostId: String?,
        @Query("lastPostId") lastPostId: String?,
        @Header(Networking.HEADER_USER_ID) userId: String,
        @Header(Networking.HEADER_API_KEY) apiKey: String = Networking.API_KEY
    ): Single<PostListResponse>

    @PUT(Endpoints.POST_LIKE)
    fun doPostLikeCall(
        @Body request: PostLikeModifyRequest,
        @Header(Networking.HEADER_USER_ID) userId: String,
        @Header(Networking.HEADER_API_KEY) apiKey: String = Networking.API_KEY
    ): Single<GeneralResponse>

    @PUT(Endpoints.POST_UNLIKE)
    fun doPostUnlikeCall(
        @Body request: PostLikeModifyRequest,
        @Header(Networking.HEADER_USER_ID) userId: String,
        @Header(Networking.HEADER_API_KEY) apiKey: String = Networking.API_KEY
    ): Single<GeneralResponse>

    @POST(Endpoints.CHECK_COUPON)
    fun checkingCoupon(
            @Path("coupon") coupon: String?
    ): Single<CouponResponse>

    @POST(Endpoints.ENDPOINTS_TOGET_AMOUNT)
    fun dogetTotalMyAmount(
            @Body request: TotalAmountRequest
    ): Single<TotalAmountResponse>

    @GET(Endpoints.COUPON_PRICE)
    fun doCheckCouponPrice(
            @Path("amount") amount: String?,
            @Path("coupon") coupon: String?
    ):Single<CouponPriceResponce>

    @POST(Endpoints.FBLOGIN)
    fun doFaceBookLoginApiRequest(
            @Body request: FbLoginRequest
    ):Single<FbLoginResponse>

    @POST(Endpoints.FBLOGIN)
    fun doSocialGmailLoginrequest(
        @Body request: SocialLoginRequest
    ): Single<SocialLoginResponse>

}