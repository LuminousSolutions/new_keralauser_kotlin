/*
package com.mongokerala.taxi.newuser.paymentgateway.adyan;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.viewpager.widget.ViewPager;

import com.adyen.checkout.core.CheckoutException;
import com.adyen.checkout.core.PaymentMethodHandler;
import com.adyen.checkout.core.PaymentResult;
import com.adyen.checkout.core.StartPaymentParameters;
import com.adyen.checkout.core.handler.StartPaymentParametersHandler;
import com.adyen.checkout.ui.CheckoutController;
import com.adyen.checkout.ui.CheckoutSetupParameters;
import com.adyen.checkout.ui.CheckoutSetupParametersHandler;
import com.adyen.checkout.ui.internal.common.util.KeyboardUtil;
import com.mongokerala.taxi.newuser.R;
import com.mongokerala.taxi.newuser.paymentgateway.OrderStatusActivity;
import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.AdyenPaymentSessionResponse;
import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.PaymentSetupRequest;
import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.PaymentVerifyRequest;
import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.PaymentVerifyResponse;

public class AdyenPaymentActivity extends AppCompatActivity implements View.OnClickListener,
        CreateAdyenPaymentSessionListener, VerifyAdyenPaymentListener {

    private static final String TAG = AdyenPaymentActivity.class.getSimpleName();

    private static final int REQUEST_CODE_CHECKOUT = 1;

    private ViewPager mViewPager;

    private Button mCheckoutButton;

    private ContentLoadingProgressBar mProgressBar;

    private TabsAdapter mTabsAdapter;

    private ViewPager.SimpleOnPageChangeListener mPageChangeListener;

    private AdyenPaymentService adyenPaymentService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adyen_payment);
        initComponent();
        if (savedInstanceState == null) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
    }


    private void initComponent(){
        mTabsAdapter = new TabsAdapter(getSupportFragmentManager());
        mPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == TabsAdapter.FRAGMENT_POSITION_SHOPPING_CART) {
                    KeyboardUtil.hide(mViewPager);
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
                } else if (position == TabsAdapter.FRAGMENT_POSITION_CONFIGURATION) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                }
            }
        };

        mViewPager = findViewById(R.id.viewPager_tabs);
        mViewPager.setAdapter(mTabsAdapter);
        mViewPager.addOnPageChangeListener(mPageChangeListener);
        mProgressBar = findViewById(R.id.progressBar);
        mCheckoutButton = findViewById(R.id.button_checkout);
        mCheckoutButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_checkout) {
            CheckoutController.startPayment(this, new CheckoutSetupParametersHandler() {
                @Override
                public void onRequestPaymentSession(@NonNull CheckoutSetupParameters checkoutSetupParameters) {
                    retrievePaymentSession(checkoutSetupParameters);
                }

                @Override
                public void onError(@NonNull CheckoutException checkoutException) {
                    handleCheckoutException(checkoutException);
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_CHECKOUT) {
            if (resultCode == PaymentMethodHandler.RESULT_CODE_OK) {
                PaymentResult paymentResult = PaymentMethodHandler.Util.getPaymentResult(data);
                //noinspection ConstantConditions
                verifyPayment(paymentResult.getPayload());
            } else {
                CheckoutException checkoutException = PaymentMethodHandler.Util.getCheckoutException(data);
                String message = checkoutException != null ? checkoutException.getMessage() : "null";

                if (resultCode == PaymentMethodHandler.RESULT_CODE_CANCELED) {
                    Toast.makeText(AdyenPaymentActivity.this, "Cancelled. Error: " + message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Error: " + message, Toast.LENGTH_SHORT).show();
                }
            }

            mCheckoutButton.setClickable(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mViewPager.removeOnPageChangeListener(mPageChangeListener);
    }

    //1. Create Adyen Payment Session
    private void retrievePaymentSession(@NonNull CheckoutSetupParameters checkoutSetupParameters) {
        */
/**
         * Note*
         *   Here you can set your Adyen payment session create request parameter
         *   Customer details and order details
         *
         *   ConfigurationFragment is just a reference to take input fields
         *
         *//*

        ConfigurationFragment configurationFragment = mTabsAdapter.getConfigurationFragment();
        PaymentSetupRequest paymentSetupRequest = configurationFragment != null
                ? configurationFragment.getPaymentSetupRequest(checkoutSetupParameters)
                : null;

        if (paymentSetupRequest == null) {
            mViewPager.setCurrentItem(TabsAdapter.FRAGMENT_POSITION_CONFIGURATION);
        } else {
            mCheckoutButton.setClickable(false);
            mProgressBar.show();
            mViewPager.setCurrentItem(TabsAdapter.FRAGMENT_POSITION_SHOPPING_CART);
            //call API to create Adyen Payment session
            adyenPaymentService = new AdyenPaymentServiceImpl();
            OrderDTO orderDTO = new OrderDTO();
            orderDTO.setPaymentSetupRequest(paymentSetupRequest);
            //orderDTO.setCustomer(new CustomerDTO()); // As per you
            adyenPaymentService.createPaymentSession(this, orderDTO);
        }
    }

    private void handleCheckoutException(@NonNull CheckoutException checkoutException) {
        Toast.makeText(AdyenPaymentActivity.this, "Error: " + checkoutException.getMessage(), Toast.LENGTH_SHORT).show();
    }

    //2. Verify Adyen Payment
    private void verifyPayment(@NonNull String payload) {
        mProgressBar.show();
        PaymentVerifyRequest paymentVerifyRequest = new PaymentVerifyRequest(payload);
        //call API to create Adyen Payment session
        adyenPaymentService = new AdyenPaymentServiceImpl();
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setPaymentVerifyRequest(paymentVerifyRequest);
        //orderDTO.setCustomer(); // As per you
        adyenPaymentService.verifyPayment(this, orderDTO);
    }

    @NonNull
    private StartPaymentParametersHandler createStartPaymentParametersHandler() {
        return new StartPaymentParametersHandler() {
            @Override
            public void onPaymentInitialized(@NonNull StartPaymentParameters startPaymentParameters) {
                PaymentMethodHandler checkoutHandler = CheckoutController.getCheckoutHandler(startPaymentParameters);
                checkoutHandler.handlePaymentMethodDetails(AdyenPaymentActivity.this, REQUEST_CODE_CHECKOUT);
            }

            @Override
            public void onError(@NonNull CheckoutException checkoutException) {
                handleCheckoutException(checkoutException);
            }
        };
    }

    //1. Adyen Payment Session Create API Response
    @Override
    public void onRequestSuccess(OrderDTO orderDTO, AdyenPaymentSessionResponse paymentSessionResponse) {
        mProgressBar.hide();
        if(null != paymentSessionResponse && null != paymentSessionResponse.getPaymentSession()){
            StartPaymentParametersHandler handler = createStartPaymentParametersHandler();
            CheckoutController.handlePaymentSessionResponse(AdyenPaymentActivity.this, paymentSessionResponse.getPaymentSession(), handler);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.adyen_payment_not_available_choose_other_error_msg), Toast.LENGTH_SHORT).show();
            mCheckoutButton.setClickable(true);
        }
    }

    @Override
    public void onRequestFailed(OrderDTO orderDTO, ApiResponse apiResponse) {
        mProgressBar.hide();
        Toast.makeText(getApplicationContext(), getString(R.string.adyen_payment_not_available_choose_other_error_msg), Toast.LENGTH_SHORT).show();
        mCheckoutButton.setClickable(true);
    }

    @Override
    public void onRequestTimeOut(OrderDTO orderDTO) {
        mProgressBar.hide();
        Toast.makeText(getApplicationContext(), getString(R.string.network_timeout_error_msg), Toast.LENGTH_SHORT).show();
        mCheckoutButton.setClickable(true);
    }

    //2. Verify Adyen Payment Response
    @Override
    public void onPaymentVerifyRequestSuccess(OrderDTO orderDTO, PaymentVerifyResponse paymentVerifyResponse) {
        mProgressBar.hide();
        PaymentVerifyResponse.ResultCode resultCode = paymentVerifyResponse.getResultCode();

        if (resultCode == PaymentVerifyResponse.ResultCode.AUTHORIZED || resultCode == PaymentVerifyResponse.ResultCode.RECEIVED) {
            startActivity(new Intent(AdyenPaymentActivity.this, OrderStatusActivity.class));
        } else {
            Toast.makeText(getApplicationContext(), "Result: " + resultCode, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPaymentVerifyRequestFailed(OrderDTO orderDTO, ApiResponse apiResponse) {
        mProgressBar.hide();
        Toast.makeText(getApplicationContext(), getString(R.string.adyen_payment_verification_failed_error_msg), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPaymentVerifyRequestTimeOut(OrderDTO orderDTO) {
        mProgressBar.hide();
        Toast.makeText(getApplicationContext(), getString(R.string.network_timeout_error_msg), Toast.LENGTH_SHORT).show();
    }


}
*/
