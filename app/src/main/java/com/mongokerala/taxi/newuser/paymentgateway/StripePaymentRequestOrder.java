package com.mongokerala.taxi.newuser.paymentgateway;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mongokerala.taxi.newuser.paymentgateway.model.CustomerDTO;
import com.mongokerala.taxi.newuser.paymentgateway.model.StripePaymentRequest;

public class StripePaymentRequestOrder {
    @SerializedName("customer")
    @Expose
    private CustomerDTO customer;
    @SerializedName("orderReqType")
    @Expose
    private String orderReqType;
    @SerializedName("stripePaymentRequest")
    @Expose
    private StripePaymentRequest stripePaymentRequest;

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public String getOrderReqType() {
        return orderReqType;
    }

    public void setOrderReqType(String orderReqType) {
        this.orderReqType = orderReqType;
    }

    public StripePaymentRequest getStripePaymentRequest() {
        return stripePaymentRequest;
    }

    public void setStripePaymentRequest(StripePaymentRequest stripePaymentRequest) {
        this.stripePaymentRequest = stripePaymentRequest;
    }
}
