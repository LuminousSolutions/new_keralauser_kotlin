/*
 * Copyright (c) 2017 Adyen N.V.
 *
 * This file is open source and available under the MIT license. See the LICENSE file for more info.
 *
 * Created by timon on 09/08/2017.
 */

package com.mongokerala.taxi.newuser.paymentgateway.adyan.req;

import com.squareup.moshi.Json;

public class PaymentVerifyResponse {
    private PaymentAdditionlData additionalData;

    private String pspReference;

    private ResultCode resultCode;

    private String merchantReference;

    private String paymentMethod;

    private String shopperLocale;

    public PaymentAdditionlData getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(PaymentAdditionlData additionalData) {
        this.additionalData = additionalData;
    }

    public String getPspReference() {
        return pspReference;
    }

    public void setPspReference(String pspReference) {
        this.pspReference = pspReference;
    }

    public ResultCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    public String getMerchantReference() {
        return merchantReference;
    }

    public void setMerchantReference(String merchantReference) {
        this.merchantReference = merchantReference;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getShopperLocale() {
        return shopperLocale;
    }

    public void setShopperLocale(String shopperLocale) {
        this.shopperLocale = shopperLocale;
    }

    /**
     * The authorization response.
     */
    public enum ResultCode {
        @Json(name = "Pending") PENDING,
        @Json(name = "Received") RECEIVED,
        @Json(name = "Authorised") AUTHORIZED,
        @Json(name = "Error") ERROR,
        @Json(name = "Refused") REFUSED,
        @Json(name = "Cancelled") CANCELLED,
        @Json(name = "Unknown") UNKNOWN
    }
}
