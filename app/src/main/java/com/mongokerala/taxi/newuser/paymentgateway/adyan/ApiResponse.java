package com.mongokerala.taxi.newuser.paymentgateway.adyan;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public class ApiResponse {
    private String code;
    private String message;

    public ApiResponse(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
