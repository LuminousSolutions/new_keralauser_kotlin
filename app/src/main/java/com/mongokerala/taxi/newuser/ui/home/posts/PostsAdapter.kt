package com.mongokerala.taxi.newuser.ui.home.posts

import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import com.mongokerala.taxi.newuser.data.model.Post
import com.mongokerala.taxi.newuser.ui.base.BaseAdapter

class PostsAdapter(
    parentLifecycle: Lifecycle,
    posts: ArrayList<Post>
) : BaseAdapter<Post, PostItemViewHolder>(parentLifecycle, posts) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PostItemViewHolder(parent)
}