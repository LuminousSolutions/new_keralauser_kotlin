package com.mongokerala.taxi.newuser.ui.main_new


data class SearchTaxiResponse(
		val infoId: String? = null,
		val data: List<DataItem>? = null,
		val jwt: String? = null,
		val message: String? = null,
		val statusCode: Int? = null,
		val status: Boolean? = null
)


data class DataItem(
		val image: Any? = null,
		val latitude: Double? = null,
		val imageBype: Any? = null,
		val phonenumber: Any? = null,
		val token: String? = null,
		val phoneNumber: String? = null,
		val carType: String? = null,
		val driverId: String? = null,
		val name: String? = null,
		val id: String? = null,
		val longitude: Double? = null,
		val status: String? = null,
		val notificationToken: String? = null
)
