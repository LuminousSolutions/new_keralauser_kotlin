package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.data.remote.request.Keys

class TotalAmountRequest(
        @field:SerializedName(Keys.driverId) @field:Expose var driverId: String?
)
{

    @Expose
    @SerializedName(Keys.balance)
    var balance: Double
    @Expose
    @SerializedName(Keys.credit)
    var credit: Double
    @Expose
    @SerializedName(Keys.debit)
    var debit: Double
    @Expose
    @SerializedName(Keys.id)
    var id: String
    @Expose
    @SerializedName(Keys.paymentType)
    var paymentType: String

    init {
        balance=0.0
        credit=0.0
        debit=0.0
        id="string"
        paymentType="string"
    }
}