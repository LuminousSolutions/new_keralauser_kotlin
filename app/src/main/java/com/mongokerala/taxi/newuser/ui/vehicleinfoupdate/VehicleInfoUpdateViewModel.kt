package com.mongokerala.taxi.newuser.ui.vehicleinfoupdate

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class VehicleInfoUpdateViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper)  {

    val vehicle_year_Field: MutableLiveData<String> = MutableLiveData()
    val vehicle_seats_Field: MutableLiveData<String> = MutableLiveData()
    val vehicle_number_Field: MutableLiveData<String> = MutableLiveData()
    val vehicle_brand_Field: MutableLiveData<String> = MutableLiveData()
    val car_type_Field: MutableLiveData<String> = MutableLiveData()
    val vehicleInfoUpdated: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val notupdated: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val taxiIn: MutableLiveData<Boolean> = MutableLiveData()


    override fun onCreate() {
    }

    fun onVehicleUpdate(vehicle_year: String,vehicle_seats: String,vehicle_number: String,vehicle_brand: String,
    car_type: String){
        var car_type_value: String = ""
        if(car_type.equals("Sedan")){
            car_type_value = "Taxi"
        }else if(car_type.equals("Suv")){
            car_type_value = "Taxi6"
        }else if(car_type.equals("hatchback/mini")){
            car_type_value = "Taxi4"
        }else if(car_type.equals("Traveller")){
            car_type_value = "Transport"
        }else if(car_type.equals("Auto")){
            car_type_value = "Auto"
        }
        else if(car_type.equals("Ambulance")){
            car_type_value = "Ambulance"
        }
        else if(car_type.equals("Bike")){
            car_type_value = "Bike"
        }


        Log.d("update vehicle", "onVehicleUpdate: "+userRepository.getVehiclenumber()!!)
        taxiIn.postValue(true)
        compositeDisposable.addAll(
            userRepository.doVehicleInfoUpdate(car_type_value,vehicle_seats, userRepository.getTaxiId()!!,vehicle_number,
                userRepository.getCurrentUserID()!!, userRepository.getSupplierId()!!,vehicle_brand,vehicle_year)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if(it.infoId == "500") {
                            vehicleInfoUpdated.postValue(Event(emptyMap()))
                        }
                        else{
                            //notupdated.postValue(Event(emptyMap()))
                            vehicleInfoUpdated.postValue(Event(emptyMap()))
                            taxiIn.postValue(false)
                        }
                        doTaxiDetail()

                    },
                    {
                        handleNetworkError(it)

                    }
                )
        )

    }

    fun doTaxiDetail(){

        taxiIn.postValue(true)
        compositeDisposable.addAll(
            userRepository.doTaxidetailRequest(userRepository.getTaxiId().toString())
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        try {
                            if (it.data.seats != null) {
                                userRepository.saveVehicle_seats(it.data.seats)
                            }
                            if (it.data.vehicleBrand != null) {
                                userRepository.saveVehicleBrand(it.data.vehicleBrand)
                            }
                            if (it.data.taxiNumber != null) {
                                userRepository.saveVehicle_number(it.data.taxiNumber)
                            }
                            if (it.data.carType != null) {
                                userRepository.saveCarType(it.data.carType)
                            }

                            if (it.data.carType != null) {
                                car_type_Field.postValue(it.data.carType)
                            }
                            if (it.data.year != null) {
                                vehicle_year_Field.postValue(it.data.year)
                                userRepository.saveVehicleYear(it.data.year)
                            }
                            if (it.data.seats != null) {
                                vehicle_seats_Field.postValue(it.data.seats)
                            }
                            if (it.data.taxiNumber != null) {
                                vehicle_number_Field.postValue(it.data.taxiNumber)
                            }
                            if (it.data.vehicleBrand != null) {
                                vehicle_brand_Field.postValue(it.data.vehicleBrand)
                            }
                            taxiIn.postValue(false)
                        }catch (e: Exception){
                            taxiIn.postValue(false)
                        }

                    },
                    {
                        handleNetworkError(it)
                        taxiIn.postValue(false)

                    }
                )
        )
    }


}