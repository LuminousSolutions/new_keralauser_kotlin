package com.mongokerala.taxi.newuser.ui.vehicleinfoupdate

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.display.Toaster
import kotlinx.android.synthetic.main.activity_vehicle_info_update.*

class VehicleInfoUpdateActivity : BaseActivity<VehicleInfoUpdateViewModel>() {

    companion object {
        const val TAG = "VehicleInfoUpdateActivity"
    }

    var car_type: String = ""


    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun provideLayoutId(): Int = R.layout.activity_vehicle_info_update

    override fun setupView(savedInstanceState: Bundle?) {
        chooseSeats()
        chooseCarType()
        chooseVehicleBrand()
        chooseYear()
    }

    override fun setupObservers() {
        super.setupObservers()

        viewModel.doTaxiDetail()

        viewModel.taxiIn.observe(this, Observer {
            pb_loading_vehicle.visibility = if (it) View.VISIBLE else View.GONE
        })


        viewModel.vehicleInfoUpdated.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                startActivity(Intent(applicationContext, MainNewActivity::class.java))
                finish()
                pb_loading_vehicle.visibility = View.GONE
                Toaster.show(applicationContext,"updated")
            }
        })

        viewModel.notupdated.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                pb_loading_vehicle.visibility = View.GONE
                Toaster.show(applicationContext,"not updated")
            }
        })

        viewModel.vehicle_year_Field.observe(this, Observer {

                if ("2000" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(0)
                } else if ("2001" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(1)
                } else if ("2002" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(2)
                } else if ("2003" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(3)
                } else if ("2004" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(4)
                } else if ("2005" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(5)
                } else if ("2006" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(6)
                } else if ("2007" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(7)
                } else if ("2008" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(8)
                } else if ("2009" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(9)
                } else if ("2010" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(10)
                } else if ("2011" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(11)
                } else if ("2012" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(12)
                } else if ("2013" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(13)
                } else if ("2014" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(14)
                } else if ("2015" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(15)
                } else if ("2016" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(16)
                } else if ("2017" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(17)
                } else if ("2018" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(18)
                } else if ("2019" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(19)
                }else if ("2020" == it) {
                    spinner_vehicle_year_update_vehicle.setSelection(20)
                }

        })

        viewModel.vehicle_seats_Field.observe(this, Observer {

                if ("3" == it) {
                    spinner_vehicle_seats.setSelection(0)
                } else if ("4" == it) {
                    spinner_vehicle_seats.setSelection(1)
                } else if ("5" == it) {
                    spinner_vehicle_seats.setSelection(2)
                } else if ("6" == it) {
                    spinner_vehicle_seats.setSelection(3)
                } else if ("7" == it) {
                    spinner_vehicle_seats.setSelection(4)
                } else if ("14" == it) {
                    spinner_vehicle_seats.setSelection(5)
                }
        })

        viewModel.vehicle_number_Field.observe(this, Observer {
            if (edtText_number_plate_update_vehicle.text.toString() != it) edtText_number_plate_update_vehicle.setText(it)
        })

        viewModel.vehicle_brand_Field.observe(this, Observer {
            Log.d("TAG", "setupObservers: "+it.toString())
            if ("Hyundai" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(0)
            } else if ("Tata" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(1)
            } else if ("Skoda" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(2)
            } else if ("Toyota" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(3)
            } else if ("Renault" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(4)
            } else if ("Datsun" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(5)
            } else if ("Maruti Suzuki" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(6)
            } else if ("Fiat" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(7)
            } else if ("Mahindra" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(8)
            } else if ("Honda" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(9)
            } else if ("Ford" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(10)
            } else if ("Volkswagen" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(11)
            } else if ("Nissan" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(12)
            } else if ("Auto" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(13)
            }else if ("Chevrolet" == it) {
                spinner_vehicle_brand_update_vehicle.setSelection(14)
            }
        })

        viewModel.car_type_Field.observe(this, Observer {
            when (it) {
                "Taxi" -> {
                    spinner_car_type_update_vehicle.setSelection(0)
                }
                "Taxi6" -> {
                    spinner_car_type_update_vehicle.setSelection(2)
                }
                "Transport" -> {
                    spinner_car_type_update_vehicle.setSelection(3)
                }
                "Auto" -> {
                    spinner_car_type_update_vehicle.setSelection(4)
                }
                "Taxi4" -> {
                    spinner_car_type_update_vehicle.setSelection(1)
                }
                "Ambulance" -> {
                    spinner_car_type_update_vehicle.setSelection(6)
                }
                "Bike" -> {
                    spinner_car_type_update_vehicle.setSelection(5)
                }
            }
        })

        btn_update_vehicle_update.setOnClickListener {
//            if (edtText_number_plate_update_vehicle.text.toString()==""){
//                Toast.makeText(this, "Please enter the vechile number", Toast.LENGTH_SHORT).show()
//
//            }else{


                viewModel.onVehicleUpdate(spinner_vehicle_year_update_vehicle.selectedItem.toString(),
                    spinner_vehicle_seats.selectedItem.toString(),edtText_number_plate_update_vehicle.text.toString(),
                    spinner_vehicle_brand_update_vehicle.selectedItem.toString(),
                    spinner_car_type_update_vehicle.selectedItem.toString())
//            }
            }

        img_back_vehicle_info.setOnClickListener {
          finish()
        }
    }

    private fun chooseSeats() {
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.list_seats,
            R.layout.spinner_car_type_text_background
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_vehicle_seats.adapter = adapter
        spinner_vehicle_seats.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                val seat = spinner_vehicle_seats.selectedItem.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun chooseVehicleBrand() {
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.list_Vehicle_brands,
            R.layout.spinner_car_type_text_background
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_vehicle_brand_update_vehicle.adapter = adapter
        spinner_vehicle_brand_update_vehicle.onItemSelectedListener = object :
            OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                val vehiclebrand =
                    spinner_vehicle_brand_update_vehicle.selectedItem.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun chooseYear() {
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.list_year,
            R.layout.spinner_car_type_text_background
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_vehicle_year_update_vehicle.setAdapter(adapter)
        spinner_vehicle_year_update_vehicle.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                val year: String = spinner_vehicle_year_update_vehicle.getSelectedItem().toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        pb_loading_vehicle.visibility = View.GONE
    }

    private fun chooseCarType() {

        val adapter =
            ArrayAdapter.createFromResource(this, R.array.list_of_car_type_kerala_cabs, R.layout.spinner_car_type_text_background)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_car_type_update_vehicle.setAdapter(adapter)
        spinner_car_type_update_vehicle.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                val value: String = spinner_car_type_update_vehicle.getSelectedItem().toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }


}
