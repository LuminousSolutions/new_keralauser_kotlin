package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class UserWalletTokenResponce (
    @SerializedName("statusCode") val statusCode : Int,
    @SerializedName("status") val status : Boolean,
    @SerializedName("message") val message : String,
    @SerializedName("data") val data : UserWalletTokenData?,
    @SerializedName("jwt") val jwt : String?,
    @SerializedName("infoId") val infoId : Int
)