package com.mongokerala.taxi.newuser.data.remote.fcm

import androidx.annotation.NonNull
import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.data.remote.request.Keys
import java.io.Serializable

class FcmCustomizeNotificationData : Serializable {
    constructor() {}

    @SerializedName(Keys.RIDE_KEY_LOCAL)
    var RIDE_KEY_LOCAL: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.source)
    var source: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.COUPON)
    var coupon: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.Category)
    var Category: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.destination)
    var destination: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.taxiNumber)
    var taxiNumber: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.vehicleBrand)
    var vehicleBrand: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.action)
    var action : String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.isAndroid)
    var isAndroid: Boolean? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.message)
    var message: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.userFcmToken)
    var userFcmToken: String? = null

    @SerializedName(Keys.newDestination)
    var newDestination: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.driverFcmToken)
    var driverFcmToken: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.cancelReason)
    var cancelReason: String = ""
        get() = field
        set(value) {
            field = value
        }
    /*For driver moving (driver picku the customer for ride)*/
    @SerializedName(Keys.userId)
    var userId : String = ""
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.latitude)
    var latitude = 0.0
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.longitude)
    var longitude = 0.0
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.taxi_type)
    var taxi_type: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.carSpeed)
    var carSpeed: String? = null
        get() = field
        set(value) {
            field = value
        }
    @NonNull
    var title = "Taxi Deals"
        get() = field
        set(value) {
            field = value
        }
    /*For ride finish*/
    @SerializedName(Keys.rideDistance)
    var rideDistance: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.rideAmount)
    var rideAmount: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.taxiDetailId)
    var taxiDetailId: String = "0"
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.driverId)
    var driverId: String = ""
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.rideId)
    var rideId: String = "0"
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.userToAddress)
    var userToAddress: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.chatId)
    var chatId: String? = null
        get() = field
        set(value) {
            field = value
        }
    /*Delivery */
    @SerializedName(Keys.deliveryQty)
    var deliveryQty: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.deliveryPickupTime)
    var deliveryPickupTime: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.apxPrice)
    var apx_price: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.paymentType)
    var paymentType: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName(Keys.phoneNumber)
    var phoneNumber: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.rideDistnce)
    var rideDistnce: Double? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.driverName)
    var driverName: String? = null
        get() = field
        set(value) {
            field = value
        }

    constructor(
        requestOrder: String,
        sourceAddress: String,
        destinationAddress: String,
        userFcmToken: String?,
        userId: String?,
        isAndroid: Boolean,
        user_mobile: String?
    ) {
        action = requestOrder
        source = sourceAddress
        destination = destinationAddress
        this.userFcmToken = userFcmToken
        this.userId = userId!!
        this.isAndroid = isAndroid
        phoneNumber = user_mobile
    }

}
