package com.mongokerala.taxi.newuser.data.remote.fcm

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Result {
    @SerializedName("message_id")
    @Expose
    var messageId: String? = null

}