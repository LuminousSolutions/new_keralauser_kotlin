package com.mongokerala.taxi.newuser.ui.triphistory

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.chootdev.typefaced.TypeFacedTextView
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.data.remote.response.TripHistoryData
import com.mongokerala.taxi.newuser.ui.custom.RoundedImageView
import com.mongokerala.taxi.newuser.utils.common.AppUtils
import com.mongokerala.taxi.newuser.utils.common.Constants.DATE_FORMAT
import com.mongokerala.taxi.newuser.utils.common.Constants.TIME_FORMAT
import java.text.SimpleDateFormat
import java.util.*

class TripHistoryAdapter(
    private val context: Context,
    tripHistoryArrayList: List<TripHistoryData>,
    tripHistory: TripHistoryActivity
) :
    RecyclerView.Adapter<TripHistoryAdapter.tripHistoryViewHolder?>() {
    private val recyclerViewItemClickListener: RecyclerViewItemClickListener
    private var tripHistoryList: List<TripHistoryData> = emptyList()
    private var myTime = ""

    /**
     * @param parent   : parent ViewPgroup
     * @param viewType : viewType
     * @return ViewHolder
     *
     *
     * Inflate the Views
     * Create the each views and Hold for Reuse
     */
    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): tripHistoryViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.trip_history, parent, false)
        return tripHistoryViewHolder(view)
    }

    /**
     * @param tripHistoryViewHolder   :view Holder
     * @param position : position of each Row
     * set the values to the views
     */
    override fun onBindViewHolder(
        @NonNull tripHistoryViewHolder: tripHistoryViewHolder,
        position: Int
    )
        {
            Log.d("trip history image", "trip istory image was working :");
        val colors = context.resources.getIntArray(R.array.androidcolors)
        val color = colors[Random().nextInt(colors.size)]
        //        Picasso.with(mContext).load(deviceList.get(position).getIconUrl()).placeholder(R.mipmap.kneepain_img).into(holder.document);

//        Picasso.get()
//                .load(BuildConfig.BASE_URL+tripHistoryList.get(position).getImageInfo().getImageUrl())
////                .load("https://www.gstatic.com/webp/gallery3/1.png")
//                .placeholder(R.drawable.userr)
//                .into(tripHistoryViewHolder.customer_pic);
      /*  val image: String = tripHistoryList[position].getImageInfo().getImageUrl()
        if (image != null) {
            tripHistoryViewHolder.customer_pic.setImageBitmap(
                AppUtils.convertStringToImage(
                    tripHistoryList[position].getImageInfo().getImageUrl()
                )
            )
        }*/


        // driverWatchDogViewHolder.profile_pic.setImageURI(Uri.parse(driverWatchDogs.get(position).getDriverProfilePic()));
        tripHistoryViewHolder.total_price.text = java.lang.String.valueOf(
            tripHistoryList[position].userTotalPrice
        )
            var endMytime=tripHistoryList[position].endTIme

            if (endMytime==null){
                myTime="null"
            }else{
                myTime=endMytime
            }
            var tripdate=getRideDate(myTime)
            var triptime=getRideTime(myTime)
            tripHistoryViewHolder.date.text = tripdate
            tripHistoryViewHolder.time.text = triptime
//            tripHistoryViewHolder.date.text = getRideDate(tripHistoryList[position].endTIme)
//            tripHistoryViewHolder.time.text = getRideTime(tripHistoryList[position].endTIme)
//        tripHistoryViewHolder.time.text = getRideTime(tripHistoryList[position].endTIme)
        tripHistoryViewHolder.kilometers.text = tripHistoryList[position].km.toString() + " km"
        tripHistoryViewHolder.duration.text = tripHistoryList[position].travelTime.toString() + " min"
        tripHistoryViewHolder.source.text = tripHistoryList[position].source
        tripHistoryViewHolder.destination.text = tripHistoryList[position].destination
        tripHistoryViewHolder.relative_layout_bg.setBackgroundColor(color)
        tripHistoryViewHolder.date_icon.text = "\uf274"
        tripHistoryViewHolder.date_icon.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.black_effective
            )
        )
        tripHistoryViewHolder.Km_icon.text = "\ue84a"
        tripHistoryViewHolder.Km_icon.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.black_effective
            )
        )
        tripHistoryViewHolder.duration_icon.text = "\ue833"
        tripHistoryViewHolder.duration_icon.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.black_effective
            )
        )
        tripHistoryViewHolder.date_icon.textSize = 12f
        tripHistoryViewHolder.dollar_icon.text = "\uf155"
        tripHistoryViewHolder.dollar_icon.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.google_green
            )
        )
        tripHistoryViewHolder.source_icon.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.google_green
            )
        )
        if (tripHistoryList[position].type.equals("Taxi4")) {
            tripHistoryViewHolder.car_icon.text = "\ue928"
        } else if (tripHistoryList[position].type.equals("Taxi")) {
            tripHistoryViewHolder.car_icon.text = "\ue92d"
        } else if (tripHistoryList[position].type.equals("Taxi6")) {
            tripHistoryViewHolder.car_icon.text = "\ue92e"
        } else if (tripHistoryList[position].type.equals("Transport")) {
            tripHistoryViewHolder.car_icon.text = "\ue923"
        } else if (tripHistoryList[position].type.equals("Auto")) {
            tripHistoryViewHolder.car_icon.text = "\ue92f"
        }
        else if (tripHistoryList[position].type.equals("Ambulance")) {
            tripHistoryViewHolder.car_icon.text = "\ue92f"
        }
        else if (tripHistoryList[position].type.equals("Bike")) {
            tripHistoryViewHolder.car_icon.text = "\ue92f"
        }else {
            tripHistoryViewHolder.car_icon.text = "\ue928"
        }
        tripHistoryViewHolder.car_icon.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.red_900
            )
        )

        if (tripHistoryList[position].rideStatus!=null){
            when (tripHistoryList[position].rideStatus) {
                "CANCEL_BY_USER" -> tripHistoryViewHolder.ride_status.setImageResource(R.drawable.cancel)
                "PRECANCEL_BY_DRIVER" -> tripHistoryViewHolder.ride_status.setImageResource(R.drawable.cancel)
                "PRECANCEL_BY_USER" -> tripHistoryViewHolder.ride_status.setImageResource(R.drawable.cancel)
                else -> tripHistoryViewHolder.ride_status.setImageResource(R.drawable.paid)
            }
        }

        tripHistoryViewHolder.source_icon.text = "\uf21d"
        tripHistoryViewHolder.source_icon.textSize = 18f
        tripHistoryViewHolder.second_circle_destination.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.red_900
            )
        )
        tripHistoryViewHolder.second_circle_destination.text = "\ue82b"
    }

    private fun getRideDate(endDate: String): String {
        Log.d("triphistory adapter", "getRideDate: $endDate")

        if (endDate.equals("null")) {
            val dateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
            dateFormat.timeZone = TimeZone.getTimeZone("GMT") // setting timezone to GMT
            val tripDateTime = AppUtils.convertTIme(dateFormat.format(Date()), DATE_FORMAT)
            Log.d("triphistory adapter", "getRideDate: endtime value null$tripDateTime")
            return tripDateTime.toString()
        }

        //Long timeMillis = Long . valueOf String.valueOf(endTIme)
       var timemills=endDate
        val dateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone("GMT") // setting timezone to GMT
        val tripDateTime = AppUtils.convertTIme(dateFormat.format(Date(timemills)), DATE_FORMAT)
        return tripDateTime.toString()
    }

    private fun getRideTime(endTIme: String): String {

        Log.d("triphistory adapter", "getRideTime: $endTIme")
//        Long timeMillis = Long.valueOf(String.valueOf(endTIme));

        if (endTIme.equals("null")) {
            val dateFormat = SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
            dateFormat.timeZone = TimeZone.getTimeZone("GMT") // setting timezone to GMT
            val tripDateTime = AppUtils.convertTIme(dateFormat.format(Date()), TIME_FORMAT)
            Log.d("triphistory adapter", "getRideDate: endtime value null$tripDateTime")
            return tripDateTime.toString()
        }
        var timeMilles=endTIme
        val dateFormat = SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone("GMT") // setting timezone to GMT

        var tripDateTime = AppUtils.convertTIme(dateFormat.format(Date(timeMilles)), TIME_FORMAT)
        return tripDateTime.toString()


//        if (endTIme!=null){
//            val timeMillis = java.lang.Long.valueOf(endTIme)
//            val dateFormat =
//                    SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
//            dateFormat.timeZone = TimeZone.getTimeZone("GMT") // setting timezone to GMT
//            return AppUtils.convertTIme(dateFormat.format(Date(timeMillis)), TIME_FORMAT)!!
//
//        }
////        val timeMillis = java.lang.Long.valueOf(endTIme)
//        val dateFormat =
//            SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
//        dateFormat.timeZone = TimeZone.getTimeZone("GMT") // setting timezone to GMT
//        return "7:30PM"
    }

    override fun getItemCount(): Int {
        return tripHistoryList.size
    }

    /**
     * Create The view First Time and hold for reuse
     * View Holder for Create and Hold the view for ReUse the views instead of create again
     * Initialize the views
     */
    inner class tripHistoryViewHolder internal constructor(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var customer_pic: RoundedImageView
        var date: TextView
        var time: TextView
        var kilometers: TextView
        var duration: TextView
        var source: TextView
        var destination: TextView
        var total_price: TypeFacedTextView
        var dollar_icon: TypeFacedTextView
        var source_icon: TypeFacedTextView
        var second_circle_destination: TypeFacedTextView
        var date_icon: TypeFacedTextView
        var Km_icon: TypeFacedTextView
        var duration_icon: TypeFacedTextView
        var car_icon: TypeFacedTextView
        var ride_status: ImageView
        var relative_layout_bg: RelativeLayout
        override fun onClick(view: View) {
            recyclerViewItemClickListener.onItemClickListener(this.adapterPosition, view)
        }

        init {
            customer_pic = itemView.findViewById(R.id.customer_pic)
            total_price = itemView.findViewById(R.id.total_price)
            date = itemView.findViewById(R.id.day)
            time = itemView.findViewById(R.id.time)
            kilometers = itemView.findViewById(R.id.kilometers)
            source = itemView.findViewById(R.id.source_address)
            second_circle_destination = itemView.findViewById(R.id.second_circle_destination)
            source_icon = itemView.findViewById(R.id.source_icon)
            destination = itemView.findViewById(R.id.destination_address)
            duration = itemView.findViewById(R.id.duration)
            date_icon = itemView.findViewById(R.id.date_icon)
            Km_icon = itemView.findViewById(R.id.Km_icon)
            dollar_icon = itemView.findViewById(R.id.dollar_icon)
            car_icon = itemView.findViewById(R.id.car_icon)
            ride_status = itemView.findViewById(R.id.ride_Status)
            duration_icon = itemView.findViewById(R.id.duration_icon)
            relative_layout_bg = itemView.findViewById(R.id.relative_layout_bg)
            itemView.setOnClickListener(this)
        }
    }

    /**
     * Initialize the values
     * @param context               : context reference
     * @param tripHistoryArrayList       : data
     * @param tripHistory
     */
    init {
        tripHistoryList = tripHistoryArrayList
        recyclerViewItemClickListener = tripHistory
    }
}


