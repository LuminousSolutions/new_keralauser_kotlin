package com.mongokerala.taxi.newuser.ui.language

import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.model.Post
import com.mongokerala.taxi.newuser.data.repository.PostRepository
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.AppErrorCode.STATUS_SUCCESS_INFOID
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor

class LanguageFragmentViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository,
    private val postRepository: PostRepository,
    private val allPostList: ArrayList<Post>,
    private val paginator: PublishProcessor<Pair<String?, String?>>
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    val launchlanguageUpdateSuccess: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchlanguageUpdatefail: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val setUpLanguage: MutableLiveData<String> = MutableLiveData()


    override fun onCreate() {
    }

    fun doLanguageUpdate(language: String){

        userRepository.saveLan(language)
        compositeDisposable.addAll(
            userRepository.doLanguageUpdateRequest(userRepository.getCurrentUserID()!!,language)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        val value:String = STATUS_SUCCESS_INFOID.toString()
                        userRepository.saveLan(language)
                        if (it.infoId == value) {
                            launchlanguageUpdateSuccess.postValue(Event(emptyMap()))
                        } else {
                            launchlanguageUpdatefail.postValue(Event(emptyMap()))
                        }

                    },
                    {
                        handleNetworkError(it)
                    }
                )
        )
    }

    fun isLanguageSelected() {
        setUpLanguage.postValue(userRepository.getLan())
    }

    

}