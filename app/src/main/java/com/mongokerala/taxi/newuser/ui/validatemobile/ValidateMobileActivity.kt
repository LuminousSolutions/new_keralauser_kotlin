package com.mongokerala.taxi.newuser.ui.validatemobile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.NonNull
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.registration.RegistrationActivity
import com.mongokerala.taxi.newuser.ui.reqotp.ReqotpActivity
import com.mongokerala.taxi.newuser.utils.common.AppUtils
import com.mongokerala.taxi.newuser.utils.common.Constants
import com.mongokerala.taxi.newuser.utils.display.Toaster
import kotlinx.android.synthetic.main.activity_validate_mobile.*
import kotlinx.android.synthetic.main.activity_validate_mobile.pb_loading_main

class ValidateMobileActivity : BaseActivity<ValidateMobileViewModel>() {


    companion object {
        const val TAG = "RegistrationActivity"
    }

    private var countDownTimer: CountDownTimer? = null

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun provideLayoutId(): Int = R.layout.activity_validate_mobile
    override fun setupObservers() {
        super.setupObservers()

        viewModel.mainIn.observe(this) {
            if (it == true) {
                pb_loading_main.visibility = View.VISIBLE
            } else {
                pb_loading_main.visibility = View.GONE
            }
        }

        viewModel.launchCorrectOtp.observe(this) {
            it.getIfNotHandled()?.run {
                startActivity(Intent(applicationContext, RegistrationActivity::class.java))
                finish()
                Toaster.show(applicationContext, "Correct Otp")
            }
        }
        viewModel.launchWrongOtp.observe(this) {
            it.getIfNotHandled()?.run {
                Toaster.show(applicationContext, "Wrong Otp")
            }
        }


        viewModel.launchValidate.observe(this) {
            it.getIfNotHandled()?.run {
                startActivity(Intent(applicationContext, ValidateMobileActivity::class.java))
                finish()
                Toaster.show(applicationContext, "Otp send successfully")
            }
        }
        viewModel.launchValidateUsedNumber.observe(this) {
            it.getIfNotHandled()?.run {
                Toaster.show(applicationContext, "This number is already in use")
            }
        }
    }


    override fun setupView(savedInstanceState: Bundle?) {
        val textCount="00:30"
        txt_count_otp.visibility = View.VISIBLE
        otp_timer.visibility = View.VISIBLE
        lay_re_otp.visibility = View.GONE
        txt_count_otp.text = textCount
        countDownTimer = object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val countOtp=millisUntilFinished / 1000
                txt_count_otp.text = countOtp.toString()
            }

            override fun onFinish() {
                txt_count_otp.visibility = View.GONE
                otp_timer.visibility = View.GONE
                lay_re_otp.visibility = View.VISIBLE
            }
        }.start()
        pin_first_edittext.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(@NonNull s: Editable) {
                if (s.toString().isNotEmpty()) {
                    pin_first_edittext.clearFocus()
                    pin_second_edittext.requestFocus()
                }
            }
        })
        pin_second_edittext.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(@NonNull s: Editable) {
                if (s.toString().isNotEmpty()) {
                    pin_second_edittext.clearFocus()
                    pin_third_edittext.requestFocus()
                } else {
                    pin_first_edittext.requestFocus()
                }
            }
        })

        pin_third_edittext.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(@NonNull s: Editable) {
                if (s.toString().isNotEmpty()) {
                    pin_third_edittext.clearFocus()
                    pin_forth_edittext.requestFocus()
                } else {
                    pin_second_edittext.requestFocus()
                }
            }
        })


        pin_forth_edittext.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(@NonNull s: Editable) {
                if (s.toString().isNotEmpty()) {
                    pin_forth_edittext.clearFocus()
                    pin_forth_edittext.requestFocus()
                    hideKeyboard()
                } else {
                    pin_third_edittext.requestFocus()
                }
            }
        })

        X_clear.setOnClickListener {
            clearTextSetFocus()
        }
    }

    private fun clearTextSetFocus(){
        if(pin_first_edittext.text != null){
            pin_first_edittext.text!!.clear()
        }
        if (pin_second_edittext.text != null){
            pin_second_edittext.text!!.clear()
        }
        if (pin_third_edittext.text != null){
            pin_third_edittext.text!!.clear()
        }
        if (pin_forth_edittext.text != null){
            pin_forth_edittext.text!!.clear()
        }
        pin_second_edittext.clearFocus()
        pin_third_edittext.clearFocus()
        pin_forth_edittext.clearFocus()
        pin_first_edittext.requestFocus()
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun validateOtpOnclick(view: View) {

        if (AppUtils.isNetworkConnected(this)){
        val token: String =
            pin_first_edittext.text.toString() + pin_second_edittext.text.toString() + pin_third_edittext.text
                .toString() + pin_forth_edittext.text.toString()
        viewModel.onValidateOtpRegistration(token)
        }
    }

    fun changeMobileNumberClick(view: View) {
        val reqOtp= Intent(this,ReqotpActivity::class.java)
        reqOtp.putExtra(Constants.mobileNumberValidation,
            Constants.mobileNumberValidation)
        startActivity(reqOtp)
        finish()
    }

    fun resendOtpClick(view: View) {
        viewModel.onSubmitClick()
    }

}
