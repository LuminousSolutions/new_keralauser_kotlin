package com.mongokerala.taxi.newuser.paymentgateway.adyan;

import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.PaymentVerifyResponse;

/**
 * Created by Akhi007 on 28-05-2019.
 */
public interface VerifyAdyenPaymentListener {

    void onPaymentVerifyRequestSuccess(OrderDTO orderDTO, PaymentVerifyResponse paymentVerifyResponse);

    void onPaymentVerifyRequestFailed(OrderDTO orderDTO, ApiResponse apiResponse);

    void onPaymentVerifyRequestTimeOut(OrderDTO orderDTO);
}
