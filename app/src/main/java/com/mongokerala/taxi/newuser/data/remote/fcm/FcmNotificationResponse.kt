package com.mongokerala.taxi.newuser.data.remote.fcm

import androidx.annotation.Nullable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class FcmNotificationResponse (
    @SerializedName("multicast_id")
    @Expose
    var multicastId: Long? = null,
    @SerializedName("success")
    @Expose
    var success: Int? = null,
    @SerializedName("failure")
    @Expose
    var failure: Int? = null,
    @SerializedName("canonical_ids")
    @Expose
    var canonicalIds: Int? = null,
    @get:Nullable
    @Nullable
    @SerializedName("results")
    @Expose
    var results: List<Result>? = null

)
