package com.mongokerala.taxi.newuser.data.remote.fcm

import com.google.gson.annotations.SerializedName

class FcmNotificationIosPriority(
    fcmNotificationIosHeaders: FcmNotificationIosHeaders,
    fcmNotificationIosPayload: FcmNotificationIosPayload
) {
    @SerializedName("headers")
    var notificationIosHeaders: FcmNotificationIosHeaders

    @SerializedName("payload")
    var notificationIosPayload: FcmNotificationIosPayload

    init {
        notificationIosHeaders = fcmNotificationIosHeaders
        notificationIosPayload = fcmNotificationIosPayload
    }
}