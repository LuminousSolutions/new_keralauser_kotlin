package com.mongokerala.taxi.newuser.ui.chat

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import java.text.SimpleDateFormat
import java.util.*

 class ChatViewmodel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {
    val update_data: MutableLiveData<Boolean> = MutableLiveData()
    override fun onCreate() {

    }

     fun  getSupplier ():String {
         return userRepository.getSupplierId().toString()
     }

    fun send_msg(chat_msg:String){
        val today = Date()
        val format = SimpleDateFormat("dd-MM-yyyy hh:mm:ss a")
        val dateToStr: String = format.format(today)
        val db = FirebaseFirestore.getInstance()
        val user: MutableMap<String, Any> =
            HashMap()
        Log.d("RAZZZZ",userRepository.getSupplierId().toString() )
        user["MSG"] = chat_msg
        user["NAME"] =  userRepository.getName().toString()
        user["USER"] = userRepository.getCurrentUserID().toString()
        user["DATE"] = dateToStr
        db.collection(userRepository.getSupplierId().toString())
            .document()
            .set(user)
            .addOnSuccessListener {
                update_data.value=true
            }
            .addOnFailureListener { }
    }
    fun send_Booking(pickup_loc:String,drop_loc:String,cus_num:String,total_price:String,is_meter:Boolean){
        onRideOtpToUser(pickup_loc,drop_loc,total_price,"0",cus_num)
        val today = Date()
        val format = SimpleDateFormat("dd-MM-yyyy hh:mm:ss a")
        val dateToStr: String = format.format(today)
        val db = FirebaseFirestore.getInstance()
        val user: MutableMap<String, Any> =
            HashMap()
        if(is_meter){
            user["MSG"] =
                "Booking Alert:" + " Pick UP :" + pickup_loc +" Drop : " + drop_loc + " Mobile num :" + cus_num + " price :" + "Meter Price"

        }else {
            user["MSG"] =
                "Booking Alert:" + " Pick UP :" + pickup_loc + " Drop : " + drop_loc + " Mobile num :" + cus_num + " price :" + total_price
        }
        user["USER"] = userRepository.getCurrentUserID().toString()
        user["NAME"] = userRepository.getName().toString()
        user["DATE"] = dateToStr
        db.collection(userRepository.getSupplierId().toString())
            .document()
            .set(user)
            .addOnSuccessListener {
                update_data.value=true
            }
            .addOnFailureListener { }
    }

     fun onRideOtpToUser(sourceaddress: String,destination:String,total_price: String,kmvalue: String,cusT_number: String){

             compositeDisposable.addAll(
                     userRepository.doUserOfflineSmsRequestToUser("+91",
                         userRepository.getCurrentUserID()!!,
                         userRepository.getName()!!,
                         sourceaddress,"0",destination ,total_price,
                         userRepository.getVehiclenumber()!!, userRepository.getMobileNUmber()!!,
                         userRepository.getDomain()!!,"TAXI","user",kmvalue,cusT_number
                     )
                         .subscribeOn(schedulerProvider.io())
                         .subscribe(
                             {
                                 if (it.infoId.equals("500")) {
                                     //launchCorrectOtptoUser.postValue(Event(emptyMap()))

                                 } else if (it.infoId.equals("701")) {
                                     //launchWOtpnotsend.postValue(Event(emptyMap()))

                                 } else {
                                     //launchWOtpnotsend.postValue(Event(emptyMap()))
                                 }

                             },
                             {
                                 handleNetworkError(it)
                             }
                         )
             )

     }

}