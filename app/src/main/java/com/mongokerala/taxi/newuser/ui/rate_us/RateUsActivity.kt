package com.mongokerala.taxi.newuser.ui.rate_us

import android.content.Intent
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.lifecycle.Observer
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.display.Toaster
import kotlinx.android.synthetic.main.activity_rate_us.*
import kotlinx.android.synthetic.main.activity_rate_us.btn_submit

class RateUsActivity : BaseActivity<RateUsViewModel>() {

    companion object {
        const val TAG = "RateUsActivity"
    }

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun provideLayoutId(): Int = R.layout.activity_rate_us

    override fun setupView(savedInstanceState: Bundle?) {

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun setupObservers() {
        super.setupObservers()

        view_rating_message.visibility = View.GONE
        view_play_store_rating.visibility = View.GONE

        val stars = rating_bar_feedback.progressDrawable as LayerDrawable

        stars.getDrawable(2).setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
            getColor(R.color.yellow), BlendModeCompat.SRC_ATOP))
        stars.getDrawable(0).setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
            getColor(R.color.shadow), BlendModeCompat.SRC_ATOP))
        stars.getDrawable(1).setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
            getColor(R.color.shadow), BlendModeCompat.SRC_ATOP))

        viewModel.feedbacksuccess.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                finish()
                Toaster.show(applicationContext,getString(R.string.Thank_you_for_sharing_your_feedback))
            }
        })

        viewModel.rateIn.observe(this, Observer {
            pb_loading_rateUs.visibility = if (it) View.VISIBLE else View.GONE
        })

        viewModel.showPlayStoreRatingView.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                view_play_store_rating.setVisibility(View.VISIBLE)
                btn_submit.visibility = View.GONE
                rating_bar_feedback.setIsIndicator(true)

            }
        })

        viewModel.showRatingMessageView.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                view_rating_message.setVisibility(View.VISIBLE)
            }
        })

        viewModel.feedback_validation.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                Toaster.show(applicationContext,getString(R.string.feedback_validation))
            }
        })

        viewModel.rating_not_provided_error.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                Toaster.show(applicationContext,getString(R.string.rating_not_provided_error))
            }
        })

        btn_submit.setOnClickListener {
             viewModel.doRateUs(rating_bar_feedback.rating,et_message.text.toString())
        }

    }

    fun rate_on_play_store(view: View) {

        startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(
                    getString(R.string.app_market_link)
//                    getString(R.string.app_market_link_driver)
                )
            )
        )
        finish()
    }


    fun On_btn_later_click(view: View) {
     finish()
    }

    fun onRateUsBackClick(view: View) {
        finish()
    }


}
