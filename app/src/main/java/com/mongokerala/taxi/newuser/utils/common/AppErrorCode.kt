package com.mongokerala.taxi.newuser.utils.common

object AppErrorCode{

    val STATUS_SUCCESS_INFOID = 500
    // val STATUS_SUCCESS_INFOID = 0

    //doForgetPasswordApiCall
    val EMAIL_NOT_EXISTING = 401

    //doOtpRequest_forgetpassword
    val NUMBER_IS_NOT_EXISTING = 501

    //doIndiaForgotSms
    val OTP_FAILED = 601
    //val OTP_FAILED = 850

    //doServerLoginApiCall
    val PASSWORD_WRONG_INFOID = 204
    val EMAIL_NOT_EXISTING_LOGIN = 206
    val PASSWORD_WRONG2_INFOID = 201
    val EMAIL_OR_PASSWORD_NOT_EXISTING = 203
    val USER_WRONG = 205

    //dosearchTaxi
    val TAXI_SEARCH = 101

    //doLogoutApiCall
    val USER_LOGOUT_ERROR = 2000
    val USER_NOT_EXISTING2 = 800

    //doOtpRequest
    val THIS_IS_NEW_NUMBER_FAILED = 102

    //mob_Number_Update
    val STATUS_SUCCESS_INFOID2 = 850 //doUpdateReview,doProfileUpdateApiCall,mob_Number_Update

    val NUMBER_IS_ALREADY_EXISTING = 701

    //doSignUPRequest
    val EMAIL_IS_EXISTING = 700

    //doValidateOtpRequest
    val WRONG_TOKEN = 701

    //doOtpRequest
    val NUMBER_IS_NOT_EXISTING_TWO = 850

    //doFCMToken
    val NO_ONE_SIGNAL_VALUE = 850

    //updatePriceAndDistance
    val KM_IS_VERY_HIGH = 850

    //createRideRequest
    val USER_NOT_EXISTING = 850

    //doValidateUpdatedMobileOtpRequest
    val WRONG_TOKEN2 = 400

    //doVehicleInfoUpdateApiCall
    val VEHICLE_INFO_UPDATE_ERROR = 600


    val CITY_WRONG_CODE = 700
    val ROLE_NOT_EXISTING = 700
    val DEVICE_ID_EXISTING = 700
    val PHONENUMBER_IS_EXISTING = 700

    val NO_TAG_CODE = 700
    val CAR_TYPE_NOT_EXISTING = 700
    val NO_SUPPLIER_ID_WHEN_ADD_TAXI = 701

    val NUMBER_IS_ALREADY_EXISTING2 = 101

    val FAILURE = 201
    val PHONE_NUMBER_IS_EXISTING = 203

    val EMAIL_NOR_PASSWORD_NOT_EXISTING = 302

    val FORGOTPASSWORD_RETRIVEL = 401
    val PASSWORD_NOT_HAS_BEEN_CHANGED_SUCCESSFULL = 501
    val USER_TRIP_LIST_NO_USER = 1500
    val LANGUAGE_UPDATE_ERROR = 3500
    val FCM_STATUS_UPDATE_ERROR = 4500
    val REVIEW_CREATED_ERROR = 5500
    val RIDE_IS_NOT_EXISTING = 5501
    //mobile verification
    val PHONENUMBER_UPDATE_FAIL = 6000
    //vechile UPDATE
    val DRIVER_ID_NOT_EXISTING = 6065
    val NO_SUPPLIER_EXISTING = 6060
    val NO_TAXIDETAILS_ID = 6061
    val UPDATE_TAXI_INFO = 6062
    val NO_SUPPLIER_NOT_MATCHING = 8063
    val PHONENUMBER_IS_NOT_VALID = 102
}