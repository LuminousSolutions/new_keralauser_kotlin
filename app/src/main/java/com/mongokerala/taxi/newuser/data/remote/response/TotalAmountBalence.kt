package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class TotalAmountBalence (
        @SerializedName("credit") val credit : Double,
        @SerializedName("paymentType") val paymentType : String,
        @SerializedName("id") val id : String,
        @SerializedName("driverId") val driverId : String,
        @SerializedName("debit") val debit : Double,
        @SerializedName("balance") val balance : Double,
        @SerializedName("driverName") val driverName : String
)