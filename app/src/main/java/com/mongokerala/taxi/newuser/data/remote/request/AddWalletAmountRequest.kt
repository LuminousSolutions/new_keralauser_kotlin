package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AddWalletAmountRequest(
        @field:SerializedName(Keys.balance) @field:Expose var balance: Int,
        @field:SerializedName(Keys.token) @field:Expose var token: String,
        @field:SerializedName(Keys.userId) @field:Expose var userId: String
) {

    @Expose
    @SerializedName(Keys.credit)
    var credit: Int = 0

    @Expose
    @SerializedName(Keys.currentDate)
    var currentDate: String = "2021-08-07T12:32:04.339Z"

    @Expose
    @SerializedName(Keys.debit)
    var debit: Int = 0

    @Expose
    @SerializedName(Keys.domain)
    var domain: String = "KERALACABS"

    @Expose
    @SerializedName(Keys.id)
    var id: String = ""

    @Expose
    @SerializedName(Keys.percentage)
    var percentage: Int = 0

}