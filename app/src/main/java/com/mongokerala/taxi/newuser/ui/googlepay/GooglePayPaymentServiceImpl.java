package com.mongokerala.taxi.newuser.ui.googlepay;

import android.util.Log;

import com.google.gson.Gson;
import com.mongokerala.taxi.newuser.paymentgateway.model.ApiResponse;
import com.mongokerala.taxi.newuser.paymentgateway.util.PaymentUtils;
import com.mongokerala.taxi.newuser.ui.payment.RequestHandler;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Akhi007 on 05-08-2019.
 */
public class GooglePayPaymentServiceImpl implements GooglePayPaymentService {
    private static final String TAG = GooglePayPaymentServiceImpl.class.getSimpleName();

    @Override
    public void createGooglePayOrderPayment(final CreateGoogleOrderPaymentListener orderPaymentListener, final GoogleOrderDTO orderDTO) {
        GooglePayPostRequestSender postRequestSender = RequestHandler.getRetrofitClient(GooglePayPostRequestSender.BASE_URL).create(GooglePayPostRequestSender.class);
        Log.i(TAG, "createGooglePayOrderPayment Request: "+new Gson().toJson(orderDTO));
        Call<GoogleOrderDTO> orderResponseDTO = postRequestSender.createGooglePayOrderPayment(orderDTO);
        orderResponseDTO.enqueue(new Callback<GoogleOrderDTO>() {
            @Override
            public void onResponse(Call<GoogleOrderDTO> call, Response<GoogleOrderDTO> response) {
                Log.i(TAG, "Response: "+ new Gson().toJson(response.body()));
                if(response.isSuccessful() && null != response.body()){
                    orderPaymentListener.onRequestSuccess(orderDTO, response.body());
                }else if(null == response.body() && null != response.errorBody()){
                    Log.i(TAG, "Error Response: "+ new Gson().toJson(response.errorBody()));
                    ApiResponse apiResponse = PaymentUtils.convertApiErrorBodyToApiResponse(response.errorBody());
                    orderPaymentListener.onRequestFailed(orderDTO, apiResponse);
                } else {
                    //TODO write your own error code and message
                    orderPaymentListener.onRequestFailed(orderDTO, new ApiResponse("FAILED", "Something went wrong. Please try after sometime!"));
                }
            }

            @Override
            public void onFailure(Call<GoogleOrderDTO> call, Throwable t) {
                if( t instanceof SocketTimeoutException){
                    orderPaymentListener.onRequestTimeOut(orderDTO);
                }
            }
        });
    }



}
