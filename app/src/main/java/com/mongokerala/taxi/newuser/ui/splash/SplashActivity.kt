package com.mongokerala.taxi.newuser.ui.splash

import android.content.Intent
import android.content.IntentSender
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.mongokerala.taxi.newuser.BuildConfig
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.data.remote.fcm.FcmCustomizeNotificationData
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity
import com.mongokerala.taxi.newuser.ui.tour.TourActivity
import com.mongokerala.taxi.newuser.utils.common.Constants.CUSTOM_NOTIFICATION_DATA
import com.mongokerala.taxi.newuser.utils.common.Constants.DRIVER_CANCEL_RIDE
import com.mongokerala.taxi.newuser.utils.common.Constants.FIRE_STORE_STATUS
import com.mongokerala.taxi.newuser.utils.common.Constants.RIDE_FINISH
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.*


class SplashActivity : BaseActivity<SplashViewModel>() {
    val db = FirebaseFirestore.getInstance()
    private var rideStatus: String = ""
    private var destination: String = ""
    private var totalAmount: String = ""
    private var rideDistance: String = ""

    companion object {
        const val TAG = "SplashActivity"
        var appUpdateManager: AppUpdateManager? = null
        private const val updateRequestCode = 1991
        var installStateUpdatedListener: InstallStateUpdatedListener? = null
    }

    override fun provideLayoutId(): Int = R.layout.activity_splash
    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun setupView(savedInstanceState: Bundle?) {
        checkVersion()
        setupBaseUrl()
        appUpdateManager = AppUpdateManagerFactory.create(applicationContext)
        checkUpdate()
    }

    private fun checkVersion() {
        val versionName = BuildConfig.VERSION_NAME
        val versionText = "VERSION  - $versionName"
        version.text = versionText
    }

    private fun checkUpdate() {
        val appUpdateInfoTask: Task<AppUpdateInfo> = appUpdateManager!!.appUpdateInfo
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE)
            ) {
                startUpdateFlow(appUpdateInfo)
            } else if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                startUpdateFlow(appUpdateInfo)
            } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                popupCompleteUpdate()
            } else {
                val timeOut: Long = 6000
                Handler().postDelayed({
                    viewModel.decideNextActivity()
                }, timeOut)
            }
        }
        appUpdateInfoTask.addOnFailureListener {
            val timeOut: Long = 6000
            Handler().postDelayed({
                viewModel.decideNextActivity()
            }, timeOut)
        }

    }

    private fun startUpdateFlow(appUpdateInfo: AppUpdateInfo) {
        try {
            appUpdateManager!!.startUpdateFlowForResult(
                appUpdateInfo,
                IMMEDIATE,
                this,
                updateRequestCode
            )
        } catch (e: IntentSender.SendIntentException) {
            e.printStackTrace()
        }
    }

    private fun removeInstallStateUpdateListener() {
        if (appUpdateManager != null) {
            installStateUpdatedListener?.let { appUpdateManager?.unregisterListener(it) }
        }
    }

    private fun popupCompleteUpdate() {

        Snackbar.make(
            findViewById(android.R.id.content),
            "New app is ready!",
            Snackbar.LENGTH_INDEFINITE
        ).apply {
            setAction("Install") {
                if (appUpdateManager != null) {
                    appUpdateManager!!.completeUpdate()
                }
            }
            setActionTextColor(resources.getColor(R.color.red_900))
            show()
        }
    }


    private fun setupBaseUrl() {
        FirebaseFirestore.getInstance().collection("base_url").document("base_url").get()
            .addOnSuccessListener { documentSnapshot ->
                val url: String = documentSnapshot.getString("base_url").toString()
                Prefs.putString("base_url", url)
            }.addOnFailureListener {

            }
    }

    private fun getStatusFireBase(status: String?, endDestination: String?) {
        val intent = Intent(this@SplashActivity, MainNewActivity::class.java)
        if ("DRIVER_CANCEL_RIDE".equals(status, ignoreCase = true)) {
            val data = FcmCustomizeNotificationData()
            data.isAndroid = true
            data.action = DRIVER_CANCEL_RIDE
            data.destination = endDestination
            data.cancelReason = "Our driver cancelled the ride can you please try again later."
            intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
        }
        intent.putExtra(FIRE_STORE_STATUS, status)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    private fun rideFinishStatusFireBase(price: String?, km: String?, endDestination: String?) {
        val intent = Intent(this@SplashActivity, MainNewActivity::class.java)
        val data = FcmCustomizeNotificationData()
        data.isAndroid = true
        data.action = RIDE_FINISH
        data.rideDistance = km
        data.destination = endDestination
        data.rideAmount = price
        data.cancelReason = getString(R.string.RIDE_Finished_please_give_your_rating)
        intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
        intent.putExtra(FIRE_STORE_STATUS, RIDE_FINISH)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    override fun setupObservers() {
        super.setupObservers()

        viewModel.launchMain.observe(this) {
            startActivity(Intent(applicationContext, MainNewActivity::class.java))
            finish()
        }
        viewModel.launchTour.observe(this) {
            startActivity(Intent(applicationContext, TourActivity::class.java))
            finish()

        }
        viewModel.setUpLanguage.observe(this) {
            val locale = Locale(it)
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            this.resources.updateConfiguration(
                config,
                this.resources.displayMetrics
            )

        }
        viewModel.rideStatus.observe(this) {
            rideStatus = it
        }
        viewModel.destination.observe(this) {
            destination = it
        }
        viewModel.userTotalPrice.observe(this) {
            totalAmount = it
        }
        viewModel.rideDistance.observe(this) {
            rideDistance = it
        }
        viewModel.fireStore.observe(this) {
            getStatusFireBase(rideStatus, destination)
        }
        viewModel.rideFinishFireStore.observe(this) {
            rideFinishStatusFireBase(totalAmount, rideDistance, destination)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == updateRequestCode) {
            when (resultCode) {
                RESULT_CANCELED -> {
                    showToastMsg("Update canceled by user! Result Code: $resultCode")
                    finish()
                }
                RESULT_OK -> {
                    showToastMsg("Update success! Result Code: $resultCode")
                }
                else -> {
                    showToastMsg("Update Failed! Result Code: $resultCode")
                    checkUpdate()
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        removeInstallStateUpdateListener()
    }

    private  fun  showToastMsg(msg: String){
        Toast.makeText(
            applicationContext,
            msg,
            Toast.LENGTH_LONG
        ).show()
    }


}




