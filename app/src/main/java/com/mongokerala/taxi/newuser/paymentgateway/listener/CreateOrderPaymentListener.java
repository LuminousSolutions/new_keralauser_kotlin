package com.mongokerala.taxi.newuser.paymentgateway.listener;

import com.mongokerala.taxi.newuser.paymentgateway.StripePaymentRequestOrder;
import com.mongokerala.taxi.newuser.paymentgateway.model.ApiResponse;
import com.mongokerala.taxi.newuser.paymentgateway.model.StripeResponse;

/**
 * Created by Akhi007 on 23-01-2019.
 */
public interface CreateOrderPaymentListener {
    void onRequestSuccess(StripePaymentRequestOrder orderRequestDTO, StripeResponse orderResponseDTO);

    void onRequestFailed(StripePaymentRequestOrder orderDTO, ApiResponse apiResponse);

    void onRequestTimeOut(StripePaymentRequestOrder orderDTO);
}
