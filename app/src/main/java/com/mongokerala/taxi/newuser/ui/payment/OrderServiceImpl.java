package com.mongokerala.taxi.newuser.ui.payment;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public class OrderServiceImpl implements OrderService {
    private static final String TAG = OrderServiceImpl.class.getSimpleName();

    @Override
    public void createOrderApiCall(@NonNull OrderCreateListener orderCreateListener, OrderDTO orderDTO) {
        PostRequestSender postRequestSender = RequestHandler.getRetrofitClient(PostRequestSender.BASE_URL).create(PostRequestSender.class);
        Log.i(TAG, "createOrderApiCall Request: "+new Gson().toJson(orderDTO));
        Call<OrderDTO> orderResponseDTO = postRequestSender.createOrder(orderDTO);
        orderResponseDTO.enqueue(new Callback<OrderDTO>() {
            @Override
            public void onResponse(Call<OrderDTO> call, Response<OrderDTO> response) {
                Log.i(TAG, "createOrderApiCall Response: "+ response.body());
                if(response.isSuccessful() && null != response.body()){
                    orderCreateListener.onRequestSuccess(orderDTO, response.body());
                }else if(null == response.body() && null != response.errorBody()){
                    //orderCreateListener.onRequestFailed(orderDTO, apiResponse);
                }
            }

            @Override
            public void onFailure(Call<OrderDTO> call, Throwable t) {
                if( t instanceof SocketTimeoutException){
                    orderCreateListener.onRequestTimeOut(orderDTO);
                }
            }
        });
    }

    @Override
    public void updateOrderPaymentApiCall(@NonNull OrderPaymentUpdateListener orderPaymentUpdateListener, OrderDTO orderDTO) {
        PostRequestSender postRequestSender = RequestHandler.getRetrofitClient(PostRequestSender.BASE_URL).create(PostRequestSender.class);
        Log.i(TAG, "updateOrderPaymentApiCall Request: "+new Gson().toJson(orderDTO));
        Call<ApiResponse> orderResponseDTO = postRequestSender.updateOrderPayment(orderDTO);
        orderResponseDTO.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                Log.i(TAG, "updateOrderPaymentApiCall Response: "+ response.body());
                if(response.isSuccessful() && null != response.body()){
                    orderPaymentUpdateListener.onRequestSuccess(orderDTO, response.body());
                }else if(null == response.body() && null != response.errorBody()){
                    //orderPaymentUpdateListener.onRequestFailed(orderDTO, apiResponse);
                }
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                if( t instanceof SocketTimeoutException){
                    orderPaymentUpdateListener.onRequestTimeOut(orderDTO);
                }
            }
        });
    }
}
