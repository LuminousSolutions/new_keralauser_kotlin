package com.mongokerala.taxi.newuser.di.module

import androidx.lifecycle.LifecycleRegistry
import com.mongokerala.taxi.newuser.di.ViewModelScope
import com.mongokerala.taxi.newuser.ui.base.BaseItemViewHolder
import dagger.Module
import dagger.Provides

@Module
class ViewHolderModule(private val viewHolder: BaseItemViewHolder<*, *>) {

    @Provides
    @ViewModelScope
    fun provideLifecycleRegistry(): LifecycleRegistry = LifecycleRegistry(viewHolder)
}