package com.mongokerala.taxi.newuser.paymentgateway.enums;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public enum PaymentStatus {

    INITIATED, PENDING, SUCCESS, FAILED;

}
