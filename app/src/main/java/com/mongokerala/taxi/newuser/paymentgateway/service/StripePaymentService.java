package com.mongokerala.taxi.newuser.paymentgateway.service;

import com.mongokerala.taxi.newuser.paymentgateway.StripePaymentRequestOrder;
import com.mongokerala.taxi.newuser.paymentgateway.listener.CreateOrderPaymentListener;

/**
 * Created by Akhi007 on 23-01-2019.
 */
public interface StripePaymentService {

    void createStripeOrderPayment(CreateOrderPaymentListener orderPaymentListener, StripePaymentRequestOrder orderDTO);
}
