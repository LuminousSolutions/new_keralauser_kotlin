package com.mongokerala.taxi.newuser.ui.chat

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.data.remote.response.ChatTypeDTO
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.triphistory.RecyclerViewItemClickListener
import com.mongokerala.taxi.newuser.utils.common.KeyboardUtils
import com.mongokerala.taxi.newuser.utils.display.Toaster
import kotlinx.android.synthetic.main.activity_chat.*
import kotlin.collections.ArrayList

public class ChatActivity : BaseActivity<ChatViewmodel>(), RecyclerViewItemClickListener {

    override fun provideLayoutId(): Int = R.layout.activity_chat
    private var chat_List: ArrayList<ChatTypeDTO>? = null
    private var is_meter:Boolean = false

    override fun injectDependencies(activityComponent: ActivityComponent) {

        activityComponent.inject(this)
    }

    override fun setupView(savedInstanceState: Bundle?) {
        load_msg()
        radioGroup1.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                val radio: RadioButton = findViewById(checkedId)
                is_meter = radio.text=="Meter"
            })
        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection(viewModel.getSupplier())
        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {

                return@addSnapshotListener
            }

            if (snapshot != null && !snapshot.isEmpty) {
                load_msg()
            }
        }
    }

    override fun setupObservers() {
        super.setupObservers()

        viewModel.update_data.observe(this, Observer {

          //  load_msg()

        })
    }

    fun load_msg(){

        chat_List = ArrayList()
        chat_List!!.clear()

        val db = FirebaseFirestore.getInstance()

        db.collection(viewModel.getSupplier())
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    Log.d("data", "${document.id} => ${document.data}")
                    var chat_datalist = ChatTypeDTO(document.data.get("DATE").toString(),document.data.get("MSG").toString(),document.data.get("USER").toString(),document.data.get("NAME").toString())

                    chat_List!!.add(chat_datalist)

                }
                chat_List!!.sort();
                //Collections.sort(chat_List, kotlin.Comparator() )
                val chatAdapter = ChatAdapter(
                    baseContext,
                    chat_List!!, this
                )

                val recycler_view: RecyclerView = findViewById(R.id.messageRecyclerView)
                val mLayoutManager = LinearLayoutManager(this)
                recycler_view.setLayoutManager(mLayoutManager)
                chatAdapter.notifyDataSetChanged()
                recycler_view.setAdapter(chatAdapter)
                recycler_view.scrollToPosition(chatAdapter.getItemCount() - 1);
            }
            .addOnFailureListener { exception ->
                Log.d("data", "Error getting documents: ", exception)
            }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun send_chat(view: View) {

        if (messageEditText?.text.toString().isEmpty()) {
            Toaster.show(this,"Please enter your msg")
        }else{
            viewModel.send_msg(messageEditText?.text.toString())
            messageEditText?.text?.clear()
            KeyboardUtils.hideSoftInput(this)
        }
    }

    override fun onItemClickListener(pos: Int, v: View?) {

    }

    fun submit_detail(view: View) {
        if(is_meter&&!et_pickup_loc?.text.isNullOrEmpty()&&!et_drop_loc?.text.isNullOrEmpty()&&!et_cus_num?.text.isNullOrEmpty()) {
            viewModel.send_Booking(
                et_pickup_loc?.text.toString(),
                et_drop_loc?.text.toString(),
                et_cus_num?.text.toString(),
                et_total_price?.text.toString(),
                is_meter
            )

        linearLayout_booking.visibility =  View.GONE
        KeyboardUtils.hideSoftInput(this)
        et_drop_loc?.text?.clear()
        et_pickup_loc?.text?.clear()
        et_total_price?.text?.clear()
        et_cus_num?.text?.clear()
        }else if(!is_meter&&!et_pickup_loc?.text.isNullOrEmpty()&&!et_drop_loc?.text.isNullOrEmpty()&&!et_cus_num?.text.isNullOrEmpty()&&!et_total_price?.text.isNullOrEmpty()){

            viewModel.send_Booking(
                et_pickup_loc?.text.toString(),
                et_drop_loc?.text.toString(),
                et_cus_num?.text.toString(),
                et_total_price?.text.toString(),
                is_meter
            )
            linearLayout_booking.visibility =  View.GONE
            KeyboardUtils.hideSoftInput(this)
            et_drop_loc?.text?.clear()
            et_pickup_loc?.text?.clear()
            et_total_price?.text?.clear()
            et_cus_num?.text?.clear()
        }else{
            Toaster.show(this,"Please Enter proper details")
        }
    }
    fun open_booking(view: View) {

        linearLayout_booking.visibility =  View.VISIBLE

    }


}



