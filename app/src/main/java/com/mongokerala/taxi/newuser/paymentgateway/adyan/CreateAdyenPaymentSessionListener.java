package com.mongokerala.taxi.newuser.paymentgateway.adyan;

import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.AdyenPaymentSessionResponse;

/**
 * Created by Akhi007 on 25-05-2019.
 */
public interface CreateAdyenPaymentSessionListener {

    void onRequestSuccess(OrderDTO orderDTO, AdyenPaymentSessionResponse paymentSessionResponse);

    void onRequestFailed(OrderDTO orderDTO, ApiResponse apiResponse);

    void onRequestTimeOut(OrderDTO orderDTO);
}
