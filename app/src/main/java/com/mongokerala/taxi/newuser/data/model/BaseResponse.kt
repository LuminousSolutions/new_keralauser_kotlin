package com.mongokerala.taxi.newuser.data.model

import com.google.gson.annotations.SerializedName

data class BaseResponse (


        @SerializedName("statusCode") val statusCode : Int,
        @SerializedName("status") val status : Boolean,
        @SerializedName("message") val message : String,
        @SerializedName("data") val data : String,
        @SerializedName("jwt") val jwt : String,
        @SerializedName("infoId") val infoId : String
)