package com.mongokerala.taxi.newuser.utils.common

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Handler
import android.util.Log
import com.mongokerala.taxi.newuser.utils.display.Toaster

class NetworkUtils private constructor() : BroadcastReceiver() {
    var mContext: Context? = null
    override fun onReceive(context: Context, intent: Intent) {
        mContext = context
        val status: String = NetworkState.getConnectivityStatusString(context)!!
        if (status.equals("wifi", ignoreCase = true)) {
            //Toast.makeText(context, "wifikvel", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(context, "Networkvel", Toast.LENGTH_SHORT).show();
        }
        if (status == NetworkState.NOT_CONNECT) {
            Log.d("CONNECTION STATUS", " ____   DISCONNECTED____")
            Toaster.show(context,"DISCONNECTED____")

        } else {
            Log.d("CONNECTION STATUS", " ____ CONNECTED____")
            Toaster.show(context,"DISCONNECTED____")

            if (status.equals("mobile", ignoreCase = true) || status.equals(
                    "wifi",
                    ignoreCase = true
                )
            ) {
                Handler().postDelayed({ }, 1500)
            }
        }
    }

    companion object {
        fun isNetworkConnected(context: Context): Boolean {
            val cm =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        }
    }
}
