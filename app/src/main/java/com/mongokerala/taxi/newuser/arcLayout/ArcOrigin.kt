package com.mongokerala.taxi.newuser.arcLayout

import android.annotation.TargetApi
import android.os.Build
import android.view.Gravity

internal object ArcOrigin {
    const val TOP = Gravity.TOP
    const val BOTTOM = Gravity.BOTTOM
    const val LEFT = Gravity.START
    const val RIGHT = Gravity.END
    const val CENTER = Gravity.CENTER
    const val VERTICAL_MASK = Gravity.VERTICAL_GRAVITY_MASK
    const val HORIZONTAL_MASK = Gravity.HORIZONTAL_GRAVITY_MASK

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    fun getAbsoluteOrigin(origin: Int, layoutDirection: Int): Int {
        return Gravity.getAbsoluteGravity(origin, layoutDirection)
    }
}