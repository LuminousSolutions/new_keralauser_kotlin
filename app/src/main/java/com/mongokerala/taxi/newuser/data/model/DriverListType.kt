package com.mongokerala.taxi.newuser.data.model

import com.mongokerala.taxi.newuser.ui.main_new.AdvanceSearhData

class DriverListType {


    private var taxi_driverLists: List<AdvanceSearhData>? = null

    private var Taxi4_driverLists: List<AdvanceSearhData>? = null

    private var Taxi6_driverLists: List<AdvanceSearhData>? = null

    private var Transport_driverLists: List<AdvanceSearhData>? = null

    private var auto_driverLists: List<AdvanceSearhData>? = null

    private var bike_driverLists: List<AdvanceSearhData>? = null

    private  var ambulance_driverList: List<AdvanceSearhData>? = null


    private fun getAmbulance_driverList(): List<AdvanceSearhData>? {
        return ambulance_driverList
    }

    fun setAmbulance_driverList(ambulanceDriverList: List<AdvanceSearhData>?) {
        ambulance_driverList = ambulanceDriverList
    }

    private fun getTaxi4_driverLists(): List<AdvanceSearhData>? {
        return Taxi4_driverLists
    }

    fun setTaxi4_driverLists(taxi4_driverLists: List<AdvanceSearhData>?) {
        Taxi4_driverLists = taxi4_driverLists
    }

    private fun getTaxi6_driverLists(): List<AdvanceSearhData>? {
        return Taxi6_driverLists
    }

    fun setTaxi6_driverLists(taxi6_driverLists: List<AdvanceSearhData>?) {
        Taxi6_driverLists = taxi6_driverLists
    }

    private fun getTransport_driverLists(): List<AdvanceSearhData>? {
        return Transport_driverLists
    }

    fun setTransport_driverLists(transport_driverLists: List<AdvanceSearhData>?) {
        Transport_driverLists = transport_driverLists
    }


    private fun getBike_driverLists(): List<AdvanceSearhData>? {
        return bike_driverLists
    }

    fun setBike_driverLists(bike_driverLists: List<AdvanceSearhData>?) {
        this.bike_driverLists = bike_driverLists
    }


    private fun getAuto_driverLists(): List<AdvanceSearhData>? {
        return auto_driverLists
    }

    fun setAuto_driverLists(auto_driverLists: List<AdvanceSearhData>?) {
        this.auto_driverLists = auto_driverLists
    }


    fun setTaxi_driverLists(taxi_driverLists: List<AdvanceSearhData>?) {
        this.taxi_driverLists = taxi_driverLists
    }


    private fun getTaxi_driverLists(): List<AdvanceSearhData>? {
        return taxi_driverLists
    }

    fun setData(taxi_driverLists: List<AdvanceSearhData>?) {
        this.taxi_driverLists = taxi_driverLists
    }

    fun getDriverLists(position: Int): List<AdvanceSearhData>? {
        var driverLists: List<AdvanceSearhData>? = null
        when (position) {
            0 -> driverLists = getTaxi4_driverLists()
            1 -> driverLists = getTaxi_driverLists()
            2 -> driverLists = getTaxi6_driverLists()
            3 -> driverLists = getAuto_driverLists()
            4 -> driverLists = getTransport_driverLists()
            5 -> driverLists = getBike_driverLists()
            6 -> driverLists = getAmbulance_driverList()
        }
        return driverLists
    }

    fun get_Is_Driver_OnlineLists(position: Int): Boolean {
        var is_online = false
        val driverLists: List<AdvanceSearhData>? = null
        when (position) {
            0 -> is_online = is_online(getTaxi4_driverLists())
            1 -> is_online = is_online(getTaxi_driverLists())
            2 -> is_online = is_online(getTaxi6_driverLists())
            3 -> is_online = is_online(getAuto_driverLists())
            4 -> is_online = is_online(getTransport_driverLists())
            5 -> is_online = is_online(getBike_driverLists())
            6 -> is_online = is_online(getAmbulance_driverList())
        }
        return is_online
    }


    private fun is_online(driverListList: List<AdvanceSearhData>?): Boolean {
        var is_online = false
        for (i in driverListList!!.indices) {
            if (driverListList[i].is_Online!! && driverListList[i].is_Online!!) {
                is_online = true
            }
        }
        return is_online
    }
}