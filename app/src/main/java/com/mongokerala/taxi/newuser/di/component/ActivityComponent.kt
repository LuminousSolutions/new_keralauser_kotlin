package com.mongokerala.taxi.newuser.di.component

import com.mongokerala.taxi.newuser.di.ActivityScope
import com.mongokerala.taxi.newuser.di.module.ActivityModule
import com.mongokerala.taxi.newuser.ui.about.AboutActivity
import com.mongokerala.taxi.newuser.ui.chat.ChatActivity
import com.mongokerala.taxi.newuser.ui.tour.TourActivity
import com.mongokerala.taxi.newuser.ui.dummy.DummyActivity
import com.mongokerala.taxi.newuser.ui.feedback.FeedbackActivity
import com.mongokerala.taxi.newuser.ui.login.LoginActivity
import com.mongokerala.taxi.newuser.ui.main.MainActivity
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity
import com.mongokerala.taxi.newuser.ui.payment.PaymentActivity
import com.mongokerala.taxi.newuser.ui.profile_edit.ProfileEditActivity
import com.mongokerala.taxi.newuser.ui.rate_us.RateUsActivity
import com.mongokerala.taxi.newuser.ui.registration.RegistrationActivity
import com.mongokerala.taxi.newuser.ui.reqotp.ReqotpActivity
import com.mongokerala.taxi.newuser.ui.setting.SettingActivity
import com.mongokerala.taxi.newuser.ui.splash.SplashActivity
import com.mongokerala.taxi.newuser.ui.tripdetail.TripDetailActivity
import com.mongokerala.taxi.newuser.ui.triphistory.TripHistoryActivity
import com.mongokerala.taxi.newuser.ui.validatemobile.ValidateMobileActivity
import com.mongokerala.taxi.newuser.ui.vehicleinfoupdate.VehicleInfoUpdateActivity
import dagger.Component

@ActivityScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ActivityModule::class]
)
interface ActivityComponent {

    fun inject(activity: SplashActivity)

    fun inject(activity: LoginActivity)

    fun inject(activity: DummyActivity)

    fun inject(activity: MainActivity)

    fun inject(activity: TourActivity)

    fun inject(activity: RegistrationActivity)

    fun inject(activity: ReqotpActivity)

    fun inject(activity: ValidateMobileActivity)

    fun inject(activity: MainNewActivity)

    fun inject(activity: ProfileEditActivity)

    fun inject(activity: VehicleInfoUpdateActivity)

    fun inject(activity: FeedbackActivity)

    fun inject(activity: RateUsActivity)

    fun inject(activity: SettingActivity)

    fun inject(activity: TripHistoryActivity)

    fun inject(activity: TripDetailActivity)

    fun inject(activity: AboutActivity)

    fun inject(activity: ChatActivity)

    fun inject(activity: PaymentActivity) {

    }
}