package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class RideCreateRequest(
    @field:SerializedName(Keys.driverId) @field:Expose var driverId: String?,
    @field:SerializedName(Keys.rideStatus) @field:Expose var rideStatus: String?,
    @field:SerializedName(Keys.source) @field:Expose var source: String?,
    @field:SerializedName(Keys.destination) @field:Expose var destination: String?,
    @field:SerializedName(Keys.userId) @field:Expose var userId: String?,
    @field:SerializedName(Keys.payment) @field:Expose var payment: String?,
    @field:SerializedName(Keys.discount) @field:Expose var discount: String?,
    @field:SerializedName(Keys.category) @field:Expose var category: String?,
    @field:SerializedName(Keys.km) @field:Expose var km: String?
    ) {

    @SerializedName("base")
    @Expose
    private var base: Int? = null

    @SerializedName("comments")
    @Expose
    private var comments: String? = null

    @SerializedName("dealId")
    @Expose
    private var dealId: Int? = null

    @SerializedName("desc")
    @Expose
    private var desc: String? = null

    @SerializedName("distance")
    @Expose
    private var distance: Int? = null

    @SerializedName("driverName")
    @Expose
    private var driverName: String? = null

    @SerializedName("endTIme")
    @Expose
    private var endTIme: Int? = null

    @SerializedName("estimateTime")
    @Expose
    private var estimateTime: Int? = null

    @SerializedName("id")
    @Expose
    private var id: Int? = null

    @SerializedName("latitude")
    @Expose
    private var latitude: Int? = null

    @SerializedName("longitude")
    @Expose
    private var longitude: Int? = null

    @SerializedName("oneSignalId")
    @Expose
    private var oneSignalId: String? = null

    @SerializedName("price")
    @Expose
    private var price: Int? = null

    @SerializedName("push")
    @Expose
    private var push: Boolean? = null

    @SerializedName("radius")
    @Expose
    private var radius: Int? = null

    @SerializedName("star")
    @Expose
    private var star: Int? = null

    @SerializedName("tax")
    @Expose
    private var tax: Int? = null

    @SerializedName("taxiDetailId")
    @Expose
    private var taxiDetailId: Int? = null

    @SerializedName("totalComments")
    @Expose
    private var totalComments: Int? = null

    @SerializedName("totalPrice")
    @Expose
    private var totalPrice: Int? = null

    @SerializedName("totalwaitTime")
    @Expose
    private var totalwaitTime: Int? = null

    @SerializedName("travelTime")
    @Expose
    private var travelTime: Int? = null

    @SerializedName("type")
    @Expose
    private var type: String? = null

    @SerializedName("updatedOn")
    @Expose
    private var updatedOn: String? = null

    @SerializedName("userKm")
    @Expose
    private var userKm: Int? = null

    @SerializedName("userName")
    @Expose
    private var userName: String? = null

    @SerializedName("userTotalPrice")
    @Expose
    private var userTotalPrice: Int? = null

    init {
        base = 0
        comments = "string"
        dealId = 0
        desc ="string"
        distance = 0
        driverName ="string"
        endTIme = 0
        estimateTime = 0
        id = 0
        latitude = 0
        longitude = 0
        oneSignalId = "string"
        price = 0
        push = true
        radius = 0
        star = 0
        tax = 0
        taxiDetailId = 0
        totalComments = 0
        totalPrice = 0
        totalwaitTime = 0
        travelTime = 0
        type = "string"
        updatedOn = "2020"
        userKm = 0
        userName = "string"
        userTotalPrice = 0
    }

}