package com.mongokerala.taxi.newuser.ui.triphistory

import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.remote.response.TripHistoryData
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class TripHistoryViewModel (
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    val tripHistoryLiveData: MutableLiveData<List<TripHistoryData>> = MutableLiveData()
    val tripIn: MutableLiveData<Boolean> = MutableLiveData()






    override fun onCreate() {

    }

    fun onLoadTripHistory(){


        tripIn.postValue(true)

        compositeDisposable.addAll(
            userRepository.doTripHistoryRequest(userRepository.getCurrentUserID()!!)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        tripHistoryLiveData.postValue(it.data)
                        tripIn.postValue(false)



                    },
                    {
                        handleNetworkError(it)
                        tripIn.postValue(false)
                    }
                )
        )
    }

}