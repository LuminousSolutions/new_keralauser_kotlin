package com.mongokerala.taxi.newuser.data.remote

import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.data.remote.request.Keys
import java.io.Serializable

class LocationData : Serializable {

    @SerializedName("lat")
    var lat: String? = null
        get() = field
        set(value) {
            field = value
        }
    @SerializedName("long")
    var long: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.action)
    var action: String? = null
        get() = field
        set(value) {
            field = value
        }



}
