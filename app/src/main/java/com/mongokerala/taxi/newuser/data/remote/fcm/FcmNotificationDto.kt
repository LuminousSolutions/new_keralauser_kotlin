package com.mongokerala.taxi.newuser.data.remote.fcm

import com.google.gson.annotations.SerializedName

class FcmNotificationDto {
    /*@SerializedName("notification")
    var notificationTitle: FcmNotificationTitle? = null*/

    @SerializedName("android")
    var notificationPriority: FcmNotificationPriority

    @SerializedName("apns")
    var notificationIosPriority: FcmNotificationIosPriority? = null

    @SerializedName("data")
    var data: FcmCustomizeNotificationData

    @SerializedName("registration_ids")
    var registrationIds: Array<String>

    constructor(
        driverTokensArray: Array<String>,
        fcmNotificationTitle: FcmNotificationTitle?,
        fcmNotificationPriority: FcmNotificationPriority,
        fcmCustomizeNotificationData: FcmCustomizeNotificationData
    ) {
        registrationIds = driverTokensArray
//        notificationTitle = fcmNotificationTitle
        notificationPriority = fcmNotificationPriority
        data = fcmCustomizeNotificationData
    }

    constructor(
        driverTokensArray: Array<String>,
        fcmNotificationTitle: FcmNotificationTitle?,
        fcmNotificationPriority: FcmNotificationPriority,
        fcmNotificationIosPriority: FcmNotificationIosPriority,
        fcmCustomizeNotificationData: FcmCustomizeNotificationData
    ) {
        registrationIds = driverTokensArray
//        notificationTitle = fcmNotificationTitle
        notificationPriority = fcmNotificationPriority
        notificationIosPriority = fcmNotificationIosPriority
        data = fcmCustomizeNotificationData
    }

    constructor(
        driverFcmToken: Array<String>,
        notificationPriority: FcmNotificationPriority,
        fcmCustomizeNotificationData: FcmCustomizeNotificationData
    ) {
        registrationIds = driverFcmToken
        data = fcmCustomizeNotificationData
        this.notificationPriority = notificationPriority

    }
}