package com.mongokerala.taxi.newuser.ui.payment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mongokerala.taxi.newuser.R;
import com.mongokerala.taxi.newuser.utils.display.Toaster;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import java.math.BigDecimal;
import java.util.Date;
import java.util.TreeMap;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public class PaymentGatewayDialog extends DialogFragment implements
        OrderCreateListener, OrderPaymentUpdateListener {
    private static final String TAG = PaymentGatewayDialog.class.getSimpleName();

    private ProgressBar progressBar;
    private OrderService orderService;

    private String value;
    //private static PaymentGateway paymentGateway;

   /* public interface PaymentGatewayDialogListener {
        void onFinishPaymentGatewayDialog(String errorMessage);
    }*/

    public PaymentGatewayDialog() {
    }

    @NonNull
    public static PaymentGatewayDialog newInstance(PaymentGateway pgType) {
        PaymentGatewayDialog fragmentDialog = new PaymentGatewayDialog();
        //paymentGateway = pgType;
        return fragmentDialog;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_payment_gateway, container);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        dialog.getWindow().requestFeature(android.R.style.Theme_Black_NoTitleBar);
        Bundle bundle = getArguments();
        if(bundle!= null )
        value = bundle.getString("card_amt_paytm", String.valueOf(bundle));
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                dismiss();
            }
        };
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        createInitializeAllView(view);
        createOrderApiCall();
    }

    private void createInitializeAllView(View view){
        progressBar = view.findViewById(R.id.progressBar);
    }

    public void sendBackResult(String errorMessage) {
       /* PaymentGatewayDialogListener listener = (PaymentGatewayDialogListener)getActivity();
        listener.onFinishPaymentGatewayDialog(errorMessage);
        getDialog().dismiss();*/
    }

    //1. Create Order In Backend Before Payment
    private void createOrderApiCall(){
        //get user from shared preference
        orderService = new OrderServiceImpl();
        OrderDTO orderDTO = new OrderDTO();
        CustomerDTO customerDTO = new CustomerDTO();
        PGDataDTO pgDataDTO = new PGDataDTO();
        //cartDetailDTO.setAddress();
        customerDTO.setCustomerId(""+new Date().getTime());
        customerDTO.setEmail("test@gmail.com");
        customerDTO.setMobileNumber("8047658745");
        pgDataDTO.setAmount(new BigDecimal(value));
        pgDataDTO.setPaymentGateway(PaymentGateway.PAYTM);
        orderDTO.setOrderReqType(OrderRequestType.ORDER_CREATE);
        orderDTO.setCustomer(customerDTO);
        orderDTO.setPgData(pgDataDTO);
        progressBar.setVisibility(View.VISIBLE);//start progress bar
        orderService.createOrderApiCall(this, orderDTO);
    }

    //2. Update Order Payment In Backend After Payment Done
    private void updatePayTMOrderPaymentResponseApiCall(OrderDTO orderDTO, @NonNull Bundle inResponse){
        orderService = new OrderServiceImpl();
        if(null != orderDTO.getPgData()){
            orderDTO.getPgData().setPaytmParams(getPayTMPaymentResponseData(inResponse));
            orderDTO.getPgData().setPaytmChecksum(inResponse.getString("CHECKSUMHASH").toString());
        }
        orderDTO.setOrderReqType(OrderRequestType.PAYMENT_UPDATE);
        orderService.updateOrderPaymentApiCall(this, orderDTO);
    }

    private void startPayTMPaymentGateway(@NonNull OrderDTO orderResponseDTO){
        PaytmPGService paytmPGService = PaytmPGService.getStagingService();
        PaytmOrder paytmOrder = new PaytmOrder(PayTMConstants.getPayTMOrderParamMap(orderResponseDTO));
        paytmPGService.initialize(paytmOrder, null);
        paytmPGService.startPaymentTransaction(getActivity(), true, true, new PaytmPaymentTransactionCallback() {
            /*Call Backs*/
            public void onTransactionResponse(@NonNull Bundle inResponse) {
                Toaster.INSTANCE.show(getActivity(), "Payment Response :: " + inResponse.toString());
                if (!validateForFirstTimePayTMResponseFromSDK(inResponse)){
                    updatePayTMOrderPaymentResponseApiCall(orderResponseDTO, inResponse);
                } else {
                    sendBackResult("Retry Payment");
                }
                dismiss();
            }
            public void someUIErrorOccurred(String inErrorMessage) {
                //TODO USE BELOW LINE with your message
                //sendBackResult(getString(R.string.facing_problem_please_try_again_error_msg)); //change are per you need
                Toaster.INSTANCE.show(getActivity(), "PayTM UI Error ");
                sendBackResult("PayTM UI Error");
            }
            public void networkNotAvailable() {
                //TODO USE BELOW LINE with your message
                //sendBackResult(getString(R.string.network_connection_problem_error_msg)); //change are per you need
                Toaster.INSTANCE.show(getActivity(), "PayTM Error ");
                sendBackResult("");
            }
            public void clientAuthenticationFailed(String inErrorMessage) {
                //TODO USE BELOW LINE with your message
                //sendBackResult(getString(R.string.something_went_wrong_error_msg)); //change are per you need
                Toaster.INSTANCE.show(getActivity(), "PayTM Error ");
                sendBackResult("");
            }
            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                //TODO USE BELOW LINE with your message
                //sendBackResult(getString(R.string.unable_to_load_pg_error_msg)); //change are per you need
                Toaster.INSTANCE.show(getActivity(), "PayTM Error ");
                sendBackResult("");
            }
            public void onBackPressedCancelTransaction() {
                //TODO USE BELOW LINE with your message
                //sendBackResult(getString(R.string.transaction_cancelled_error_msg)); //change are per you need
               // Toaster.INSTANCE.show(getContext(), "PayTM Error ");
                sendBackResult(""+"Canceled");
            }
            public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                //TODO USE BELOW LINE with your message
                //sendBackResult(getString(R.string.transaction_cancelled_error_msg)); //change are per you need
              //  Toaster.INSTANCE.show(getActivity(), "PayTM Error ");
                sendBackResult("");
            }
        });
    }

    //Create Order Response
    @Override
    public void onRequestSuccess(OrderDTO orderRequestDTO, @Nullable OrderDTO orderResponseDTO) {
        progressBar.setVisibility(View.GONE);
        if(null != orderResponseDTO){
            startPayTMPaymentGateway(orderResponseDTO);
        } else {
            Toaster.INSTANCE.show(getActivity(), "Something went wrong. pLease try after sometime!");
            dismiss();
        }
    }

    //Update order payment
    @Override
    public void onRequestSuccess(OrderDTO orderDTO, ApiResponse apiResponse) {
        progressBar.setVisibility(View.GONE);
        startActivity(new Intent(getActivity(), OrderPaymentStatusActivity.class));
    }

    @Override
    public void onRequestFailed(OrderDTO orderDTO, ApiResponse apiResponse) {
        progressBar.setVisibility(View.GONE);
        Toaster.INSTANCE.show(getActivity(), "Something went wrong. pLease try after sometime!");
        dismiss();
    }

    @Override
    public void onRequestTimeOut(OrderDTO orderDTO) {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), "Something went wrong. pLease try after sometime!", Toast.LENGTH_LONG).show();
        dismiss();
    }

    private boolean validateForFirstTimePayTMResponseFromSDK(@NonNull Bundle inResponse) {
        boolean isTxnStatusFailure = true;
        try{
            String txnStatus = inResponse.getString("STATUS").toString();//TXN_FAILURE or TXN_SUCCESS
            if(null != txnStatus && !txnStatus.isEmpty() && txnStatus.equals("TXN_SUCCESS")){
                isTxnStatusFailure = false;
            }
        }catch (Exception e){
            Log.e(TAG, "Exception while converting PayTM Payment Response Bundle, e : ",e);
        }
        return isTxnStatusFailure;
    }

    @NonNull
    private TreeMap<String, String> getPayTMPaymentResponseData(@NonNull Bundle inResponse) {
        TreeMap<String, String> paytmParams = new TreeMap<>();
        try{
            paytmParams.put("STATUS", inResponse.getString("STATUS").toString());
            paytmParams.put("BANKNAME", inResponse.getString("BANKNAME").toString());
            paytmParams.put("ORDERID", inResponse.getString("ORDERID").toString());
            paytmParams.put("TXNAMOUNT", inResponse.getString("TXNAMOUNT").toString());
            paytmParams.put("TXNDATE", inResponse.getString("TXNDATE").toString());
            paytmParams.put("MID", inResponse.getString("MID").toString());
            paytmParams.put("TXNID", inResponse.getString("TXNID").toString());
            paytmParams.put("RESPCODE", inResponse.getString("RESPCODE").toString());
            paytmParams.put("PAYMENTMODE", inResponse.getString("PAYMENTMODE").toString());
            paytmParams.put("BANKTXNID", inResponse.getString("BANKTXNID").toString());
            paytmParams.put("CURRENCY", inResponse.getString("CURRENCY").toString());
            paytmParams.put("GATEWAYNAME", inResponse.getString("GATEWAYNAME").toString());
            paytmParams.put("RESPMSG", inResponse.getString("RESPMSG").toString());
        }catch (Exception e){
            Log.e(TAG, "Exception while converting PayTM Payment Response Bundle, e : ",e);
        }
        return paytmParams;
    }

    public void onResume() {
        super.onResume();
        // crash fixed
       /* Window window = getDialog().getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);//(width , height) // (int) (width * 0.80),(int) (width * 0.80)
        window.setGravity(Gravity.CENTER_HORIZONTAL);*/
    }
}
