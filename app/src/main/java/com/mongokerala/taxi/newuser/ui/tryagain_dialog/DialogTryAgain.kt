package com.mongokerala.taxi.newuser.ui.tryagain_dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mongokerala.taxi.newuser.R


class DialogTryAgain(): Fragment(){

    private val TAG = "ForgetPasswordDialog"
    private var callback: OfflineClick? = null
    private var isTryAgain: Boolean? = null
    private var error_msg: String? = null
    private val reason_msg: String? = null
    private var alert_icon: String? = null
    private var alert_title: String? = null
    var context = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.dialog_try_again, container, false)
        return view
    }
    fun newInstance(): DialogTryAgain? {
        return DialogTryAgain()
    }




    /*override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_try_again)

        val mMessage =findViewById<EditText>(R.id.editText) as EditText
        val lay_two_btn =findViewById<RelativeLayout>(R.id.lay_two_btn) as RelativeLayout
        val btn_ok =findViewById<Button>(R.id.btn_ok) as Button
        val txt_No_worriess =findViewById<TypeFacedTextView>(R.id.txt_No_worriess) as TypeFacedTextView
        val txt_errormsg =findViewById<TypeFacedTextView>(R.id.txt_errormsg) as TypeFacedTextView
        val close =findViewById<TypeFacedTextView>(R.id.close) as TypeFacedTextView
        val txt_alert_icon =findViewById<TypeFacedTextView>(R.id.txt_alert_icon) as TypeFacedTextView
        val lay_1 =findViewById<LinearLayout>(R.id.lay_1) as LinearLayout
        val edt_cancel_reason =findViewById<EditText>(R.id.edt_cancel_reason) as EditText
        val lay_cancel_reason =findViewById<RelativeLayout>(R.id.lay_cancel_reason) as RelativeLayout
        val spinner_cancel_reasons =findViewById<Spinner>(R.id.spinner_cancel_reasons) as Spinner
    }*//*
    fun newInstance(): DialogTryAgain? {
        return DialogTryAgain(context)
    }
    fun newInstance(
        mcontext: Context,
        misTryAgain: Boolean,
        msg: String,
        err_title: String,
        icon_txt: String
    ): DialogTryAgain? {
        val fragment = DialogTryAgain(context)
        val bundle = Bundle()
        isTryAgain = misTryAgain
        error_msg = msg
        alert_icon = icon_txt
        alert_title = err_title
        return fragment
    }

    override fun dismiss() {
        dismiss()
    }*/




}

