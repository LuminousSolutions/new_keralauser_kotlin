package com.mongokerala.taxi.newuser.utils.common

import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Build
import android.os.IBinder
import android.view.*
import android.view.View.OnTouchListener
import android.widget.ImageView
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity

class FloatingViewService : Service(), View.OnClickListener,
    Floating_Widget_Onclick {
    private var mWindowManager: WindowManager? = null
    private var mFloatingView: View? = null
    private var collapsedView: View? = null
    var sInstance: FloatingViewService? = null


    //private View expandedView;
    private var buttonClose: ImageView? = null
    override fun onBind(intent: Intent): IBinder? {
        return null
    }


    @Nullable
    fun getInstance(): FloatingViewService? {
        return FloatingViewService.sInstance
    }

    override fun onCreate() {
        super.onCreate()


        //getting the widget layout from xml using layout inflater
        mFloatingView = LayoutInflater.from(this).inflate(R.layout.layout_floating_widget, null)
        val LAYOUT_FLAG: Int
        LAYOUT_FLAG = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            WindowManager.LayoutParams.TYPE_PHONE
        }
        //setting the layout parameters
        val params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            LAYOUT_FLAG,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )
        params.gravity = Gravity.START


        //getting windows services and adding the floating view to it
        mWindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        mWindowManager!!.addView(mFloatingView, params)


        //getting the collapsed and expanded view from the floating view
        collapsedView = mFloatingView!!.findViewById(R.id.layoutCollapsed)
        //expandedView = mFloatingView.findViewById(R.id.layoutExpanded);

        //adding click listener to close button and expanded view
        buttonClose = mFloatingView!!.findViewById(R.id.buttonClose)
        //val fab = findViewById<View>(R.id.fab) as? FloatingActionButton
        // expandedView.setOnClickListener(this);

        //adding an touchlistener to make drag movement of the floating widget
        mFloatingView!!.findViewById<View>(R.id.relativeLayoutParent)
            .setOnTouchListener(object : OnTouchListener {
                private var initialX = 0
                private var initialY = 0
                private var initialTouchX = 0f
                private var initialTouchY = 0f
                override fun onTouch(
                    v: View,
                    @NonNull event: MotionEvent
                ): Boolean {
                    when (event.action) {
                        MotionEvent.ACTION_DOWN -> {
                            initialX = params.x
                            initialY = params.y
                            initialTouchX = event.rawX
                            initialTouchY = event.rawY
                            return true
                        }
                        MotionEvent.ACTION_UP -> {
                            //when the drag is ended switching the state of the widget
                            val intent =
                                Intent(applicationContext, MainNewActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            return true
                        }
                        MotionEvent.ACTION_MOVE -> {
                            //this code is helping the widget to move around the screen with fingers
                            params.x = initialX + (event.rawX - initialTouchX).toInt()
                            params.y = initialY + (event.rawY - initialTouchY).toInt()
                            mWindowManager!!.updateViewLayout(mFloatingView, params)
                            return true
                        }
                    }
                    return false
                }
            })
        FloatingViewService.sInstance = this
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mFloatingView != null)
            mWindowManager!!.removeView(mFloatingView)
    }

    override fun onClick(v: View) {
        val intent = Intent(applicationContext, MainNewActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    override fun close_Widget() {
        stopSelf()
    }

    companion object {
        var sInstance: FloatingViewService? = null


        @Nullable
        val BROADCAST_TAG = MainNewActivity::class.java.canonicalName

        fun getInstance(): FloatingViewService? {
            return FloatingViewService().getInstance()
        }
    }
}
