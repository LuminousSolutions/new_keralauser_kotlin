package com.mongokerala.taxi.newuser.ui.registration

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.login.LoginActivity
import com.mongokerala.taxi.newuser.ui.reqotp.ReqotpActivity
import com.mongokerala.taxi.newuser.utils.common.AppUtils
import com.mongokerala.taxi.newuser.utils.display.Toaster
import kotlinx.android.synthetic.main.activity_registration.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class RegistrationActivity : BaseActivity<RegistrationViewModel>() {


    companion object {
        const val TAG = "RegistrationActivity"
    }


    private val userNamePattern = Pattern.compile("[a-zA-Z ]+")

    override fun provideLayoutId(): Int = R.layout.activity_registration

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun setupView(savedInstanceState: Bundle?) {
        val mobileNum=viewModel.getMobilenumber()
        etxt_mobile_number.text= mobileNum
        showPasswordreg.visibility = View.GONE
        etxt_password.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT
        etxt_password.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (etxt_password.text.isNotEmpty()) {
                    showPasswordreg.visibility = View.VISIBLE
                } else {
                    showPasswordreg.visibility = View.GONE
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        showPasswordreg.setOnClickListener {
            if (showPasswordreg.text === "\ue81e") {
                showPasswordreg.text = "\ue81f"
                etxt_password.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                etxt_password.setSelection(etxt_password.length())
            } else {
                showPasswordreg.text = "\ue81e"
                etxt_password.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT
                etxt_password.setSelection(etxt_password.length())
            }
        }

    }


    override fun setupObservers() {

        viewModel.mainIn.observe(this) {
            if (it == true) {
                pb_loading_main.visibility = View.VISIBLE
            } else {
                pb_loading_main.visibility = View.GONE
            }
        }

        login_back.setOnClickListener {
            viewModel.onSignup()
        }
        viewModel.launchRegistrationtoLogin.observe(this) {
            it.getIfNotHandled()?.run {
                startActivity(Intent(applicationContext, LoginActivity::class.java))
                finish()

            }
        }

        viewModel.name.observe(this) {
            if (etxt_First_Name.text.toString() != it) etxt_First_Name.setText(it)
        }

        viewModel.emailField.observe(this) {
            if (etxt_Email_id.text.toString() != it) etxt_Email_id.setText(it)
        }

        viewModel.passwordField.observe(this) {
            if (etxt_password.text.toString() != it) etxt_password.setText(it)
        }

        viewModel.mobileNumber.observe(this) {
            if (etxt_mobile_number.text.toString() != it) etxt_mobile_number.setText(it)
        }

        viewModel.launchRegistrationtoLogin.observe(this) {
            it.getIfNotHandled()?.run {
                val toast: Toast = Toast.makeText(applicationContext, "Your Registration is successfull", Toast.LENGTH_LONG)
                val v = toast.view?.findViewById(android.R.id.message) as TextView
                v.setTextColor(Color.BLACK)
                v.setBackgroundColor(Color.WHITE)
                toast.show()
                startActivity(Intent(applicationContext, LoginActivity::class.java))
                finish()
            }
        }
        viewModel.launchEmailExisting.observe(this) {
            it.getIfNotHandled()?.run {
                Toaster.show(applicationContext, "This email is already registered")
            }
        }

    }

    override fun onBackPressed() {
        alertDialogue()
    }


    private fun alertDialogue() {
        val alertDialogue = AlertDialog.Builder(this)
        alertDialogue.setTitle(getString(R.string.close_this_screen))
        alertDialogue.setCancelable(false)
        alertDialogue.setPositiveButton(getString(R.string.yes)) { dialog, which ->
            finish()
        }
        alertDialogue.setNegativeButton(getString(R.string.no)) { dialog, which ->
            dialog.dismiss()
        }
        alertDialogue.show()
    }

    fun onRegistrationButtonClick(view: View) {

        if (AppUtils.isNetworkConnected(this)){
            val name: String = etxt_First_Name.text.toString()
            val email: String = etxt_Email_id.text.toString()
            val mobileNumber: String = etxt_mobile_number.text.toString()
            val password: String = etxt_password.text.toString()
            viewModel.signUpValidation()
            if (checkbox.isChecked) {

                if (name.isEmpty() || !validateUserName(name) || name.length < 3) {
                    Toaster.show(applicationContext, getString(R.string.Please_enter_name))
                } else if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    Toaster.show(applicationContext, getString(R.string.Please_enter_valid_email))
                } else if (password.isEmpty()) {
                    Toaster.show(applicationContext, getString(R.string.Please_enter_the_password))
                } else if (password.length < 6) {
                    Toaster.show(applicationContext, getString(R.string.Please_enter_the_password_greater_than_4))
                } else if (mobileNumber.isEmpty() || !Patterns.PHONE.matcher(mobileNumber).matches() ||
                    (mobileNumber.length < 10 || mobileNumber.length > 10)
                ) {
                    Toaster.show(applicationContext, "Please Enter Valid PhoneNumber")
                } else {
                    viewModel.doRegistrationRequest(email, name, password, mobileNumber)
                }
            } else {
                Toaster.show(this, getString(R.string.accept_term_error))
                return
            }

        }


    }

    private fun validateUserName(userName: String): Boolean {
        val match: Matcher = userNamePattern.matcher(userName)
        return match.matches()
    }


}
