package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.utils.network.ERole

class ForgetPasswordOtpRequest(
    @field:SerializedName(Keys.countryCode) @field:Expose var countryCode: String?,
    @field:SerializedName(Keys.phoneNumber) @field:Expose var phoneNumber: String

) {

    @Expose
    @SerializedName(Keys.token)
    var token: String
    @Expose
    @SerializedName(Keys.role)
    var role: String
    @Expose
    @SerializedName(Keys.password)
    var password: String
    @Expose
    @SerializedName(Keys.rideId)
    var rideId: String
    @Expose
    @SerializedName(Keys.userId)
    var userId: String
    @Expose
    @SerializedName(Keys.companyName)
    var companyName: String


    init {
        companyName ="KERALACABS"
        role = ERole.ROLE_USER.toString()
        token = "string"
        password = ""
        rideId = "string"
        userId = "string"
    }


}