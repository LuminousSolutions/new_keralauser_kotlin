package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class PriceCalculationResponse (

    @SerializedName("statusCode") val statusCode : Int,
    @SerializedName("status") val status : Boolean,
    @SerializedName("data") val data : PriceCalculationData,
    @SerializedName("infoId") val infoId : Int

)