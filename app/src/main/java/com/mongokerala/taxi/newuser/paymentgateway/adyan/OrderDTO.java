package com.mongokerala.taxi.newuser.paymentgateway.adyan;


import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.PaymentSetupRequest;
import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.PaymentVerifyRequest;
import com.mongokerala.taxi.newuser.ui.payment.CustomerDTO;
import com.mongokerala.taxi.newuser.ui.payment.OrderRequestType;
import com.mongokerala.taxi.newuser.ui.payment.PGDataDTO;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public class OrderDTO {
    private OrderRequestType orderReqType;
    private String orderId;
    private CustomerDTO customer; // customer info doing payment
    private PGDataDTO pgData; // payment gateway checksum

    // This variable is for Adyen Payment Only
    private PaymentSetupRequest paymentSetupRequest; // for adyen payment session create
    private PaymentVerifyRequest paymentVerifyRequest;// for adyen payment verify

    public OrderRequestType getOrderReqType() {
        return orderReqType;
    }

    public void setOrderReqType(OrderRequestType orderReqType) {
        this.orderReqType = orderReqType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public PGDataDTO getPgData() {
        return pgData;
    }

    public void setPgData(PGDataDTO pgData) {
        this.pgData = pgData;
    }

    public PaymentSetupRequest getPaymentSetupRequest() {
        return paymentSetupRequest;
    }

    public void setPaymentSetupRequest(PaymentSetupRequest paymentSetupRequest) {
        this.paymentSetupRequest = paymentSetupRequest;
    }

    public PaymentVerifyRequest getPaymentVerifyRequest() {
        return paymentVerifyRequest;
    }

    public void setPaymentVerifyRequest(PaymentVerifyRequest paymentVerifyRequest) {
        this.paymentVerifyRequest = paymentVerifyRequest;
    }
}
