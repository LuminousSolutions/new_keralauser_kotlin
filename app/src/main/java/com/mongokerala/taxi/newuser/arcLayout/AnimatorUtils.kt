package com.mongokerala.taxi.newuser.arcLayout

import android.animation.PropertyValuesHolder
import android.annotation.TargetApi

class AnimatorUtils {
    private val ROTATION = "rotation"
    private val SCALE_Y = "scaleY"
    private val TRANSLATION_X = "translationX"
    private val TRANSLATION_Y = "translationY"

    private fun AnimatorUtils() {
        //No instances.
    }

    @TargetApi(11)
    fun rotation(vararg values: Float): PropertyValuesHolder? {
        return PropertyValuesHolder.ofFloat(ROTATION, *values)
    }

    @TargetApi(11)
    fun translationX(vararg values: Float): PropertyValuesHolder? {
        return PropertyValuesHolder.ofFloat(TRANSLATION_X, *values)
    }

    @TargetApi(11)
    fun translationY(vararg values: Float): PropertyValuesHolder? {
        return PropertyValuesHolder.ofFloat(TRANSLATION_Y, *values)
    }
}