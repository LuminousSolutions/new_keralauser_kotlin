package com.mongokerala.taxi.newuser.data.model

import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.data.remote.request.ImageInfo
import com.mongokerala.taxi.newuser.data.remote.request.Keys

class DriverList {

    @SerializedName(Keys.totalTime)
    var totalTime: Long = 0
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.endTIme)
    var endTIme: Long = 0
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.basePrice)
    var basePrice = 0.0
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.blobKey)
    var blobKey: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.cartype)
    var cartype: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.category)
    var category: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.deviceId)
    var deviceId: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.id)
    var id: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.latitude)
    var latitude: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.longitude)
    var longitude: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.name)
    var name: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.phoneNumber)
    var phoneNumber: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.price)
    var price = 0.0
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.supplierId)
    var supplierId: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.transporttype)
    var transportType: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.waitingTime)
    var waitingTime: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.driverId)
    var driverId: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.oneSignalValue)
    var oneSignalValue: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.star)
    var star: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.totalComments)
    var totalComments: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.driverLoginstatus)
    var loginStatus: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.driverNotificationSetting)
    var driverNotificationSetting: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.imageInfo)
    var imageInfo : ImageInfo?=null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.driverStatus)
    var driverStatus: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.is_Online)
    var is_Online = false
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.token)
    var token: String? = null
        get() = field
        set(value) {
            field = value
        }

    @SerializedName(Keys.notificationToken)
    var notification_token: String? = null
        get() = field
        set(value) {
            field = value
        }



    var total_taxi = 0


}