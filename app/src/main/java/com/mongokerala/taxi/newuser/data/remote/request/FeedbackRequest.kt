package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName





class FeedbackRequest (@field:SerializedName(Keys.message) @field:Expose var message: String,
                       @field:SerializedName(Keys.name) @field:Expose var name: String?,
                       @field:SerializedName(Keys.phonenumber) @field:Expose var phonenumber: String,
                       @field:SerializedName(Keys.senderMail) @field:Expose var senderMail: String,
                       @field:SerializedName(Keys.taxiId) @field:Expose var taxiId: String?


){

    @SerializedName("carType")
    @Expose
    private var carType: String? = null

    @SerializedName("destination")
    @Expose
    private var destination: String? = null

    @SerializedName("source")
    @Expose
    private var source: String? = null

    @SerializedName("email")
    @Expose
    private var email: String? = null

    @SerializedName("endDate")
    @Expose
    private var endDate: String? = null

    @SerializedName("id")
    @Expose
    private var id: String? = null


    @SerializedName("job")
    @Expose
    private var job: String? = null


    @SerializedName("startDate")
    @Expose
    private var startDate: String? = null

    @SerializedName("subject")
    @Expose
    private var subject: String? = null



//
//    {
//        "carType": "Taxi4",
//        "destination": "meenambakkam",
//        "email": "bikeuserbike@gmail.com",
//        "endDate": "2020-12-21T15:26:03.023Z",
//        "id": "string",
//        "job": "oneway",
//        "message": "good",
//        "name": "string",
//        "phonenumber": "9655555577",
//        "senderMail": "string",
//        "source": "guindy",
//        "startDate": "2020-12-21T15:26:03.023Z",
//        "subject": "good",
//        "taxiId": "5fdf4f7cfdc17b4e57506055"
//    }

    init {
        this.carType = "string";
        this.destination = "string";
        this.email = "taxideals.ch@gmail.com";
        this.endDate = "2020-05-09T11:00:45.017Z";
        this.id = 0.toString();
        this.job="string";
        this.message = message;
        this.name = name;
        this.phonenumber = phonenumber;
        this.senderMail = senderMail;
        this.source = "string";
        this.startDate = "2020-05-09T11:00:45.017Z";
        this.subject = "Feedback from USER - Android app";
        this.taxiId=taxiId
      /*  if (taxiid==null){
            this.taxiId = "dummy";
        }
        else {
            this.taxiId = taxiid;

    }*/

}}