package com.mongokerala.taxi.newuser.data.remote.fcm

import com.google.gson.annotations.SerializedName

class FcmNotificationIosPayload(fcmNotificationIosAps: FcmNotificationIosAps) {
    @SerializedName("aps")
    var notificationIosaps: FcmNotificationIosAps

    init {
        notificationIosaps = fcmNotificationIosAps
    }
}
