package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OtpRequest(
    @field:SerializedName(Keys.countryCode) @field:Expose var countryCode: String?,
    @field:SerializedName(Keys.phoneNumber) @field:Expose var phoneNumber: String

) {

    @Expose
    @SerializedName(Keys.token)
    var token: String
    @Expose
    @SerializedName(Keys.role)
    var role: String
    @Expose
    @SerializedName(Keys.password)
    var password: String

    @Expose
    @SerializedName(Keys.rideId)
    var rideId: String

    @Expose
    @SerializedName(Keys.userId)
    var userId: String

    @Expose
    @SerializedName(Keys.companyName)
    var companyName: String


    init {
        role = "ROLE_USER"
        token = "string"
        password = "string"
        rideId = "string"
        userId = "string"
        companyName = "KERALACABS"
    }


}