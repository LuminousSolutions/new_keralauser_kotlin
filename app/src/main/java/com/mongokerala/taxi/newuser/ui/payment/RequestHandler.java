package com.mongokerala.taxi.newuser.ui.payment;


import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Akhi007 on 14-08-2018.
 */
public class RequestHandler {

    private static RequestHandler instance;

    private static Retrofit retrofit;

    final static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build();

    //Common for all without Security Headers
    public static Retrofit getRetrofitClient(@NonNull String baseUrl){
            retrofit = new Retrofit.Builder().baseUrl(baseUrl).client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit;
    }

    @NonNull
    public static OkHttpClient getClient(){
        return okHttpClient;
    }

}
