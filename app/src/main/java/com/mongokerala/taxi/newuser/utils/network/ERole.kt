package com.mongokerala.taxi.newuser.utils.network

enum class ERole {
    ROLE_USER,
    ROLE_DRIVER,
    ROLE_SUPPLIER,
    ROLE_DELIVERY
}