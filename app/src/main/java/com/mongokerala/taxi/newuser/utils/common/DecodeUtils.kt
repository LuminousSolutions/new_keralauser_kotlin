package com.mongokerala.taxi.newuser.utils.common

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import java.io.FileNotFoundException

 object DecodeUtils {

    @Nullable
    @Throws(FileNotFoundException::class)
     fun decodeUri(activity: Activity, @NonNull selectedImage: Uri): Bitmap? {

        // Decode image size
        val o = BitmapFactory.Options()
        o.inJustDecodeBounds = true
        BitmapFactory.decodeStream(activity.contentResolver.openInputStream(selectedImage), null, o)

        // The new size we want to scale to
        val REQUIRED_SIZE = 140

        // Find the correct scale value. It should be the power of 2.
        var width_tmp = o.outWidth
        var height_tmp = o.outHeight
        var scale = 1
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break
            }
            width_tmp /= 2
            height_tmp /= 2
            scale *= 2
        }

        // Decode with inSampleSize
        val o2 = BitmapFactory.Options()
        o2.inSampleSize = scale
        return BitmapFactory.decodeStream(
            activity.contentResolver.openInputStream(selectedImage),
            null,
            o2
        )

    }

}