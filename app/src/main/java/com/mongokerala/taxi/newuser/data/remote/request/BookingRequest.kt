package com.mongokerala.taxi.newuser.data.remote.request

data class BookingRequest(
	val km: String? = null,
	val latitude: String? = null,
	val rideKeyLocal: String? = null,
	val tripId: String? = null,
	val source: String? = null,
	val message: String? = null,
	val title: String? = null,
	val userId: String? = null,
	val paymentType: String? = null,
	val token: String? = null,
	val userToken: String? = null,
	val coupen: String? = null,
	val des: String? = null,
	val phoneNumber: String? = null,
	val driverId: String? = null,
	val price: String? = null,
	val action: String? = null,
	val topic: String? = null,
	val category: String? = null,
	val isAndroid: String? = null,
	val longitude: String? = null
)

