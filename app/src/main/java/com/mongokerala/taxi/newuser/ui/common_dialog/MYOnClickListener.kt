package com.mongokerala.taxi.newuser.ui.common_dialog

interface MYOnClickListener {

    fun onButtonClick(action: String?)

    fun onBunAcceptClick(action: String?, coupon: String?)

    fun onBunRejectClick(action: String?)

    fun onUpdateReview(action: String?, cmd: String?, review: Double, ride_amt: String?)

    fun validateCoupon(coupon: String?): Boolean

}