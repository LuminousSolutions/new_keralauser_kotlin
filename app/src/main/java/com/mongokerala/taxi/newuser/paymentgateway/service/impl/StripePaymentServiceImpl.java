package com.mongokerala.taxi.newuser.paymentgateway.service.impl;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.mongokerala.taxi.newuser.paymentgateway.StripePaymentRequestOrder;
import com.mongokerala.taxi.newuser.paymentgateway.listener.CreateOrderPaymentListener;
import com.mongokerala.taxi.newuser.paymentgateway.model.ApiResponse;
import com.mongokerala.taxi.newuser.paymentgateway.model.StripeResponse;
import com.mongokerala.taxi.newuser.paymentgateway.service.StripePaymentService;
import com.mongokerala.taxi.newuser.paymentgateway.util.PaymentUtils;
import com.mongokerala.taxi.newuser.paymentgateway.util.PostRequestSender;
import com.mongokerala.taxi.newuser.paymentgateway.util.RequestHandler;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Akhi007 on 23-01-2019.
 */
public class StripePaymentServiceImpl implements StripePaymentService {
    private static final String TAG = StripePaymentServiceImpl.class.getSimpleName();

    @Override
    public void createStripeOrderPayment(@NonNull final CreateOrderPaymentListener orderPaymentListener, final StripePaymentRequestOrder orderDTO) {
        PostRequestSender postRequestSender = RequestHandler.getRetrofitClient(PostRequestSender.BASE_URL).create(PostRequestSender.class);
        Log.i(TAG, "createStripeOrderPayment Request: "+new Gson().toJson(orderDTO));
        Call<StripeResponse> orderResponseDTO = postRequestSender.createStripeOrderPayment(orderDTO);
        orderResponseDTO.enqueue(new Callback<StripeResponse>() {
            @Override
            public void onResponse(Call<StripeResponse> call, Response<StripeResponse> response) {
                Log.i(TAG, "Response: "+ new Gson().toJson(response.body()));
                if(response.isSuccessful() && null != response.body()){
                    orderPaymentListener.onRequestSuccess(orderDTO, response.body());
                }else if(null == response.body() && null != response.errorBody()){
                    Log.i(TAG, "Error Response: "+ new Gson().toJson(response.errorBody()));
                    ApiResponse apiResponse = PaymentUtils.convertApiErrorBodyToApiResponse(response.errorBody());
                    orderPaymentListener.onRequestFailed(orderDTO, apiResponse);
                } else {
                    //TODO write your own error code and message
                    orderPaymentListener.onRequestFailed(orderDTO, new ApiResponse("FAILED", "Something went wrong. Please try after sometime!"));
                }
            }

            @Override
            public void onFailure(Call<StripeResponse> call, Throwable t) {
                if( t instanceof SocketTimeoutException){
                    orderPaymentListener.onRequestTimeOut(orderDTO);
                }
            }
        });
    }

}
