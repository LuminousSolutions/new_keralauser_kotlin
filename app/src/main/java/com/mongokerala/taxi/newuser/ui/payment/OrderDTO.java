package com.mongokerala.taxi.newuser.ui.payment;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public class OrderDTO {
    private OrderRequestType orderReqType;
    private String orderId;
    private CustomerDTO customer; // customer info doing payment
    private PGDataDTO pgData; // payment gateway checksum

    public OrderRequestType getOrderReqType() {
        return orderReqType;
    }

    public void setOrderReqType(OrderRequestType orderReqType) {
        this.orderReqType = orderReqType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public PGDataDTO getPgData() {
        return pgData;
    }

    public void setPgData(PGDataDTO pgData) {
        this.pgData = pgData;
    }
}
