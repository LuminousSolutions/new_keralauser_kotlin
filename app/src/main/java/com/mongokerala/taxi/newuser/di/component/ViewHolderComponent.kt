package com.mongokerala.taxi.newuser.di.component

import com.mongokerala.taxi.newuser.di.ViewModelScope
import com.mongokerala.taxi.newuser.di.module.ViewHolderModule
import com.mongokerala.taxi.newuser.ui.dummies.DummyItemViewHolder
import com.mongokerala.taxi.newuser.ui.home.posts.PostItemViewHolder
import dagger.Component

@ViewModelScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ViewHolderModule::class]
)
interface ViewHolderComponent {

    fun inject(viewHolder: DummyItemViewHolder)

    fun inject(viewHolder: PostItemViewHolder)
}