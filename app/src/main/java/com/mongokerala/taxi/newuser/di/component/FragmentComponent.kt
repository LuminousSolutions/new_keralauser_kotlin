package com.mongokerala.taxi.newuser.di.component

import com.mongokerala.taxi.newuser.di.FragmentScope
import com.mongokerala.taxi.newuser.di.module.FragmentModule
import com.mongokerala.taxi.newuser.ui.dummies.DummiesFragment
import com.mongokerala.taxi.newuser.ui.forgetpassword.ForgetPasswordFragment
import com.mongokerala.taxi.newuser.ui.home.HomeFragment
import com.mongokerala.taxi.newuser.ui.language.LanguageFragment
import com.mongokerala.taxi.newuser.ui.profile.ProfileFragment
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [FragmentModule::class]
)
interface FragmentComponent {

    fun inject(fragment: DummiesFragment)

    fun inject(fragment: ProfileFragment)


    fun inject(fragment: HomeFragment)

    fun inject(fragment: ForgetPasswordFragment)

    fun inject(fragment: LanguageFragment)


}