 package com.mongokerala.taxi.newuser.data.remote.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


 public class FcmUpdateRequest
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("notificationToken")
    @Expose
    private String notificationToken;
    @SerializedName("values")
    @Expose
    private String values;
    private final static long serialVersionUID = 5605037063646454297L;

    public FcmUpdateRequest( String id, double latitude, double longitude,  String token, String values,String notification_token) {




        this.id=id;
        this.latitude=latitude;
        this.longitude=longitude;
        this.token=token;
        this.notificationToken=notification_token;
        this.values=values;



    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

}

