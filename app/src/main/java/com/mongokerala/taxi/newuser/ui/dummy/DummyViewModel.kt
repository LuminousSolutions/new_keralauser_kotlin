package com.mongokerala.taxi.newuser.ui.dummy

import com.mongokerala.taxi.newuser.data.repository.DummyRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable


class DummyViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val dummyRepository: DummyRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    override fun onCreate() {
        // do something
    }
}