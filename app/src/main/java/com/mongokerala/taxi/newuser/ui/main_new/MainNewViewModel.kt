package com.mongokerala.taxi.newuser.ui.main_new

import android.net.Uri
import android.os.AsyncTask
import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.data.remote.fcm.FcmCustomizeNotificationData
import com.mongokerala.taxi.newuser.data.remote.fcm.FcmNotificationDto
import com.mongokerala.taxi.newuser.data.remote.fcm.FcmNotificationPriority
import com.mongokerala.taxi.newuser.data.remote.fcm.FcmNotificationTitle
import com.mongokerala.taxi.newuser.data.remote.request.*
import com.mongokerala.taxi.newuser.data.remote.response.PriceCalculationResponse
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.AppUtils
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.common.FireStoreCloud
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.network.ServerFasade
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import org.json.JSONObject
import timber.log.Timber
import java.net.URL
import java.util.*


class MainNewViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {
    val launchLogin: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val loadGoogleMapsRoadPointsTask: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val success: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val getuseremail: MutableLiveData<String> = MutableLiveData()
    val getusername: MutableLiveData<String> = MutableLiveData()
    val RIDE_Status: MutableLiveData<String> = MutableLiveData()
    val sourceId: MutableLiveData<String> = MutableLiveData()
    val driverTrackingId: MutableLiveData<String> = MutableLiveData()
    val couponPrice: MutableLiveData<String> = MutableLiveData()
    val mainIn: MutableLiveData<Boolean> = MutableLiveData()
    val checkCoupon: MutableLiveData<Boolean> = MutableLiveData()
    val myCoupon: MutableLiveData<String> = MutableLiveData()
    var liveDataSearchTaxiResponse:MutableLiveData<AdvanceSearchResponse> = MutableLiveData()
    var liveDataShowTaxiResponse:MutableLiveData<AdvanceSearchResponse> = MutableLiveData()
    var liveDataNotaxi:MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    var updateVechile:MutableLiveData<List<AdvanceSearhData?>> = MutableLiveData()
    var liveBookingResponse:MutableLiveData<BookingResponse> = MutableLiveData()
    var rideFinishReview:MutableLiveData<Boolean> =MutableLiveData()
    val isprofileImage: MutableLiveData<String?> = MutableLiveData()
    val totalamout: MutableLiveData<String> = MutableLiveData()
    val profileimagestatus: MutableLiveData<Boolean> = MutableLiveData()
    private val countDownTimer: CountDownTimer? = null
    private val db = FirebaseFirestore.getInstance()
    var wallet_check:MutableLiveData<Boolean> =MutableLiveData()
    var wallet_balance:MutableLiveData<Int> =MutableLiveData()
    var wallet_credit:MutableLiveData<Int> =MutableLiveData()
    var walletTokenBalance:MutableLiveData<String> =MutableLiveData()
    var add_walletTokenBalance:MutableLiveData<String> =MutableLiveData()
    var checkToken:MutableLiveData<Boolean> =MutableLiveData()
    var userWalletBalance:MutableLiveData<Boolean> =MutableLiveData()


    override fun onCreate() {

    }

   fun showProgress(){
       mainIn.postValue(true)
   }
    fun closeShowProgress(){
        mainIn.postValue(false)
    }
    fun searchTaxies(searchTaxiIRequest: AdvanceSeachApiRequest) {
        compositeDisposable.addAll(
            userRepository.doSearchTaxiRequest(searchTaxiIRequest)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if (it.data != null) {
                            if (searchTaxiIRequest.latitude != null && searchTaxiIRequest.longitude != null){
                                userRepository.saveUserSearchLatLng(searchTaxiIRequest.latitude,searchTaxiIRequest.longitude)
                            }
                            liveDataSearchTaxiResponse.postValue(it)
                            updateVechile.postValue(it.data!!)
                        }
                        if (it.data?.size == 0 || it.data?.isEmpty()!!) {
                            liveDataNotaxi.postValue(Event(emptyMap()))


                        }
                    },

                    {
                        handleNetworkError(it)
                    }
                )
        )
    }
    fun showTaxies(showTaxiIRequest: AdvanceSeachApiRequest) {
        compositeDisposable.addAll(
            userRepository.doShowTaxiRequest(showTaxiIRequest)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if (it.data != null) {

                            liveDataShowTaxiResponse.postValue(it)

                        }

                    },

                    {
                        handleNetworkError(it)
                    }
                )
        )
    }

    fun getBaseDistance():String{
        return userRepository.getbaseKm().toString()
    }

    fun saveBasekm(km:String){
        userRepository.saveBaseKm(km)
    }


    inner class LoadGoogleMapsRoadPointsTask :
            AsyncTask<String?, Void?, Boolean>() {
        var place_id = ""
        override fun onPreExecute() {
        }

        override fun doInBackground(vararg params: String?): Boolean {
            try {
                val googlePlaceIDApiRequest: String? =
                        AppUtils.getPlaceIdRequest(params[0], params[1], params[2])
                val uriBuilder =
                        Uri.parse(googlePlaceIDApiRequest).buildUpon()
                val googlePlaciIDApiUrl = URL(uriBuilder.build().toString())
                val googleDirectionsApiResponse: String =
                        ServerFasade.httpPost(googlePlaciIDApiUrl, null, null)
                val directionJson: JSONObject = ServerFasade.makeJsonFromResponse(
                        googleDirectionsApiResponse,
                        JSONObject::class.java
                )
                place_id = directionJson.getJSONArray("results").getJSONObject(0)["place_id"]
                        .toString()
                if (place_id.isNotEmpty()){
                    sourceId.postValue(place_id)
                }
                //new
                return true
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                loadGoogleMapsRoadPointsTask.postValue(Event(emptyMap()))
            }
            return false
        }
    }
    inner class DriverTrackingId :
        AsyncTask<String?, Void?, Boolean>() {
        var place_id = ""
        override fun onPreExecute() {
        }

        override fun doInBackground(vararg params: String?): Boolean {
            try {
                val googlePlaceIDApiRequest: String? =
                    AppUtils.getPlaceIdRequest(params[0], params[1], params[2])
                val uriBuilder =
                    Uri.parse(googlePlaceIDApiRequest).buildUpon()
                val googlePlaciIDApiUrl = URL(uriBuilder.build().toString())
                val googleDirectionsApiResponse: String =
                    ServerFasade.httpPost(googlePlaciIDApiUrl, null, null)
                val directionJson: JSONObject = ServerFasade.makeJsonFromResponse(
                    googleDirectionsApiResponse,
                    JSONObject::class.java
                )
                place_id = directionJson.getJSONArray("results").getJSONObject(0)["place_id"]
                    .toString()
                if (place_id.isNotEmpty()){
                    driverTrackingId.postValue(place_id)
                }
                //new
                return true
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            return false
        }
    }

        fun hitBookingApi(searchTaxiIRequest: BookingRequest) {
            compositeDisposable.addAll(
                    userRepository.doBookingTaxiRequest(searchTaxiIRequest)
                            .subscribeOn(schedulerProvider.io())
                            .subscribe(
                                    {
                                        if (it.data != null) {
                                            liveBookingResponse.postValue(it)
                                        }
                                    },
                                    {
                                        handleNetworkError(it)
                                    }
                            )
            )
        }

        fun couponBookingApi(coupon_searchTaxiIRequest: CouponBookingRequest) {
            compositeDisposable.addAll(
                userRepository.doCouponBookingTaxiRequest(coupon_searchTaxiIRequest)
                    .subscribeOn(schedulerProvider.io())
                    .subscribe(
                        {

                        },
                        {

                            handleNetworkError(it)
                        }
                    )
            )
        }

        ///12345
        fun onUserDetail() {
            mainIn.postValue(true)
            compositeDisposable.addAll(
                    userRepository.doUserdetailRequest(userRepository.getCurrentUserID()!!)
                            .subscribeOn(schedulerProvider.io())
                            .subscribe(
                                    {

                        if (it.data.phoneNumber != null) {
                            userRepository.saveMobileNumber(it.data.phoneNumber)
                        }
                        /*if (it.data.imageInfo != null) {
                            userRepository.setImageUrl(ImageInfo(it.data.imageInfo.imageUrl))
                        }*/

                        if (it.data.domain != null) {
                            userRepository.saveDomain(it.data.domain)
                        }
                        if (it.data.website != null) {
                            userRepository.saveWebsite(it.data.website)
                        }
                        if (it.data.email != null) {
                            userRepository.saveEmail(it.data.email)
                            getuseremail.postValue(it.data.email)
                        }
                        if (it.data.firstName != null) {
                            userRepository.saveName(it.data.firstName)
                            getusername.postValue(it.data.firstName)
                        }
                    },
                    {
                        handleNetworkError(it)
                    }
                )
        )
    }

    fun onUserwalletBalance() {
        mainIn.postValue(true)
        compositeDisposable.addAll(
            userRepository.doUserwalletBalanceRequest(userRepository.getCurrentUserID()!!)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {

                        wallet_check.postValue(it.status)

                        if (it.status){
                            if (it.data != null){
                                wallet_balance.postValue(it.data.balance)
                                wallet_credit.postValue(it.data.credit)

                            }
                        }


                    },
                    {

                        handleNetworkError(it)
                    }
                )
        )
    }

    fun addWalletAmount(token: String){
        compositeDisposable.addAll(
                userRepository.addWalletAmount(500, token, userRepository.getCurrentUserID()!!)
                        .subscribeOn(schedulerProvider.io())
                        .subscribe(
                                {
                                    if (it.status){
                                        userWalletBalance.postValue(it.status)
                                    }else{
                                        userWalletBalance.postValue(it.status)
                                    }

                                },
                                {
                                    handleNetworkError(it)
                                }
                        )
        )
    }






    fun onUserTokencheck(token:String) {
        mainIn.postValue(true)
        compositeDisposable.addAll(
            userRepository.doUserwalletTokenRequest(token)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {

                        if (it.status){
                            checkToken.postValue(true)
                            if (it.message == "Valid" && it.data != null){
                                add_walletTokenBalance.postValue(it.data.token)
                            }
                            if (it.data != null){
                                if (it.data.balance != 0){
                                    walletTokenBalance.postValue(it.data.balance.toString())
                                }else{
                                    walletTokenBalance.postValue(it.data.credit.toString())

                                }
                            }
                        }else{
                            checkToken.postValue(false)
                        }

                    },
                    {

                        handleNetworkError(it)
                    }
                )
        )
    }
    fun onUserDetaiImage(){

        compositeDisposable.addAll(
                userRepository.doCheckUserImage(userRepository.getCurrentUserID()!!)
                        .subscribeOn(schedulerProvider.io())
                        .subscribe(
                                {
                                    if (it.data != null) {
                                        isprofileImage.postValue(it.data.isImage)

                                    }

                                },
                                {
                                    handleNetworkError(it)
                                }
                        )
        )

    }

    fun doLoginStatus() {

        compositeDisposable.addAll(
            userRepository.doLoginStatusRequest(
                userRepository.getCurrentUserID()!!,
                "LOGOUT"
            )
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if (it.infoId.equals("500")) {
                            userRepository.setCategory("")
                        }
                        if (it.status.equals("true")) {
                            userRepository.saveLoginStatus("LOGOUT")
                            userRepository.saveFbLoginStatus("LOGOUT")
                            userRepository.savegmailLoginStatus("LOGOUT")
                        }
                        ClearData()
                        launchLogin.postValue(Event(emptyMap()))

                    },
                    {
                        handleNetworkError(it)
                    }
                )
        )
    }
    fun getFacebookLoginStatus(): String? {
        return userRepository.getFbLoginstatus()
    }

    fun onReviewSubmitButtonClicked(cmd: String?, review: Double){
       compositeDisposable.addAll(
           userRepository.doUpdateReview(
               userRepository.getClient_Id()!!, userRepository.getTaxiId()!!, "4",
               userRepository.getCurrentUserID()!!, userRepository.getRide_Id()!!, "USER"
           )
               .subscribeOn(schedulerProvider.io())
               .subscribe(
                   {

                                   rideFinishReview.postValue(true)
                               },
                               {
                                   handleNetworkError(it)
                               }
                       )
       )


    }

    private fun ClearData() {
       userRepository.setAccessToken("")
       userRepository.saveOneSignalValue("")
       userRepository.saveCurrentUserID("")
       userRepository.saveSupplierId("")
       userRepository.saveTaxiId("")
       userRepository.saveLan("")
       userRepository.saveCurrentStatus("")
       userRepository.saveName("")
       userRepository.saveEmail("")
       userRepository.saveCarType("")
       userRepository.setFirstLogin(true)
        userRepository.setRIDE_KEY_LOCAL("")
        userRepository.setIs_Android(false)
        userRepository.setClient_FCM_Token(null)
//        userRepository.set_Source_Place_id("")
//        userRepository.set_Destination_Place_id("")
        userRepository.saveSource("")
        userRepository.setClient_ID("")
        userRepository.saveDestination("")
        userRepository.setClient_FCM_Token("")

    }

    fun  getGmailLoginStatus():String?{
        return userRepository. getGmailLoginStatus()
    }

    fun getgMailLoginImageUrl(): String? {
        return userRepository.getgMailLoginImageUrl()
    }

    fun getCustomerCareNumber(): String? {
        return userRepository.getCustomerCareNumber()
    }

    fun saveCustomerCareNumber(customercareNumber: String) {
        userRepository.saveCustomerCareNumber(customercareNumber)
    }


    fun getRIDE_KEY_LOCAL():String?{
        return userRepository.getRIDE_KEY_LOCAL()
    }

    fun setRIDE_KEY_LOCAL(rideKey:String){
        userRepository.setRIDE_KEY_LOCAL(rideKey)
    }
    fun saveCategeory(category: String){
        userRepository.setCategory(category)
    }
    fun getCategory():String?{
        return userRepository.getCategory()
    }
    fun getDestination(): String? {
        return userRepository.getDestination()
    }
    fun saveDestination(destination:String){
       userRepository.saveDestination(destination)
    }
    fun getSource():String?{
         return  userRepository.getSource()
    }
    fun saveSource(source:String){
        userRepository.saveSource(source)
    }
    fun saveCurrentStataus(status:String) {
        userRepository.saveCurrentStatus(status)
    }
    fun getCurrentStatus(){
        userRepository.getCurrentStatus()
    }
    fun showProgressBar() {
        mainIn.postValue(true)
    }

    fun getimageresponse(): ImageInfo? {
        return userRepository.getImageUrl()
    }

    fun getUserID(): String? {
        return userRepository.getCurrentUserID()
    }

    fun getRidekey(): String?{
        return (userRepository.getCurrentUserID()+System.currentTimeMillis())
    }
    fun getSupplierID(): String? {
        return userRepository.getSupplierId()
    }

    fun getClient_Mobile_NUM(): String? {
        return userRepository.getClientMobileNUmber()
    }

    fun getDriverMobileNumber() : String? {
        return userRepository.getDriverMobileNUmber()
    }
    fun saveDriverMobileNumber(driverNumber :String){
        userRepository.saveDriverMobileNumber(driverNumber)
    }

    fun getUserGoogleKm(): String? {
        return userRepository.getApxGoogleKm()
    }

    fun getSourcePlaceid(): String? {
        return userRepository.get_Source_Place_id()
    }

    fun setSource_Place_ID(source_place_id: String?) {
        userRepository.set_Source_Place_id(source_place_id)
    }

    fun getLocalStataus(): String?{
        return userRepository.getLocalStatus()
    }

    fun saveLocalStatus(status: String) {
        return userRepository.saveLocalStatus(status)
    }

    fun stopTimer(){
        countDownTimer?.cancel()
    }

    fun getlocationlat():Double{
        return userRepository.getDriverCurrentlatitude()!!
    }
    fun getlocationlang():Double{
        return userRepository.getDriverCurrentLongitude()!!
    }
    fun setDriverCurrentLatLng(latitude: Double, longitude: Double) {
        userRepository.setDriverCurrentLatLng(latitude, longitude)
    }
    fun getClient_FCM_Token():String?{
        return userRepository.getClient_FCM_Token()
    }
    fun setClient_FCM_Token(driverFcm:String){
        userRepository.setClient_FCM_Token(driverFcm)
    }
    fun getCilentId(): String? {
        return userRepository.getClient_Id()
    }
    fun saveCilentId(cilentId:String){
        userRepository.setClient_ID(cilentId)
    }
    fun getRideId(){
        userRepository.getRide_Id()
    }
    fun setRideId(rideId:String){
        userRepository.setRide_ID(rideId)
    }
    fun getVehiclenumber():String?{
        return  userRepository.getVehiclenumber()
    }
    fun saveVehicle_number(vehicleNumber: String){
         userRepository.saveVehicle_number(vehicleNumber)
    }
    fun getVehicleBrand(): String? {
        return userRepository.getVehicleBrand()
    }
    fun saveVehicleBrand(VehicleBrand:String){
        userRepository.saveVehicleBrand(VehicleBrand)
    }
    fun getClientName(): String? {
        return userRepository.getClientName()
    }
    fun saveClientName(client_name: String) {
        userRepository.saveClientName(client_name)
    }
    fun doGetFbImageurl(): String?{
        return userRepository.getFbImageUrl()
    }
    fun getUserSearchLatitude(): Double{
        return userRepository.getUserSearchLatitude()
    }
    fun getUserSearchLangitude(): Double{
        return userRepository.getUserSearchLangitude()
    }
    fun saveUserSearchLatLng(latitude: Double,langitude:Double){
        userRepository.saveUserSearchLatLng(latitude,langitude)
    }
    fun getDriverTrackingLocationId(source_latitude: String, source_langtitude: String, place: String){
        DriverTrackingId().execute(
            java.lang.String.valueOf(source_latitude),
            java.lang.String.valueOf(source_langtitude),
            place
        )
    }
    fun saveSearchDestinationLatLang(latitude: Double, longitude: Double) {
        userRepository.saveSearchDestinationLatLang(latitude,longitude)
    }
    fun getSearchDestinationLatitude(): Double{
        return userRepository.getSearchDestinationLatitude()
    }
    fun getSearchDestinationLangitude(): Double{
        return userRepository.getSearchDestinationLangitude()
    }


    fun change_Destination_ByFCM(new_Des: String, action: String, msg: String) {
        if (userRepository.getUser_FCM_Token() == null) {
//            onDrawerOptionLogoutClick(false)
        } else {
            val fcmCustomizeNotificationData =
                FcmCustomizeNotificationData(
                    action,
                        userRepository.getSource()!!,
                        userRepository.getDestination()!!,
//                        userRepository.get_Source_Place_id()!!,
//                        userRepository.get_Destination_Place_id()!!,
                    userRepository.getUser_FCM_Token(),
                        userRepository.getCurrentUserID(),
                    true,
//                    getDataManager().getMobileNumber()
                        userRepository.getMobileNUmber()
                )
            fcmCustomizeNotificationData.latitude= userRepository.getDriverCurrentlatitude()!!
            fcmCustomizeNotificationData.longitude=userRepository.getDriverCurrentLongitude()!!
            fcmCustomizeNotificationData.newDestination=new_Des
            fcmCustomizeNotificationData.message=msg
            fcmCustomizeNotificationData.RIDE_KEY_LOCAL=userRepository.getRIDE_KEY_LOCAL()
            val notificationTitle = FcmNotificationTitle()
            notificationTitle.setMessageText(msg)
            if (!userRepository.getIsAndroid()) {
                sendNotificationbyFCMAndroid_IOS(
                    getClient_FCM_Token()!!,
                    action,
                    msg,
                    notificationTitle,
                    fcmCustomizeNotificationData
                )
            } else {
                sendNotificationbyFCMAndroid_IOS(
                    getClient_FCM_Token()!!,
                    action,
                    msg,
                    notificationTitle,
                    fcmCustomizeNotificationData
                )
            }
            saveDestination(new_Des)
        }
    }

    fun cancel_Ride_ByFCM(rideCancelBooking: String, reasonMsg: String) {
        try {
            if (userRepository.getUser_FCM_Token() == null) {
//                onDrawerOptionLogoutClick(false)
            }
            else {
                val fcmCustomizeNotificationData =
                    FcmCustomizeNotificationData(
                        rideCancelBooking,
                            userRepository.getSource()!!,
                            userRepository.getDestination()!!,
//                        userRepository.get_Source_Place_id()!!,
//                        userRepository.get_Destination_Place_id()!!,
                        userRepository.getUser_FCM_Token(),
                        userRepository.getCurrentUserID(),
                        true,
//                        getClient_Mobile_NUM()
                        userRepository.getMobileNUmber()
                    )

                fcmCustomizeNotificationData.latitude= userRepository.getDriverCurrentlatitude()!!
                fcmCustomizeNotificationData.longitude=userRepository.getDriverCurrentLongitude()!!
                fcmCustomizeNotificationData.message=reasonMsg
                fcmCustomizeNotificationData.RIDE_KEY_LOCAL=userRepository.getRIDE_KEY_LOCAL()
                val notificationTitle = FcmNotificationTitle()
                notificationTitle.setMessageText(reasonMsg)
                if (!userRepository.getIsAndroid()) {
                    sendNotificationbyFCMAndroid_IOS(
//                        userRepository.getClient_FCM_Token()!!,
                            getClient_FCM_Token()!!,
                        rideCancelBooking,
                        reasonMsg,
                        notificationTitle,
                        fcmCustomizeNotificationData
                    )
                }
                else {
                    sendNotificationbyFCMAndroid_IOS(
//                        userRepository.getClient_FCM_Token()!!,
                            getClient_FCM_Token()!!,
                        rideCancelBooking,
                        reasonMsg,
                        notificationTitle,
                        fcmCustomizeNotificationData
                    )
                }
            }
        } catch (e: Exception) {
        }
    }

    private fun sendNotificationbyFCMAndroid_IOS(userFcmToken: String, action: String, message: String, notification_title: FcmNotificationTitle, fcmCustomizeNotificationData: FcmCustomizeNotificationData) {

        /* getMvpView().RIDE_KEY();*/
        Timber.tag("FCM_NOTIFICATION")
            .i("Action: $action , User Fcm Token: $userFcmToken")
        val userToken = arrayOf<String>(userFcmToken)
        val fcmNotificationDto = FcmNotificationDto(
            userToken,
            notification_title,
            FcmNotificationPriority(),
            fcmCustomizeNotificationData
        )
        fcmNotificationDto.data.cancelReason=message
        if (userRepository.getRide_Id() != null) {
            fcmNotificationDto.data.rideId= userRepository.getRide_Id()!!
        }

        compositeDisposable.addAll(
            userRepository.doFcmRequest(fcmNotificationDto)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if (it.results != null) {

                        }
                    },
                    {
                        handleNetworkError(it)
                    }
                )
        )
    }

    fun createRideReq(donepickup: String, payment_mode: String) {
      var paymentMode:String=payment_mode
        if (payment_mode.equals(R.string.Cash)) {
            paymentMode = "CASH"
        }
        if (payment_mode.equals(R.string.googlepay)) {
            paymentMode = "GPay"
        }
//        doRideCreateRequest
        compositeDisposable.addAll(
                userRepository.doRideCreateRequest(
                    userRepository.getClient_Id().toString(),
                    donepickup,
                    getSource()!!,
                    getDestination()!!,
                    userRepository.getCurrentUserID()!!,
                    paymentMode
                )
                        .subscribeOn(schedulerProvider.io())
                        .subscribe(
                                {

                                },
                                {
                                    handleNetworkError(it)
                                }
                        )

        )
    }

    fun createRideStatus(donepickup: String,reasonmsg:String, payment_mode: String) {
        var paymentMode:String=payment_mode
        if (payment_mode.equals(R.string.Cash)) {
            paymentMode = "CASH"
        }
        if (payment_mode.equals(R.string.googlepay)) {
            paymentMode = "GPay"
        }
        var kmvalue: String? = userRepository.getApxGoogleKm()
        if (kmvalue.isNullOrEmpty()) {
            kmvalue = "0"
        }

        compositeDisposable.addAll(
                userRepository.doRideStatusCreateRequest(
                        userRepository.getClient_Id().toString(),
                        donepickup,
                        getSource()!!,
                        getDestination()!!,
                        userRepository.getCurrentUserID()!!,
                        paymentMode,
                        "string",
                        userRepository.getCategory()!!,
                        kmvalue,
                        reasonmsg
                )
                        .subscribeOn(schedulerProvider.io())
                        .subscribe(
                                {

                                },
                                {
                                    handleNetworkError(it)
                                }
                        )

        )
    }

    fun onViewInitialized() {
        /*
        commanded this line due to crash issue
        val settings = FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build()
                db.firestoreSettings = settings
                */
//            New_RIde_key()
            getUpdatedStatusBooking()
    }

    private fun getUpdatedStatusBooking() {

        if (!userRepository.getRIDE_KEY_LOCAL().toString().isEmpty()) {
            val db = FirebaseFirestore.getInstance()
            val docRef =
                    userRepository.getRIDE_KEY_LOCAL()?.let { db.collection("RIDE_KEY").document(it) }
            docRef?.get()
                    ?.addOnSuccessListener { documentSnapshot -> //City city = documentSnapshot.toObject(City.class);
                        try {
                            if (documentSnapshot == null) {

                            } else if (Objects.requireNonNull(documentSnapshot["RIDE_Status"]).toString().isNotEmpty()) {
                                RIDE_Status.postValue(documentSnapshot["RIDE_Status"].toString())
                            }
                        } catch (e: java.lang.Exception) {
                            Timber.e(e.toString())
                            if (userRepository.getCurrentStatus().toString().isNotEmpty()) {
                                RIDE_Status.postValue(userRepository.getCurrentStatus())
                            }
                        }
                    }
        }
    }

    private fun New_RIde_key() {
        if (getRIDE_KEY_LOCAL() == null || getRIDE_KEY_LOCAL().toString().isEmpty()) {
            userRepository.setRIDE_KEY_LOCAL(getRidekey())
            FireStoreCloud.cloud_ride_key(getRIDE_KEY_LOCAL().toString(), db)
        }
        if (getRIDE_KEY_LOCAL() != null && !getRIDE_KEY_LOCAL().toString().isEmpty()) {
            val docRef = db.collection("RIDE_KEY").document(getRIDE_KEY_LOCAL().toString())
            docRef.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val document = task.result!!
                    if (document.exists() && !document.metadata.hasPendingWrites()) {

                    } else {
//                        assert(getMvpView() != null)
                        // Toaster.INSTANCE.show(getMvpView().getContext(),"Check Your Internet");
                        // FireStoreCloud.cloud_Ride_Key_Update(getDataManager().getRide_key_Local());
                    }
                } else {
                    FireStoreCloud.cloud_ride_key(getRIDE_KEY_LOCAL().toString(), db)
                }
            }
        }

    }

    fun getSourceid(source_latitude: String, source_langtitude: String, place: String) {
        LoadGoogleMapsRoadPointsTask().execute(
                java.lang.String.valueOf(source_latitude),
                java.lang.String.valueOf(source_langtitude),
                place
        )

    }

    fun checkingCoupon(coupon: String) {
        compositeDisposable.addAll(
                userRepository.checkingCoupon(coupon)
                        .subscribeOn(schedulerProvider.io())
                        .subscribe(
                                {
                                    if (it.status){
                                        checkCoupon.postValue(true)
                                        myCoupon.postValue(coupon)
                                    }else{
                                        checkCoupon.postValue(false)
                                    }
                                },

                                {
                                    handleNetworkError(it)
                                })
                        )

    }

    fun pricecalculation(km: String, car_type: String){

        compositeDisposable.addAll(
            userRepository.getCategory()?.let {
                userRepository

                    .doPriceCalculation(

                        km, car_type,
                        "",
                        "",
                        "KERALACABS",
                        "",
                        "",
                        "",
                        "kerala"

                    )
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .subscribe({ response: PriceCalculationResponse ->

                    }, {

                    })
            }
        )
    }



    fun dogetTotalAmount(userId: String?) {
        compositeDisposable.addAll(
                userRepository.dogetTotalAmount(userId.toString())
                        .subscribeOn(schedulerProvider.io())
                        .subscribe(
                                {
                                    try {
                                        if (it.status) {
//                                            totalamout.postValue(it.data.debit.toString())
                                        }
                                    } catch (e: Exception) {
                                    }
                                },
                                {
                                    //handleNetworkError(it)
                                }
                        )
        )
    }

    fun doCheckCouponPrice(amount:String,coupon: String){

        compositeDisposable.addAll(
                userRepository.doCheckCouponPrice(amount,coupon)
                        .subscribeOn(schedulerProvider.io())
                        .subscribe(
                                {
                                    if (it.status){
                                        couponPrice.postValue(it.data!!)
                                    }
                                },
                                {
                                    handleNetworkError(it)
                                }
                        )
                 )
    }

    fun saveOnPauseState(onpause: Boolean) {
        userRepository.saveOnPauseState(onpause)
    }
    fun getOnPauseState():Boolean{
        return userRepository.getOnPauseState()
    }

    fun onPauseSaveBooking(onpauseBooking: Boolean) {
        userRepository.onPauseSaveBooking(onpauseBooking)
    }
    fun onPausegetBooking():Boolean {
        return userRepository.onPausegetBooking()
    }

    fun driverAcceptStatus(isAccept: Boolean) {
         userRepository.saveisDriverAccept(isAccept)
    }
    fun getIsDriverAccept():Boolean{
        return userRepository.getIsDriverAccept()
    }

    fun getDomain():String? {
        return userRepository.getDomain()
    }

    fun setUser_FCM_Token(FCM_Token: String?) {
        userRepository.setUser_FCM_Token(FCM_Token)
    }

    fun getUser_FCM_Token(): String? {
        return userRepository.getUser_FCM_Token()
    }

    fun  getUserName():String?{
        return userRepository.getName()
    }

    fun getUserEmail():String? {
        return userRepository.getEmail()
    }

    fun getUserMobileNumber():String?{
        return userRepository.getMobileNUmber()
    }


    /* fun getLocation_Filter(context: Context?): String? {
         var country: String? = null
         if (!userRepository.getDriverCurrentlatitude().equals(0.0) && AppUtils.latLongToAddress(
                 userRepository.getDriverCurrentlatitude(), getDataManager()
                     .getDriverCurrentLongitude(),
                 context!!, is_country
             ) != null
         ) {
             country = if (AppUtils.latLongToAddress(
                     getDataManager().getDriverCurrentlatitude(), getDataManager()
                         .getDriverCurrentLongitude(),
                     context, is_country
                 ).equalsIgnoreCase("india")
             ) {
                 "in"
             } else if (AppUtils.latLongToAddress(
                     getDataManager().getDriverCurrentlatitude(), getDataManager()
                         .getDriverCurrentLongitude(),
                     context, is_country
                 ).equalsIgnoreCase("france")
             ) {
                 "fr"
             } else if (AppUtils.latLongToAddress(
                     getDataManager().getDriverCurrentlatitude(), getDataManager()
                         .getDriverCurrentLongitude(),
                     context, is_country
                 ).equalsIgnoreCase("United Kingdom")
             ) {
                 "gb"
             } else if (AppUtils.latLongToAddress(
                     getDataManager().getDriverCurrentlatitude(), getDataManager()
                         .getDriverCurrentLongitude(),
                     context, is_country
                 ).equalsIgnoreCase("ഇന്ത്യ")
             ) {
                 "in"
             } else {
                 "ch"
             }
         } else {
             getMvpView().intiMyLocation()
         }
         return country
     }*/

}



