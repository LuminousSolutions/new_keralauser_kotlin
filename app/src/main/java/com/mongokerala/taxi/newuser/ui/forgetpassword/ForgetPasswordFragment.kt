package com.mongokerala.taxi.newuser.ui.forgetpassword

import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.FragmentComponent
import com.mongokerala.taxi.newuser.ui.base.BaseFragment
import com.mongokerala.taxi.newuser.ui.login.LoginActivity
import com.mongokerala.taxi.newuser.ui.reqotp.ReqotpActivity
import com.mongokerala.taxi.newuser.ui.setting.SettingActivity
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.display.Toaster
import kotlinx.android.synthetic.main.fragment_forget_password.*

class ForgetPasswordFragment : BaseFragment<ForgetPasswordViewModel>() {

    companion object {

        const val TAG = "ForgetPasswordFragment"

        fun newInstance(): ForgetPasswordFragment {
            val args = Bundle()
            val fragment = ForgetPasswordFragment()
            fragment.arguments = args
            return fragment
        }
    }

    var me: Fragment = this
    var countDownTimer: CountDownTimer? = null


    override fun provideLayoutId(): Int = R.layout.fragment_forget_password

    override fun injectDependencies(fragmentComponent: FragmentComponent) {
        fragmentComponent.inject(this)
    }

    override fun setupObservers() {
        super.setupObservers()

        viewModel.launchValidate.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                lay_forget_dialog.visibility = View.GONE
                lay_validate_otp.visibility = View.VISIBLE
                validate_setUp()
                context?.let { it1 -> Toaster.show(it1,getString(R.string.success))
                }
            }
        })
        viewModel.launchValidateUsedNumber.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                context?.let { it1 -> Toaster.show(it1,getString(R.string.number_is_not_existing_in_database)) }
            }
        })


        viewModel.launchOtpSuccess.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                targetFragment?.let { it1 ->
                    activity?.supportFragmentManager?.beginTransaction()?.remove(
                        it1
                    )?.commit()
                };
                val intent = Intent(context, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                context?.let { it1 -> Toaster.show(it1,getString(R.string.password_changed)) }

            }
        })
        viewModel.launchOtpfail.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                context?.let { it1 -> Toaster.show(it1,getString(R.string.number_is_not_existing_in_database)) }
            }
        })

    }

    override fun setupView(view: View) {
        turnGPSOn()
        num_editText.addTextChangedListener(mTextWatcher)
        // run once to disable if empty
        checkFieldsForEmptyValues()
        //req_otp_forget.background = resources.getDrawable(R.drawable.button_curved_shadow)
        req_otp_forget.background = ResourcesCompat.getDrawable(getResources(), R.drawable.button_curved_shadow, null);
        registerCarrierEditText()

        close_forget_password.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit();
            var loginStatus:String?=viewModel.onLoginStatus()
            Log.d(TAG, "login status based on closed icon: "+loginStatus.toString())
            if (loginStatus.toString() == "LOGOUT" || loginStatus.toString() == ""){
                val intent = Intent(context, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }else{
                val intent = Intent(context, SettingActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        }
        close_validation.setOnClickListener {
            countDownTimer!!.cancel()
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit();
            val intent = Intent(context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

        req_otp_forget.setOnClickListener(View.OnClickListener {
            val number = num_editText.text.toString().trim { it <= ' ' }
            val sp_number = number.replace(" ", "")
            viewModel.onForgetSubmitClick("+91",sp_number)
        })

        resend_otp.setOnClickListener(View.OnClickListener {

            viewModel.onResendForgetSubmitClick()
        })

        req_validate.setOnClickListener(View.OnClickListener {
            if (pin_first_edittext.text.toString().isEmpty() && pin_second_edittext.text.toString()
                    .isEmpty() && pin_third_edittext.text.toString().isEmpty()
                && pin_forth_edittext.text.toString().isEmpty()
            ) {
                context?.let { it1 -> Toaster.show(it1,getString(R.string.Please_Enter_OTP)) }

            } else if (password_editText.text == null || password_editText.text
                    .toString()
                    .isEmpty() || password_editText_match.text == null || password_editText_match.text
                    .toString().isEmpty()
            ) {
                context?.let { it1 -> Toaster.show(it1,getString(R.string.empty_password)) }

            } else if (password_editText.text
                    .toString().length < 6 && password_editText_match.text
                    .toString().length < 6
            ) {
                context?.let { it1 -> Toaster.show(it1,getString(R.string.Password_should_be_greater_then_6_char)) }
            } else {
                val token: String =
                    pin_first_edittext.text.toString() + pin_second_edittext.text.toString() +
                            pin_third_edittext.text.toString() +
                            pin_forth_edittext.getText().toString()
                val number = num_editText.text.toString().trim { it <= ' ' }
                val sp_number = number.replace(" ", "")
                Log.d(TAG, "setupView number: "+number)
                Log.d(TAG, "setupView  sp_number : "+sp_number)
                if (password_editText_match.text.toString() == password_editText.text
                        .toString()
                ) {
                    viewModel.onForgetValidationSubmitClick("+91",token,password_editText.text.toString())
                } else {
                    context?.let { it1 -> Toaster.show(it1,getString(R.string.password_not_match)) }

                }
            }

        })

    }

    private fun turnGPSOn() {

        val googleApiClient = GoogleApiClient.Builder((context as Activity?)!!)
            .addApi(LocationServices.API).build()
        googleApiClient.connect()

        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = (10000 / 2).toLong()

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)

        val result =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> Log.i(
                    ReqotpActivity.TAG,
                    "All location settings are satisfied."
                )
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    Log.i(
                        ReqotpActivity.TAG,
                        "Location settings are not satisfied. Show the user a dialog to upgrade location settings "
                    )

                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        status.startResolutionForResult(context as Activity, 1)
                    } catch (e: IntentSender.SendIntentException) {
                        Log.i(ReqotpActivity.TAG, "PendingIntent unable to execute request.")
                    }

                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                    ReqotpActivity.TAG,
                    "Location settings are inadequate, and cannot be fixed here. Dialog not created."
                )
            }
        }
    }

    @NonNull
    private val mTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {

        }

        override fun afterTextChanged(editable: Editable) {
            // check Fields For Empty Values
            checkFieldsForEmptyValues()
            // checkFieldsForChangedValues();
        }
    }

    internal fun checkFieldsForEmptyValues() {

        val s1 = num_editText.getText().toString()

        if (s1 == "") {
            req_otp_forget.isEnabled = false
        } else {
            req_otp_forget.isEnabled = true
        }
    }

    private fun registerCarrierEditText() {
        cpp_txtforget.registerCarrierNumberEditText(num_editText)

        num_editText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (num_editText.text.length != 0) {

                    error_txt_forget.visibility = View.VISIBLE

                    error_txt_icon_forget.visibility = View.VISIBLE
                } else {
                    error_txt_forget.visibility = View.GONE
                    error_txt_icon_forget.visibility = View.GONE
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
                assert(num_editText != null)


            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {

            }
        })
        cpp_txtforget.setPhoneNumberValidityChangeListener { isValidNumber ->
            if (isValidNumber) {
                if (num_editText.text.length > 4) {
                    error_txt_icon_forget.text = "\ue809"
                    error_txt_icon_forget.setTextColor(ContextCompat.getColor((context as Activity?)!!, R.color.green_500))
                    error_txt_forget.setText(getText(R.string.Yes_This_is_an_valid_number))
                    req_otp_forget.isEnabled = true
                    req_otp_forget.background = ResourcesCompat.getDrawable(getResources(), R.drawable.button_curved_shape2, null);
                }
                //Toast.makeText(ReqotpActivity.this,"valid number",Toast.LENGTH_LONG).show();
            } else {
                error_txt_forget.visibility = View.VISIBLE
                error_txt_icon_forget.visibility = View.VISIBLE
                error_txt_icon_forget.text = "\uf12a"
                error_txt_icon_forget.setTextColor(ContextCompat.getColor((context as Activity?)!!, R.color.red_900))
                error_txt_forget.setText(getText(R.string.This_is_not_an_valid_number))
                req_otp_forget.isEnabled = false
                req_otp_forget.background = ResourcesCompat.getDrawable(getResources(), R.drawable.button_curved_shadow, null);
            }
        }

        cpp_txtforget.registerCarrierNumberEditText(num_editText)


    }


    fun validate_setUp() {
        assert(txt_count_otp != null)
        txt_count_otp.visibility = View.VISIBLE
        assert(otp_timer != null)
        otp_timer.visibility = View.VISIBLE
        assert(lay_re_otp != null)
        lay_re_otp.visibility = View.GONE
        txt_count_otp.text = "00:15"
        countDownTimer = object : CountDownTimer(15000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (txt_count_otp != null) txt_count_otp.text = "" + millisUntilFinished / 1000
                //Toast.makeText(ChatRequestActivity.this, "Time Remaining: " + millisUntilFinished / 1000, Toast.LENGTH_SHORT).show();
            }

            override fun onFinish() {
                // Toast.makeText(ValidateMobileActivity.this, "OTP not received click on the resend OTP", Toast.LENGTH_LONG).show();
                if (txt_count_otp != null) {
                    context?.let { Toaster.show(it,"OTP has not received click on the resend OTP") }


                }
                txt_count_otp.visibility = View.GONE
                otp_timer.visibility = View.GONE
                lay_re_otp.visibility = View.VISIBLE
                try {
                    /*if( isShowing())
                        myOnClickListener.onBunRejectClick(Action);
                    dismiss();*/
                } catch (exce: NullPointerException) {
                    exce.printStackTrace()
                } catch (exce: Exception) {
                    exce.printStackTrace()
                }
            }
        }.start()
        pin_first_edittext.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(@NonNull s: Editable) {
                if (!s.toString().isEmpty()) {
                    pin_first_edittext.clearFocus()
                    pin_second_edittext.requestFocus()
                }
            }
        })
        pin_second_edittext.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(@NonNull s: Editable) {
                if (!s.toString().isEmpty()) {
                    pin_second_edittext.clearFocus()
                    pin_third_edittext.requestFocus()
                    // setFocusedPinBackground(e_txt_3);
                } else {
                    pin_first_edittext.requestFocus()
                }
            }
        })
        pin_third_edittext.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(@NonNull s: Editable) {
                if (!s.toString().isEmpty()) {
                    pin_third_edittext.clearFocus()
                    pin_forth_edittext.requestFocus()
                    //setFocusedPinBackground(e_txt_4);
                } else {
                    pin_second_edittext.requestFocus()
                }
            }
        })
        pin_forth_edittext.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(@NonNull s: Editable) {
                if (!s.toString().isEmpty()) {
                    pin_forth_edittext.clearFocus()
                    password_editText_match.requestFocus()
                } else {
                    pin_third_edittext.requestFocus()
                }
            }
        })
    }

}