package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName




data class UserdetailResponse (


    @SerializedName("statusCode") val statusCode : Int,
    @SerializedName("status") val status : Boolean,
    @SerializedName("data") val data : UserdetailData,
    @SerializedName("infoId") val infoId : Int
)