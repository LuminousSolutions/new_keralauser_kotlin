package com.mongokerala.taxi.newuser.paymentgateway.adyan.req;

/**
 * Created by Akhi007 on 28-05-2019.
 */
public class AdyenPaymentSessionResponse {
    private String paymentSession;

    public String getPaymentSession() {
        return paymentSession;
    }

    public void setPaymentSession(String paymentSession) {
        this.paymentSession = paymentSession;
    }
}
