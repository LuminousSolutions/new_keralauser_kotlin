package com.mongokerala.taxi.newuser.ui.main_new

import android.graphics.Color
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.LocationResult
import com.google.android.libraries.places.api.model.Place
import com.mongokerala.taxi.newuser.R
import kotlinx.android.synthetic.main.row_cabs_booking.view.*

class BookingListAdapter(
    var list: List<AdvanceSearhData?>?,
    val currentLocationUser: LocationResult?,
    val place: Place?,
    var total_km: String?,
    var carTypeCoupon: String,
    var carPrice: String,
    var listSelectionCallback: ListSelectionCallback
) :

    RecyclerView.Adapter<BookingListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun data(bookingData: AdvanceSearhData?) {

        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_cabs_booking, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvKM.text = total_km + "  KM"
        val s = SpannableStringBuilder(carPrice)
        val strike = StrikethroughSpan()
        if (carPrice.isNotEmpty()) {
            s.setSpan(strike, 0, carPrice.length - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }


        when (position) {
            0 -> {
                holder.itemView.tvVehicleType.text = "HatchBack"
                val v = list?.filter { it?.carType == "Taxi4" && it.status == "WAITING" }
                holder.itemView.ivVehicle.setImageResource(R.mipmap.hatch)
                holder.itemView.editTextTextPersonName.text = "Affordable Rides"
                holder.itemView.tvSubType.text =
                    if (!v.isNullOrEmpty() && v.isNotEmpty()) "Available" else "Not Available"
                holder.itemView.tvFair.text =
                    if (!v.isNullOrEmpty() && !v[0]?.price.isNullOrBlank()) "Rs ${v[0]?.price}" else "Rs --"
                if (carPrice.isNotEmpty() && carTypeCoupon == "Taxi4") {
                    holder.itemView.couponPriceFair.visibility = View.VISIBLE
                    holder.itemView.couponPriceTxtview.visibility = View.VISIBLE
                    holder.itemView.couponPriceFair.text = s
                } else {
                    holder.itemView.couponPriceFair.visibility = View.GONE
                    holder.itemView.couponPriceTxtview.visibility = View.GONE
                }
                if (!v.isNullOrEmpty()) {
                    holder.itemView.layContainer.setOnClickListener {
                        handleClickedItemBackground(position, holder)
                    }
                    if (v[0]?.isSelcted!!) holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(holder.itemView.context, R.color.blue_100)
                    ) else holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )
                    holder.itemView.ivIndicator.drawable.setTint(Color.GREEN)
                } else {
                    holder.itemView.ivIndicator.drawable.setTint(Color.RED)
                    holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )
                }
            }
            1 -> {
                holder.itemView.tvVehicleType.text = "Sedan"
                val v = list?.filter { it?.carType == "Taxi" && it.status == "WAITING" }
                holder.itemView.ivVehicle.setImageResource(R.mipmap.sedan)
                holder.itemView.editTextTextPersonName.text = "Extra Luggage space"
                holder.itemView.tvSubType.text =
                    if (!v.isNullOrEmpty()) "Available" else "Not Available"
                holder.itemView.tvFair.text =
                    if (!v.isNullOrEmpty() && !v[0]?.price.isNullOrBlank()) "Rs ${v[0]?.price}" else "Rs --"
                if (carPrice.isNotEmpty() && carTypeCoupon == "Taxi") {
                    holder.itemView.couponPriceFair.visibility = View.VISIBLE
                    holder.itemView.couponPriceTxtview.visibility = View.VISIBLE
                    holder.itemView.couponPriceFair.text = s
                } else {
                    holder.itemView.couponPriceFair.visibility = View.GONE
                    holder.itemView.couponPriceTxtview.visibility = View.GONE
                }
                if (!v.isNullOrEmpty()) {
                    holder.itemView.layContainer.setOnClickListener {
                        handleClickedItemBackground(position, holder)
                    }
                    if (v[0]?.isSelcted!!) holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(holder.itemView.context, R.color.blue_100)
                    ) else holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )
                    holder.itemView.ivIndicator.drawable.setTint(Color.GREEN)
                } else {
                    holder.itemView.ivIndicator.drawable.setTint(Color.RED)
                    holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )
                }
            }
            2 -> {
                holder.itemView.tvVehicleType.text = "Suv"
                val v = list?.filter { it?.carType == "Taxi6" && it.status == "WAITING" }
                holder.itemView.ivVehicle.setImageResource(R.mipmap.suvv)
                holder.itemView.editTextTextPersonName.text = "Extra leg room space"
                holder.itemView.tvSubType.text =
                    if (!v.isNullOrEmpty()) "Available" else "Not Available"
                holder.itemView.tvFair.text =
                    if (!v.isNullOrEmpty() && !v[0]?.price.isNullOrBlank()) "Rs ${v[0]?.price}" else "Rs --"
                if (carPrice.isNotEmpty() && carTypeCoupon == "Taxi6") {
                    holder.itemView.couponPriceFair.visibility = View.VISIBLE
                    holder.itemView.couponPriceTxtview.visibility = View.VISIBLE
                    holder.itemView.couponPriceFair.text = s
                } else {
                    holder.itemView.couponPriceFair.visibility = View.GONE
                    holder.itemView.couponPriceTxtview.visibility = View.GONE
                }
                if (!v.isNullOrEmpty()) {
                    holder.itemView.layContainer.setOnClickListener {
                        handleClickedItemBackground(position, holder)
                    }
                    if (v[0]?.isSelcted!!) holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(holder.itemView.context, R.color.blue_100)
                    ) else holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )

                    holder.itemView.ivIndicator.drawable.setTint(Color.GREEN)
                } else {
                    holder.itemView.ivIndicator.drawable.setTint(Color.RED)
                    holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )
                }
            }
            3 -> {
                holder.itemView.tvVehicleType.text = "Auto"
                val v = list?.filter { it?.carType == "Auto" && it.status == "WAITING" }
                holder.itemView.ivVehicle.setImageResource(R.mipmap.auto_copy)
                holder.itemView.editTextTextPersonName.text = "cheapest price"
                holder.itemView.tvSubType.text =
                    if (!v.isNullOrEmpty()) "Available" else "Not Available"
                if (!v.isNullOrEmpty()) {
                    holder.itemView.layContainer.setOnClickListener {
                        handleClickedItemBackground(position, holder)
                    }
                    if (v[0]?.isSelcted!!) holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(holder.itemView.context, R.color.blue_100)
                    ) else holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )

                    holder.itemView.ivIndicator.drawable.setTint(Color.GREEN)
                } else {
                    holder.itemView.ivIndicator.drawable.setTint(Color.RED)
                    holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )
                }
                holder.itemView.tvFair.text =
                    if (!v.isNullOrEmpty() && !v[0]?.price.isNullOrBlank()) "Rs ${v[0]?.price}" else "Rs --"
                if (carPrice.isNotEmpty() && carTypeCoupon == "Auto") {
                    holder.itemView.couponPriceFair.visibility = View.VISIBLE
                    holder.itemView.couponPriceTxtview.visibility = View.VISIBLE
                    holder.itemView.couponPriceFair.text = s
                } else {
                    holder.itemView.couponPriceFair.visibility = View.GONE
                    holder.itemView.couponPriceTxtview.visibility = View.GONE
                }
            }
            4 -> {
                holder.itemView.tvVehicleType.text = "Transport"
                val v = list?.filter { it?.carType == "Transport" && it.status == "WAITING" }
                holder.itemView.ivVehicle.setImageResource(R.mipmap.traveller)
                holder.itemView.editTextTextPersonName.text = "AdvanceBooking Only"
                holder.itemView.tvSubType.text =
                    if (!v.isNullOrEmpty()) "Available" else "Not Available"
                holder.itemView.tvFair.text =
                    if (!v.isNullOrEmpty() && !v[0]?.price.isNullOrBlank()) "Rs ${v[0]?.price}" else "Rs --"
                if (carPrice.isNotEmpty() && carTypeCoupon == "Transport") {
                    holder.itemView.couponPriceFair.visibility = View.VISIBLE
                    holder.itemView.couponPriceTxtview.visibility = View.VISIBLE
                    holder.itemView.couponPriceFair.text = s
                } else {
                    holder.itemView.couponPriceFair.visibility = View.GONE
                    holder.itemView.couponPriceTxtview.visibility = View.GONE
                }
                if (!v.isNullOrEmpty()) {
                    holder.itemView.layContainer.setOnClickListener {
                        handleClickedItemBackground(position, holder)
                    }
                    holder.itemView.ivIndicator.drawable.setTint(Color.GREEN)
                    if (v[0]?.isSelcted!!)
                        holder.itemView.layContainer.setBackgroundColor(
                            ContextCompat.getColor(
                                holder.itemView.context,
                                R.color.blue_100
                            )
                        )
                    else holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )

                } else {
                    holder.itemView.ivIndicator.drawable.setTint(Color.RED)
                    holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )
                }
            }
            5 -> {
                holder.itemView.ivVehicle.setImageResource(R.mipmap.bike_3)
                holder.itemView.editTextTextPersonName.text = "For goods Delivery only"
                holder.itemView.tvVehicleType.text = "Bike"
                val v = list?.filter { it?.carType == "Bike" && it.status == "WAITING" }
                holder.itemView.tvSubType.text =
                    if (!v.isNullOrEmpty()) "Available" else "Not Available"
                holder.itemView.tvFair.text =
                    if (!v.isNullOrEmpty() && !v[0]?.price.isNullOrBlank()) "Rs ${v[0]?.price}" else "Rs --"
                if (carPrice.isNotEmpty() && carTypeCoupon == "Bike") {
                    holder.itemView.couponPriceFair.visibility = View.VISIBLE
                    holder.itemView.couponPriceTxtview.visibility = View.VISIBLE
                    holder.itemView.couponPriceFair.text = s
                } else {
                    holder.itemView.couponPriceFair.visibility = View.GONE
                    holder.itemView.couponPriceTxtview.visibility = View.GONE
                }
                if (!v.isNullOrEmpty()) {
                    holder.itemView.layContainer.setOnClickListener {
                        handleClickedItemBackground(position, holder)
                    }
                    if (v[0]?.isSelcted!!)
                        holder.itemView.layContainer.setBackgroundColor(
                            ContextCompat.getColor(
                                holder.itemView.context,
                                R.color.blue_100
                            )
                        )
                    else
                        holder.itemView.layContainer.setBackgroundColor(
                            ContextCompat.getColor(
                                holder.itemView.context,
                                R.color.white
                            )
                        )
                    holder.itemView.ivIndicator.drawable.setTint(Color.GREEN)
                } else {
                    holder.itemView.ivIndicator.drawable.setTint(Color.RED)
                    holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )
                }
            }
            6 -> {
                holder.itemView.ivVehicle.setImageResource(R.drawable.ambulance)
                holder.itemView.editTextTextPersonName.text = "Have service within city limit"
                holder.itemView.tvVehicleType.text = "Ambulance"
                val v = list?.filter { it?.carType == "Ambulance" && it.status == "WAITING" }
                holder.itemView.tvSubType.text =
                    if (!v.isNullOrEmpty()) "Available"
                    else
                        "Not Available"
                holder.itemView.tvFair.text =
                    if (!v.isNullOrEmpty() && !v[0]?.price.isNullOrBlank()) "Rs ${v[0]?.price}" else "Rs --"
                if (carPrice.isNotEmpty() && carTypeCoupon == "Ambulance") {
                    holder.itemView.couponPriceFair.visibility = View.VISIBLE
                    holder.itemView.couponPriceTxtview.visibility = View.VISIBLE
                    holder.itemView.couponPriceFair.text = s
                } else {
                    holder.itemView.couponPriceFair.visibility = View.GONE
                    holder.itemView.couponPriceTxtview.visibility = View.GONE
                }
                if (!v.isNullOrEmpty()) {
                    holder.itemView.layContainer.setOnClickListener {
                        handleClickedItemBackground(position, holder)
                    }
                    if (v[0]?.isSelcted!!) holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(holder.itemView.context, R.color.blue_100)
                    ) else holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )
                    holder.itemView.ivIndicator.drawable.setTint(Color.GREEN)
                } else {
                    holder.itemView.ivIndicator.drawable.setTint(Color.RED)
                    holder.itemView.layContainer.setBackgroundColor(
                        ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.white
                        )
                    )
                }
            }
        }
    }

    private fun handleClickedItemBackground(position: Int, holder: ViewHolder) {
        when (position) {
            0 -> {
                list?.forEach { if (!it?.carType.equals("Taxi4")) it?.isSelcted = false }
                list?.forEach { if (it?.carType.equals("Taxi4")) it?.isSelcted = !it?.isSelcted!! }
                listSelectionCallback.onSelection(list?.filter { it?.carType.equals("Taxi4") } as List<AdvanceSearhData>,
                    position)
                notifyDataSetChanged()
            }
            1 -> {
                list?.forEach { if (!it?.carType.equals("Taxi")) it?.isSelcted = false }

                list?.forEach { if (it?.carType.equals("Taxi")) it?.isSelcted = !it?.isSelcted!! }
                listSelectionCallback.onSelection(list?.filter { it?.carType.equals("Taxi") } as List<AdvanceSearhData>,
                    position)
                notifyDataSetChanged()
            }
            2 -> {
                list?.forEach { if (!it?.carType.equals("Taxi6")) it?.isSelcted = false }

                list?.forEach { if (it?.carType.equals("Taxi6")) it?.isSelcted = !it?.isSelcted!! }
                listSelectionCallback.onSelection(list?.filter { it?.carType.equals("Taxi6") } as List<AdvanceSearhData>,
                    position)
                notifyDataSetChanged()
            }
            3 -> {
                list?.forEach { if (!it?.carType.equals("Auto")) it?.isSelcted = false }

                list?.forEach { if (it?.carType.equals("Auto")) it?.isSelcted = !it?.isSelcted!! }
                listSelectionCallback.onSelection(list?.filter { it?.carType.equals("Auto") } as List<AdvanceSearhData>,
                    position)
                notifyDataSetChanged()
            }
            4 -> {
                list?.forEach { if (!it?.carType.equals("Transport")) it?.isSelcted = false }

                list?.forEach {
                    if (it?.carType.equals("Transport")) it?.isSelcted = !it?.isSelcted!!
                }
                listSelectionCallback.onSelection(list?.filter { it?.carType.equals("Transport") } as List<AdvanceSearhData>,
                    position)
                notifyDataSetChanged()
            }
            5 -> {
                list?.forEach { if (!it?.carType.equals("Bike")) it?.isSelcted = false }

                list?.forEach { if (it?.carType.equals("Bike")) it?.isSelcted = !it?.isSelcted!! }
                listSelectionCallback.onSelection(list?.filter { it?.carType.equals("Bike") } as List<AdvanceSearhData>,
                    position)
                notifyDataSetChanged()
            }
            6 -> {
                list?.forEach { if (!it?.carType.equals("Ambulance")) it?.isSelcted = false }

                list?.forEach {
                    if (it?.carType.equals("Ambulance")) it?.isSelcted = !it?.isSelcted!!
                }
                listSelectionCallback.onSelection(list?.filter { it?.carType.equals("Ambulance") } as List<AdvanceSearhData>,
                    position)
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int {
        return 7
    }
}

interface ListSelectionCallback {
    fun onSelection(advanceSearchData: List<AdvanceSearhData>, position: Int)
}