package com.mongokerala.taxi.newuser

import android.app.Application
import com.mongokerala.taxi.newuser.di.component.ApplicationComponent
import com.mongokerala.taxi.newuser.di.component.DaggerApplicationComponent
import com.mongokerala.taxi.newuser.di.module.ApplicationModule
import com.pixplicity.easyprefs.library.Prefs


open class TaxiDealsFrameWorkApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        injectDependencies()
        //shared prefs

        // CalligraphyConfig.initDefault(mCalligraphyConfig);


        //shared prefs
        Prefs.Builder()
            .setContext(this)
            .setMode(MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()

    }

    private fun injectDependencies() {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)
    }

    // Needed to replace the component with a test specific one
    fun setComponent(applicationComponent: ApplicationComponent) {
        this.applicationComponent = applicationComponent
    }

    /*Auth :Thiyagesh
     * Des : status of setIsMainActivityAlive added */
    companion object {
        private var isShowMainActivityAlive = false
         fun getIsMainActivityAlive(): Boolean {
            return isShowMainActivityAlive
        }

         fun setIsMainActivityAlive(isHomeFragmentAlive: Boolean) {
            isShowMainActivityAlive = isHomeFragmentAlive
        }
    }

}