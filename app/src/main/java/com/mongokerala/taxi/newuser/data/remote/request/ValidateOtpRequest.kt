package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.utils.network.ERole


class ValidateOtpRequest (
    @field:SerializedName(Keys.token) @field:Expose var token: String?,
    @field:SerializedName(Keys.phoneNumber) @field:Expose var phoneNumber: String?

){


    @SerializedName("countryCode")
    @Expose
    private var countryCode: String? = null

    @SerializedName("password")
    @Expose
    private var password: String? = null

    @SerializedName("rideId")
    @Expose
    private var rideId: String? = null

    @SerializedName("role")
    @Expose
    private var role: String? = null

    @SerializedName("userId")
    @Expose
    private var userId: String? = null

    @SerializedName("companyName")
    @Expose
    private var companyName: String? = null

    init {
        companyName = "KERALACABS"
        role = ERole.ROLE_USER.toString()
        password = "string"
        rideId = "string"
        userId = "string"
        countryCode = "+91"
    }
}