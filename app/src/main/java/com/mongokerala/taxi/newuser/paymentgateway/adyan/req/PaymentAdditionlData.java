package com.mongokerala.taxi.newuser.paymentgateway.adyan.req;

/**
 * Created by Akhi007 on 28-05-2019.
 */
public class PaymentAdditionlData {

    private String recurringProcessingModel;
    // private String recurring.recurringDetailReference;
    // private String recurring.shopperReference;

    public String getRecurringProcessingModel() {
        return recurringProcessingModel;
    }

    public void setRecurringProcessingModel(String recurringProcessingModel) {
        this.recurringProcessingModel = recurringProcessingModel;
    }
}
