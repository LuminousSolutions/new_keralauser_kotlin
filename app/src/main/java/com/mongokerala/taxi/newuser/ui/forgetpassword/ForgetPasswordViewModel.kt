package com.mongokerala.taxi.newuser.ui.forgetpassword

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.model.Post
import com.mongokerala.taxi.newuser.data.repository.PostRepository
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.AppErrorCode.NUMBER_IS_NOT_EXISTING
import com.mongokerala.taxi.newuser.utils.common.AppErrorCode.OTP_FAILED
import com.mongokerala.taxi.newuser.utils.common.AppErrorCode.STATUS_SUCCESS_INFOID
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor

class ForgetPasswordViewModel (
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val userRepository: UserRepository,
    private val postRepository: PostRepository,
    private val allPostList: ArrayList<Post>,
    private val paginator: PublishProcessor<Pair<String?, String?>>
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper)  {

    val launchValidate: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchValidateUsedNumber: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchOtpSuccess: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchOtpfail: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()


    override fun onCreate() {

    }

    fun onLoginStatus():String?{
        return userRepository.getLoginstatus()

    }

    fun onForgetSubmitClick(country_code: String,number: String){


        compositeDisposable.addAll(
            userRepository.doForgetUserOtpRequest(country_code,number)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        var value:String = STATUS_SUCCESS_INFOID.toString()
                        var valuetwo:String = NUMBER_IS_NOT_EXISTING.toString()
                        if(it.infoId == value) {
                            userRepository.saveClientMobileNumber(number)
                            launchValidate.postValue(Event(emptyMap()))
                        }
                        else if(it.infoId == valuetwo){
                            launchValidateUsedNumber.postValue(Event(emptyMap()))
                        }else{

                        }

                    },
                    {
                        handleNetworkError(it)
                    }
                )
        )
    }

    fun onForgetValidationSubmitClick(country_code: String,otp: String,new_password: String){
         compositeDisposable.addAll(
            userRepository.doForgetUserOtpValidationRequest(country_code,
                userRepository.getClientMobileNUmber()!!,otp,new_password)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        Log.d("otp request","otp request   :"+it.infoId)

                        var value:String = STATUS_SUCCESS_INFOID.toString()
                        var valuetwo:String = OTP_FAILED.toString()


                        if(it.infoId == "0") {
                            launchOtpSuccess.postValue(Event(emptyMap()))
                        }
                        else if(it.infoId == "850"){
                            launchOtpfail.postValue(Event(emptyMap()))
                        }else{

                        }

//                        if(it.infoId == value) {
//                            launchOtpSuccess.postValue(Event(emptyMap()))
//                        }
//                        else if(it.infoId == valuetwo){
//                            launchOtpfail.postValue(Event(emptyMap()))
//                        }else{
//
//                        }

                    },
                    {
                        handleNetworkError(it)
                    }
                )
        )
    }

    fun onResendForgetSubmitClick(){

        var mob_number: String = userRepository.getClientMobileNUmber().toString()

        compositeDisposable.addAll(
            userRepository.doForgetUserOtpRequest("+91",mob_number)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        var value:String = STATUS_SUCCESS_INFOID.toString()
                        var valuetwo:String = NUMBER_IS_NOT_EXISTING.toString()
                        if(it.infoId == value) {
                            userRepository.saveClientMobileNumber(mob_number)
                            launchValidate.postValue(Event(emptyMap()))
                        }
                        else if(it.infoId == valuetwo){
                            launchValidateUsedNumber.postValue(Event(emptyMap()))
                        }else{

                        }

                    },
                    {
                        handleNetworkError(it)
                    }
                )
        )
    }



}