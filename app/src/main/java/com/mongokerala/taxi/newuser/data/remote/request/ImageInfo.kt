package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImageInfo (
    @field:SerializedName(Keys.imageUrl) @field:Expose var imageUrl: String?
){
    /* @Nullable
    @SerializedName(Keys.blobKey)
    public String blobkey;*/
    @SerializedName(Keys.fileName)
    var fileName: String? = null

}
