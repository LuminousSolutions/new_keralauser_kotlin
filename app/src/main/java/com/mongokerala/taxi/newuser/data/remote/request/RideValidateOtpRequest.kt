package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class RideValidateOtpRequest (
    @field:SerializedName(Keys.countryCode) @field:Expose var countryCode: String?,
    @field:SerializedName(Keys.userId) @field:Expose var userId: String?,
    @field:SerializedName(Keys.phoneNumber) @field:Expose var phoneNumber: String?,
    @field:SerializedName(Keys.token) @field:Expose var token: String?
    ){

    @SerializedName("password")
    @Expose
    private var password: String? = null

    @SerializedName("rideId")
    @Expose
    private var rideId: Int? = null

    @SerializedName("role")
    @Expose
    private var role: String? = null

    init {
        rideId = 0
        role = "string"
        password = "string"
    }

}