package com.mongokerala.taxi.newuser.data.remote.fcm

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService

class FcmInstanceIDService : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)


        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String) {
        Log.e("Token is","$token")
    }

    companion object {
        private const val TAG = "MyFirebaseIDService"
    }
}
