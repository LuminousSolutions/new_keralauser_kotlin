package com.mongokerala.taxi.newuser.ui.payment;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.mongokerala.taxi.newuser.R;
import com.mongokerala.taxi.newuser.paymentgateway.Stripe_MainActivity;
//import com.mongokerala.taxi.newuser.paymentgateway.adyan.AdyenPaymentActivity;
import com.mongokerala.taxi.newuser.ui.googlepay.GooglePayPaymentWithPGActivity;
import com.stripe.android.PaymentResultListener;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener, PaymentResultListener {
    private static final String TAG = PaymentActivity.class.getSimpleName();

    private LinearLayout layoutPayTMCheck,layoutStripeCheck,layoutGoogle;
    private TextView tvPayTMCheck;
    private Button btPayNow;
    private LinearLayout layoutAdyen;
    private static final int TEZ_REQUEST_CODE = 123;
    private LinearLayout mGooglePayButton;
    private static final String GOOGLE_TEZ_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);
        createInitializeAllView();
        mGooglePayButton = findViewById(R.id.layoutGooglePay);

        boolean isAppInstalled = checkGooglePayInstalled("com.google.android.apps.nbu.paisa.user");
        if(isAppInstalled)
        {
            mGooglePayButton.setVisibility(View.VISIBLE);
        }
        else
        {
            mGooglePayButton.setVisibility(View.GONE);
        }

    }

    private void createInitializeAllView() {
        layoutPayTMCheck = findViewById(R.id.layoutPayTMCheck);
        layoutStripeCheck = findViewById(R.id.layoutStripeCheck);
        layoutGoogle = findViewById(R.id.layoutGoogle);
        tvPayTMCheck = findViewById(R.id.tvPayTMCheck);
        btPayNow = findViewById(R.id.btPayNow);
        layoutAdyen = findViewById(R.id.layoutAdyen);
        layoutPayTMCheck.setOnClickListener(this);
        layoutStripeCheck.setOnClickListener(this);
        layoutGoogle.setOnClickListener(this);
        layoutAdyen.setOnClickListener(this);
        btPayNow.setOnClickListener(this);
    }

    @Override
    public void onClick(@NonNull View view) {
        switch (view.getId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            case R.id.layoutPayTMCheck: {
                showPaymentGatewayDialog();
                break;
            }
            case R.id.layoutStripeCheck: {
                startActivity(new Intent(PaymentActivity.this, Stripe_MainActivity.class));
                break;
            }
            case R.id.layoutAdyen:{
//                startActivity(new Intent(this, AdyenPaymentActivity.class));
            }
            case R.id.layoutGoogle:{
                startActivity(new Intent(this, GooglePayPaymentWithPGActivity.class));
            }
        }
    }

    public void showPaymentGatewayDialog() {
        PaymentGatewayDialog paymentGatewayDialog = PaymentGatewayDialog.newInstance(PaymentGateway.PAYTM);
        paymentGatewayDialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_DialogTransparentLayout);
        paymentGatewayDialog.show(getFragmentManager(), "paymentGatewayDialog");
    }
/*
    @Override
    public void onFinishPaymentGatewayDialog(@Nullable String errorMessage) {
        if (null != errorMessage && !errorMessage.isEmpty()) {
           // Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
            btPayNow.setText(getString(R.string.Retry_Payment));
        }
        finish();
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onGooglePayClick(View view) {
        try {
            Uri uri =
                    new Uri.Builder()
                            .scheme("upi")
                            .authority("pay")
                            .appendQueryParameter("pa", "stanleytvm@oksbi")
                            .appendQueryParameter("pn", "Test Merchant")
                            .appendQueryParameter("mc", "1234")
                            .appendQueryParameter("tr", "123456789")
                            .appendQueryParameter("tn", "test transaction note")
                            .appendQueryParameter("am", "10.01")
                            .appendQueryParameter("cu", "INR")
                            .appendQueryParameter("url", "https://test.merchant.website")
                            .build();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(uri);
            intent.setPackage(GOOGLE_TEZ_PACKAGE_NAME);
            startActivityForResult(intent, TEZ_REQUEST_CODE);
        }catch (Exception e){

        }
    }

    @Override
    public void onPaymentResult(@NonNull String paymentResult) {

    }

    private boolean checkGooglePayInstalled(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {
                case TEZ_REQUEST_CODE:
                    Log.i("result", data.getStringExtra("Status"));
                    break;
            }
        }catch (Exception e){

        }
    }
}