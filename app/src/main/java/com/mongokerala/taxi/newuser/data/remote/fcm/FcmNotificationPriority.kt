package com.mongokerala.taxi.newuser.data.remote.fcm

import androidx.annotation.NonNull


class FcmNotificationPriority {
    @NonNull
    private val priority = "high"

    @NonNull
    private val ttl: String = com.mongokerala.taxi.newuser.utils.common.Constants.FCM_PUSH_NOTIFICATION_EXPIRY_TIME
}
