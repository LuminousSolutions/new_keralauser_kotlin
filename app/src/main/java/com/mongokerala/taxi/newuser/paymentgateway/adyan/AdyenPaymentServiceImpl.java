package com.mongokerala.taxi.newuser.paymentgateway.adyan;

import android.util.Log;

import com.google.gson.Gson;
import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.AdyenPaymentSessionResponse;
import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.AdyenPostRequestSender;
import com.mongokerala.taxi.newuser.paymentgateway.adyan.req.PaymentVerifyResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Akhi007 on 25-05-2019.
 */
public class AdyenPaymentServiceImpl implements AdyenPaymentService {
    private static final String TAG = AdyenPaymentServiceImpl.class.getSimpleName();

    @Override
    public void createPaymentSession(final CreateAdyenPaymentSessionListener paymentSessionListener,
                                     final OrderDTO orderDTO) {
        AdyenPostRequestSender postRequestSender = RequestHandler.getRetrofitClient(AdyenPostRequestSender.BASE_URL).create(AdyenPostRequestSender.class);
        Log.i(TAG, "createPaymentSession Request: "+new Gson().toJson(orderDTO));
        Call<AdyenPaymentSessionResponse> response = postRequestSender.createPaymentSession(orderDTO);
        response.enqueue(new Callback<AdyenPaymentSessionResponse>() {
            @Override
            public void onResponse(Call<AdyenPaymentSessionResponse> call, Response<AdyenPaymentSessionResponse> response) {
                Log.i(TAG, "Response: "+ new Gson().toJson(response.body()));
                if(response.isSuccessful() && null != response.body()){
                    paymentSessionListener.onRequestSuccess(orderDTO, response.body());
                }else if(null == response.body() && null != response.errorBody()){
                    ApiResponse apiResponse = PaymentUtils.convertApiErrorBodyToApiResponse(response.errorBody());
                    paymentSessionListener.onRequestFailed(orderDTO, apiResponse);
                } else {
                    //TODO write your own error code and message
                    paymentSessionListener.onRequestFailed(orderDTO, new ApiResponse("FAILED", "Something went wrong. Please try after sometime!"));
                }
            }

            @Override
            public void onFailure(Call<AdyenPaymentSessionResponse> call, Throwable t) {
                if( t instanceof SocketTimeoutException){
                    paymentSessionListener.onRequestTimeOut(orderDTO);
                }
            }
        });
    }

    @Override
    public void verifyPayment(final VerifyAdyenPaymentListener verifyAdyenPaymentListener, final OrderDTO orderDTO) {
        AdyenPostRequestSender postRequestSender = RequestHandler.getRetrofitClient(AdyenPostRequestSender.BASE_URL).create(AdyenPostRequestSender.class);
        Log.i(TAG, "verifyPayment Request: "+new Gson().toJson(orderDTO));
        Call<PaymentVerifyResponse> response = postRequestSender.verifyPayment(orderDTO);
        response.enqueue(new Callback<PaymentVerifyResponse>() {
            @Override
            public void onResponse(Call<PaymentVerifyResponse> call, Response<PaymentVerifyResponse> response) {
                Log.i(TAG, "Response: "+ new Gson().toJson(response.body()));
                if(response.isSuccessful() && null != response.body()){
                    verifyAdyenPaymentListener.onPaymentVerifyRequestSuccess(orderDTO, response.body());
                }else if(null == response.body() && null != response.errorBody()){
                    ApiResponse apiResponse = PaymentUtils.convertApiErrorBodyToApiResponse(response.errorBody());
                    verifyAdyenPaymentListener.onPaymentVerifyRequestFailed(orderDTO, apiResponse);
                } else {
                    //TODO write your own error code and message
                    verifyAdyenPaymentListener.onPaymentVerifyRequestFailed(orderDTO, new ApiResponse("FAILED", "Something went wrong. Please try after sometime!"));
                }
            }

            @Override
            public void onFailure(Call<PaymentVerifyResponse> call, Throwable t) {
                if( t instanceof SocketTimeoutException){
                    verifyAdyenPaymentListener.onPaymentVerifyRequestTimeOut(orderDTO);
                }
            }
        });
    }
}
