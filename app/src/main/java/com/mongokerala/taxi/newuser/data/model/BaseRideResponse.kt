package com.mongokerala.taxi.newuser.data.model

import com.google.gson.annotations.SerializedName


data class BaseRideResponse (


    @SerializedName("statusCode") val statusCode : Int,
    @SerializedName("status") val status : String,
    @SerializedName("message") val message : String,
    @SerializedName("infoId") val infoId : String,
    @SerializedName("data") val data : String?

)