package com.mongokerala.taxi.newuser.data.local.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.mongokerala.taxi.newuser.data.local.db.entity.DriverDetails


@Dao
interface DummyDao {

    @Query("SELECT * FROM driver_details")
    fun getAll(): List<DriverDetails>

    @Insert
    fun insert(entity: DriverDetails)

    @Delete
    fun delete(entity: DriverDetails)
}