package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class TaxidetailsResponse (



    @SerializedName("statusCode") val statusCode : Int,
    @SerializedName("status") val status : Boolean,
    @SerializedName("data") val data : TaxidetailData,
    @SerializedName("infoId") val infoId : Int
)