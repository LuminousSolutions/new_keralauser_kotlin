package com.mongokerala.taxi.newuser.ui.tour

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.mongokerala.taxi.newuser.R


class ThirdTourFragment : Fragment() {

    companion object {
        fun newInstance(title: String): Fragment {
            val fragment = ThirdTourFragment()
            val args = Bundle()
            args.putString(title, title)
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.third_tour_frag, container, false)
    }

}

class ThirdTourViewPagerAdapter(supportFragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(supportFragmentManager) {
    override fun getItem(position: Int): Fragment {
        return ThirdTourFragment.newInstance("\"ThirdTourFragment, Instance 2\"" + position)
    }

    override fun getCount(): Int {
        return 4
    }
}
