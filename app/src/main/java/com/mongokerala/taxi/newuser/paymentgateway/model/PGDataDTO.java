package com.mongokerala.taxi.newuser.paymentgateway.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mongokerala.taxi.newuser.paymentgateway.enums.PaymentGateway;
import com.mongokerala.taxi.newuser.paymentgateway.enums.PaymentStatus;

import java.math.BigDecimal;
import java.util.TreeMap;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public class PGDataDTO {
    @SerializedName("txnId")
    @Expose
    private String txnId;
    @SerializedName("payStatus")
    @Expose
    private PaymentStatus payStatus;
    private PaymentGateway paymentGateway;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("amount")
    @Expose
    private BigDecimal amount;
    private String paytmChecksum;
    private TreeMap<String, String> paytmParams;
    //Only for stripe payment
    @SerializedName("stripeToken")
    @Expose
    private String stripeToken;

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public PaymentStatus getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(PaymentStatus payStatus) {
        this.payStatus = payStatus;
    }

    public PaymentGateway getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(PaymentGateway paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaytmChecksum() {
        return paytmChecksum;
    }

    public void setPaytmChecksum(String paytmChecksum) {
        this.paytmChecksum = paytmChecksum;
    }

    public TreeMap<String, String> getPaytmParams() {
        return paytmParams;
    }

    public void setPaytmParams(TreeMap<String, String> paytmParams) {
        this.paytmParams = paytmParams;
    }

    public String getStripeToken() {
        return stripeToken;
    }

    public void setStripeToken(String stripeToken) {
        this.stripeToken = stripeToken;
    }
}
