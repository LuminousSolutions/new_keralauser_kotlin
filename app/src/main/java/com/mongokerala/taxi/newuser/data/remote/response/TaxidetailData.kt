package com.mongokerala.taxi.newuser.data.remote.response


import com.google.gson.annotations.SerializedName

data class TaxidetailData (

    @SerializedName("id") val id : String,
    @SerializedName("taxiId") val taxiId : String,
    @SerializedName("supplierId") val supplierId : String,
    @SerializedName("userId") val userId : String,
    @SerializedName("name") val name : String,
    @SerializedName("description") val description : String,
    @SerializedName("hour") val hour : Int,
    @SerializedName("km") val km : Double,
    @SerializedName("additionalInformation") val additionalInformation : String,
    @SerializedName("latitude") val latitude : Double,
    @SerializedName("longitude") val longitude : Double,
    @SerializedName("phoneNumber") val phoneNumber : String,
    @SerializedName("taxiNumber") val taxiNumber : String,
    @SerializedName("drivername") val drivername : String,
    @SerializedName("driverPhonenumber") val driverPhonenumber : String,
    @SerializedName("status") val status : String,
    @SerializedName("currency") val currency : String,
    @SerializedName("carType") val carType : String,
    @SerializedName("airPortprice") val airPortprice : Double,
    @SerializedName("perDay") val perDay : Double,
    @SerializedName("weekEndOffer") val weekEndOffer : String,
    @SerializedName("price") val price : Double,
    @SerializedName("peakPrice") val peakPrice : Double,
    @SerializedName("basePrice") val basePrice : Double,
    @SerializedName("waitingTime") val waitingTime : Int,
    @SerializedName("minimumFare") val minimumFare : Double,
    @SerializedName("source") val source : String,
    @SerializedName("destination") val destination : String,
    @SerializedName("cityDTO") val cityDTO : CityDTO,
    @SerializedName("active") val active : String,
    @SerializedName("updatedOn") val updatedOn : String,
    @SerializedName("city") val city : String,
    @SerializedName("year") val year : String,
    @SerializedName("vehicleBrand") val vehicleBrand : String,
    @SerializedName("seats") val seats : String,
    @SerializedName("tags") val tags : List<String>
)