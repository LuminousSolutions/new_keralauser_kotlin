package com.mongokerala.taxi.newuser.ui.payment;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public class PayTMConstants {
    private static final String TAG = PayTMConstants.class.getSimpleName();

    static boolean isProduction = false;

    public enum KEY {
        MERCHANT_ID, MERCHANT_KEY, CALLBACK_URL, PAYTM_URL, PAYTM_TXN_VERIFICATION_URL, CHANNEL_ID, WEBSITE, INDUSTRY_TYPE_ID, RESPONSE_URL;
    }

    //TODO : Change API KEYS HERE
    @NonNull
    public static Map<KEY, String> getPayTMCredentialMap(){
        if(isProduction){ //ApiSecurityUtils.isProduction()
            //Production Environment
            Map<KEY, String> productionMap = new HashMap<>();
            productionMap.put(KEY.MERCHANT_ID,"Raksha30153028268351");
            productionMap.put(KEY.MERCHANT_KEY,"fK210l%Oe#j%RXYZ");
            productionMap.put(KEY.PAYTM_URL,"https://securegw.paytm.in/theia/processTransaction");
            productionMap.put(KEY.PAYTM_TXN_VERIFICATION_URL,"https://securegw.paytm.in/merchant-status/getTxnStatus");
            productionMap.put(KEY.WEBSITE,"WEBSTAGING");
            productionMap.put(KEY.INDUSTRY_TYPE_ID,"Retail");
            productionMap.put(KEY.RESPONSE_URL, "");//not in use for android app
            productionMap.put(KEY.CALLBACK_URL,"https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=");
            productionMap.put(KEY.CHANNEL_ID,"WAP");
            return productionMap;
        }else{
            //Staging Environment
            Map<KEY, String> stagingMap = new HashMap<>();
            stagingMap.put(KEY.MERCHANT_ID,"Raksha30153028268351");
            stagingMap.put(KEY.MERCHANT_KEY,"fK210l%Oe#j%RXYZ");
            stagingMap.put(KEY.PAYTM_URL,"https://securegw-stage.paytm.in/theia/processTransaction");
            stagingMap.put(KEY.PAYTM_TXN_VERIFICATION_URL,"https://securegw-stage.paytm.in/merchant-status/getTxnStatus");
            stagingMap.put(KEY.WEBSITE,"WEBSTAGING");
            stagingMap.put(KEY.INDUSTRY_TYPE_ID,"Retail");
            stagingMap.put(KEY.RESPONSE_URL, "");//not in use for android app
            stagingMap.put(KEY.CALLBACK_URL,"https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=");
            stagingMap.put(KEY.CHANNEL_ID,"WAP");
            return stagingMap;
        }
    }

    @NonNull
    public static HashMap<String, String> getPayTMOrderParamMap(OrderDTO orderDTO){
        HashMap<String, String> paramMap = new HashMap<>();
        paramMap.put( "ORDER_ID" , orderDTO.getOrderId());
        paramMap.put( "CUST_ID" , orderDTO.getCustomer().getCustomerId());
        paramMap.put( "MOBILE_NO" , orderDTO.getCustomer().getMobileNumber());
        paramMap.put( "EMAIL" , orderDTO.getCustomer().getEmail());
        paramMap.put( "TXN_AMOUNT" , orderDTO.getPgData().getAmount().toString());

        paramMap.put( "MID" , getPayTMCredentialMap().get(KEY.MERCHANT_ID));
        paramMap.put( "CHANNEL_ID" , getPayTMCredentialMap().get(KEY.CHANNEL_ID));
        paramMap.put( "WEBSITE" , getPayTMCredentialMap().get(KEY.WEBSITE));
        paramMap.put( "INDUSTRY_TYPE_ID" , getPayTMCredentialMap().get(KEY.INDUSTRY_TYPE_ID));
        paramMap.put( "CALLBACK_URL", getPayTMCredentialMap().get(KEY.CALLBACK_URL) + orderDTO.getOrderId());
        paramMap.put( "CHECKSUMHASH", orderDTO.getPgData().getPaytmChecksum());

        Log.d(TAG, "OrderCreateResponse : "+new Gson().toJson(paramMap));
        return paramMap;
    }
}
