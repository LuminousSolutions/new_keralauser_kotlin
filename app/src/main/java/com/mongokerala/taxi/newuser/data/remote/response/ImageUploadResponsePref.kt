package com.mongokerala.taxi.newuser.data.remote.response

import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.data.remote.request.ImageInfopref
import com.mongokerala.taxi.newuser.data.remote.request.Keys

class ImageUploadResponsePref(
    imageInfo: ImageInfopref
) {
    @Nullable
    @SerializedName(Keys.blobKey)
    var blobkey: String? = null

    @Nullable
    @SerializedName(Keys.fileName)
    var fileName: String? = null

    @Nullable
    @SerializedName(Keys.imageUrl)
    var imageUrl: String? = null

}