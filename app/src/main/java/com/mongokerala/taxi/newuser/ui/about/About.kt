package com.mongokerala.taxi.newuser.ui.about

import android.os.Bundle

import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity

class AboutActivity : BaseActivity<AboutViewModel>() {

    companion object {
        const val TAG = "AboutActivity"
    }


    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun provideLayoutId(): Int = R.layout.activity_about

    override fun setupView(savedInstanceState: Bundle?) {


    }







}






