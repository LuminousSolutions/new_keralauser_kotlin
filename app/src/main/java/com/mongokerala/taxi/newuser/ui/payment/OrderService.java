package com.mongokerala.taxi.newuser.ui.payment;

/**
 * Created by Akhi007 on 09-01-2019.
 */

public interface OrderService {

    void createOrderApiCall(OrderCreateListener orderCreateListener, OrderDTO orderDTO);

    void updateOrderPaymentApiCall(OrderPaymentUpdateListener orderPaymentUpdateListener, OrderDTO orderDTO);
}
