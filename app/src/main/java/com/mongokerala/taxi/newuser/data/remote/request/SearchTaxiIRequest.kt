package com.mongokerala.taxi.newuser.data.remote.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchTaxiIRequest(

	@field:SerializedName("notification")
	val notification: String? = null,

	@field:SerializedName("km")
	val km: Int? = null,

	@field:SerializedName("distance")
	val distance: Int? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("latitude")
	val latitude: Double? = null,

	@field:SerializedName("destination")
	val destination: String? = null,

	@field:SerializedName("company")
	val company: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("source")
	val source: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("longitude")
	val longitude: Double? = null
) : Parcelable
