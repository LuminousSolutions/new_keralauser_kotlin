package com.mongokerala.taxi.newuser.ui.language

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import androidx.annotation.NonNull
import androidx.lifecycle.Observer
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.FragmentComponent
import com.mongokerala.taxi.newuser.ui.base.BaseFragment
import com.mongokerala.taxi.newuser.ui.splash.SplashActivity
import com.mongokerala.taxi.newuser.utils.common.AppUtils
import com.mongokerala.taxi.newuser.utils.common.Constants.LANGUAGE_ENGLISH_CODE
import com.mongokerala.taxi.newuser.utils.common.Constants.LANGUAGE_FRENCH_CODE
import com.mongokerala.taxi.newuser.utils.common.Constants.LANGUAGE_GERMAN_CODE
import com.mongokerala.taxi.newuser.utils.common.Constants.LANGUAGE_ITALY_CODE
import com.mongokerala.taxi.newuser.utils.common.Constants.LANGUAGE_MALAYALAM_CODE
import com.mongokerala.taxi.newuser.utils.common.Constants.LANGUAGE_SPANISH_CODE
import com.mongokerala.taxi.newuser.utils.common.Constants.LANGUAGE_TAMIL_CODE
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.display.Toaster
import kotlinx.android.synthetic.main.fragment_language.*
import java.util.*

class LanguageFragment : BaseFragment<LanguageFragmentViewModel>() {

    companion object {

        const val TAG = "LanguageFragment"

        fun newInstance(): LanguageFragment {
            val args = Bundle()
            val fragment = LanguageFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private var language_data = ""

    override fun provideLayoutId(): Int = R.layout.fragment_language

    override fun injectDependencies(fragmentComponent: FragmentComponent) {
        fragmentComponent.inject(this)
    }

    override fun setupObservers() {
        super.setupObservers()

        viewModel.launchlanguageUpdateSuccess.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                context?.let { it1 -> Toaster.show(it1,getString(R.string.language_updated)) }
            }
        })
        viewModel.launchlanguageUpdatefail.observe(this, Observer<Event<Map<String, String>>> {
            it.getIfNotHandled()?.run {
                context?.let { it1 -> Toaster.show(it1,getString(R.string.not_updated)) }
            }
        })
        viewModel.setUpLanguage.observe(this, Observer {
            //if (et_email.text.toString() != it) et_email.setText(it)
            if (it == null) {
                radio0.setChecked(true)
                viewModel.doLanguageUpdate(LANGUAGE_ENGLISH_CODE)
            } else if (it != null) {
                when (it) {
                    LANGUAGE_ENGLISH_CODE -> radio0.isChecked = true
                    LANGUAGE_FRENCH_CODE -> radio2.isChecked = true
                    LANGUAGE_GERMAN_CODE -> radio1.isChecked = true
                    LANGUAGE_ITALY_CODE -> radio3.isChecked = true
                    LANGUAGE_TAMIL_CODE -> radio4.isChecked = true
                    LANGUAGE_SPANISH_CODE -> radio5.isChecked = true
                    LANGUAGE_MALAYALAM_CODE -> radio6.isChecked = true
                }
            }

        })
    }

    override fun setupView(view: View) {

        viewModel.isLanguageSelected()

        btn_apply.setOnClickListener { v: View? ->
            if (AppUtils.isNetworkConnected(activity)) {
                if (!language_data.isEmpty()) {
                    changeLanguage(language_data)
                }
            }
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit();
        }

        radio0.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (b) CheckedChanged(
                compoundButton,
                b
            )
        })

        radio1.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (b) CheckedChanged(
                compoundButton,
                b
            )
        })

        radio2.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (b) CheckedChanged(
                compoundButton,
                b
            )
        })

        radio3.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (b) CheckedChanged(
                compoundButton,
                b
            )
        })

        radio4.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (b) CheckedChanged(
                compoundButton,
                b
            )
        })
        radio5.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (b) CheckedChanged(
                compoundButton,
                b
            )
        })

        radio6.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (b) CheckedChanged(
                compoundButton,
                b
            )
        })
    }

    fun changeLanguage(selectedlanguage: String?) {
        val locale = Locale(selectedlanguage)
        viewModel.doLanguageUpdate(selectedlanguage!!)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        this.resources.updateConfiguration(
            config,
            this.resources.displayMetrics
        )
        val refresh = Intent(activity, SplashActivity::class.java)
        startActivity(refresh)
        activity?.finish()
    }

    fun CheckedChanged(
        @NonNull compoundButton: CompoundButton,
        b: Boolean
    ) {
        val id = compoundButton.id
        when (id) {
            R.id.radio0 -> if (b) {
                radio1.isChecked = false
                radio2.isChecked = false
                radio3.isChecked = false
                radio4.isChecked = false
                radio5.isChecked = false
                radio6.isChecked = false
                language_data = LANGUAGE_ENGLISH_CODE
            } else {
                radio0.isChecked = true
            }
            R.id.radio1 -> if (b) {
                radio0.isChecked = false
                radio2.isChecked = false
                radio3.isChecked = false
                radio4.isChecked = false
                radio5.isChecked = false
                radio6.isChecked = false
                language_data = LANGUAGE_GERMAN_CODE
            } else {
                radio1.isChecked = true
            }
            R.id.radio2 -> if (b) {
                radio0.isChecked = false
                radio1.isChecked = false
                radio3.isChecked = false
                radio4.isChecked = false
                radio5.isChecked = false
                radio6.isChecked = false
                language_data = LANGUAGE_FRENCH_CODE
            } else {
                radio2.isChecked = true
            }
            R.id.radio3 -> if (b) {
                radio0.isChecked = false
                radio1.isChecked = false
                radio2.isChecked = false
                radio4.isChecked = false
                radio5.isChecked = false
                radio6.isChecked = false
                language_data = LANGUAGE_ITALY_CODE
            } else {
                radio3.isChecked = true
            }
            R.id.radio4 -> if (b) {
                radio0.isChecked = false
                radio1.isChecked = false
                radio2.isChecked = false
                radio3.isChecked = false
                radio5.isChecked = false
                radio6.isChecked = false
                language_data = LANGUAGE_TAMIL_CODE
            } else {
                radio4.isChecked = true
            }
            R.id.radio5 -> if (b) {
                radio0.isChecked = false
                radio1.isChecked = false
                radio2.isChecked = false
                radio3.isChecked = false
                radio4.isChecked = false
                radio6.isChecked = false
                language_data = LANGUAGE_SPANISH_CODE
            } else {
                radio5.isChecked = true
            }
            R.id.radio6 -> if (b) {
                radio0.isChecked = false
                radio1.isChecked = false
                radio2.isChecked = false
                radio3.isChecked = false
                radio4.isChecked = false
                radio5.isChecked = false
                language_data = LANGUAGE_MALAYALAM_CODE
            } else {
                radio6.isChecked = true
            }
            else -> {
            }
        }
    }


}