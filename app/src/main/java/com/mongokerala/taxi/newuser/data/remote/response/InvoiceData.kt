package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.data.remote.request.ImageInfo

data class InvoiceData (
    @SerializedName("id") val id : String,
    @SerializedName("latitude") val latitude : Double,
    @SerializedName("longitude") val longitude : Double,
    @SerializedName("distance") val distance : String,
    @SerializedName("endTIme") val endTIme : Long,
    @SerializedName("estimateTime") val estimateTime : Double,
    @SerializedName("km") val km : Double,
    @SerializedName("userKm") val userKm : Double,
    @SerializedName("radius") val radius : String,
    @SerializedName("source") val source : String,
    @SerializedName("destination") val destination : String,
    @SerializedName("userId") val userId : String,
    @SerializedName("driverId") val driverId : String,
    @SerializedName("taxiDetailId") val taxiDetailId : String,
    @SerializedName("dealId") val dealId : String,
    @SerializedName("rideStatus") val rideStatus : String,
    @SerializedName("userName") val userName : String,
    @SerializedName("driverName") val driverName : String,
    @SerializedName("oneSignalId") val oneSignalId : String,
    @SerializedName("desc") val desc : String,
    @SerializedName("type") val type : String,
    @SerializedName("payment") val payment : String,
    @SerializedName("price") val price : Double,
    @SerializedName("totalPrice") val totalPrice : Double,
    @SerializedName("push") val push : Boolean,
    @SerializedName("imageInfo") val imageInfo : ImageInfo,
    @SerializedName("star") val star : Int,
    @SerializedName("totalComments") val totalComments : Int,
    @SerializedName("comments") val comments : String,
    @SerializedName("updatedOn") val updatedOn : Long,
    @SerializedName("base") val base : Double,
    @SerializedName("travelTime") val travelTime : Double,
    @SerializedName("totalwaitTime") val totalwaitTime : Double,
    @SerializedName("userTotalPrice") val userTotalPrice : Double,
    @SerializedName("discount") val discount : String,
    @SerializedName("tax") val tax : Int
)