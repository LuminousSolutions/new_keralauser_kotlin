package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class FbLoginData(
        @SerializedName("id") val id : String,
        @SerializedName("supplierId") val supplierId: String?,
        @SerializedName("role") val role: String,
        @SerializedName("taxiId") val taxiId: String?,
        @SerializedName("lan") val lan: String?,
        @SerializedName("hourly") val hourly: Int,
        @SerializedName("price") val price : Long,
        @SerializedName("peakPrice") val peakPrice : Double,
        @SerializedName("basePrice") val basePrice : Long,
        @SerializedName("token") val token : String?,
        @SerializedName("status") val status: String?,
        @SerializedName("isApproved") val isApproved: String?,
        @SerializedName("type") val type: String?,
        @SerializedName("phoneNumber") val phoneNumber: String,
        @SerializedName("email") val email: String,
        @SerializedName("isImage") val isImage: String?,
        @SerializedName("oneSignalValue") val oneSignalValue: String?,
        @SerializedName("name") val name: String

)