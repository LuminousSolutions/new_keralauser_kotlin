package com.mongokerala.taxi.newuser.utils.network

import android.net.Uri
import android.text.TextUtils
import org.json.JSONException
import org.json.JSONTokener
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset

object ServerFasade {
    const val BASE_SERVER_URL = "http://calltaxi.ch"
    const val PARAM_API_KEY = "?apikey="
    const val API_KEY = "274ae1604034be8d2b4296a9c64b5b6e"
    const val API_KEY_URL_PATH = PARAM_API_KEY + API_KEY
    const val CONTENT_JSON = "application/json"
    const val kHttpHeaderContentType = "Content-type"
    val UTF8_CHARSET = Charset.forName("UTF-8")
    const val GZIP_MIN_LENGTH = 1024
    private val baseUri: Uri
        private get() = Uri.parse(BASE_SERVER_URL)

    @Throws(MalformedURLException::class)
    fun makeUrl(pRelativePath: String?, needApiKey: Boolean): URL {
        return makeUrl(pRelativePath, null, needApiKey)
    }

    @Throws(MalformedURLException::class)
    fun makeUrl(
        pRelativePath: String?,
        pQueryParams: Map<String?, String?>?
    ): URL {
        return makeUrl(pRelativePath, pQueryParams, false)
    }

    @Throws(MalformedURLException::class)
    fun makeUrl(
        pRelativePath: String?,
        pQueryParams: Map<String?, String?>?,
        needApiKey: Boolean
    ): URL {
        var pRelativePath = pRelativePath
        if (needApiKey) pRelativePath += API_KEY_URL_PATH
        val uriBuilder = baseUri.buildUpon()
        uriBuilder.appendEncodedPath(pRelativePath)
        if (pQueryParams != null) {
            for (key in pQueryParams.keys) {
                uriBuilder.appendQueryParameter(key, pQueryParams[key])
            }
        }
        return URL(uriBuilder.build().toString())
    }

    @Throws(JSONException::class, ClassCastException::class)
    fun <T> makeJsonFromResponse(response: String?, tClass: Class<T>): T {
        val tokener = JSONTokener(response)
        val json = tokener.nextValue()
        return if (!tClass.isInstance(json)) {
            throw ClassCastException(
                String.format(
                    "Is neeed class (%s), response (%s), data: %s",
                    tClass.simpleName,
                    json?.javaClass?.simpleName ?: "NULL",
                    json
                )
            )
        } else {
            json as T
        }
    }

    @Throws(Exception::class)
    fun httpPostInputStream(
        pUrl: URL,
        pContentType: String?,
        pBody: ByteArray?
    ): InputStream {
        val conn = pUrl.openConnection() as HttpURLConnection
        conn.requestMethod = "POST"
        conn.setRequestProperty(kHttpHeaderContentType, CONTENT_JSON)
        conn.allowUserInteraction = false
        conn.useCaches = false
        if (pBody != null) {
            conn.setRequestProperty(kHttpHeaderContentType, pContentType)
            conn.doInput = true
            conn.doOutput = true
            conn.connect()
            val os: OutputStream
            os = conn.outputStream
            os.write(pBody)
            os.flush()
            os.close()
        } else {
            conn.connect()
        }
        val resposeCode = conn.responseCode
        return conn.inputStream
    }

    @Throws(Exception::class)
    fun httpPost(
        pUrl: URL,
        pContentType: String?,
        pBody: ByteArray?
    ): String {
        val `is`: BufferedInputStream =
            BufferedInputStream(httpPostInputStream(pUrl, pContentType, pBody))
        val result = makeStringFromInputStream(`is`)
        `is`.close()
        return result
    }

    @Throws(IOException::class)
    fun makeStringFromInputStream(stream: InputStream): String {
        var length: Int
        val buffer = CharArray(1024 * 4)
        val reader = InputStreamReader(stream)
        val writer = StringWriter()
        while (reader.read(buffer).also { length = it } != -1) {
            writer.write(buffer, 0, length)
        }
        stream.close()
        return writer.toString()
    }

    @Throws(Exception::class)
    fun httpPostJsonInputStrem(
        pRelativePath: String?,
        pJsonStr: String?
    ): InputStream {
        return httpPostJsonInputStrem(
            makeUrl(pRelativePath, true),
            pJsonStr
        )
    }

    @Throws(Exception::class)
    fun httpPostJsonInputStrem(
        pRelativePath: String?,
        pQueryParams: Map<String?, String?>?,
        pJsonStr: String?
    ): InputStream {
        return httpPostJsonInputStrem(
            makeUrl(
                pRelativePath,
                pQueryParams
            ), pJsonStr
        )
    }

    @Throws(Exception::class)
    fun httpPostJsonInputStrem(pUrl: URL, pJsonStr: String?): InputStream {
        val body =
            pJsonStr?.toByteArray(UTF8_CHARSET)
        return httpPostInputStream(pUrl, CONTENT_JSON, body)
    }

    @Throws(Exception::class)
    fun httpPostJson(pRelativePath: String?, pJsonStr: String): String {
        return httpPostJson(makeUrl(pRelativePath, true), pJsonStr)
    }

    @Throws(Exception::class)
    fun httpPostJson(
        pRelativePath: String?,
        pJsonStr: String,
        needApiKey: Boolean
    ): String {
        return httpPostJson(makeUrl(pRelativePath, needApiKey), pJsonStr)
    }

    @Throws(Exception::class)
    fun httpPostJson(
        pRelativePath: String?,
        pQueryParams: Map<String?, String?>?,
        pJsonStr: String
    ): String {
        return httpPostJson(
            makeUrl(pRelativePath, pQueryParams),
            pJsonStr
        )
    }

    @Throws(Exception::class)
    fun httpPostJson(pUrl: URL, pJsonStr: String): String {
        val body =
            if (TextUtils.isEmpty(pJsonStr)) null else pJsonStr.toByteArray(UTF8_CHARSET)
        return httpPost(pUrl, CONTENT_JSON, body)
    }
}
