package com.mongokerala.taxi.newuser.ui.login

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.messaging.FirebaseMessaging
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.forgetpassword.ForgetPasswordFragment
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity
import com.mongokerala.taxi.newuser.ui.registration.RegistrationActivity
import com.mongokerala.taxi.newuser.ui.reqotp.ReqotpActivity
import com.mongokerala.taxi.newuser.utils.common.Constants
import com.mongokerala.taxi.newuser.utils.common.Status
import com.mongokerala.taxi.newuser.utils.display.Toaster
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class LoginActivity : BaseActivity<LoginViewModel>(),GoogleApiClient.OnConnectionFailedListener {

    companion object {
        const val TAG = "LoginActivity"
        private const val REQUEST_PERMISSIONS_REQUEST_CODE = 34
        private const val REQUEST_PERMISSIONS_REQUEST_CODEE = 35
        var mCallbackmanager: CallbackManager? = null
        var loginButton: LoginButton? =null
        var fb_first_name =""
        var fb_email =""
        var fb_id =""
        var fb_last_name =""
        var fb_image_url =""
        var gmail_image_url =""
    }



    private var activeFragment: Fragment? = null
    private var googleApiClient: GoogleApiClient? = null
    private val RC_SIGN_IN = 1
    private var mGoogleSignInClient:GoogleSignInClient? = null
    private val GOOGLE_SIGN_IN = 101
    var name: String? = null
    var email:String? = null
    var password:String? = null
    var idToken: String? = null
    private var firebaseAuth: FirebaseAuth? = null
    private var authStateListener: AuthStateListener? = null
    var callbackManager:CallbackManager? = null
    var notification_token:String = ""

    override fun provideLayoutId(): Int = R.layout.activity_login

    override fun injectDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun setupView(savedInstanceState: Bundle?) {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        googleApiClient = GoogleApiClient.Builder(this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        if (intent.getSerializableExtra("LOGOUT") != null){
            if (viewModel.getGmailLoginStatus()=="LOGOUT"){
                if (googleApiClient != null){
                    if (!googleApiClient?.isConnected!!) {
                        googleApiClient?.connect()
                        val googleSignInClient = GoogleSignIn.getClient(this, gso)
                        googleSignInClient.signOut()
                        googleApiClient?.disconnect()
                        googleApiClient?.connect()
                        intent.removeExtra("LOGOUT")
                    }
                }
            }
            if(viewModel.getFbLoginstatus() == "LOGOUT"){
            FirebaseAuth.getInstance().signOut()
            LoginManager.getInstance().logOut()
            intent.removeExtra("LOGOUT")
            }
        }
        turnGPSOn()

        val loggedOut = AccessToken.getCurrentAccessToken() == null

        if (!loggedOut){
            getUserProfile(AccessToken.getCurrentAccessToken())
        }
        FacebookSdk.sdkInitialize(applicationContext)
        val EMAIL = "email"
        val public_profile = "public_profile"
        loginButton = findViewById<View>(R.id.login_button_fb) as LoginButton
        loginButton?.setReadPermissions(Arrays.asList(EMAIL,public_profile))
        mCallbackmanager = CallbackManager.Factory.create()
        facebookLogin()



        val signInButton = findViewById<SignInButton>(R.id.sign_in_button)
        signInButton.setSize(SignInButton.SIZE_STANDARD)
        signInButton.setOnClickListener {


            val account = GoogleSignIn.getLastSignedInAccount(this)
            if (account==null){
                val signInIntent: Intent = mGoogleSignInClient!!.signInIntent
                startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
            }else{
                if (googleApiClient != null){
                Auth.GoogleSignInApi.signOut(googleApiClient!!)
                googleApiClient?.disconnect()
                }
            }

        }
    }

    private fun getUserProfile(currentAccessToken: AccessToken?) {
        val request = GraphRequest.newMeRequest(
            currentAccessToken, object : GraphRequest.GraphJSONObjectCallback {
                override fun onCompleted(`object`: JSONObject, response: GraphResponse?) {
                    try {
                        fb_first_name = if (`object`.has("first_name")) {
                            `object`.getString("first_name")
                        } else {
                            ""
                        }

                        fb_last_name = if (`object`.has("last_name")) {
                            `object`.getString("last_name")
                        } else {
                            ""
                        }

                        fb_email = if (`object`.has("email")) {
                            `object`.getString("email")
                        } else {
                            ""
                        }

                        if (`object`.has("id")) {
                            fb_id = `object`.getString("id")
                            fb_image_url = "https://graph.facebook.com/$fb_id/picture?type=normal"
                            facebookLoginMessage()
                        } else {
                            fb_id = ""
                            fb_image_url = ""
                        }
                        facebookLoginApi(
                            fb_first_name,
                            fb_last_name,
                            fb_email,
                            fb_id,
                            currentAccessToken
                        )


                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            })

        val parameters = Bundle()
        parameters.putString("fields", "first_name,last_name,email,id")
        request.parameters = parameters
        request.executeAsync()

    }

    private fun facebookLoginApi(firstName: String, lastName: String, email: String, id: String, currentAccessToken: AccessToken?) {

        viewModel.faceBookLoginApi(firstName,lastName,email,id,currentAccessToken)
    }


    override fun setupObservers() {
        super.setupObservers()
            val mFirebaseInstance = FirebaseDatabase.getInstance()
            val mFirebaseDatabase = mFirebaseInstance.getReference("token")

        FirebaseMessaging.getInstance().token.addOnSuccessListener{
            notification_token=it.toString()
            mFirebaseDatabase.child("result").setValue(notification_token)
        }

        viewModel.launchMain.observe(this) {
            it.getIfNotHandled()?.run {
                val deviceID =
                    Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

                startActivity(Intent(applicationContext, MainNewActivity::class.java))
                viewModel.user_status_update(applicationContext, deviceID)
                finish()
                val toast: Toast =
                    Toast.makeText(applicationContext, "Successfully Logged in", Toast.LENGTH_LONG)
                val v = toast.view?.findViewById(android.R.id.message) as? TextView
                if (v != null) {
                    v.setTextColor(Color.BLACK)
                    v.setBackgroundColor(Color.YELLOW)
                    toast.show()
                }
            }
        }
        viewModel.already_login.observe(this) {
            it.getIfNotHandled()?.run {
                val toast: Toast = Toast.makeText(
                    applicationContext,
                    "Already logged in the another device",
                    Toast.LENGTH_LONG
                )
                val v = toast.view?.findViewById(android.R.id.message) as? TextView
                if (v != null) {
                    v.setTextColor(Color.BLACK)
                    v.setBackgroundColor(Color.YELLOW)
                    toast.show()
                }
            }
        }

        viewModel.Password_wrong_infoid.observe(this) {
            val toast: Toast =
                Toast.makeText(applicationContext, "password_wrong", Toast.LENGTH_LONG)
            val v = toast.view?.findViewById(android.R.id.message) as? TextView
            if (v != null) {
                v.setTextColor(Color.BLACK)
                v.setBackgroundColor(Color.YELLOW)
                toast.show()
            }


        }
        viewModel.Please_Enter_Valid_Email_or_Mobile_Number.observe(this, Observer {
            Toaster.show(applicationContext, getString(R.string.Please_Enter_Valid_Email_or_Mobile_Number))
        })
        viewModel.Email_not_exiting_login.observe(this) {
            val toast: Toast =
                Toast.makeText(applicationContext, "email_not_existing", Toast.LENGTH_LONG)
            val v = toast.view?.findViewById(android.R.id.message) as? TextView
            if (v != null) {
                v.setTextColor(Color.BLACK)
                v.setBackgroundColor(Color.YELLOW)
                toast.show()
            }

        }
        viewModel.Password_wrong2_infoid.observe(this) {
            val toast: Toast =
                Toast.makeText(applicationContext, "password_wrong", Toast.LENGTH_LONG)
            val v = toast.view?.findViewById(android.R.id.message) as? TextView
            if (v != null) {
                v.setTextColor(Color.BLACK)
                v.setBackgroundColor(Color.YELLOW)
                toast.show()
            }
        }
        viewModel.email_or_password_not_exiting.observe(this) {
            val toast: Toast = Toast.makeText(
                applicationContext,
                "email_or_domain_not_existing",
                Toast.LENGTH_LONG
            )
            val v = toast.view?.findViewById(android.R.id.message) as? TextView
            if (v != null) {
                v.setTextColor(Color.BLACK)
                v.setBackgroundColor(Color.YELLOW)
                toast.show()
            }
        }
        viewModel.user_wrong.observe(this) {
            val toast: Toast = Toast.makeText(applicationContext, "user_wrong", Toast.LENGTH_LONG)
            val v = toast.view?.findViewById(android.R.id.message) as? TextView
            if (v != null) {
                v.setTextColor(Color.BLACK)
                v.setBackgroundColor(Color.YELLOW)
                toast.show()
            }

        }

        viewModel.noOneSignalValue.observe(this) {
            it.getIfNotHandled()?.run {
                val toast: Toast =
                    Toast.makeText(applicationContext, "No One Signal Value", Toast.LENGTH_LONG)
                val v = toast.view?.findViewById(android.R.id.message) as? TextView
                if (v != null) {
                    v.setTextColor(Color.BLACK)
                    v.setBackgroundColor(Color.YELLOW)
                    toast.show()
                }
            }
        }
        viewModel.underVerification.observe(this) {
            it.getIfNotHandled()?.run {
                val toast: Toast = Toast.makeText(
                    applicationContext,
                    "Please wait while we activate",
                    Toast.LENGTH_LONG
                )
                val v = toast.view?.findViewById(android.R.id.message) as? TextView
                if (v != null) {
                    v.setTextColor(Color.BLACK)
                    v.setBackgroundColor(Color.YELLOW)
                    toast.show()
                }




                et_email.visibility = View.GONE
                et_password.visibility = View.GONE
                bt_login.visibility = View.GONE
                sign_up_txt.visibility = View.GONE
            }


        }



        viewModel.emailField.observe(this) {
            if (et_email.text.toString() != it) et_email.setText(it)
        }

        viewModel.credential_verification.observe(this) {
            credentialVerification()
        }

        viewModel.fbLoginError.observe(this) {
            fbLoginResponseError()
        }

        viewModel.passwordField.observe(this) {
            if (et_password.text.toString() != it) et_email.setText(it)
        }

        viewModel.passwordValidation.observe(this) {
            when (it.status) {
                Status.ERROR -> layout_password.error = it.data?.run { getString(this) }
                else -> layout_password.isErrorEnabled = false
            }
        }

        viewModel.loggingIn.observe(this) {
            spin_kit.visibility = if (it) View.VISIBLE else View.GONE
        }


        viewModel.fbSingOut.observe(this) {
            FirebaseAuth.getInstance().signOut()
            LoginManager.getInstance().logOut()
        }

        et_email.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.onEmailChange(s.toString())
            }

            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        })
        et_email.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.onPhoneNumberChange(s.toString())
            }

            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        })


        et_password.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.onPasswordChange(s.toString())
            }

            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        })

        bt_login.setOnClickListener {

            if (et_email.text.toString().isNotEmpty() && et_password.text.toString().isNotEmpty()){
                hideKeyboard(this)
                viewModel.onLogin(notification_token)
            }else{
                Toaster.show(this,"Please Enter Field")
            }

        }

                sign_up_txt.setOnClickListener {
                    val mobileNumberValidation = Intent(this, ReqotpActivity::class.java)
                    mobileNumberValidation.putExtra(Constants.mobileNumberValidation,Constants.mobileNumberValidation)
                    startActivity(mobileNumberValidation)
                    finish()
                }
        viewModel.launchRegistration.observe(this) {
            it.getIfNotHandled()?.run {
                startActivity(Intent(applicationContext, RegistrationActivity::class.java))
                finish()

            }
        }

        txt_forget_password.setOnClickListener {
            containerForgetPasswordFragment.visibility=View.VISIBLE
            loginscreen.visibility=View.GONE

            showForgetPasswword()

        }

        firebaseAuth = FirebaseAuth.getInstance()
        authStateListener = AuthStateListener { firebaseAuth ->

            val user = firebaseAuth.currentUser


        }



        callbackManager = CallbackManager.Factory.create()

        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedIn = accessToken != null && !accessToken.isExpired

    }

    private fun fbLoginResponseError() {
        social_login_logo.visibility=View.GONE
        logo.visibility=View.VISIBLE
        layout_email.visibility=View.VISIBLE
        layout_password.visibility=View.VISIBLE
        bt_login.visibility=View.VISIBLE
        connectwith.visibility=View.VISIBLE
        txt_forget_password.visibility=View.VISIBLE
        sign_up_txt.visibility=View.VISIBLE
        Not_a_member_yet_txt.visibility=View.VISIBLE
        FirebaseAuth.getInstance().signOut()
        LoginManager.getInstance().logOut()
        Toast.makeText(this, "Facebook Login Response was Error...", Toast.LENGTH_SHORT).show()
    }

    private fun facebookLoginMessage() {
        social_login_logo.visibility=View.VISIBLE
        logo.visibility=View.GONE
        layout_email.visibility=View.GONE
        layout_password.visibility=View.GONE
        bt_login.visibility=View.GONE
        connectwith.visibility=View.GONE
        txt_forget_password.visibility=View.GONE
        sign_up_txt.visibility=View.GONE
        Not_a_member_yet_txt.visibility=View.GONE
        if (fb_image_url.isNotEmpty()){
            Picasso.get().load(fb_image_url).into(social_login_logo)
        }

    }

    private fun gMailLoginMessage(){
        social_login_logo.visibility=View.VISIBLE
        logo.visibility=View.GONE
        layout_email.visibility=View.GONE
        layout_password.visibility=View.GONE
        bt_login.visibility=View.GONE
        connectwith.visibility=View.GONE
        txt_forget_password.visibility=View.GONE
        sign_up_txt.visibility=View.GONE
        Not_a_member_yet_txt.visibility=View.GONE
        sign_in_button.visibility=View.GONE
        if (gmail_image_url.isNotEmpty()){
            Picasso.get().load(gmail_image_url).into(social_login_logo)
        }

    }

    private fun credentialVerification() {
        spin_kit.visibility=View.GONE
        Toast.makeText(this, "Please Enter Your Valid PhoneNnumber or Email", Toast.LENGTH_SHORT).show()
    }

    private fun facebookLogin() {


        loginButton?.registerCallback(mCallbackmanager, object : FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {
                val loggedIn = AccessToken.getCurrentAccessToken() == null
                handleFacebookAccessToken(loginResult!!.accessToken)
            }

            override fun onCancel() {

            }

            override fun onError(exception: FacebookException?) {


            }
        })


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            if (data != null){
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result!!)
            }
        }

        if (requestCode == GOOGLE_SIGN_IN ) {

            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            if (task.isSuccessful){
                if (data !=null){
                    val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                    handleSignInResult(result!!)
                }

            }else{
                Toast.makeText(this, "Login failed", Toast.LENGTH_SHORT).show()
            }

        }
        mCallbackmanager?.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            val account = result.signInAccount
            if (account != null){
                 idToken = account.idToken
                val name = account.displayName!!
                val email = account.email!!
                gmail_image_url= account.photoUrl!!.toString()
                val id=account.id!!
                gMailLoginMessage()
                if (gmail_image_url.isNotEmpty()){
                    viewModel.savegMailLoginImageUrl(gmail_image_url)
                }
                Toast.makeText(this, "$email Login Successfully", Toast.LENGTH_LONG).show()
                viewModel.gMailLoginApi(email,name,"","",id)
            }
            
            val credential = GoogleAuthProvider.getCredential(idToken, null)
            firebaseAuthWithGoogle(credential)
        }
    }

    private fun firebaseAuthWithGoogle(credential: AuthCredential) {
        firebaseAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(this, object : OnCompleteListener<AuthResult?> {
                override fun onComplete(@NonNull task: Task<AuthResult?>) {
                    if (task.isSuccessful) {
                        val toast: Toast = Toast.makeText(applicationContext, "Login successful", Toast.LENGTH_LONG)
                        val v = toast.view?.findViewById(android.R.id.message) as? TextView
                        if (v != null) {
                        v.setTextColor(Color.BLACK)
                        v.setBackgroundColor(Color.YELLOW)
                        toast.show()
                        }
                    } else {
                        task.exception!!.printStackTrace()
                        val toast: Toast = Toast.makeText(applicationContext, "Authentication failed", Toast.LENGTH_LONG)
                        val v = toast.view?.findViewById(android.R.id.message) as? TextView
                        if (v != null) {
                        v.setTextColor(Color.BLACK)
                        v.setBackgroundColor(Color.YELLOW)
                        toast.show()
                        }

                    }
                }
            })
    }

    override fun onStart() {
        super.onStart()
        if (authStateListener != null){
            FirebaseAuth.getInstance().signOut()
        }
        authStateListener?.let { firebaseAuth!!.addAuthStateListener(it) }

        if (!checkPermissions()) {
            requestPermissions()
        }
        if (!checkPermissions2()) {
            requestPermissions2()
        }
    }

    override fun onStop() {
        super.onStop()
        if (authStateListener != null){
            firebaseAuth!!.removeAuthStateListener(authStateListener!!)
        }
    }

    private fun showForgetPasswword() {
        if (activeFragment is ForgetPasswordFragment) return

        val fragmentTransaction = supportFragmentManager.beginTransaction()

        var fragment = supportFragmentManager.findFragmentByTag(ForgetPasswordFragment.TAG) as ForgetPasswordFragment?

        if (fragment == null) {
            fragment = ForgetPasswordFragment.newInstance()
            fragmentTransaction.add(R.id.containerForgetPasswordFragment, fragment, ForgetPasswordFragment.TAG)
        } else {
            fragmentTransaction.show(fragment)
        }

        if (activeFragment != null) fragmentTransaction.hide(activeFragment as Fragment)

        fragmentTransaction.commit()
        activeFragment = fragment


    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }


    private fun handleFacebookAccessToken(accessToken: AccessToken?) {
        val credential = FacebookAuthProvider.getCredential(accessToken!!.token)
        firebaseAuth!!.signInWithCredential(credential)
            .addOnFailureListener{ e->
                e.message?.let {
                    Toaster.show(applicationContext, it)
                }
            }
            .addOnSuccessListener { result ->
                val email = result.user!!.email
                result.user!!.phoneNumber
                if (email.isNullOrEmpty()){
                    Toaster.show(applicationContext, "You Logged FaceBook Successfully")
                }else{
                    Toaster.show(applicationContext, "Your logged with email: $email")
                }
                viewModel.saveFbLoginStatus()
                getUserProfile(accessToken)
            }
    }

    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun checkPermissions2(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    private fun startLocationPermissionRequest2() {
        ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODEE
        )
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
        )

        if (shouldProvideRationale) {
            showSnackbar(R.string.permission_rationale, android.R.string.ok) {
                startLocationPermissionRequest()
            }

        } else {
            startLocationPermissionRequest()
        }
    }

    private fun requestPermissions2() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
        )

        if (shouldProvideRationale) {

            showSnackbar(R.string.permission_rationale, android.R.string.ok) {
                startLocationPermissionRequest2()
            }

        } else {
            startLocationPermissionRequest2()
        }
    }


    private fun showSnackbar(mainTextStringId: Int, actionStringId: Int,
                             listener: View.OnClickListener) {

        Toast.makeText(this, getString(mainTextStringId), Toast.LENGTH_LONG).show()
    }

    private fun turnGPSOn() {

        val googleApiClient = GoogleApiClient.Builder(this)
            .addApi(LocationServices.API).build()
        googleApiClient.connect()

        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = (10000 / 2).toLong()

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)

        val result =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {

                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    try {
                        status.startResolutionForResult(this, 1)
                    } catch (e: IntentSender.SendIntentException) {
                    }

                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {

                }
            }
        }
    }

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View? = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onBackPressed() {
        alertDialogue()
    }

    private fun alertDialogue() {
        val alertDialogue = AlertDialog.Builder(this)
        alertDialogue.setTitle(getString(R.string.close_this_screen))
        alertDialogue.setCancelable(false)
        alertDialogue.setPositiveButton(getString(R.string.yes)) { dialog, which ->
            finish()
        }
        alertDialogue.setNegativeButton(getString(R.string.no)) { dialog, which ->
            dialog.dismiss()
        }
        alertDialogue.show()
    }


}

