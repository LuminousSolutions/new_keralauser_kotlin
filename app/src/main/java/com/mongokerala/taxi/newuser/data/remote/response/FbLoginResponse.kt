package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class FbLoginResponse(
        @SerializedName("statusCode") val statusCode : Int,
        @SerializedName("status") val status : Boolean,
        @SerializedName("message") val message : String?,
        @SerializedName("data") val data : FbLoginData,
        @SerializedName("jwt") val jwt : String?,
        @SerializedName("infoId") val infoId : Int

)