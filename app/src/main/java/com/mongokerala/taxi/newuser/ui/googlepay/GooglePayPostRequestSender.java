package com.mongokerala.taxi.newuser.ui.googlepay;



import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Akhi007 on 05-08-2019.
 */
public interface GooglePayPostRequestSender {

   // String BASE_URL = "http://63.142.252.78:8080/payment/";
    //String BASE_URL = "http://10.0.2.2:8080";

    String BASE_URL = "https://1-dot-taxi2dealin.appspot.com/";

   /* @POST("api/orders/v1/order/google-pay-payment/create")
    Call<GoogleOrderDTO> createGooglePayOrderPayment(@Body GoogleOrderDTO orderDTO);

    @POST("api/orders/v1/order/google-pay-payment/direct/create")
    Call<GoogleOrderDTO> createGooglePayOrderPaymentDirect(@Body GoogleOrderDTO orderDTO);*/

    @POST("/api/orders/v1/order/google-pay-payment/create")
    Call<GoogleOrderDTO> createGooglePayOrderPayment(@Body GoogleOrderDTO orderDTO);

    @POST("/api/orders/v1/order/google-pay-payment/direct/create")
    Call<GoogleOrderDTO> createGooglePayOrderPaymentDirect(@Body GoogleOrderDTO orderDTO);
}
