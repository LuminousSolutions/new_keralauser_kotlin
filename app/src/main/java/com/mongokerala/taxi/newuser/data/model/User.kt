package com.mongokerala.taxi.newuser.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class User(

    @Expose
    @SerializedName("userId")
    val id: String,

    @Expose
    @SerializedName("userName")
    val name: String,

    @Expose
    @SerializedName("userEmail")
    val email: String,


    @Expose
    @SerializedName("taxiId")
    val taxiId: String,

    @Expose
    @SerializedName("supplierId")
    val supplierId: String,

    @Expose
    @SerializedName("lan")
    val lan: String,

    @Expose
    @SerializedName("isApproved")
    val isApproved: String,

    @Expose
    @SerializedName("type")
    val type: String,

    @Expose
    @SerializedName("oneSignalValue")
    val oneSignalValue: String,

    @Expose
    @SerializedName("profilePicUrl")
    val profilePicUrl: String? = null


)