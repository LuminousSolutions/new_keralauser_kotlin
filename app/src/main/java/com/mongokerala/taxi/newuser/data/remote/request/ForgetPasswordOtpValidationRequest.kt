package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mongokerala.taxi.newuser.utils.network.ERole

class ForgetPasswordOtpValidationRequest(
    @field:SerializedName(Keys.countryCode) @field:Expose var countryCode: String?,
    @field:SerializedName(Keys.phoneNumber) @field:Expose var phoneNumber: String,
    @field:SerializedName(Keys.token) @field:Expose var token: String,
    @field:SerializedName(Keys.password) @field:Expose var password: String

) {
    @Expose
    @SerializedName(Keys.role)
    var role: String = ERole.ROLE_USER.toString()

    @Expose
    @SerializedName(Keys.rideId)
    var rideId: String = "string"

    @Expose
    @SerializedName(Keys.userId)
    var userId: String = "string"
}