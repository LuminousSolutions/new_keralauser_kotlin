package com.mongokerala.taxi.newuser.ui.common_dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.utils.common.Constants.CANCEl_BOOKING
import com.mongokerala.taxi.newuser.utils.common.Constants.DRIVER_CANCEL_RIDE
import com.mongokerala.taxi.newuser.utils.common.Constants.REQUEST_CHANGE_DESTINATION
import com.mongokerala.taxi.newuser.utils.common.Constants.REQUEST_ORDER
import com.mongokerala.taxi.newuser.utils.common.Constants.RIDE_CANCEL_BOOKING
import com.mongokerala.taxi.newuser.utils.common.Constants.RIDE_FINISH
import com.mongokerala.taxi.newuser.utils.common.Constants.RIDE_REQUEST_CHANGEDESTINATION


class RideFinishCustomAlertDialog(context: Context,var myOnClickListener:MYOnClickListener ) : Dialog(context){

    var source_address: String? = null
    private var destination_address: String? = null
    private var user_total_ride_km: String? = null
    private var user_total_ride_amt: String? = null
    var Action: String? = null
    private var reason: String? = null
    private  var paymentType:String? = null
    private var taxi_type: String? = null
    var rating = 0.0



     constructor(
           context: Context,
           source: String,
           destination_address: String?,
           user_total_ride_km: String,
           user_total_ride_amt: String,
           action: String,
           reason: String,
           paymentType: String,
           myOnClickListener:MYOnClickListener
   ) : this(context, myOnClickListener) {
       source_address = source
       this.destination_address = destination_address
       this.user_total_ride_km = user_total_ride_km
       this.user_total_ride_amt = user_total_ride_amt
       Action = action
       taxi_type = reason
       this.reason = reason
       this.paymentType = paymentType
   }
        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_ride_finish)
        val close_cancel =findViewById<TextView>(R.id.close_cancel) as TextView
        val txt_title =findViewById<TextView>(R.id.txt_title) as TextView
        val txt_reason = findViewById<TextView>(R.id.txt_reason_msg) as TextView
        val txt_reason_title = findViewById<TextView>(R.id.txt_reason) as TextView
        val txt_source_address = findViewById<TextView>(R.id.tv_source_address) as TextView
        val source_address_txtview = findViewById<TextView>(R.id.source_address) as TextView
        val destination_address_txtview = findViewById<TextView>(R.id.destination_address) as TextView
        val tv_payment_Type =findViewById<TextView>(R.id.tv_payment_Type) as TextView
        val lay_paytype =findViewById<RelativeLayout>(R.id.lay_paytype) as RelativeLayout
        val txt_destination_address =findViewById<TextView>(R.id.tv_destination_address) as TextView
        val Cartype_booking_dialog =findViewById<TextView>(R.id.Cartype_booking_dialog) as TextView
        val tv_Cartype_booking_dialogs =findViewById<TextView>(R.id.tv_Cartype_booking_dialogs) as TextView
        val txt_user_total_ride_km =findViewById<TextView>(R.id.total_km_value) as TextView
        val txt_user_total_ride_amt =findViewById<TextView>(R.id.total_price_value) as TextView
        val ratingBarRideFinish =findViewById<RatingBar>(R.id.ratingBar_user_ride_finish_finish) as RatingBar
        val editTextCommentsRideFinish =findViewById<EditText>(R.id.edtText_comments_ride_finish) as EditText
        val editTextCoupon_text = findViewById<EditText>(R.id.etxt_coupon) as EditText
        val lay_cartype = findViewById<LinearLayout>(R.id.lay_cartype) as LinearLayout
        val lay_km = findViewById<LinearLayout>(R.id.lay_km) as LinearLayout
        val lay_two_button = findViewById<RelativeLayout>(R.id.lay_two_button) as RelativeLayout
        val copun_lay = findViewById<LinearLayout>(R.id.copun_lay) as LinearLayout
        val lay_ratting: CardView = findViewById<CardView>(R.id.lay_ratting) as CardView
        val btn_accept = findViewById<Button>(R.id.btn_yes) as Button
        val btn_checkCoupon = findViewById<Button>(R.id.checkCoupon) as Button
        val btn_reject = findViewById<Button>(R.id.btn_later) as Button
        val btn = findViewById<Button>(R.id.btn_ok) as Button
        val btn_change = findViewById<Button>(R.id.btn_ok_change_destination) as Button
        ratingBarRideFinish.setOnClickListener { rating = ratingBarRideFinish.rating.toDouble() }

        btn_accept.setOnClickListener {
            dismiss()
        }

        btn_reject.setOnClickListener {
            dismiss()
        }


        editTextCoupon_text.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                btn_accept.visibility = View.VISIBLE
                btn_reject.visibility = View.VISIBLE
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    btn_checkCoupon.visibility = View.GONE
                    btn_accept.visibility = View.VISIBLE
                    btn_reject.visibility = View.VISIBLE
                } else {
                    btn_checkCoupon.visibility = View.VISIBLE
                    btn_accept.visibility = View.GONE
                    btn_reject.visibility = View.GONE
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        btn.setOnClickListener {
            if (Action.equals(RIDE_FINISH, ignoreCase = true)) {
                rating = ratingBarRideFinish.rating.toDouble()
                myOnClickListener.onUpdateReview(Action, editTextCommentsRideFinish.text.toString(), rating, txt_user_total_ride_amt.text.toString())
            } else if (Action.equals(DRIVER_CANCEL_RIDE, ignoreCase = true)) {
                rating = ratingBarRideFinish.rating.toDouble()
            }
            dismiss()
        }

       btn_change.setOnClickListener {
           dismiss()
       }


       close_cancel.setOnClickListener { dismiss() }

        if (Action.equals(REQUEST_ORDER, ignoreCase = true)) {
            txt_title.setText(R.string.Booking_Detail)
            lay_km.visibility = View.GONE
            close_cancel.visibility = View.GONE
            txt_reason.visibility = View.VISIBLE
            txt_source_address.text = source_address
            txt_destination_address.text = destination_address
            lay_ratting.visibility = View.GONE
            btn.visibility = View.GONE
            tv_Cartype_booking_dialogs.text = reason
            if (paymentType == "GPay") {
                tv_payment_Type.text = "GooglePay"
            } else {
                tv_payment_Type.text = paymentType
            }
            lay_two_button.visibility = View.VISIBLE
            copun_lay.visibility = View.GONE
        } else if (Action.equals(REQUEST_CHANGE_DESTINATION, ignoreCase = true) || Action.equals(RIDE_REQUEST_CHANGEDESTINATION, ignoreCase = true)) {
            lay_km.visibility = View.GONE
            txt_title.setText(R.string.Change_Destination)
            source_address_txtview.visibility = View.GONE
            //txt_reason.setVisibility(View.GONE);
            editTextCommentsRideFinish.visibility = View.VISIBLE
            editTextCommentsRideFinish.setText(R.string.Please_enter_change_destination_address)
            lay_cartype.visibility = View.GONE
            destination_address_txtview.visibility = View.GONE
            lay_paytype.visibility = View.GONE
            txt_source_address.visibility = View.GONE
            txt_destination_address.text = destination_address
            close_cancel.visibility = View.GONE
            btn_change.visibility = View.VISIBLE
            btn.visibility = View.GONE
            lay_two_button.visibility = View.GONE
            lay_ratting.visibility = View.GONE
        } else if (Action.equals(CANCEl_BOOKING, ignoreCase = true) || Action.equals(RIDE_CANCEL_BOOKING, ignoreCase = true)) {
            txt_title.setText(R.string.Cancel_Booking)
            lay_km.visibility = View.GONE
            txt_reason.visibility = View.VISIBLE
            txt_reason_title.visibility = View.VISIBLE
            //txt_reason.setText(reason);
            lay_paytype.visibility = View.GONE
            txt_reason.visibility = View.GONE
            editTextCommentsRideFinish.visibility = View.GONE
            editTextCommentsRideFinish.setText(R.string.Please_enter_reason)
            lay_cartype.visibility = View.GONE
            close_cancel.visibility = View.GONE
            txt_source_address.text = source_address
            txt_destination_address.text = destination_address
            lay_ratting.visibility = View.GONE
            btn.visibility = View.VISIBLE
            btn.setText(R.string.OK)
            lay_two_button.visibility = View.GONE
        } else if (Action.equals(RIDE_FINISH, ignoreCase = true)) {
            txt_title.setText(R.string.ride_finished)
            lay_km.visibility = View.VISIBLE
            txt_reason.visibility = View.GONE
            close_cancel.visibility = View.GONE
            lay_cartype.visibility = View.GONE
            txt_source_address.text = source_address
            if (paymentType == "GPay") {
                tv_payment_Type.text = "GooglePay"
            } else {
                tv_payment_Type.text = paymentType
            }
            txt_destination_address.text = destination_address
            lay_ratting.visibility = View.VISIBLE
            btn.visibility = View.VISIBLE
            lay_two_button.visibility = View.GONE
            txt_user_total_ride_km.text = user_total_ride_km
            txt_user_total_ride_amt.text = user_total_ride_amt
        } else if (Action.equals(DRIVER_CANCEL_RIDE, ignoreCase = true)) {
            txt_title.setText(R.string.Cancel_Ride)
            lay_km.visibility = View.GONE
            source_address_txtview.visibility = View.VISIBLE
            txt_reason.visibility = View.GONE
            txt_reason.text = reason
            destination_address_txtview.visibility = View.VISIBLE
            lay_ratting.visibility = View.VISIBLE
            close_cancel.visibility = View.VISIBLE
            lay_cartype.visibility = View.GONE
            btn.visibility = View.VISIBLE
            lay_two_button.visibility = View.GONE
            txt_user_total_ride_km.text = user_total_ride_km
            txt_user_total_ride_amt.text = user_total_ride_amt
        } else if (Action.equals("CANCEL_BY_USER", ignoreCase = true)) {
            txt_title.setText(R.string.CANCEL_ACCEPTED)
            lay_km.visibility = View.VISIBLE
            txt_reason.visibility = View.GONE
            close_cancel.visibility = View.GONE
            lay_cartype.visibility = View.GONE
            txt_source_address.text = source_address
            txt_destination_address.text = destination_address
            lay_ratting.visibility = View.VISIBLE
            btn.visibility = View.VISIBLE
            lay_two_button.visibility = View.GONE
            if (paymentType == "GPay") {
                tv_payment_Type.text = "GooglePay"
            } else {
                tv_payment_Type.text = paymentType
            }
            txt_user_total_ride_km.text = user_total_ride_km
            txt_user_total_ride_amt.text = user_total_ride_amt
        }
    }
 }