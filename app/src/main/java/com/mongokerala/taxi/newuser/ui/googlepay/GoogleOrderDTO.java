package com.mongokerala.taxi.newuser.ui.googlepay;

import com.mongokerala.taxi.newuser.ui.payment.CustomerDTO;

public class GoogleOrderDTO {

    private PaymentInfoDTO paymentInfo;

    // Below for : Google Pay Payment Gateway only
    private String googlePayResponsePayload;
    private String googlePayPaymentToken;

    private GoogleOrderRequestType orderReqType;

    private CustomerDTO customer;

    public PaymentInfoDTO getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfoDTO paymentInfo) {
        this.paymentInfo = paymentInfo;
    }





    public String getGooglePayResponsePayload() {
        return googlePayResponsePayload;
    }

    public void setGooglePayResponsePayload(String googlePayResponsePayload) {
        this.googlePayResponsePayload = googlePayResponsePayload;
    }

    public String getGooglePayPaymentToken() {
        return googlePayPaymentToken;
    }

    public void setGooglePayPaymentToken(String googlePayPaymentToken) {
        this.googlePayPaymentToken = googlePayPaymentToken;
    }

    public GoogleOrderRequestType getOrderReqType() {
        return orderReqType;
    }

    public void setOrderReqType(GoogleOrderRequestType orderReqType) {
        this.orderReqType = orderReqType;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }
}
