package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class OfflineRideSmsToUserRequest (
    @field:SerializedName(Keys.country) @field:Expose var countryCode: String?,
    @field:SerializedName("userPhoneNumber") @field:Expose var userPhoneNumber: String?,
    @field:SerializedName(Keys.source) @field:Expose var source: String?,
    @field:SerializedName(Keys.userId) @field:Expose var userId: String?,
    @field:SerializedName(Keys.destination) @field:Expose var destination: String?,
    @field:SerializedName(Keys.driverId) @field:Expose var driverId: String?,
    @field:SerializedName(Keys.driverName) @field:Expose var driverName: String?,
    @field:SerializedName("vechileNumber") @field:Expose var vechileNumber: String?,
    @field:SerializedName("totalPrice") @field:Expose var totalPrice: String?,
    @field:SerializedName("driverPhoneNumber") @field:Expose var driverPhoneNumber: String?,
    @field:SerializedName("contact") @field:Expose var contact: String?,
    @field:SerializedName("category") @field:Expose var category: String?,
    @field:SerializedName("userName") @field:Expose var userName: String?,
    @field:SerializedName("km") @field:Expose var km: String?
){


    @SerializedName("bookingId")
    @Expose
    private var bookingId: Int? = null

    @SerializedName("destLatitude")
    @Expose
    private var destLatitude: Int? = null

    @SerializedName("destLongitude")
    @Expose
    private var destLongitude: Int? = null

    @SerializedName("display")
    @Expose
    private var display: String? = null

    @SerializedName("id")
    @Expose
    private var id: Int? = null

    @SerializedName("isMobile")
    @Expose
    private var isMobile: String? = null

    @SerializedName("otp")
    @Expose
    private var otp: String? = null

    @SerializedName("sourceLatitude")
    @Expose
    private var sourceLatitude: Int? = null

    @SerializedName("sourceLongitude")
    @Expose
    private var sourceLongitude: Int? = null


    init {
        isMobile = "AND"
        otp = "string"
        sourceLongitude = 0
        sourceLatitude = 0
        id = 0
        display = "string"
        destLongitude = 0
        destLatitude = 0
        bookingId = 0
    }


}