package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class BaseResponse (


    @SerializedName("statusCode") val statusCode : Int,
    @SerializedName("status") val status : String,
    @SerializedName("message") val message : String,
    @SerializedName("data") val data : String?,
    @SerializedName("jwt") val jwt : String?,
    @SerializedName("infoId") val infoId : String

)