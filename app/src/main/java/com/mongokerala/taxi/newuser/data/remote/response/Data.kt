package com.mongokerala.taxi.newuser.data.remote.response
import com.google.gson.annotations.SerializedName

/*
Copyright (c) 2020 Kotlin Data Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/seyedabsar */


data class Data (

	@SerializedName("id") val id : String,
	@SerializedName("status") val status: String,
	@SerializedName("isApproved") val isApproved: String,
	@SerializedName("type") val type: String,
	@SerializedName("phoneNumber") val phoneNumber: String,
	@SerializedName("email") val email: String,
	@SerializedName("oneSignalValue") val oneSignalValue: String,
	@SerializedName("name") val name: String,
	@SerializedName("supplierId") val supplierId: String,
	@SerializedName("role") val role: String,
	@SerializedName("taxiId") val taxiId: String,
	@SerializedName("lan") val lan: String,
	@SerializedName("hourly") val hourly: Int,
	@SerializedName("userId") val userId : String,
	@SerializedName("description") val description : String,
	@SerializedName("hour") val hour : Long,
	@SerializedName("km") val km : Long,
	@SerializedName("additionalInformation") val additionalInformation : String,
	@SerializedName("latitude") val latitude : Double,
	@SerializedName("longitude") val longitude : Double,
	@SerializedName("taxiNumber") val taxiNumber : String,
	@SerializedName("drivername") val drivername : String,
	@SerializedName("driverPhonenumber") val driverPhonenumber : Long,
	@SerializedName("imageInfos") val imageInfos : List<String>,
	@SerializedName("currency") val currency : String,
	@SerializedName("transporttype") val transporttype : String,
	@SerializedName("carType") val carType : String,
	@SerializedName("airPortprice") val airPortprice : Long,
	@SerializedName("perDay") val perDay : String,
	@SerializedName("weekEndOffer") val weekEndOffer : String,
	@SerializedName("price") val price : Long,
	@SerializedName("peakPrice") val peakPrice : Double,
	@SerializedName("basePrice") val basePrice : Long,
	@SerializedName("waitingTime") val waitingTime : Long,
	@SerializedName("minimumFare") val minimumFare : Long,
	@SerializedName("source") val source : String,
	@SerializedName("destination") val destination : String,
	@SerializedName("pickUpLocation") val pickUpLocation : String,
	@SerializedName("cityDTO") val cityDTO : CityDTO,
	@SerializedName("supplierDTO") val supplierDTO : String,
	@SerializedName("active") val active : String,
	@SerializedName("updatedOn") val updatedOn : String,
	@SerializedName("city") val city : String,
	@SerializedName("year") val year : Int,
	@SerializedName("vehicleBrand") val vehicleBrand : String,
	@SerializedName("seats") val seats : Int,
	@SerializedName("vehicleTypeDTO") val vehicleTypeDTO : VehicleTypeDTO,
	@SerializedName("tags") val tags : List<String>




)