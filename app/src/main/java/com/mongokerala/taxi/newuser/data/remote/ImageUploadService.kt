package com.mongokerala.taxi.newuser.data.remote

import com.google.gson.GsonBuilder
import com.mongokerala.taxi.newuser.BuildConfig
import com.mongokerala.taxi.newuser.data.remote.response.ImageResponse
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

class ImageUploadService {
    private val retrofit: Retrofit
    private val apiClient: ApiClient
    private val loggingInterceptor: HttpLoggingInterceptor
    private val okHttpClient: OkHttpClient
    fun uploadFile(
        file: MultipartBody.Part?,
        driverName: String?,
        type: String?,
        userId: String?
    ): Call<ImageResponse> {
        return apiClient.upload(file, driverName, type, userId)
    }

    interface ApiClient {
        @Multipart
        @POST(Endpoints.ADD_PHOTO)
        fun upload(
            @Part file: MultipartBody.Part?,
            @Query("driver_name") driverName: String?,
            @Query("type") type: String?,
            @Query("userId") userId: String?
        ): Call<ImageResponse>
    }

    companion object {
        const val BASE_URL = BuildConfig.BASE_URL
    }

    init {
        loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        okHttpClient = OkHttpClient.Builder() //debug
            .addInterceptor(loggingInterceptor)
            .build()
        val gson = GsonBuilder()
            .serializeNulls()
            .create()
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
        apiClient = retrofit.create(ApiClient::class.java)
    }
}
