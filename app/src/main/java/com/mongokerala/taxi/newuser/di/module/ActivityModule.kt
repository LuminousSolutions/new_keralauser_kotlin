package com.mongokerala.taxi.newuser.di.module

import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.mongokerala.taxi.newuser.data.repository.DummyRepository
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.about.AboutViewModel
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.chat.ChatViewmodel
import com.mongokerala.taxi.newuser.ui.dummy.DummyViewModel
import com.mongokerala.taxi.newuser.ui.feedback.FeedbackViewModel
import com.mongokerala.taxi.newuser.ui.login.LoginViewModel
import com.mongokerala.taxi.newuser.ui.main.MainViewModel
import com.mongokerala.taxi.newuser.ui.main_new.MainNewViewModel
import com.mongokerala.taxi.newuser.ui.profile_edit.ProfileEditViewModel
import com.mongokerala.taxi.newuser.ui.rate_us.RateUsViewModel
import com.mongokerala.taxi.newuser.ui.registration.RegistrationViewModel
import com.mongokerala.taxi.newuser.ui.reqotp.ReqotpViewModel
import com.mongokerala.taxi.newuser.ui.setting.SettingViewModel
import com.mongokerala.taxi.newuser.ui.splash.SplashViewModel
import com.mongokerala.taxi.newuser.ui.tour.TourViewModel
import com.mongokerala.taxi.newuser.ui.tripdetail.TripDetailViewModel
import com.mongokerala.taxi.newuser.ui.triphistory.TripHistoryViewModel
import com.mongokerala.taxi.newuser.ui.validatemobile.ValidateMobileViewModel
import com.mongokerala.taxi.newuser.ui.vehicleinfoupdate.VehicleInfoUpdateViewModel
import com.mongokerala.taxi.newuser.utils.ViewModelProviderFactory
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Kotlin Generics Reference: https://kotlinlang.org/docs/reference/generics.html
 * Basically it means that we can pass any class that extends BaseActivity which take
 * BaseViewModel subclass as parameter
 */
@Module
class ActivityModule(private val activity: BaseActivity<*>) {

    @Provides
    fun provideLinearLayoutManager(): LinearLayoutManager = LinearLayoutManager(activity)

    @Provides
    fun provideSplashViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): SplashViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(SplashViewModel::class) {
            SplashViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
            //this lambda creates and return SplashViewModel
        }).get(SplashViewModel::class.java)

    @Provides
    fun provideDummyViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        dummyRepository: DummyRepository
    ): DummyViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(DummyViewModel::class) {
            DummyViewModel(schedulerProvider, compositeDisposable, networkHelper, dummyRepository)
        }).get(DummyViewModel::class.java)

    @Provides
    fun provideLoginViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): LoginViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(LoginViewModel::class) {
            LoginViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(LoginViewModel::class.java)

    @Provides
    fun provideProfileEditViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): ProfileEditViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(ProfileEditViewModel::class) {
            ProfileEditViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(ProfileEditViewModel::class.java)

    @Provides
    fun provideMainNewViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): MainNewViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(MainNewViewModel::class) {
            MainNewViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(MainNewViewModel::class.java)


    @Provides
    fun provideRegistrationViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): RegistrationViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(RegistrationViewModel::class) {
            RegistrationViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(RegistrationViewModel::class.java)

    @Provides
    fun provideTripHistoryViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): TripHistoryViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(TripHistoryViewModel::class) {
            TripHistoryViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(TripHistoryViewModel::class.java)

    @Provides
    fun provideReqotpViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): ReqotpViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(ReqotpViewModel::class) {
            ReqotpViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(ReqotpViewModel::class.java)

    @Provides
    fun provideFeedbackViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): FeedbackViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(FeedbackViewModel::class) {
            FeedbackViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(FeedbackViewModel::class.java)

    @Provides
    fun provideRateUsViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): RateUsViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(RateUsViewModel::class) {
            RateUsViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(RateUsViewModel::class.java)

    @Provides
    fun provideTripDetailViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): TripDetailViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(TripDetailViewModel::class) {
            TripDetailViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(TripDetailViewModel::class.java)


    @Provides
    fun provideValidateMobileViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): ValidateMobileViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(ValidateMobileViewModel::class) {
            ValidateMobileViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(ValidateMobileViewModel::class.java)

    @Provides
    fun provideSettingViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): SettingViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(SettingViewModel::class) {
            SettingViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(SettingViewModel::class.java)

    @Provides
    fun provideVehicleInfoUpdateViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): VehicleInfoUpdateViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(VehicleInfoUpdateViewModel::class) {
            VehicleInfoUpdateViewModel(schedulerProvider, compositeDisposable, networkHelper, userRepository)
        }).get(VehicleInfoUpdateViewModel::class.java)

    @Provides
    fun provideMainViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): MainViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(MainViewModel::class) {
            MainViewModel(schedulerProvider, compositeDisposable, networkHelper,userRepository)
        }).get(MainViewModel::class.java)



    @Provides
    fun provideTourViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        userRepository: UserRepository
    ): TourViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(TourViewModel::class) {
            TourViewModel(schedulerProvider, compositeDisposable, networkHelper,userRepository)
        }).get(TourViewModel::class.java)

    @Provides
    fun provideAboutViewModel(schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable, networkHelper: NetworkHelper, userRepository: UserRepository): AboutViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(AboutViewModel::class) {
            AboutViewModel(schedulerProvider, compositeDisposable, networkHelper,userRepository)
        }).get(AboutViewModel::class.java)

    @Provides
    fun provideChatViewModel(schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable, networkHelper: NetworkHelper, userRepository: UserRepository): ChatViewmodel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(ChatViewmodel::class) {
            ChatViewmodel(schedulerProvider, compositeDisposable, networkHelper,userRepository)
        }).get(ChatViewmodel::class.java)

}