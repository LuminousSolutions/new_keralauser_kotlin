package com.mongokerala.taxi.newuser.arcLayout

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import com.mongokerala.taxi.newuser.R
import java.util.*

@Suppress("CAST_NEVER_SUCCEEDS")
class ArcLayoutController(
    context: Context
) : ViewGroup(context), View.OnClickListener {
    private var fab: View? = null
    private var menuLayout: View? = null
    private var popupWindow: PopupWindow? = null
    private var arcLayout: com.ogaclejapan.arclayout.ArcLayout? = null
    private var arcLayout1: ArcLayout? =null
    private var fabTextView: TextView? = null
    private var fab1: TextView? = null
    private var mCallBackListener: OnOptionSClickListener? = null
    var animatorUtils: AnimatorUtils ?= AnimatorUtils()

    /**+
     *
     * @param mCallBackListener here we have the call back of button clicks
     */
    fun setCallBackListener(mCallBackListener: OnOptionSClickListener?) {
        this.mCallBackListener = mCallBackListener
    }

    override fun onLayout(b: Boolean, i: Int, i1: Int, i2: Int, i3: Int) {}

    /**+
     * inter face for click listener
     */
    interface OnOptionSClickListener {
        fun onIconClick(position: Int)
    }

    /**
     *
     * @param item open arc progress_bar_animator
     * @return
     */
    private fun createShowItemAnimator(item: View): Animator {
        val dx = fab!!.x - item.x
        val dy = fab!!.y - item.y
        item.rotation = 0f
        item.translationX = dx
        item.translationY = dy
        return ObjectAnimator.ofPropertyValuesHolder(
                item,
            animatorUtils?.rotation(0f, 0f),
                animatorUtils?.translationX(dx, 0f),
                animatorUtils?.translationY(dy, 0f)
        )
    }

    /**
     *
     * @param item close arc progress_bar_animator
     * @return
     */
    private fun createHideItemAnimator(item: View): Animator {
//close arc progress_bar_animator
        val dx = fab!!.x - item.x
        val dy = fab!!.y - item.y
        val anim: Animator = ObjectAnimator.ofPropertyValuesHolder(
                item,
                animatorUtils?.rotation(0f, 0f),
                animatorUtils?.translationX(0f, dx),
                animatorUtils?.translationY(0f, dy)
        )
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                item.translationX = 0f
                item.translationY = 0f
            }
        })
        return anim
    }

    /**
     *
     * @param view  popupWindow.showAtLocation from this view
     * @param names label names above the button
     * @param fontsText font icon mapping text for the button
     * @param fontIconPath font icon file path
     * @param fabLabelText fab center button label text
     * @param fabText fab font icon mapping text for the button
     * @param fontIconColour font icon text colour
     */
    fun showArcMenu(view: View?, names: Array<String>, fontsText: Array<String>, fontIconPath: String, fabLabelText: String, fabText: String, fontIconColour: String) {
        createPopupWindow(names, fontsText, fontIconPath, fabLabelText, fabText, fontIconColour)
        popupWindow!!.showAtLocation(view, Gravity.CENTER, 0, 0)
        fab!!.performClick()
    }

    /**
     *
     * @param names label names above the button
     * @param fonts font icon mapping text for the button
     * @param fontIconPath font icon file path
     * @param fabText fab center button label text
     * @param fontText fab font icon mapping text for the button
     * @param fontIconColour font icon text colour
     */
    private fun createPopupWindow(names: Array<String>, fonts: Array<String>, fontIconPath: String, fabText: String, fontText: String, fontIconColour: String) {
        val layoutInflater = LayoutInflater.from(context) //LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        val popupView: View = layoutInflater.inflate(R.layout.arcview1, null)
        popupWindow = PopupWindow(popupView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        fab = popupView.findViewById(R.id.fab)
        fabTextView = popupView.findViewById<View>(R.id.detradedsymbol) as TextView
        fabTextView!!.text = fabText
        fab1 = popupView.findViewById<View>(R.id.fab) as TextView
        fab1!!.text = fontText
        fab1!!.gravity = Gravity.CENTER
        val typeface = Typeface.createFromAsset(context.assets, fontIconPath)
        fab1!!.setTypeface(typeface)
        fab?.setOnClickListener(this)
        fab?.setPressed(true)
        fab?.invalidate()
        menuLayout = popupView.findViewById(R.id.menuLayout)
//        arcLayout = popupView.findViewById<View>(R.id.arcLayout) as ArcLayout
//        arcLayout = popupView.findViewById(R.id.arcLayout)
//        arcLayout = popupView.findViewById<com.ogaclejapan.arclayout.ArcLayout>(R.id.arcLayout) as com.ogaclejapan.arclayout.ArcLayout
        arcLayout1 = popupView.findViewById<View>(R.id.arcLayout) as ArcLayout
//        arcLayout =(ArcLayout?) popupView.findViewById<View>(R.id.arcLayout) as com.ogaclejapan.arclayout.ArcLayout
//        arcLayout1=arcLayout as? ArcLayout
        createArcMenus(arcLayout1!!, names, fonts, fontIconPath, fontIconColour)
        arcLayout!!.visibility = View.VISIBLE
        popupView.findViewById<View>(R.id.root_layout).setOnClickListener(this)
    }

    /**
     *
     * @param arcLayout  com.ogaclejapan.arclayout.ArcLayout arcLayout
     * @param names  label names above the button
     * @param fonts  font icon mapping text for the button
     * @param fontIconPath font icon file path
     * @param fontIconColour font icon text colour
     */
    private fun createArcMenus(arcLayout: ArcLayout, names: Array<String>, fonts: Array<String>, fontIconPath: String, fontIconColour: String) {

//        int angleVal = 180 / names.length;
        var angle = 0
        if (names.size == 6) {
            angle = 14
        } else if (names.size == 5) {
            angle = 30
        } else if (names.size == 4) {
            angle = 45
        } else if (names.size == 3) {
            angle = 60
        } else if (names.size == 2) {
            angle = 75
        }
        for (i in names.indices) {
            arcLayout.addView(createArcMenu(i, names[i], fonts[i], fontIconPath, angle, fontIconColour))
            angle = angle + 30
        }
    }

    /**
     *
     * @param id id for the text view
     * @param name label names above the button
     * @param icon font icon mapping text for the button
     * @param iconPath  font icon file path
     * @param angleVal  for the arc view we need to put angles
     * @param fontIconColour font icon text colour
     * @return
     */
    private fun createArcMenu(id: Int, name: String, icon: String, iconPath: String, angleVal: Int, fontIconColour: String): LinearLayout {
        val parent = LinearLayout(context)
        val params1: ArcLayout.LayoutParams = ArcLayout.LayoutParams(ViewGroup.MarginLayoutParams(10,20))
        params1.angle = angleVal.toFloat()
        parent.orientation = LinearLayout.VERTICAL
        parent.layoutParams = params1
        val nameTxt = TextView(context)
        val name_Text_Parameter = LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        nameTxt.layoutParams = name_Text_Parameter
        nameTxt.id = id
        nameTxt.gravity = Gravity.CENTER
        nameTxt.setBackgroundResource(R.drawable.rounded_edges)
        nameTxt.setPadding(Math.round(resources.getDimension(R.dimen._8ssp)), Math.round(resources.getDimension(R.dimen._4ssp)), Math.round(resources.getDimension(R.dimen._8ssp)), Math.round(resources.getDimension(R.dimen._4ssp)))
        nameTxt.text = name
        nameTxt.setTextColor(Color.WHITE)
        nameTxt.textSize = 8f
        val iconTxt = TextView(context)
        iconTxt.id = id
//        iconTxt.setTextAppearance(context, R.style.Pat)
        val params = LinearLayout.LayoutParams(Math.round(resources.getDimension(R.dimen._35ssp)), Math.round(resources.getDimension(R.dimen._35ssp)))
        params.setMargins(Math.round(resources.getDimension(R.dimen._4ssp)), Math.round(resources.getDimension(R.dimen._4ssp)), Math.round(resources.getDimension(R.dimen._4ssp)), 0)
        params.weight = 1f
        params.gravity = Gravity.CENTER
        iconTxt.layoutParams = params
        iconTxt.gravity = Gravity.CENTER
        iconTxt.setBackgroundResource(R.drawable.circle_arcmenu_btn)
        iconTxt.text = icon
        iconTxt.setTextColor(Color.parseColor(fontIconColour))
        iconTxt.textSize = Math.round(resources.getDimension(R.dimen._10ssp)).toFloat()
        iconTxt.setOnClickListener(this)
        val typeface = Typeface.createFromAsset(context.assets, iconPath)
        iconTxt.setTypeface(typeface)
        parent.addView(nameTxt)
        parent.addView(iconTxt)
        return parent
    }

    /**
     *
     * @param view fab on click
     */
    override fun onClick(view: View) {
        if (R.id.fab === view.id) {
            onFabClick(view)
        } else if (R.id.root_layout === view.id) {
            fab!!.performClick()
        } else {
            mCallBackListener!!.onIconClick(view.id)
            fab!!.performClick()
        }
    }

    /**
     *
     * @param v fab the x button oc click
     */
    private fun onFabClick(v: View) {
        val mHandler = Handler()
        val mUpdateTimeTask = Runnable {
            if (v.isSelected) {
                exitMenu()
            } else {
                showMenu()
            }
            v.isSelected = !v.isSelected
        }
        mHandler.postDelayed(mUpdateTimeTask, 500)
    }

    /**
     * To open the arc menu
     */
    private fun showMenu() {
        menuLayout!!.visibility = View.VISIBLE
        val animList: MutableList<Animator> = ArrayList()
        var i = 0
        val len = arcLayout!!.childCount
        while (i < len) {
            animList.add(createShowItemAnimator(arcLayout!!.getChildAt(i)))
            i++
        }
        val animSet = AnimatorSet()
        animSet.duration = 700
        animSet.interpolator = OvershootInterpolator()
        animSet.playTogether(animList)
        animSet.start()
        val mHandler = Handler()
        val mUpdateTimeTask = Runnable { fabTextView!!.visibility = View.VISIBLE }
        mHandler.postDelayed(mUpdateTimeTask, 400)
    }

    /**
     * To exit the arc layout pop up
     */
    private fun exitMenu() {
        val animList: MutableList<Animator> = ArrayList()
        for (i in arcLayout!!.childCount - 1 downTo 0) {
            animList.add(createHideItemAnimator(arcLayout!!.getChildAt(i)))
        }
        val animSet = AnimatorSet()
        animSet.duration = 700
        animSet.interpolator = AnticipateInterpolator()
        animSet.playTogether(animList)
        animSet.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                menuLayout!!.visibility = View.INVISIBLE
            }
        })
        animSet.start()
        val mHandler = Handler()
        val mUpdateTimeTask = Runnable { popupWindow!!.dismiss() }
        mHandler.postDelayed(mUpdateTimeTask, 650)
    }

}