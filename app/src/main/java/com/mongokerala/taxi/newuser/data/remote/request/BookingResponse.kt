package com.mongokerala.taxi.newuser.data.remote.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BookingResponse(

	@field:SerializedName("infoId")
	val infoId: Int? = null,

	@field:SerializedName("data")
	val data: String? = null,

	@field:SerializedName("jwt")
	val jwt: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
) : Parcelable
