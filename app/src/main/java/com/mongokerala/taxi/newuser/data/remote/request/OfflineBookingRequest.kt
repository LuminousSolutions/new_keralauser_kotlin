package com.mongokerala.taxi.newuser.data.remote.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class OfflineBookingRequest(
    @field:SerializedName(Keys.name) @field:Expose var name: String?,
    @field:SerializedName("phoneNumber") @field:Expose var phoneNumber: String?,
    @field:SerializedName(Keys.source) @field:Expose var source: String?,
    @field:SerializedName(Keys.destination) @field:Expose var destination: String?,
    @field:SerializedName(Keys.driverId) @field:Expose var driverId: String?,
    @field:SerializedName("emailId") @field:Expose var emailId: String?
) {

    @SerializedName("id")
    @Expose
    private var id: Int? = null

    @SerializedName("locationUrl")
    @Expose
    private var locationUrl: String? = null

   init {
       id = 0
       locationUrl = "string"
   }


}