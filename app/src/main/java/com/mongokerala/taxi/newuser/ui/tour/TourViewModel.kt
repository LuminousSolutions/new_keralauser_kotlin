package com.mongokerala.taxi.newuser.ui.tour

import androidx.lifecycle.MutableLiveData
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.Event
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable


class TourViewModel(
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    // Event is used by the view model to tell the activity to launch another Activity
    // view model also provided the Bundle in the event that is needed for the Activity
    val launchMain: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchLogin: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchReqotp: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()



    override fun onCreate() {

    }


    fun ToLogin() {
        launchMain.postValue(Event(kotlin.collections.emptyMap()))
    }

    fun ToRegistration(){
        launchReqotp.postValue(Event(kotlin.collections.emptyMap()))
    }
}