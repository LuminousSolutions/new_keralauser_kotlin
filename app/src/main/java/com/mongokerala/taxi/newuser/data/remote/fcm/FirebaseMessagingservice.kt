package com.mongokerala.taxi.newuser.data.remote.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.provider.ContactsContract
import android.util.Log
import androidx.annotation.NonNull
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.data.remote.request.Keys
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.getIsMainActivityAlive
import com.mongokerala.taxi.newuser.ui.main_new.MainNewActivity.Companion.getIsMainNewActivity
import com.mongokerala.taxi.newuser.ui.splash.SplashActivity
import com.mongokerala.taxi.newuser.utils.common.Constants.ACCEPT_ORDER
import com.mongokerala.taxi.newuser.utils.common.Constants.CUSTOM_NOTIFICATION_DATA
import com.mongokerala.taxi.newuser.utils.common.Constants.DRIVER_CANCEL_RIDE
import com.mongokerala.taxi.newuser.utils.common.Constants.LOCAL_START_PICKUP
import com.mongokerala.taxi.newuser.utils.common.Constants.LOCAL_START_RIDE
import com.mongokerala.taxi.newuser.utils.common.Constants.PICKUP_DONE
import com.mongokerala.taxi.newuser.utils.common.Constants.RIDE_FINISH
import timber.log.Timber
import java.lang.Boolean


class FirebaseMessagingservice : FirebaseMessagingService() {
    private val TAG = "MyFirebaseMsgService"

    var mainNewActivity:MainNewActivity? =null
    override fun onNewToken(token: String) {
        super.onNewToken(token)

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(@NonNull remoteMessage: RemoteMessage) {
        //Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        //Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.i(FirebaseMessagingservice.TAG, "On receive msg")
        Log.d(FirebaseMessagingservice.TAG, "From: " + remoteMessage.from)
        // Check if message contains a data payload.
        // Check if message contains a data payload.
        if (remoteMessage.data.size > 0) {
            /*Auth Thiyagesh
             * DES: if the recived notification time is differ moren then 5 min we reject the notification*/
            val currentTimestamp = System.currentTimeMillis()
            val searchTimestamp = remoteMessage.sentTime // this also gives me back timestamp in 13 digit (1425506040493)
            val difference = Math.abs(currentTimestamp - searchTimestamp)
            println(difference)
            /*Auth : thiyagesh
             * Des :check time diff*/
            if (difference <= 2 * 60 * 1000) {
                Timber.d("NotificationTitle data payload: %s", remoteMessage.data)
                Log.d(FirebaseMessagingservice.TAG, "remoteMessage.getData().get(Keys.action) : " + remoteMessage.data[Keys.action])
                if (remoteMessage.notification != null && remoteMessage.data.containsValue("OFFER")) {
                    sendNotification(remoteMessage)
                } else {
                    handleNow(remoteMessage)
                }
            }
        }
        // Check if message contains a notification payload.
        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.d(FirebaseMessagingservice.TAG, "NotificationTitle Notification Body: " + remoteMessage.notification!!.body)
        }

    }

    private fun handleNow(remoteMessage: RemoteMessage) {
        val action = remoteMessage.data[Keys.action]
        Log.e("Action is : ", remoteMessage.toString())
        mainNewActivity=getIsMainNewActivity()
        Log.e("Action is : ", remoteMessage.data.toString())
        Log.e(TAG, "handleNow: checking   :"+action)
        when (action) {
            ACCEPT_ORDER -> {
                val data = FcmCustomizeNotificationData()
                data.action = remoteMessage.data[Keys.action]
                data.source = remoteMessage.data[Keys.source]
                data.destination = remoteMessage.data[Keys.destination]
                data.taxiNumber = remoteMessage.data[Keys.taxiNumber]
                data.paymentType = (remoteMessage.data[Keys.paymentType])
                data.vehicleBrand = (remoteMessage.data[Keys.vehicleBrand])
                data.RIDE_KEY_LOCAL=(remoteMessage.data[Keys.RIDE_KEY_LOCAL])
                if (remoteMessage.data[Keys.driverId] != null) {
                    data.userId = (remoteMessage.data[Keys.driverId]!!)
                }
                if (remoteMessage.data[Keys.driverFcmToken] != null) {
                    data.driverFcmToken  = (remoteMessage.data[Keys.driverFcmToken])
                }
                data.isAndroid = (Boolean.parseBoolean(remoteMessage.data[Keys.isAndroid]))
                if (remoteMessage.data[Keys.latitude] != null && remoteMessage.data[Keys.longitude] != null) {
                    data.latitude = (remoteMessage.data[Keys.latitude]!!.toDouble())
                    data.longitude = (remoteMessage.data[Keys.longitude]!!.toDouble())
                }
                data.phoneNumber = (remoteMessage.data[Keys.phoneNumber])
                data.driverName=(remoteMessage.data[Keys.driverName])
                Log.d(TAG, "handleNow: getIsMainActivityAlive   :"+getIsMainActivityAlive())
               /* if (getIsMainActivityAlive()) {
                    val intent = Intent(applicationContext, MainNewActivity::class.java)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    Log.d(TAG, "handleNow:       :"+intent.getSerializableExtra(CUSTOM_NOTIFICATION_DATA))
                    Log.d(TAG, "handleNow:       :"+(intent.extras != null))
                    startActivity(intent)
                } else {
                    val intent = Intent(MainNewActivity.BROADCAST_TAG)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    /*LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
                    localBroadcastManager.sendBroadcast(intent);*/
                }*/
                mainNewActivity?.driverAcceptStatus(true)
                if (getIsMainActivityAlive()) {
                    val intent = Intent(MainNewActivity.BROADCAST_TAG)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    val localBroadcastManager =
                            LocalBroadcastManager.getInstance(this)
                    localBroadcastManager.sendBroadcast(intent)
                } else {
                    mainNewActivity?.backgroundBooking(data)
                    notify_alert(data.action, data.cancelReason, data)
                    /*val intent = Intent(applicationContext, MainNewActivity::class.java)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)*/
                }
            }
            LOCAL_START_PICKUP -> {
                val data = FcmCustomizeNotificationData()
                data.action = (remoteMessage.data[Keys.action])
                data.cancelReason = (remoteMessage.data[Keys.cancelReason]!!)
                data.paymentType = (remoteMessage.data[Keys.paymentType])
                data.isAndroid = (Boolean.parseBoolean(remoteMessage.data[Keys.isAndroid]))
                if (remoteMessage.data[Keys.latitude] != null && remoteMessage.data[Keys.longitude] != null) {
                    data.latitude = (remoteMessage.data[Keys.latitude]!!.toDouble())
                    data.longitude = (remoteMessage.data[Keys.longitude]!!.toDouble())
                }
                if (getIsMainActivityAlive()) {
                    val intent = Intent(MainNewActivity.BROADCAST_TAG)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    val localBroadcastManager = LocalBroadcastManager.getInstance(this)
                    localBroadcastManager.sendBroadcast(intent)
                } else {
                    /* Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);*/
                    notify_alert(data.action, data.cancelReason, data)
                }
            }
            PICKUP_DONE -> {
                if (!Boolean.parseBoolean(remoteMessage.data[Keys.isAndroid])) return
                val data = FcmCustomizeNotificationData()
                data.rideAmount = (remoteMessage.data[Keys.rideAmount])
                data.paymentType = (remoteMessage.data[Keys.paymentType])
                data.userFcmToken = (remoteMessage.data[Keys.userFcmToken])
                data.rideDistance = (remoteMessage.data[Keys.rideDistance])
                data.cancelReason = (remoteMessage.data[Keys.cancelReason]!!)
                if (remoteMessage.data[Keys.taxiDetailId] != null) {
                    data.taxiDetailId = (remoteMessage.data[Keys.taxiDetailId]!!)
                }
                data.driverId = (remoteMessage.data[Keys.driverId]!!)
                data.rideId = (remoteMessage.data[Keys.rideId]!!)
                data.action = (remoteMessage.data[Keys.action])
                data.source = (remoteMessage.data[Keys.source])
                data.destination = (remoteMessage.data[Keys.destination])
                data.isAndroid = (Boolean.parseBoolean(remoteMessage.data[Keys.isAndroid]))
                if (remoteMessage.data[Keys.latitude] != null && remoteMessage.data[Keys.longitude] != null) {
                    data.latitude = (remoteMessage.data[Keys.latitude]!!.toDouble())
                    data.longitude = (remoteMessage.data[Keys.longitude]!!.toDouble())
                }
                data.phoneNumber = (remoteMessage.data[Keys.phoneNumber])
                if (getIsMainActivityAlive()) {
                    val intent = Intent(MainNewActivity.BROADCAST_TAG)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    val localBroadcastManager = LocalBroadcastManager.getInstance(this)
                    localBroadcastManager.sendBroadcast(intent)
                } else {
                    /* Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);*/
                    notify_alert(data.action, data.cancelReason, data)
                }
            }
            LOCAL_START_RIDE -> {
                val data = FcmCustomizeNotificationData()
                data.cancelReason = (remoteMessage.data[Keys.cancelReason]!!)
                data.action = (remoteMessage.data[Keys.action])
                data.paymentType = (remoteMessage.data[Keys.paymentType])
                data.isAndroid = (Boolean.parseBoolean(remoteMessage.data[Keys.isAndroid]))
                if (remoteMessage.data[Keys.latitude] != null && remoteMessage.data[Keys.longitude] != null) {
                    data.latitude = (remoteMessage.data[Keys.latitude]!!.toDouble())
                    data.longitude = (remoteMessage.data[Keys.longitude]!!.toDouble())
                }
                if (getIsMainActivityAlive()) {
                    val intent = Intent(MainNewActivity.BROADCAST_TAG)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    val localBroadcastManager = LocalBroadcastManager.getInstance(this)
                    localBroadcastManager.sendBroadcast(intent)
                } else {
                    /* Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);*/
                    notify_alert(data.action, data.cancelReason, data)
                }
            }
            RIDE_FINISH -> {
                val data = FcmCustomizeNotificationData()
                data.rideAmount = (remoteMessage.data[Keys.rideAmount])
                data.userFcmToken = (remoteMessage.data[Keys.userFcmToken])
                data.paymentType = (remoteMessage.data[Keys.paymentType])
                data.rideDistance = (remoteMessage.data[Keys.rideDistance])
                data.taxiDetailId = (remoteMessage.data[Keys.taxiDetailId]!!)
                data.driverId = (remoteMessage.data[Keys.driverId]!!)
                data.rideId = (remoteMessage.data[Keys.rideId]!!)
                data.action = (remoteMessage.data[Keys.action])
                data.source = (remoteMessage.data[Keys.source])
                data.destination = (remoteMessage.data[Keys.destination])
                data.isAndroid = (Boolean.parseBoolean(remoteMessage.data[Keys.isAndroid]))
                if (remoteMessage.data[Keys.latitude] != null && remoteMessage.data[Keys.longitude] != null) {
                    data.latitude = (remoteMessage.data[Keys.latitude]!!.toDouble())
                    data.longitude = (remoteMessage.data[Keys.longitude]!!.toDouble())
                }
                data.phoneNumber = (remoteMessage.data[Keys.phoneNumber])
                if (getIsMainActivityAlive()) {
                    val intent = Intent(MainNewActivity.BROADCAST_TAG)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    val localBroadcastManager = LocalBroadcastManager.getInstance(this)
                    localBroadcastManager.sendBroadcast(intent)
                } else {
                    /*Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);*/
                    notify_alert(data.action, data.cancelReason, data)
                }
            }
            DRIVER_CANCEL_RIDE -> {
                val data = FcmCustomizeNotificationData()
                data.cancelReason = (remoteMessage.data[Keys.cancelReason]!!)
                data.message = (remoteMessage.data[Keys.cancelReason])
                data.paymentType = (remoteMessage.data[Keys.paymentType])
                data.action = (remoteMessage.data[Keys.action])

                /*Auth :Thiyagesh
                 * Des : status of isShowBookedTaxiActivityAlive added */
                //com.taxi.deals.user.chat.util.Settings.get().setBookedStatus(false);
                if (getIsMainActivityAlive()) {
                    val intent = Intent(MainNewActivity.BROADCAST_TAG)
                    intent.putExtra(ContactsContract.Intents.Insert.ACTION, action)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    val localBroadcastManager = LocalBroadcastManager.getInstance(this)
                    localBroadcastManager.sendBroadcast(intent)
                } else {
                    //Settings.get().setRideFlag(action);
                    data.action = (remoteMessage.data[Keys.action])
                    data.cancelReason = (remoteMessage.data[Keys.cancelReason]!!)
                    /* Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(ACTION, action);
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data);
                    intent.putExtra("isFromNotification", true);
                    startActivity(intent);*/notify_alert(data.action, data.cancelReason, data)

                    //sendNotification(remoteMessage, intent);
                }
            }
            "CANCEL_BY_USER" -> {
                val data = FcmCustomizeNotificationData()
                data.rideAmount = (remoteMessage.data[Keys.rideAmount])
                data.userFcmToken = (remoteMessage.data[Keys.userFcmToken])
                data.rideDistance = (remoteMessage.data[Keys.rideDistance])
                data.paymentType = (remoteMessage.data[Keys.paymentType])
                data.taxiDetailId = (remoteMessage.data[Keys.taxiDetailId]!!)
                data.driverId = (remoteMessage.data[Keys.driverId]!!)
                data.rideId = (remoteMessage.data[Keys.rideId]!!)
                data.action = (remoteMessage.data[Keys.action])
                data.source = (remoteMessage.data[Keys.source])
                data.destination = (remoteMessage.data[Keys.destination])
                data.isAndroid = (Boolean.parseBoolean(remoteMessage.data[Keys.isAndroid]))
                if (remoteMessage.data[Keys.latitude] != null && remoteMessage.data[Keys.longitude] != null) {
                    data.latitude = (remoteMessage.data[Keys.latitude]!!.toDouble())
                    data.longitude = (remoteMessage.data[Keys.longitude]!!.toDouble())
                }
                data.phoneNumber = (remoteMessage.data[Keys.phoneNumber])
                if (getIsMainActivityAlive()) {
                    val intent = Intent(MainNewActivity.BROADCAST_TAG)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    val localBroadcastManager = LocalBroadcastManager.getInstance(this)
                    localBroadcastManager.sendBroadcast(intent)
                } else {
                    val intent = Intent(applicationContext, MainNewActivity::class.java)
                    intent.putExtra(CUSTOM_NOTIFICATION_DATA, data)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            }
        }
    }

    fun notify_alert(title: String?, txt: String?, notificationData: FcmCustomizeNotificationData?) {
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(this, SplashActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(ContactsContract.Intents.Insert.ACTION, title)
        intent.putExtra(CUSTOM_NOTIFICATION_DATA, notificationData)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        val icon = BitmapFactory.decodeResource(applicationContext.resources,
                R.mipmap.car)
        val builder = NotificationCompat.Builder(this, createNotificationChannel(this)!!)
                .setSmallIcon(R.drawable.icon_user)
                .setLargeIcon(icon)
                .setContentTitle(title)
                .setContentText(txt)
                .setShowWhen(true)
                .setPriority(NotificationCompat.PRIORITY_MAX) // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
        notificationManager.notify(0, builder.build())
    }


    private fun sendNotification(remoteMessage: RemoteMessage) {
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(this, SplashActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        val icon = BitmapFactory.decodeResource(applicationContext.resources,
                R.mipmap.car)
        Log.d(FirebaseMessagingservice.TAG, "sendNotification: " + remoteMessage.notification!!.body)
        Log.d(FirebaseMessagingservice.TAG, "sendNotification: " + remoteMessage.notification!!.title)
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, createNotificationChannel(this)!!)
                .setSmallIcon(R.drawable.icon_user)
                .setLargeIcon(icon)
                .setContentTitle(remoteMessage.notification!!.title)
                .setContentText(remoteMessage.notification!!.body)
                .setShowWhen(true)
                .setPriority(NotificationCompat.PRIORITY_MAX) // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
        notificationManager.notify(0, builder.build())
    }

    fun createNotificationChannel(context: Context): String? {

        // NotificationChannels are required for Notifications on O (API 26) and above.
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // The id of the channel.
            val channelId = "Channel_id"

            // The user-visible name of the channel.
            val channelName: CharSequence = "Taxi Deals User"
            // The user-visible description of the channel.
            val channelDescription = "Taxi Deals User Alert"
            val channelImportance = NotificationManager.IMPORTANCE_HIGH
            val channelEnableVibrate = true
            //            int channelLockscreenVisibility = Notification.;

            // Initializes NotificationChannel.
            val notificationChannel = NotificationChannel(channelId, channelName, channelImportance)
            notificationChannel.description = channelDescription
            notificationChannel.enableVibration(channelEnableVibrate)
            notificationChannel.canShowBadge()
            notificationChannel.lockscreenVisibility = 1

            //            notificationChannel.setLockscreenVisibility(channelLockscreenVisibility);

            // Adds NotificationChannel to system. Attempting to create an existing notification
            // channel with its original values performs no operation, so it's safe to perform the
            // below sequence.
            val notificationManager = (context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
            notificationManager.createNotificationChannel(notificationChannel)
            channelId
        } else {
            // Returns null for pre-O (26) devices.
            null
        }
    }


    private fun sendRegistrationToServer(token: String) {
        // TODO: Implement this method to send token to your app server.
    }


    companion object {
        private const val TAG = "MyFirebaseMsgService"
        private const val DATE_TIME_FORMAT = "dd-MM-yyyy' , 'hh:mm aaa"
    }
}




