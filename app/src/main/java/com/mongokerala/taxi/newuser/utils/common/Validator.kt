package com.mongokerala.taxi.newuser.utils.common

import android.util.Log
import com.mongokerala.taxi.newuser.R
import java.util.regex.Pattern

object Validator {

    public val EMAIL_ADDRESS = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )

    val MOBILENUMBER = Pattern.compile("^[+]?[0-9]{10,13}\$")

    private val LICENCE_NUMBER_VALUE = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")

    private val NAME_VALUE = Pattern.compile("[a-zA-Z ]+")

    private val VEHICLE_NUMBER_VALUE = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")

    public const val MIN_PASSWORD_LENGTH = 6

    fun validateRegisFields(name: String?,email: String?,company: String?, password: String?, license_number: String?, phoneNumber: String?): List<Validation> =
        ArrayList<Validation>().apply {
            when {
                name.isNullOrBlank() ->
                    add(Validation(Validation.Field.NAME, Resource.error(R.string.Enter_your_name)))
                !NAME_VALUE.matcher(name).matches() ->
                    add(Validation(Validation.Field.NAME, Resource.error(R.string.Enter_your_name)))
                else ->
                    add(Validation(Validation.Field.NAME, Resource.success()))
            }
            when {
                email.isNullOrBlank() ->
                    add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))
                !EMAIL_ADDRESS.matcher(email).matches() ->
                    add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))
                !MOBILENUMBER.matcher(phoneNumber).matches()->
                    add(Validation(Validation.Field.MOBILENUMBER, Resource.error(R.string.Email_id_hint)))

                else ->
                    add(Validation(Validation.Field.EMAIL, Resource.success()))
            }
           /* when {
                company.isNullOrBlank() ->
                    add(Validation(Validation.Field.COMAPANY, Resource.error(R.string.company_validation)))
                else ->
                    add(Validation(Validation.Field.COMAPANY, Resource.success()))
            }*/
            when {
                password.isNullOrBlank() ->
                    add(Validation(Validation.Field.PASSWORD, Resource.error(R.string.empty_password)))
                password.length < MIN_PASSWORD_LENGTH ->
                    add(Validation(Validation.Field.PASSWORD, Resource.error(R.string.Please_enter_the_password_greater_than_4)))
                else -> add(Validation(Validation.Field.PASSWORD, Resource.success()))
            }
            when {
                license_number.isNullOrBlank() ->
                    add(Validation(Validation.Field.LICENCE_NUMBER, Resource.error(R.string.Licence_number_hint)))
                !LICENCE_NUMBER_VALUE.matcher(license_number).matches() ->
                    add(Validation(Validation.Field.LICENCE_NUMBER, Resource.error(R.string.valid_licence_number)))
                else ->
                    add(Validation(Validation.Field.LICENCE_NUMBER, Resource.success()))
            }
        }

    fun validateVehicleRegisFields(vehicle_year: String?,vehicle_brand: String?,vehicle_number: String?, vehicle_seats: String?, base_price: String?, price_per_km: String?, night_price: String?): List<Validation> =
        ArrayList<Validation>().apply {
            when {
                vehicle_year.isNullOrBlank() ->
                    add(Validation(Validation.Field.VEHICLE_YEAR, Resource.error(R.string.Vehicle_year_hint)))
                else ->
                    add(Validation(Validation.Field.VEHICLE_YEAR, Resource.success()))
            }
            when {
                vehicle_brand.isNullOrBlank() ->
                    add(Validation(Validation.Field.VEHICLE_BRAND, Resource.error(R.string.Vehicle_brand_hint)))
                else ->
                    add(Validation(Validation.Field.VEHICLE_BRAND, Resource.success()))
            }
            when {
                vehicle_number.isNullOrBlank() ->
                    add(Validation(Validation.Field.VEHICLE_NUMBER, Resource.error(R.string.Number_plate_hint)))
                !VEHICLE_NUMBER_VALUE.matcher(vehicle_number).matches() ->
                    add(Validation(Validation.Field.VEHICLE_NUMBER, Resource.error(R.string.Number_plate_hint)))
                else ->
                    add(Validation(Validation.Field.VEHICLE_NUMBER, Resource.success()))
            }
            when {
                vehicle_seats.isNullOrBlank() ->
                    add(Validation(Validation.Field.VEHICLE_SEATS, Resource.error(R.string.Seats_hint)))
                else -> add(Validation(Validation.Field.VEHICLE_SEATS, Resource.success()))
            }
            when {
                base_price.isNullOrBlank() ->
                    add(Validation(Validation.Field.BASE_PRICE, Resource.error(R.string.Base_price_hint)))
                else ->
                    add(Validation(Validation.Field.BASE_PRICE, Resource.success()))
            }
            when {
                night_price.isNullOrBlank() ->
                    add(Validation(Validation.Field.PEAK_PRICE, Resource.error(R.string.Night_price_hint)))
                else ->
                    add(Validation(Validation.Field.PEAK_PRICE, Resource.success()))
            }
            when {
                price_per_km.isNullOrBlank() ->
                    add(Validation(Validation.Field.PRICE, Resource.error(R.string.Price_pe_km_hint)))
                else ->
                    add(Validation(Validation.Field.PRICE, Resource.success()))
            }
        }

    fun validateLoginFields(email: String?, password: String?): List<Validation> =
        ArrayList<Validation>().apply {
            Log.d("TAG", "validateLoginFieldsemail: "+email)
            Log.d("TAG", "validateLoginFieldspassword: "+password)
            when {
                email.isNullOrBlank() ->
                    add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))
                !EMAIL_ADDRESS.matcher(email).matches() ->
                    add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))
                else ->
                    add(Validation(Validation.Field.EMAIL, Resource.success()))
            }

            when {
                email.isNullOrBlank() ->
                    add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))
                !MOBILENUMBER.matcher(email).matches() ->
                    add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))

                else ->
                    add(Validation(Validation.Field.EMAIL, Resource.success()))
            }
            when {
                password.isNullOrBlank() ->
                    add(Validation(Validation.Field.PASSWORD, Resource.error(R.string.password_hint)))
                password.length < MIN_PASSWORD_LENGTH ->
                    add(Validation(Validation.Field.PASSWORD, Resource.error(R.string.password_hint)))
                else -> add(Validation(Validation.Field.PASSWORD, Resource.success()))
            }
        }

    fun signUpValidation(name: String?,email: String?, phoneNumber: String?,password: String?): List<Validation> =
        ArrayList<Validation>().apply {
            when {
                name.isNullOrBlank() ->
                    add(Validation(Validation.Field.NAME, Resource.error(R.string.Enter_your_name)))
                !NAME_VALUE.matcher(name).matches() ->
                    add(Validation(Validation.Field.NAME, Resource.error(R.string.Enter_your_name)))
                else ->
                    add(Validation(Validation.Field.NAME, Resource.success()))
            }
            when {
                email.isNullOrBlank() ->
                    add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))
                !EMAIL_ADDRESS.matcher(email).matches() ->
                    add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))
                else ->
                    add(Validation(Validation.Field.EMAIL, Resource.success()))
            }
            when {
                phoneNumber.isNullOrBlank() ->
                    add(Validation(Validation.Field.MOBILENUMBER, Resource.error(R.string.phonenumber)))
                !MOBILENUMBER.matcher(email).matches() ->
                    add(Validation(Validation.Field.EMAIL, Resource.error(R.string.phonenumber)))
                else -> add(Validation(Validation.Field.PASSWORD, Resource.success()))
            }
            when {
                password.isNullOrBlank() ->
                    add(Validation(Validation.Field.PASSWORD, Resource.error(R.string.password_hint)))
                password.length < MIN_PASSWORD_LENGTH ->
                    add(Validation(Validation.Field.PASSWORD, Resource.error(R.string.password_hint)))
                else -> add(Validation(Validation.Field.PASSWORD, Resource.success()))
            }
        }
}

data class Validation(val field: Field, val resource: Resource<Int>) {

    enum class Field {
        NAME,
        EMAIL,
        MOBILENUMBER,
        COMAPANY,
        PASSWORD,
        LICENCE_NUMBER,
        VEHICLE_YEAR,
        VEHICLE_BRAND,
        VEHICLE_NUMBER,
        VEHICLE_SEATS,
        BASE_PRICE,
        PEAK_PRICE,
        PRICE
    }
}
