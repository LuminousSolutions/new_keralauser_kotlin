package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

class UserWalletTokenData (

    @SerializedName("id") val id : String,
    @SerializedName("percentage") val percentage : Int,
    @SerializedName("userId") val userId : String,
    @SerializedName("credit") val credit : Int,
    @SerializedName("debit") val debit : Int,
    @SerializedName("balance") val balance : Int,
    @SerializedName("currentDate") val currentDate : String,
    @SerializedName("token") val token : String,
    @SerializedName("domain") val domain : String
)
