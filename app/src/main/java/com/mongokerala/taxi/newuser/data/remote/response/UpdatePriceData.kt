package com.mongokerala.taxi.newuser.data.remote.response

import com.google.gson.annotations.SerializedName

data class UpdatePriceData (

    @SerializedName("km") var km : Double,
    @SerializedName("totalPrice") var totalPrice : Double,
    @SerializedName("userTotalPrice") var userTotalPrice : Double,
    @SerializedName("totalTime") var totalTime : Int,
    @SerializedName("basePrice") var basePrice : Int,
    @SerializedName("totalTravelTimePrice ") var totalTravelTimePrice  : Double
)
