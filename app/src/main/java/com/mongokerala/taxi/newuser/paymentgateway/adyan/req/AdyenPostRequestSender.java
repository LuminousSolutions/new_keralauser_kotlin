package com.mongokerala.taxi.newuser.paymentgateway.adyan.req;

import com.mongokerala.taxi.newuser.paymentgateway.adyan.OrderDTO;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Akhi007 on 25-05-2019.
 */
public interface AdyenPostRequestSender {

    //String BASE_URL = "http://63.142.252.78:8080/paymentgateway-api";
    //String BASE_URL = "http://10.0.2.2:8080";

    String BASE_URL = "https://1-dot-taxi2dealin.appspot.com";

    @POST("/api/orders/v1/adyen-payment-session/create")
    Call<AdyenPaymentSessionResponse> createPaymentSession(@Body OrderDTO orderDTO);

    @POST("/api/orders/v1/adyen-payment/verify")
    Call<PaymentVerifyResponse> verifyPayment(@Body OrderDTO orderDTO);

}
