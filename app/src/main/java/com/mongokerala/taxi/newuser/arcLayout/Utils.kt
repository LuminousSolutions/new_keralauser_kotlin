package com.mongokerala.taxi.newuser.arcLayout

import android.os.Build
import android.util.Log
import android.view.View

internal object Utils {
    const val DEBUG = false //Set to true only when developing
    val JELLY_BEAN_MR1_OR_LATER = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1
    val LOLLIPOP_OR_LATER = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
    fun d(tag: String?, format: String, vararg args: Any?) {
        Log.d(tag, String.format(format, *args))
    }

    fun computeMeasureSize(measureSpec: Int, defSize: Int): Int {
        val mode = View.MeasureSpec.getMode(measureSpec)
        return when (mode) {
            View.MeasureSpec.EXACTLY -> View.MeasureSpec.getSize(measureSpec)
            View.MeasureSpec.AT_MOST -> Math.min(defSize, View.MeasureSpec.getSize(measureSpec))
            else -> defSize
        }
    }

    fun computeCircleX(r: Float, degrees: Float): Float {
        return (r * Math.cos(Math.toRadians(degrees.toDouble()))).toFloat()
    }

    fun computeCircleY(r: Float, degrees: Float): Float {
        return (r * Math.sin(Math.toRadians(degrees.toDouble()))).toFloat()
    }

    fun computeWidth(origin: Int, size: Int, x: Int): Int {
        return when (origin and ArcOrigin.HORIZONTAL_MASK) {
            ArcOrigin.LEFT ->                 //To the right edge
                size - x
            ArcOrigin.RIGHT ->                 //To the left edge
                x
            else ->                 //To the shorter * 2 than the right edge and left edge
                Math.min(x, size - x) * 2
        }
    }

    fun computeHeight(origin: Int, size: Int, y: Int): Int {
        return when (origin and ArcOrigin.VERTICAL_MASK) {
            ArcOrigin.TOP ->                 //To the bottom edge
                size - y
            ArcOrigin.BOTTOM ->                 //To the top edge
                y
            else ->                 //To the shorter * 2 than the top edge and bottom edge
                Math.min(y, size - y) * 2
        }
    }
}
