package com.mongokerala.taxi.newuser.ui.login

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.facebook.AccessToken
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.data.remote.request.Keys
import com.mongokerala.taxi.newuser.data.remote.request.Keys.userId
import com.mongokerala.taxi.newuser.data.repository.UserRepository
import com.mongokerala.taxi.newuser.ui.base.BaseViewModel
import com.mongokerala.taxi.newuser.utils.common.*
import com.mongokerala.taxi.newuser.utils.common.AppErrorCode.EMAIL_NOT_EXISTING_LOGIN
import com.mongokerala.taxi.newuser.utils.common.AppErrorCode.EMAIL_OR_PASSWORD_NOT_EXISTING
import com.mongokerala.taxi.newuser.utils.common.AppErrorCode.PASSWORD_WRONG2_INFOID
import com.mongokerala.taxi.newuser.utils.common.AppErrorCode.PASSWORD_WRONG_INFOID
import com.mongokerala.taxi.newuser.utils.common.AppErrorCode.STATUS_SUCCESS_INFOID
import com.mongokerala.taxi.newuser.utils.common.AppErrorCode.USER_WRONG
import com.mongokerala.taxi.newuser.utils.network.NetworkHelper
import com.mongokerala.taxi.newuser.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import java.util.*

class LoginViewModel(
        schedulerProvider: SchedulerProvider,
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        private val userRepository: UserRepository
) : BaseViewModel(schedulerProvider, compositeDisposable, networkHelper) {

    private val validationsList: MutableLiveData<List<Validation>> = MutableLiveData()

    val launchMain: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val already_login: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val launchRegistration: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val emailField: MutableLiveData<String> = MutableLiveData()
    val phonenumberField: MutableLiveData<String> = MutableLiveData()
    val passwordField: MutableLiveData<String> = MutableLiveData()
    val loggingIn: MutableLiveData<Boolean> = MutableLiveData()
    val noOneSignalValue: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val Password_wrong_infoid: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val Please_Enter_Valid_Email_or_Mobile_Number: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val Email_not_exiting_login: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val Password_wrong2_infoid: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val email_or_password_not_exiting: MutableLiveData<Event<Map<String, String>>> =
            MutableLiveData()
    val user_wrong: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val underVerification: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val credential_verification: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val fbSingOut: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()
    val fbLoginError: MutableLiveData<Event<Map<String, String>>> = MutableLiveData()


    val emailValidation: LiveData<Resource<Int>> = filterValidation(Validation.Field.EMAIL)
    val passwordValidation: LiveData<Resource<Int>> = filterValidation(Validation.Field.PASSWORD)
    var email1:String = ""
    var phoneNumber1:String = ""


    private fun filterValidation(field: Validation.Field) =
            Transformations.map(validationsList) {
                it.find { validation -> validation.field == field }
                        ?.run { return@run this.resource }
                        ?: Resource.unknown()
            }

    override fun onCreate() {}

    fun onEmailChange(email: String) = emailField.postValue(email)

    fun onPhoneNumberChange(email: String) = phonenumberField.postValue(email)

    fun onPasswordChange(email: String) = passwordField.postValue(email)

    fun onLogin(token: String) {
        val email = emailField.value
        val password = passwordField.value



        email1= ""
        phoneNumber1=""

        findCheck(email.toString())
        val validations = Validator.validateLoginFields(email, password)
        validationsList.postValue(validations)
        val db = FirebaseFirestore.getInstance()

        if (validations.isNotEmpty() && email != null  && password != null) {

            val successValidation = validations.filter { it.resource.status == Status.SUCCESS }

            if (successValidation.size == validations.size || checkInternetConnectionWithMessage()) {
                loggingIn.postValue(true)
                if (email1.isNotEmpty() || phoneNumber1.isNotEmpty()) {
                    compositeDisposable.addAll(
                            userRepository.doUserLogin(email1, password, phoneNumber1)
                                    .subscribeOn(schedulerProvider.io())
                                    .subscribe(
                                            {

                                                userRepository.saveCurrentUserID(it.data.id)

                                                if (it.infoId == STATUS_SUCCESS_INFOID) {
                                                    userRepository.saveName(it.data.name)
                                                    userRepository.saveEmail(it.data.email)
                                                    userRepository.saveMobileNumber(it.data.phoneNumber)
                                                    userRepository.setAccessToken("Bearer " + it.jwt)
                                                    update_FCM_TOCKEN()
                                                    onUserDetail();
                                                    loggingIn.postValue(false)
                                                    userRepository.saveLoginStatus("LOGIN")
                                                    userRepository.saveCurrentStatus("LOGIN_IN")
                                                    userRepository.saveLocalStatus("LOGIN")
                                                } else if (it.infoId == PASSWORD_WRONG_INFOID) {
                                                    Password_wrong_infoid.postValue(Event(emptyMap()))
                                                    loggingIn.postValue(false)
                                                } else if (it.infoId == EMAIL_NOT_EXISTING_LOGIN) {
                                                    Email_not_exiting_login.postValue(Event(emptyMap()))
                                                    loggingIn.postValue(false)
                                                } else if (it.infoId == PASSWORD_WRONG2_INFOID) {
                                                    Password_wrong2_infoid.postValue(Event(emptyMap()))
                                                    loggingIn.postValue(false)
                                                } else if (it.infoId == EMAIL_OR_PASSWORD_NOT_EXISTING) {
                                                    email_or_password_not_exiting.postValue(Event(emptyMap()))
                                                    loggingIn.postValue(false)
                                                } else if (it.infoId == USER_WRONG) {
                                                    user_wrong.postValue(Event(emptyMap()))
                                                    loggingIn.postValue(false)
                                                }


                                            },
                                            {
                                                handleNetworkError(it)
                                                loggingIn.postValue(false)
                                            }
                                    )
                    )
                }else{
                    credential_verification.postValue(Event(emptyMap()))
                }
            }
        }
    }



    private fun findCheck(email:String ): List<Validation> =
        ArrayList<Validation>().apply  {
        when {
            email.isBlank() ->
                add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))
            !Validator.EMAIL_ADDRESS.matcher(email).matches() ->
                add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))
            else ->
            {
                email1=email
                add(Validation(Validation.Field.EMAIL, Resource.success()))
            }
        }

        when {
            email.isBlank() ->
                add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))
            !Validator.MOBILENUMBER.matcher(email).matches() ->
                add(Validation(Validation.Field.EMAIL, Resource.error(R.string.Email_id_hint)))

            else ->
            {
                phoneNumber1=email
                add(Validation(Validation.Field.EMAIL, Resource.success()))
            }
        }
    }



    private fun onUserDetail() {
        try {
            compositeDisposable.addAll(
                    userRepository.doUserdetailRequest(userRepository.getCurrentUserID()!!)
                            .subscribeOn(schedulerProvider.io())
                            .subscribe(
                                    {
                                        userRepository.saveMobileNumber(it.data.phoneNumber)

                                        userRepository.saveDomain(it.data.domain)
                                        userRepository.saveEmail(it.data.email)
                                        userRepository.saveName(it.data.firstName)
                                        launchMain.postValue(Event(emptyMap()))
                                    },
                                    {
                                        handleNetworkError(it)
                                    }
                            )
            )
        } catch (e: Exception) {

        }
    }

    fun update_FCM_TOCKEN() {

        FirebaseMessaging.getInstance().token.addOnSuccessListener{
            val deviceToken = it.toString()
            Timber.d("update_FCM_TOCKEN: user fcm token :  $deviceToken")
            userRepository.setUser_FCM_Token(deviceToken)
            val fcmTOKEN_HashMap: MutableMap<String, String> = HashMap()
            fcmTOKEN_HashMap[userId] = java.lang.String.valueOf(userRepository.getCurrentUserID())
            fcmTOKEN_HashMap[Keys.oneSignalValue] = deviceToken
        }
    }

    fun faceBookLoginApi(firstName: String, lastName: String, email: String, id: String, currentAccessToken: AccessToken?) {
        if ((userRepository.getFbLoginstatus().toString() == "LOGOUT")){
            fbSingOut.postValue(Event(emptyMap()))
        }else{
            compositeDisposable.addAll(
                    userRepository.dofaceBookLoginApiRequest(firstName, lastName, email, id)
                            .subscribeOn(schedulerProvider.io())
                            .subscribe({
                                userRepository.saveCurrentUserID(it.data.id)
                                if (it.status == true) {
                                    userRepository.saveName(it.data.name)
                                    userRepository.saveEmail(it.data.email)
                                    userRepository.saveMobileNumber(it.data.phoneNumber)
                                    userRepository.setAccessToken("Bearer " + it.jwt)
                                    update_FCM_TOCKEN()
                                    onUserDetail()
                                    loggingIn.postValue(false)
                                    userRepository.saveLoginStatus("LOGIN")
                                    userRepository.saveFbLoginStatus("LOGIN")
                                    userRepository.saveFbImageUrl("https://graph.facebook.com/$id/picture?type=normal")
                                    userRepository.saveCurrentStatus("LOGIN_IN")
                                    userRepository.saveLocalStatus("LOGIN")
                                }

                            }, {
                                fbLoginError.postValue(Event(emptyMap()))
                                handleNetworkError(it)
                            }))
        }

    }


    fun gMailLoginApi(email: String,firstName:String,lastName: String,phoneNumber:String,socialId:String){
        compositeDisposable.addAll(
            userRepository.doSocialGmailLoginrequest(email,firstName, lastName,phoneNumber , socialId)
                .subscribeOn(schedulerProvider.io())
                .subscribe({
                    userRepository.saveCurrentUserID(it.data.id)
                    if (it.status) {
                        userRepository.saveName(it.data.name)
                        if (it.data.email.isNullOrEmpty()){
                            userRepository.saveEmail("")
                        }else{
                            userRepository.saveEmail(it.data.email)
                        }
                        if (it.data.phoneNumber.isNullOrEmpty()){
                            userRepository.saveMobileNumber("")
                        }else{
                            userRepository.saveMobileNumber(it.data.phoneNumber)
                        }


                        userRepository.setAccessToken("Bearer " + it.jwt)
                        update_FCM_TOCKEN()
                        onUserDetail()
                        loggingIn.postValue(false)
                        userRepository.savegmailLoginStatus("LOGIN")
                        userRepository.saveLoginStatus("LOGIN")
                        userRepository.saveCurrentStatus("LOGIN_IN")
                        userRepository.saveLocalStatus("LOGIN")
                    }

                }, {
                    handleNetworkError(it)
                }))
    }

    fun getGmailLoginStatus():String{
        return userRepository.getGmailLoginStatus().toString()
    }
    fun savegMailLoginImageUrl(imageurl:String){
        userRepository.savegMailLoginImageUrl(imageurl)
    }

    fun saveFbLoginStatus() {
        userRepository.saveFbLoginStatus("LOGIN")
    }
    fun getFbLoginstatus():String{
        return  userRepository.getFbLoginstatus().toString()
    }

    fun user_status_update(context: Context, device_id: String) {
        AppUtils.storeOnlineOffline(
                context,
                java.lang.String.valueOf(userRepository.getCurrentUserID()),
                false,
                java.lang.String.valueOf(userRepository.getSupplierId()),
                userRepository.getName().toString(),
                userRepository.getMobileNUmber().toString(),
                userRepository.getUserCurrentlatitude().toString(),
                userRepository.getUserCurrentLongitude().toString(),
                device_id
        )


    }

}

