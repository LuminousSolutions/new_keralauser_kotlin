package com.mongokerala.taxi.newuser.paymentgateway;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.appcompat.app.AppCompatActivity;

import com.mongokerala.taxi.newuser.R;
import com.mongokerala.taxi.newuser.ui.main.MainActivity;

public class OrderStatusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);


        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //Toast.makeText(ChatRequestActivity.this, "Time Remaining: " + millisUntilFinished / 1000, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                //Toast.makeText(ChatRequestActivity.this, "time is finished", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                try {

                } catch (NullPointerException exce) {
                    exce.printStackTrace();
                } catch (Exception exce) {
                    exce.printStackTrace();
                }

            }

        }.start();
    }

}
