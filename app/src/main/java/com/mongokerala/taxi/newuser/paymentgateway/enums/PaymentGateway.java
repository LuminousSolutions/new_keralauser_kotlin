package com.mongokerala.taxi.newuser.paymentgateway.enums;

/**
 * Created by Akhi007 on 15-12-2018.
 */

public enum PaymentGateway {

    STRIPE, PAYU, PAYTM, CCAVENUE, GOOGLE_PAY;

}
