package com.mongokerala.taxi.newuser.utils.common

import android.content.Context
import android.graphics.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Polyline
import com.google.android.gms.maps.model.PolylineOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.mongokerala.taxi.newuser.R
import kotlin.math.abs
import kotlin.math.atan


object MapUtils {

    private const val TAG = "MapUtils"

    private var grayPolyline: Polyline? = null
    private var blackPolyline: Polyline? = null
    var mMap: GoogleMap? = null
    val db = FirebaseFirestore.getInstance()
    var locationList = ArrayList<LatLng>()
    var number = 0
    var driverid = ""



    fun getCarBitmap(context: Context): Bitmap {
    val bitmap = BitmapFactory.decodeResource(context.resources, R.mipmap.cab_image)
    return Bitmap.createScaledBitmap(bitmap, 100, 100, false)
    }

    fun getDestinationBitmap(): Bitmap {
        val height = 20
        val width = 20
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        val canvas = Canvas(bitmap)
        val paint = Paint()
        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        canvas.drawRect(0F, 0F, width.toFloat(), height.toFloat(), paint)
        return bitmap
    }

    fun getDriverId(driverId:String){
        driverid = driverId
    }

   fun getListOfLocationsTracking(): ArrayList<LatLng> {

       if(driverid.isNotEmpty()){
           var docRef = db.collection("KERALACABS_DRIVER_STATUS").document(driverid)
           docRef.get()
                   .addOnSuccessListener { document ->
                       if (document != null) {

                           if (document.data != null) {
                               var latitude = document.data!!["LATITUDE"].toString().toDouble()
                               var longitude = document.data!!["LONGITUDE"].toString().toDouble()
                               locationList.add(LatLng(latitude, longitude))
                           }
                       }
                   }
                   .addOnFailureListener {
                   }

       }else{
           locationList.clear()
       }


       /* if (number == 0){
            locationList.add(LatLng(13.049571674007842, 80.20974652551297))
            number++
        }else if (number == 1){
            locationList.add(LatLng(13.050988929298338, 80.20992462392962))
            number++
        }else if (number == 2){
            locationList.add(LatLng(13.050792437298577, 80.2118365026464))
            number++
        }else if (number == 3){
            locationList.add(LatLng(13.052009013640413, 80.21195451984306))
            number++
        }else if (number == 4){
            locationList.add(LatLng(13.053250667793131, 80.21167771534886))
            number++
        }else if (number == 5){
            locationList.add(LatLng(13.054542483365267, 80.21147815860525))
            number++
        }else if (number == 6){
            locationList.add(LatLng(13.054429606359896, 80.21366469526322))
            number++
        }else if (number == 7){
            locationList.add(LatLng(13.054550844481113, 80.21488992847219))
            number++
        }else if (number == 8){
            locationList.add(LatLng(13.054513218819665, 80.21626107358964))
            number++
        }else if (number == 9){
            locationList.add(LatLng(13.056866898080989, 80.21603576803238))
            number++
        }
        else if (number == 10){
            locationList.add(LatLng(13.05874815286387, 80.21800558141615))
            number++
        }

        else if (number == 11){
            locationList.add(LatLng(13.058986443926976, 80.21932522759664))
            number++
        }
        else if (number == 12){
            locationList.add(LatLng(13.059429581483684, 80.21970502734541))
            number++
        }
        else if (number == 13){
            locationList.add(LatLng(13.056866898080989, 80.21603576803238))
            number++
        }else if (number == 14){
            locationList.add(LatLng(13.059542455805937, 80.22009770248326))
            number++
        }
        else if (number == 15){
            locationList.add(LatLng(13.060106827600327, 80.22023288555567))
            number++
        }else{
            locationList.add(LatLng(13.060094285809814, 80.22090021863471))
        }*/

       return locationList
   }




  /* fun getListOfLocations(): ArrayList<LatLng> {
       val locationList = ArrayList<LatLng>()
       locationList.add(LatLng(28.436970000000002, 77.11272000000001))
       locationList.add(LatLng(28.43635, 77.11289000000001))
       locationList.add(LatLng(28.4353, 77.11317000000001))
       locationList.add(LatLng(28.435280000000002, 77.11332))
       locationList.add(LatLng(28.435350000000003, 77.11368))
       locationList.add(LatLng(28.4356, 77.11498))
       locationList.add(LatLng(28.435660000000002, 77.11519000000001))
       locationList.add(LatLng(28.43568, 77.11521))
       locationList.add(LatLng(28.436580000000003, 77.11499))
       locationList.add(LatLng(28.436590000000002, 77.11507))
       return locationList
   }*/



    private fun showPath(latLngList: ArrayList<LatLng>) {
        val builder = LatLngBounds.Builder()
        for (latLng in latLngList) {
            builder.include(latLng)
        }
        // this is used to set the bound of the Map
        val bounds = builder.build()
        mMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 2))

        val polylineOptions = PolylineOptions()
        polylineOptions.color(Color.GRAY)
        polylineOptions.width(5f)
        polylineOptions.addAll(latLngList)
//        grayPolyline = mMap?.addPolyline(polylineOptions)

        val blackPolylineOptions = PolylineOptions()
        blackPolylineOptions.color(Color.BLACK)
        blackPolylineOptions.width(5f)
//        blackPolyline = mMap?.addPolyline(blackPolylineOptions)

    }

   fun getOriginDestinationMarkerBitmap(): Bitmap {
        val height = 20
        val width = 20
        val bitmap = Bitmap.createBitmap(height, width, Bitmap.Config.RGB_565)
        val canvas = Canvas(bitmap)
        val paint = Paint()
        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        canvas.drawRect(0F, 0F, width.toFloat(), height.toFloat(), paint)
        return bitmap
    }

    fun getRotation(start: LatLng, end: LatLng): Float {
        val latDifference: Double = abs(start.latitude - end.latitude)
        val lngDifference: Double = abs(start.longitude - end.longitude)
        var rotation = -1F
        when {
            start.latitude < end.latitude && start.longitude < end.longitude -> {
                rotation = Math.toDegrees(atan(lngDifference / latDifference)).toFloat()
            }
            start.latitude >= end.latitude && start.longitude < end.longitude -> {
                rotation = (90 - Math.toDegrees(atan(lngDifference / latDifference)) + 90).toFloat()
            }
            start.latitude >= end.latitude && start.longitude >= end.longitude -> {
                rotation = (Math.toDegrees(atan(lngDifference / latDifference)) + 180).toFloat()
            }
            start.latitude < end.latitude && start.longitude >= end.longitude -> {
                rotation =
                    (90 - Math.toDegrees(atan(lngDifference / latDifference)) + 270).toFloat()
            }
        }
        return rotation
    }
}