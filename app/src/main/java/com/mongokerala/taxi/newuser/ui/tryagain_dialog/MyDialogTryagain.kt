package com.mongokerala.taxi.newuser.ui.tryagain_dialog

interface MyDialogTryagain {

    fun noTaxiavilable()
    fun tryAgain_button()
    fun offlineButton()
    fun closeIconBtn()
    fun closeIconTryAgain()
    fun driverCanceledRide(title:String,reasonMsg:String)
    fun changeDestination(title:String,reasonMsg:String)
    fun callCustomerCare()

}