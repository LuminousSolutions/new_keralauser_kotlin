package com.mongokerala.taxi.newuser.paymentgateway.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StripePaymentRequest {

    @SerializedName("amount")
    @Expose
    public Integer amount;
    @SerializedName("currency")
    @Expose
    public String currency;
    @SerializedName("payStatus")
    @Expose
    public String payStatus;
    @SerializedName("paymentGateway")
    @Expose
    public String paymentGateway;
    @SerializedName("stripeToken")
    @Expose
    public String stripeToken;
    @SerializedName("txnId")
    @Expose
    public String txnId;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getStripeToken() {
        return stripeToken;
    }

    public void setStripeToken(String stripeToken) {
        this.stripeToken = stripeToken;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }


}
