package com.mongokerala.taxi.newuser.ui.reqotp

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.mongokerala.taxi.newuser.R
import com.mongokerala.taxi.newuser.di.component.ActivityComponent
import com.mongokerala.taxi.newuser.ui.base.BaseActivity
import com.mongokerala.taxi.newuser.ui.login.LoginActivity
import com.mongokerala.taxi.newuser.ui.profile_edit.ProfileEditActivity
import com.mongokerala.taxi.newuser.ui.validatemobile.ValidateMobileActivity
import com.mongokerala.taxi.newuser.utils.common.AppUtils
import com.mongokerala.taxi.newuser.utils.common.Constants
import com.mongokerala.taxi.newuser.utils.display.Toaster
import kotlinx.android.synthetic.main.activity_reqotp.*
import kotlinx.android.synthetic.main.activity_reqotp.login
import kotlinx.android.synthetic.main.activity_reqotp.pb_loading_main


class ReqotpActivity : BaseActivity<ReqotpViewModel>() {

    companion object {
        const val TAG = "ReqotpActivity"
        var mobileNumber=""
        var intentKey= ""
        var network_enabled=false
    }

    override fun setupView(savedInstanceState: Bundle?) {
//        turnGPSOn()
        num_editText.addTextChangedListener(mTextWatcher)
        btn_reqotp.background = ResourcesCompat.getDrawable(resources, R.drawable.button_curved_shadow, null);
        registerCarrierEditText()

        viewModel.mainIn.observe(this) {
            if (it == true) {
                pb_loading_main.visibility = View.VISIBLE
            } else {
                pb_loading_main.visibility = View.GONE
            }
        }
    }

    override fun injectDependencies(activityComponent: ActivityComponent) {
        try {
            activityComponent.inject(this)
        }catch (e: Exception){

        }
    }

    override fun provideLayoutId(): Int = R.layout.activity_reqotp

    private fun turnGPSOn() {

        val googleApiClient = GoogleApiClient.Builder(this)
            .addApi(LocationServices.API).build()
        googleApiClient.connect()

        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10000
        locationRequest.fastestInterval = (10000 / 2).toLong()

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)

        val result =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> Log.i(TAG, "All location settings are satisfied.")
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ")

                    try {
                        status.startResolutionForResult(this@ReqotpActivity, 1)
                    } catch (e: IntentSender.SendIntentException) {
                        Log.i(TAG, "PendingIntent unable to execute request.")
                    }

                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    Log.i(
                        TAG,
                        "Location settings are inadequate, and cannot be fixed here. Dialog not created."
                    )
                }
            }
        }
    }

    override fun setupObservers() {
        super.setupObservers()

        if (intent != null && intent.getBooleanExtra(Constants.is_Settings, false)) {
            login.visibility = View.GONE
            intent.removeExtra(Constants.is_Settings)
        } else {
            login.visibility = View.VISIBLE
        }

        if (intent != null && !intent.getStringExtra(Constants.mobileNumberValidation).isNullOrEmpty()) {
            if (intent.getStringExtra(Constants.mobileNumberValidation) == Constants.mobileNumberValidation){
                intentKey=Constants.mobileNumberValidation
                intent.removeExtra(Constants.mobileNumberValidation)
            }else{
                intentKey=""
            }
        } else {
            intentKey=""
        }

        viewModel.launchValidate.observe(this) {
            it.getIfNotHandled()?.run {
                startActivity(Intent(applicationContext, ValidateMobileActivity::class.java))
                finish()
                Toaster.show(applicationContext, "Otp send successfully")
            }
        }
        viewModel.launchValidateUsedNumber.observe(this) {
            it.getIfNotHandled()?.run {
                Toaster.show(applicationContext, "This number is already in use")
            }
        }

        viewModel.mobileNumber.observe(this) {
            mobileNumber = it
        }

        viewModel.numberExisting.observe(this) {
            pb_loading_main.visibility = View.GONE
            Toaster.show(this, "This Number is already existing")
        }


        viewModel.launchProfile.observe(this) {
            val intent = Intent(this, ProfileEditActivity::class.java)
            intent.putExtra("UpdateMobileNumber", mobileNumber)
            startActivity(intent)
            finish()
        }
    }

    @NonNull
    private val mTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
        }

        override fun afterTextChanged(editable: Editable) {
        }
    }

    private fun registerCarrierEditText() {
        cpp_txt.registerCarrierNumberEditText(num_editText)
        num_editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (num_editText.text.isNotEmpty()) {
                    error_txt.visibility = View.VISIBLE
                    error_txt_icon.visibility = View.VISIBLE
                } else {
                    error_txt.visibility = View.GONE
                    error_txt_icon.visibility = View.GONE
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
                assert(num_editText != null)
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {

            }
        })
        cpp_txt.setPhoneNumberValidityChangeListener { isValidNumber ->
            if (isValidNumber && num_editText.text.length > 4) {
                error_txt_icon.text = "\ue809"
                error_txt_icon.setTextColor(ContextCompat.getColor(applicationContext, R.color.green_500))
                error_txt.setText(R.string.Yes_This_is_an_valid_number)
                btn_reqotp.isEnabled = true
                btn_reqotp.background = ResourcesCompat.getDrawable(resources, R.drawable.button_curved_shape2, null);
            } else {
                error_txt.visibility = View.VISIBLE
                error_txt_icon.visibility = View.VISIBLE
                error_txt_icon.text = "\uf12a"
                error_txt_icon.setTextColor(ContextCompat.getColor(applicationContext, R.color.red_900))
                error_txt.setText(R.string.This_is_not_an_valid_number)
                btn_reqotp.isEnabled = false
                btn_reqotp.background = ResourcesCompat.getDrawable(resources, R.drawable.button_curved_shadow, null);
            }
        }

        cpp_txt.registerCarrierNumberEditText(num_editText)
    }

    fun submitOnclick(view: View) {

        hideKeyboard()
        val number = num_editText.text.toString().trim { it <= ' ' }
        val spNumber = number.replace(" ", "")
        if (AppUtils.isNetworkConnected(this)){
            viewModel.onSubmitClick("+91",spNumber, intentKey)
        }

    }

    private fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun reqOtpToLoginClick(view: View) {
        startActivity(Intent(applicationContext, LoginActivity::class.java))
    }
}
