package com.mongokerala.taxi.newuser.ui.googlepay;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;
import com.mongokerala.taxi.newuser.R;
import com.mongokerala.taxi.newuser.paymentgateway.OrderStatusActivity;
import com.mongokerala.taxi.newuser.paymentgateway.enums.PaymentGateway;
import com.mongokerala.taxi.newuser.paymentgateway.model.ApiResponse;
import com.mongokerala.taxi.newuser.ui.payment.CustomerDTO;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.Optional;

public class GooglePayPaymentWithPGActivity extends AppCompatActivity implements CreateGoogleOrderPaymentListener {
    private static final String TAG = GooglePayPaymentWithPGActivity.class.getSimpleName();

    //A client for interacting with the Google Pay API.
    private PaymentsClient mPaymentsClient;
    //A Google Pay payment button presented to the viewer for interaction.
    private View mGooglePayButton;
    private TextView tvTotalAmountPayable;
    private TextView mGooglePayStatusText;
    private ImageView img_back_g_pay;
    private int totalAmountPayableSample = 10; //here $10

    //Arbitrarily-picked constant integer you define to track a request for payment data activity.
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 991;

    private GooglePayPaymentService googlePayPaymentService;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_pay_payment_with_pg);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initInitializeUI();
        initComponent();
    }

    private void initInitializeUI() {
        progressDialog = new ProgressDialog(this);
        tvTotalAmountPayable = findViewById(R.id.tvTotalAmountPayable);
        mGooglePayButton = findViewById(R.id.googlepay_button);
        mGooglePayStatusText = findViewById(R.id.googlepay_status);
        img_back_g_pay = findViewById(R.id.img_back_g_pay);

        img_back_g_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //itemPrice.setText(GooglePayPaymentWithAdyenUtil.microsToString(mBikeItem.getPriceMicros()));
    }

    private void initComponent(){
        // Initialize a Google Pay API client for an environment suitable for testing.
        // It's recommended to create the PaymentsClient object inside of the onCreate method.
        mPaymentsClient = GooglePayPaymentWithAdyenUtil.createPaymentsClient(this);
        possiblyShowGooglePayButton();

        mGooglePayButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        googlePayRequestPayment(view);
                    }
                });
    }

    /**
     * Determine the viewer's ability to pay with a payment method supported by your app and display a
     * Google Pay payment button.
     */
    private void possiblyShowGooglePayButton() {
        final Optional<JSONObject> isReadyToPayJson = GooglePayPaymentWithAdyenUtil.getIsReadyToPayRequest();
        if (!isReadyToPayJson.isPresent()) {
            return;
        }
        IsReadyToPayRequest request = IsReadyToPayRequest.fromJson(isReadyToPayJson.get().toString());
        if (request == null) {
            return;
        }

        // The call to isReadyToPay is asynchronous and returns a Task. We need to provide an
        // OnCompleteListener to be triggered when the result of the call is known.
        Task<Boolean> task = mPaymentsClient.isReadyToPay(request);
        task.addOnCompleteListener(this,
                new OnCompleteListener<Boolean>() {
                    @Override
                    public void onComplete(@NonNull Task<Boolean> task) {
                        if (task.isSuccessful()) {
                            setGooglePayAvailable(task.getResult());
                        } else {
                            Log.w("isReadyToPay failed", task.getException());
                        }
                    }
                });
    }

    /**
     * If isReadyToPay returned {@code true}, show the button and hide the "checking" text. Otherwise,
     * notify the user that Google Pay is not available. Please adjust to fit in with your current
     * user flow. You are not required to explicitly let the user know if isReadyToPay returns {@code
     * false}.
     *
     * @param available isReadyToPay API response.
     */
    private void setGooglePayAvailable(boolean available) {
        if (available) {
            mGooglePayStatusText.setVisibility(View.GONE);
            mGooglePayButton.setVisibility(View.VISIBLE);
        } else {
            mGooglePayStatusText.setText(R.string.googlepay_status_unavailable);
        }
    }

    /**
     * Handle a resolved activity from the Google Pay payment sheet.
     *
     * @param requestCode Request code originally supplied to AutoResolveHelper in requestPayment().
     * @param resultCode Result code returned by the Google Pay API.
     * @param data Intent from the Google Pay API containing payment or error data.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // value passed in AutoResolveHelper
            case LOAD_PAYMENT_DATA_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        PaymentData paymentData = PaymentData.getFromIntent(data);
                        handlePaymentSuccess(paymentData);
                        break;
                    case Activity.RESULT_CANCELED:
                        // Nothing to here normally - the user simply cancelled without selecting a
                        // payment method.
                        break;
                    case AutoResolveHelper.RESULT_ERROR:
                        Status status = AutoResolveHelper.getStatusFromIntent(data);
                        handleError(status.getStatusCode());
                        break;
                    default:
                        // Do nothing.
                }

                // Re-enables the Google Pay payment button.
                mGooglePayButton.setClickable(true);
                break;
        }
    }

    /**
     * PaymentData response object contains the payment information, as well as any additional
     * requested information, such as billing and shipping address.
     *
     * @param paymentData A response object returned by Google after a payer approves payment.
     */
    private void handlePaymentSuccess(PaymentData paymentData) {
        String paymentInformation = paymentData.toJson();
        // Token will be null if PaymentDataRequest was not constructed using fromJson(String).
        //Here Payment not captured if null comes
        if (paymentInformation == null) {
            return;
        }
        JSONObject paymentMethodData;

        try {
            paymentMethodData = new JSONObject(paymentInformation).getJSONObject("paymentMethodData");
            Log.i(TAG,"GooglePay Success Response Payload: "+paymentMethodData);
            //validate google pay tokenizationData setting
            if (paymentMethodData.getJSONObject("tokenizationData").getString("type").equals("PAYMENT_GATEWAY")
                    && paymentMethodData.getJSONObject("tokenizationData").getString("token").equals("examplePaymentMethodToken")) {
                AlertDialog alertDialog =
                        new AlertDialog.Builder(this)
                                .setTitle("Warning")
                                .setMessage(
                                        "Problem with Google Pay Payment Gateway! Please try with other Payment option!")
                                .setPositiveButton("OK", null)
                                .create();
                alertDialog.show();
            }

            String billingName =  paymentMethodData.getJSONObject("info").getJSONObject("billingAddress").getString("name");
            String googlePayPaymentToken = paymentMethodData.getJSONObject("tokenizationData").getString("token");

            mGooglePayButton.setVisibility(View.GONE); //disable google pay button first before api call
            //Here Payment has been captured successfully you can show message
            //At the same time updated order in server
            //Note* You should created order first in server then should shoe sucess message for payment transaction
            createOrderWithGooglePaySuccessTrxApiCall(paymentInformation, googlePayPaymentToken);
            //Payment captured successfully show confirmation status
            Toast.makeText(this, getString(R.string.payments_show_name, billingName), Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
            Log.e("handlePaymentSuccess", "Error: " + e.toString());
            return;
        }
    }

    /**
     * At this stage, the user has already seen a popup informing them an error occurred. Normally,
     * only logging is required.
     *
     * @param statusCode will hold the value of any constant from CommonStatusCode or one of the
     *     WalletConstants.ERROR_CODE_* constants.
     */
    private void handleError(int statusCode) {
        Log.w("loadPaymentData failed", String.format("Error code: %d", statusCode));
    }

    // This method is called when the Pay with Google button is clicked.
    public void googlePayRequestPayment(View view) {

        mGooglePayButton.setClickable(false); // Disables the button to prevent multiple clicks.

        // The price provided to the API should include taxes and shipping.
        // This price is not displayed to the user.

        //validate Total AMount Payable

        //String price = GooglePayPaymentWithAdyenUtil.microsToString(mBikeItem.getPriceMicros() + mShippingCost);
        String price = GooglePayPaymentWithAdyenUtil.microsToString(totalAmountPayableSample);

        // TransactionInfo transaction = GooglePayPaymentWithAdyenUtil.createTransaction(price);
        Optional<JSONObject> paymentDataRequestJson = GooglePayPaymentWithAdyenUtil.getPaymentDataRequestWithPG(price);
        if (!paymentDataRequestJson.isPresent()) { //!paymentDataRequestJson.isPresent()
            return;
        }
        PaymentDataRequest request = PaymentDataRequest.fromJson(paymentDataRequestJson.get().toString());

        // Since loadPaymentData may show the UI asking the user to select a payment method, we use
        // AutoResolveHelper to wait for the user interacting with it. Once completed,
        // onActivityResult will be called with the result.
        if (request != null) {
            AutoResolveHelper.resolveTask(
                    mPaymentsClient.loadPaymentData(request), this, LOAD_PAYMENT_DATA_REQUEST_CODE);
        }
    }


    //API Call Section on Ower Server
    //create order details for google pay success response in DB via API Call
    private void createOrderWithGooglePaySuccessTrxApiCall(String googlePayPaymentResponsePayload, String googlePayResponsePaymentToken){
        googlePayPaymentService = new GooglePayPaymentServiceImpl();
        GoogleOrderDTO orderDTO = new GoogleOrderDTO();
        CustomerDTO customerDTO = new CustomerDTO();
        PaymentInfoDTO paymentInfoDTO = new PaymentInfoDTO();
        //TODO : your customer info
        customerDTO.setMobileNumber("8888888888");
        customerDTO.setEmail("testing001@gmail.com");
        customerDTO.setName("Akhilesh Tripathi");
        //TODO : payment details
        paymentInfoDTO.setPaymentGateway(PaymentGateway.GOOGLE_PAY);
        paymentInfoDTO.setPaymentAmount(new BigDecimal(totalAmountPayableSample));
        paymentInfoDTO.setPaymentCurrency(GooglePayConstants.CURRENCY_CODE); //USD
        //orderId : created in server side only
        orderDTO.setOrderReqType(GoogleOrderRequestType.ORDER_CREATE_AND_PAYMENT_UPDATE); // as per you
        orderDTO.setCustomer(customerDTO);
        orderDTO.setPaymentInfo(paymentInfoDTO);
        orderDTO.setGooglePayResponsePayload(googlePayPaymentResponsePayload);
        orderDTO.setGooglePayPaymentToken(googlePayResponsePaymentToken);
        showProgressDialog(progressDialog, "Please wait", "We are updating your order..");//start progress dialog
        googlePayPaymentService.createGooglePayOrderPayment(this, orderDTO);
    }

    @Override
    public void onRequestSuccess(GoogleOrderDTO orderRequestDTO, GoogleOrderDTO orderResponseDTO) {
        progressDialog.dismiss();
        Toast.makeText(this, "Order Created successfully!!", Toast.LENGTH_LONG).show();
        startActivity(new Intent(GooglePayPaymentWithPGActivity.this, OrderStatusActivity.class));
    }

    @Override
    public void onRequestFailed(GoogleOrderDTO orderDTO, ApiResponse apiResponse) {
        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(), "Facing problem while updating order in our server", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestTimeOut(GoogleOrderDTO orderDTO) {
        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(), "Network Timeout", Toast.LENGTH_LONG).show();
    }

    public void showProgressDialog(ProgressDialog progressDialog, String title, String message){
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
}
