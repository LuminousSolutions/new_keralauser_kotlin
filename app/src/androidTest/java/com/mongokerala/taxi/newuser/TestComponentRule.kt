package com.mongokerala.taxi.newuser

import android.content.Context
import com.mongokerala.taxi.newuser.di.component.DaggerTestComponent
import com.mongokerala.taxi.newuser.di.component.TestComponent
import com.mongokerala.taxi.newuser.di.module.ApplicationTestModule
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class TestComponentRule(private val context: Context) : TestRule {

    private var testComponent: TestComponent? = null

    fun getContext() = context

    fun getDatabaseService() = testComponent?.getDatabaseService()

    private fun setupDaggerTestComponentInApplication() {
        val application = context.applicationContext as TaxiDealsFrameWorkApplication
        testComponent = DaggerTestComponent.builder()
            .applicationTestModule(ApplicationTestModule(application))
            .build()
        application.setComponent(testComponent!!)
    }

    override fun apply(base: Statement, description: Description?): Statement {
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                try {
                    setupDaggerTestComponentInApplication()
                    base.evaluate()
                } finally {
                    testComponent = null
                }
            }
        }
    }

}