package com.mongokerala.taxi.newuser.di.component

import com.mongokerala.taxi.newuser.di.module.ApplicationTestModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationTestModule::class])
interface TestComponent : ApplicationComponent {
}